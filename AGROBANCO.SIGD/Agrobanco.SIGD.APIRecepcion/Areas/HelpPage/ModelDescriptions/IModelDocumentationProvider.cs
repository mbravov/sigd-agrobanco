using System;
using System.Reflection;

namespace Agrobanco.SIGD.APIRecepcion.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}