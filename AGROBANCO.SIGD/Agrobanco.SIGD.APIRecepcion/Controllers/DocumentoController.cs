﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;

namespace Agrobanco.SIGD.APIRecepcion.Controllers
{
    public class DocumentoController : ApiController
    {
        /// <summary>
        /// Servicio API que realiza el Registro del Documento
        /// </summary>
        /// <returns>Retorna el correlativo autogenerado</returns>   
        [HttpPost]
        [Route("api/registrar")]
        public IHttpActionResult RegistrarDocumento([FromBody] DocumentoRequest documento)
        {
            try
            {
                if (documento == null) return BadRequest();
                DocumentoBL objDocBL = new DocumentoBL();
                Documento objDoc = mapearDocumento(documento);

                int rpta = objDocBL.RegistrarDocumentoRecepcionSIED(objDoc, out string correlativo, out int RespuestaCorreo);
                if (rpta > 0)
                {

                    return Ok(rpta);
                }

                return BadRequest();
            }
            catch(Exception e)
            {
                throw e;
                return BadRequest();
            }
            
        }

        private Documento mapearDocumento(DocumentoRequest documento)
        {
            DateTime fechaActual = DateTime.Today;
            Documento documentoNegocio = new Documento
            {
                Anio = documento.Anio,
                Origen = documento.Origen,
                NumDocumento = documento.NumDocumento,
                EstadoDocumento = documento.EstadoDocumento,
                FechaDocumento = documento.FechaDocumento,
                Asunto = documento.Asunto,
                Prioridad = Convert.ToInt16(documento.Prioridad),
                PrioridadSIED = documento.Prioridad,
                Referencia = documento.Referencia,
                CodArea = documento.CodArea,
                CodTrabajador = documento.CodTrabajador,
                FechaRecepcion = documento.FechaRecepcion,
                Plazo = documento.Plazo,
                FechaPlazo = new DocumentoBL().CalcularFechaPlazoDocumento(documento.Plazo, fechaActual),
                Observaciones = documento.Observaciones,
                CodTipoDocumento = documento.CodTipoDocumento,
                CodRepresentante = documento.CodRepresentante,
                UsuarioCreacion = documento.UsuarioCreacion,
                FechaCreacion = documento.FechaCreacion,
                LoginCreacion = documento.LoginCreacion,
                LstDocAdjunto = new List<DocAdjunto>(),
                LstDocAnexo = new List<DocAnexo>(),
                TipoEntidadSIED = documento.TipoEntidad,
                EntidadSIED = documento.Entidad,
                DirigidoASIED = documento.DirigidoA,
                IndicacionSIED = documento.Indicacion
            };

            documento.LstDocAdjunto?.ForEach(x => {
                DocAdjunto docAdjunto = new DocAdjunto{FileArray = x.FileArray};
                documentoNegocio.LstDocAdjunto.Add(docAdjunto);
            });

            documento.LstDocAnexo?.ForEach(x => {
                DocAnexo docAnexo = new DocAnexo 
                { 
                    FileArray = x.FileArray, 
                    NombreDocumento = x.NombreDocumento,
                    AlmacenArchivoSIED = x.AlmacenArchivoSIED,
                    DescripcionSIED = x.DescripcionSIED,
                    NombreFisicoSIED = x.NombreFisicoSIED
                };
                documentoNegocio.LstDocAnexo.Add(docAnexo);
            });

            return documentoNegocio;
        }
    }
}