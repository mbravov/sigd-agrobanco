﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class TrabajadorViewModel
    {
        public ResultadoBusquedaModel<Trabajador> ResultadoBusqueda { get; set; }
        public Trabajador NuevoTipoTrabajador { get; set; }

    }
}