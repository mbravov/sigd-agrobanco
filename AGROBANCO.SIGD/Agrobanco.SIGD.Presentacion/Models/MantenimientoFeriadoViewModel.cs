﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class MantenimientoFeriadoListadoViewModel
    {
        public ResultadoBusquedaModel<Feriado> ResultadoBusqueda { get; set; }
        public string FechaDesde { get; set; }
        public string FechaHasta { get; set; }
    }
    public class MantenimientoFeriadoViewModel : Feriado
    {
    }
    public class MantenimientoFeriadoFiltrosViewModel
    {
        public string TextoBusqueda { get; set; }
    }
}