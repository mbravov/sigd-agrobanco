﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class EnvioSIED
    {
        public int codDocumento { get; set; }
        public string dniFirmante { get; set; }
        public string numeroDocumento { get; set; }
        public int numSecuencial { get; set; }
        public string asunto { get; set; }
        public int categoriaAsunto { get; set; }
        public int codSTD { get; set; }
        public string tipoDocumento { get; set; }
        public int nroFolio { get; set; }
        public int esExterno { get; set; }
        public int codigoDocumentoSIEDRef { get; set; }
        public remitente remitente { get; set; }
        public List<EmpresaDestino> listaEmpresaDestino { get; set; }
        public string urlDocumento { get; set; }
        public string nomDocPrincipal { get; set; }
        public List<documentoSeg> listaDocumentosAnexos { get; set; }
        //extras
        public int origen { get; set; }
    }

    public class remitente
    {
        public int codCargo { get; set; }
        public string dni { get; set; }
    }

    public class EmpresaDestino
    {
        public string nombreDestinatario { get; set; }
        public string nombreCargo { get; set; }
        public string unidadOrganica { get; set; }
        public string nombreEntidad { get; set; }
        public string ruc { get; set; }
        public int original { get; set; }
    }

    public class documentoSeg
    {
        public string nomDocumento { get; set; }
        public string desDocumento { get; set; }
    }
}