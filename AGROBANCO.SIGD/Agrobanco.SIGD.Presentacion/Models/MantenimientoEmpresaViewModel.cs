﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class MantenimientoEmpresaListadoViewModel
    {
        public ResultadoBusquedaModel<Empresa> ResultadoBusqueda { get; set; }
        public string FechaDesde { get; set; }
        public string FechaHasta { get; set; }
    }
    public class MantenimientoEmpresaViewModel : Empresa
    {
    }
    public class MantenimientoEmpresaFiltrosViewModel
    {
        public string TextoBusqueda { get; set; }
    }
}