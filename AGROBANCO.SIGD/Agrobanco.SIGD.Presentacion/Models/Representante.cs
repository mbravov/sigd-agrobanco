﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class Representante
    {
        public int CodRepresentante { get; set; }
        public string Nombre { get; set; }
        public short Estado { get; set; }

        public String NumDocumento { get; set; }

        public String Nombres { get; set; }

        public string EstadoDescripcion { get; set; }

        public short? TipRepresentante { get; set; }
        public int? CodTipDocIdentidad { get; set; }
        public String Direccion { get; set; }

        public String Telefono { get; set; }
        public string Fax { get; set; }
        public String Celular { get; set; }

        public String Email { get; set; }

        public int? CodEmpresa { get; set; }

        public String UsuCreacion { get; set; }

        public DateTime? FecCreacion { get; set; }

        public String UsuActualizacion { get; set; }

        public DateTime? FecActualizacion { get; set; }

    }
}