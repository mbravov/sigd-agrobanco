﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class DocumentoSeguimientoItemViewModel
    {
        public List<DocumentoSeguimientoDTO> Lista { get; set; }
        public int? iCodMovimientoActual { get; set; }
        
    }
}