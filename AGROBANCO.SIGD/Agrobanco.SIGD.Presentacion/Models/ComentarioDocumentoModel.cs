﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class ComentarioDocumentoModel
    {

        public int CodComentario { get; set; }
        public int CodMovimiento { get; set; }
        public int? CodDocumentoDerivacion { get; set; }
        public int CodDocumento { get; set; }
        public int? CodDocDerivacion { get; set; }
        public string Usuario { get; set; }
        public string Comentario { get; set; }
        public string ComentadoPor { get; set; }
        public int EstadoDocumento { get; set; }
        public int? EstadoDerivacion { get; set; }
        public List<DocAnexo> LstDocAnexo { get; set; }
        public int CodResponsable { get; set; }
        public int OrigenFinalizar { get; set; }
        #region"Seguridad"
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }
        #endregion
    }
}