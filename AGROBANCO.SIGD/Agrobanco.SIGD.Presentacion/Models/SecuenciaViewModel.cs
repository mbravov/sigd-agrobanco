﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class SecuenciaViewModel
    {
        public ResultadoBusquedaModel<Secuencia> ResultadoBusqueda { get; set; }
        public TipoDocumento NuevoTIpodocumento { get; set; }
    }
    public class SecuenciaFiltroViewModel
    {
        public string TextoBusqueda { get; set; }
    }
}