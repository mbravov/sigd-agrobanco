﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class ReportesAtencionDocumentos
    {


        #region Reporte de Atencion De Documentos

        public string vCorrelativo { get; set; }
        public DateTime fecInicio { get; set; }
        public DateTime fecFinal { get; set; }
        public string CodAreaResponsable { get; set; }
        public string CodAreaDerivada { get; set; }
        public string CodTipoDocumento { get; set; }
        public string CodEstadoDoc { get; set; }
        public string siFirma { get; set; }
        public string Remitente { get; set; }
        public string Destinatario { get; set; }
        public string Asunto { get; set; }
        public string Comentarios { get; set; }
        public DateTime? fechaFirma { get; set; }
        public string DescripcionEstado { get; set; }
        public int numPagina { get; set; }
        public int tamPagina { get; set; }
        public int Pagina { get; set; }
        #endregion



    }
}