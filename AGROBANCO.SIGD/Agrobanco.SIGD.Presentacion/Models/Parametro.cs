﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class Parametro
    {
        public Parametro()
        {
            auditoria = new AuxiliarModel();
        }
        public int CodParametro { get; set; }
        public int CodGrupo { get; set; }

        public string Valor { get; set; }
        public string Campo { get; set; }
        public short Estado { get; set; }
        public AuxiliarModel auditoria { get; set; }
    }
}