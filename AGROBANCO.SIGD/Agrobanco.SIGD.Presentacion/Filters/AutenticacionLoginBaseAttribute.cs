﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Agrobanco.SIGD.Presentacion.Filters
{
    public class AutenticacionLoginBaseAttribute : AuthorizeAttribute
    {
        public delegate void FunRespuestaUsuarioAutorizado(EUsuario usuarioDto);
        public delegate void FunRespuestaOk();
        public FunRespuestaUsuarioAutorizado RespuestaUsuarioAutorizado;
        public FunRespuestaOk RespuestaOk;
        public delegate EUsuario FunEnvioUsuarioAutorizado();
        public FunEnvioUsuarioAutorizado EnvioUsuarioAutorizado;


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            try
            {
                bool esRutaAutorizada = EsAccesoRutaAutorizada(filterContext.RequestContext.RouteData);

                if (esRutaAutorizada == false)//Si la ruta no es una página Autorizada para Anonimos=> verifica acceso por usuario
                {
                    base.OnAuthorization(filterContext);
                }
                if (RespuestaOk != null)
                {
                    RespuestaOk();
                }
            }
            catch (Exception ex)
            {
                new LogWriter(MethodBase.GetCurrentMethod().Name + " - " + ex.Message + "-" + ex.StackTrace);
            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            try
            {
                bool autenticado = false;
                EUsuario oUsuarioDTO = null;

                oUsuarioDTO = EnvioUsuarioAutorizado();

                if (oUsuarioDTO != null)
                {
                    RespuestaUsuarioAutorizado(oUsuarioDTO);
                    autenticado = true;
                }
                else
                {
                    httpContext.Response.StatusCode = 404;
                    return false;
                }

                bool? esRutaAutorizada = null;
                if (autenticado == true)
                {
                    string urlReferencial = "/";
                    if (httpContext.Request.UrlReferrer != null)
                    {
                        urlReferencial = httpContext.Request.UrlReferrer.AbsolutePath;
                    }
                    else
                    {
                        urlReferencial = httpContext.Request.RawUrl;
                    }

                    esRutaAutorizada = EsAccesoRutaAutorizadaUsuario(httpContext.Request.RequestContext.RouteData, oUsuarioDTO, urlReferencial);
                }
                else
                {
                    string UrlAcceso = "/";

                    foreach (var r in httpContext.Request.RequestContext.RouteData.Values.Values.ToArray())
                    {
                        UrlAcceso = UrlAcceso + "/" + r.ToString();
                    }
                    new LogWriter("Sin Acceso:" + UrlAcceso);
                }

                if (esRutaAutorizada == null)
                    esRutaAutorizada = true;
                return esRutaAutorizada.Value;
            }
            catch (Exception ex)
            {
                new LogWriter(MethodBase.GetCurrentMethod().Name + " - " + ex.Message + "-" + ex.StackTrace);
                return false;
            }
        }




        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            try
            {
               /* string token = (String)HttpContext.Current.Session["TokenSIGD"];
                 if (helperSec.ValidarTokenActivo(token) == false)
                {
                    filterContext.Result = new RedirectResult("~/Redirect/RedirectExternalUrl?optionRedirect=1");
                }
                else { 
                */
                var urlbase = ConfigurationManager.AppSettings["urlbase"];
                if (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Representante")
                {
                    filterContext.HttpContext.Response.StatusCode = filterContext.RequestContext.HttpContext.Response.StatusCode;
                    return;
                }

                if (!filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = filterContext.RequestContext.HttpContext.Response.StatusCode;
                    if (filterContext.RequestContext.HttpContext.Response.StatusCode == 440)
                    {   
                        //la session ha expirado
                        filterContext.Result = new RedirectResult(urlbase+"/Error/NoEncontrado");// en caso exista login va al login.
                    }
                    else
                    {
                        if (filterContext.RequestContext.HttpContext.Response.StatusCode == 200)
                        {
                            filterContext.Result = new RedirectResult(urlbase + "/Error/AccesoRestringido");// sin errores
                        }
                            
                        else
                        {
                            string token = (String)HttpContext.Current.Session["TokenSIGD"];
                            HelperAgroSeguridad helperSec = new HelperAgroSeguridad();
                            if (helperSec.ValidarTokenActivo(token) == false)
                            {
                                filterContext.Result = new RedirectResult(urlbase + "/Redirect/Inactividad");// si es otro código va a la página de error.
                            }
                            else
                            {
                                filterContext.Result = new RedirectResult(urlbase + "/Error/ErrorInterno");// si es otro código va a la página de error.
                            }
                                
                        }
                            
                            
                    }
                }
                else
                {
                    filterContext.HttpContext.Response.StatusCode = filterContext.RequestContext.HttpContext.Response.StatusCode;
                    //if (filterContext.RequestContext.HttpContext.Response.StatusCode == 440)
                    //{
                    //    //la session ha expirado
                    //    filterContext.HttpContext.Response.StatusCode = 440;
                    //    filterContext.HttpContext.Response.Write("la session ha expirado");
                    //    filterContext.HttpContext.Response.End();
                    //}
                    //else
                    //{
                    //    filterContext.HttpContext.Response.StatusCode = 500;
                    //    filterContext.HttpContext.Response.Write("ocurrió un error interno");
                    //    filterContext.HttpContext.Response.End();
                    //}
                }
                filterContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
            }
            catch (Exception ex)
            {
                new LogWriter(MethodBase.GetCurrentMethod().Name + " - " + ex.Message + "-" + ex.StackTrace);
                throw;
            }
        }




        #region "Métodos Auxiliares"
        private bool EsAccesoRutaAutorizada(RouteData routeData)
        {
            bool salida = true;
            var section = (Hashtable)ConfigurationManager.GetSection("PaginasAnonimas");



            var routeDataTemp = routeData;
            String UrlAcceso = "";

            if (Convert.ToString(routeDataTemp.Values.Values.ToArray()[1]) == "APPP")
                UrlAcceso = "/" + routeDataTemp.Values.Values.ToArray()[1] + "/" + routeDataTemp.Values.Values.ToArray()[0];
            else
                if (routeDataTemp.Values.Values.ToArray()[routeDataTemp.Values.Values.Count - 1].GetType().ToString().Contains("Mvc.DictionaryValueProvider"))
                UrlAcceso = "/" + routeDataTemp.Values.Values.ToArray()[routeDataTemp.Values.Values.Count - 3] + "/" + routeDataTemp.Values.Values.ToArray()[routeDataTemp.Values.Values.Count - 2];
            else
                UrlAcceso = "/" + routeDataTemp.Values.Values.ToArray()[0] + "/" + routeDataTemp.Values.Values.ToArray()[1];


            Dictionary<string, string> dicSection = section.Cast<DictionaryEntry>().ToDictionary(d => (string)d.Key, d => (string)d.Value);
            var encuentra = (from xx in dicSection where xx.Value.ToUpper() == UrlAcceso.ToUpper() || xx.Value.ToUpper() + "/" == UrlAcceso.ToUpper() select xx).Count();


            if (encuentra == 0)
            {
                salida = false;
            }
            return salida;
            //return true;
        }
        private bool? EsAccesoRutaAutorizadaUsuario(RouteData routeData, EUsuario usuario, string urlReferencia)
        {
            bool? autorizado = null;
            var routeDataTemp = routeData;

            string controllerTemp = "";// Convert.ToString(routeDataTemp.Values["controller"]);
            string actionTemp = "";// Convert.ToString(routeDataTemp.Values["action"]);
            
            //String UrlAcceso = "";
            //UrlAcceso = "/" + routeDataTemp.Values.Values.ToArray()[0] + "/" + routeDataTemp.Values.Values.ToArray()[1];
            controllerTemp = Convert.ToString(routeDataTemp.Values.Values.ToArray()[0]);
            actionTemp = Convert.ToString(routeDataTemp.Values.Values.ToArray()[1]);
            //var lstpermisos = (from xx in usuario.Permisos where xx.ToUpper() == UrlAcceso.Replace("Index", "").ToUpper() || xx.ToUpper() == UrlAcceso.ToUpper() || xx.ToUpper() + "/" == UrlAcceso.ToUpper() || "/ES-PE" + xx.ToUpper() == UrlAcceso.ToUpper() || "ES-PE" + xx.ToUpper() == UrlAcceso.ToUpper() || "ES-PE/" + xx.ToUpper() + "/" == UrlAcceso.ToUpper() || "/ES-PE/" + xx.ToUpper() + "/" == UrlAcceso.ToUpper() || "/ES-PE" + xx.ToUpper() == UrlAcceso.ToUpper() + "/" || "ES-PE" + xx.ToUpper() == UrlAcceso.ToUpper() + "/" || "/" + xx.ToUpper() == UrlAcceso.ToUpper() + "/" || xx.ToUpper() == UrlAcceso.ToUpper() + "/" || ContieneLista(UrlAcceso, xx) select xx);
            //autorizado = lstpermisos.Count() > 0;
            string actionReferencial = string.Empty;

            if (!string.IsNullOrEmpty(urlReferencia))
            {
                var urlSplit = urlReferencia.Split('/');

                if (urlSplit.Length >= 1)
                {
                    actionReferencial = urlSplit.LastOrDefault();
                }
            }
            
            autorizado = HelperSeguridad.VerificarPrivilegio(controllerTemp, actionTemp, actionReferencial, true);
            if (autorizado == null)
            {
                var noautorizado = HelperSeguridad.VerificarPrivilegio(controllerTemp, actionTemp, actionReferencial, false);
                if (noautorizado != null)
                    autorizado = false;
            }

            return autorizado;

        }

        private bool ContieneLista(string UrlAcceso, string listaElementos)
        {
            bool valido = false;
            string[] arrayElementos = listaElementos.Split(',');
            if (arrayElementos.Length > 1)
            {
                valido = arrayElementos.ToList().Exists(x => x.ToUpper() == UrlAcceso.ToUpper());
            }
            return valido;
        }
        #endregion

    }
}