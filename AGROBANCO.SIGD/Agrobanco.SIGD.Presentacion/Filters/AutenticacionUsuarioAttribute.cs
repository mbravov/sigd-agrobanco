﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Filters
{
    public class AutenticacionUsuarioAttribute : AutenticacionLoginBaseAttribute
    {
        public AutenticacionUsuarioAttribute()
        {
            RespuestaUsuarioAutorizado = FunRespuestaUsuarioAutorizado;
            EnvioUsuarioAutorizado = FunEnvioUsuarioAutorizado;
            RespuestaOk = FunRespuestaOk;
        }
        public new void FunRespuestaOk()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-PE");
            System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
        }
        public new void FunRespuestaUsuarioAutorizado(EUsuario usuarioDto)
        {
            try
            {

            }
            catch (Exception ex)
            {
                new LogWriter(MethodBase.GetCurrentMethod().Name + " - " + ex.Message + "-" + ex.StackTrace);
            }
        }
        public new EUsuario FunEnvioUsuarioAutorizado()
        {
            return HelperSeguridad.ObtenerSessionUsuario();
        }
    }
}