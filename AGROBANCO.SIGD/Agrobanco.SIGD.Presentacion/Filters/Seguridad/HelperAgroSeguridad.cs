﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Filters
{
    public static class SeguridadKeys
    {
        public static string UrlApiSGS => ConfigurationManager.AppSettings["UrlAPISGS"];
        public static string UrlWebSSA => ConfigurationManager.AppSettings["UrlWebSSA"];
    }

    public class HelperAgroSeguridad
    {
        public ResultadoObtenerModel<SUsuario> ObtenerSesionUsuario(string Token, string idPerfil)
        {
            try
            {
                string urlRequest = $"{SeguridadKeys.UrlApiSGS}/api/usuario/ObtenerSesionUsuario/{idPerfil}";

                //OBTENEMOS LA LISTA DE PERFILES
                ResultadoObtenerModel<SUsuario> Resultado = SendRequest<ResultadoObtenerModel<SUsuario>>
                                         (urlRequest, null, Token, "GET", "application/json");

                return Resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ServicioResponse ValidarUsuarioAD(string Token, string usuarioAD)
        {

            string urlApi = $"{SeguridadKeys.UrlApiSGS}/api/usuario/ValidarUsuarioAD?usuarioAD={usuarioAD}";
            ServicioResponse eServicioResponse = SendRequest<ServicioResponse>
                                                (urlApi, null, Token, "GET", "application/json");

            return eServicioResponse;
        }

        public bool ValidarTokenActivo(string token)
        {
            bool validate = true;

            string urlApi = $"{SeguridadKeys.UrlApiSGS}/api/validate/token";

            String Response = SendRequest<String>
                                     (urlApi, null, token, "GET", "application/json");

            if (Response == "401" || Response == "ex")
            {
                validate = false;
            }

            return validate;

        }

        public static T SendRequest<T>(string requestUrl, object JSONRequest,
                                      string token, string tipoRequest, string JSONContentType)
        {

            T objResponse = default(T);
            GenerarLog genLog = new GenerarLog();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(requestUrl);

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSONContentType));

                    if (!string.IsNullOrEmpty(token))
                        client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");

                    genLog.GenerarArchivoLog("Tipo Request: " + tipoRequest + " - URL Request:" + requestUrl);

                    HttpResponseMessage response = new HttpResponseMessage();
                    if (tipoRequest == "POST")
                    {
                        StringContent sc = new StringContent(JsonConvert.SerializeObject(JSONRequest), Encoding.UTF8, JSONContentType);
                        response = client.PostAsync(requestUrl, sc).Result;
                    }
                    else
                    {
                        response = client.GetAsync(requestUrl).Result;
                    }

                    //response.EnsureSuccessStatusCode();
                    //var message = response.Content.ReadAsStringAsync();
                    //objResponse = JsonConvert.DeserializeObject<T>(message.Result);

                    //return objResponse;

                    if (response.IsSuccessStatusCode)
                    {
                        var message = response.Content.ReadAsStringAsync();
                        objResponse = JsonConvert.DeserializeObject<T>(message.Result);

                        return objResponse;

                    }
                    else
                    {

                        genLog.GenerarArchivoLog("Response Status Code: " + response.StatusCode.ToString());
                        genLog.GenerarArchivoLog("Response ReasonPhrase:" + response.ReasonPhrase.ToString());

                        //if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                        //{
                        objResponse = JsonConvert.DeserializeObject<T>("401");
                        //}


                        return objResponse;
                    }
                }
            }
            catch (Exception ex)
            {
                genLog.GenerarArchivoLog("Ex Message: " + ex.Message);
                genLog.GenerarArchivoLog("StackTrace: " + ex.StackTrace);
                throw ex;
            }

        }

        public ResultadoObtenerModel<SUsuario> ObtenerSesionAgroSeguridad()
        {
            return (ResultadoObtenerModel<SUsuario>)HttpContext.Current.Session["UsuarioSIGD"];
        }
    }
}