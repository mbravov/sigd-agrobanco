﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Filters
{
    public class AgroSeguridadAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToUpper();
            bool validate = true;

            switch (controllerName)
            {
                case "ERROR":
                    validate = false;
                    break;
                case "REDIRECT":
                    validate = false;
                    break;
                case "DOCUMENTOEXTERNAL":
                    validate = false;
                    break;
                default:
                    validate = true;
                    break;
            }

            if (validate == true)
            {
                string token = (String)HttpContext.Current.Session["TokenSIGD"];
                string idPerfil = (string)HttpContext.Current.Session["IdPerfilSIGD"];
                EUsuario oUsuario = (EUsuario)HttpContext.Current.Session["oUsuario"];
                HelperAgroSeguridad helperSec = new HelperAgroSeguridad();
                
                if (string.IsNullOrEmpty(token))
                {
                    filterContext.Result = new RedirectResult("~/Redirect/RedirectExternalUrl?optionRedirect=1");
                }
                else if (helperSec.ValidarTokenActivo(token) == false)
                {
                    filterContext.Result = new RedirectResult("~/Redirect/RedirectExternalUrl?optionRedirect=1");
                }
                else
                {
                    
                    ResultadoObtenerModel<SUsuario> usuarioSession = helperSec.ObtenerSesionUsuario(token, idPerfil);                    
                    HttpContext.Current.Session["UsuarioSIGD"] = usuarioSession;

                    if (usuarioSession.Model.EstadoAD == "1")
                    {
                        ServicioResponse eServicioResponse = helperSec.ValidarUsuarioAD(token, usuarioSession.Model.UsuarioAD);
                        if (eServicioResponse.Resultado == 0)
                        {
                            filterContext.Result = new RedirectResult("~/Error/UnauthorizedOperation?msjErrorExcepcion=" + eServicioResponse.Mensaje);
                        }
                    }

                    bool respuestaSIGD = HelperSeguridad.AutenticacionSIGD();

                    if (!respuestaSIGD)
                    {
                        filterContext.Result = new RedirectResult("~/Redirect/TrabajadorSIGDInexistente?usuarioWeb=" + usuarioSession.Model.UsuarioWeb);
                    }
                }
            }

        }
    }
}