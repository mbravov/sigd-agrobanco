﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Filters.Seguridad
{
    public class ServicioResponse
    {

        public ServicioResponse()
        {
            Resultado = 1;
        }

        public int Resultado { get; set; }

        public string Mensaje { get; set; }
    }
}