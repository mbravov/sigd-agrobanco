﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Utilities
{
    public class ConfigurationUtilities
    {
        public static int PageSize
        {
            get
            {
                return ConfigurationManager.AppSettings.AllKeys.Any(k => k.Contains("PageSize")) &&
                     ConfigurationManager.AppSettings["PageSize"] != null &&
                     ConfigurationManager.AppSettings["PageSize"].TryParseToInt()
                     ? ConfigurationManager.AppSettings["PageSize"].ToInt() : 10;
            }
        }
        public static bool IgnorarPw => Convert.ToBoolean(ConfigurationManager.AppSettings["IgnorarPw"]);
        public static int DiasDiferenciaBusqueda
        {
            get
            {
                return ConfigurationManager.AppSettings.AllKeys.Any(k => k.Contains("DiasDiferenciaBusqueda")) &&
                     ConfigurationManager.AppSettings["DiasDiferenciaBusqueda"] != null &&
                     ConfigurationManager.AppSettings["DiasDiferenciaBusqueda"].TryParseToInt()
                     ? ConfigurationManager.AppSettings["DiasDiferenciaBusqueda"].ToInt() : -30;
            }
        }

        public static string NombreReporteBandejaMesaPartes
        {
            get
            {
                return ConfigurationManager.AppSettings.AllKeys.Any(k => k.Contains("NombreReporteBandejaMesaPartes")) &&
                     ConfigurationManager.AppSettings["NombreReporteBandejaMesaPartes"] != null
                     ? ConfigurationManager.AppSettings["NombreReporteBandejaMesaPartes"].ToString() : "Reporte NombreReporteBandejaMesaPartes";
            }
        }
        public static string NombreReporteBandejaEntradaConPlazo
        {
            get
            {
                return ConfigurationManager.AppSettings.AllKeys.Any(k => k.Contains("NombreReporteBandejaEntradaConPlazo")) &&
                     ConfigurationManager.AppSettings["NombreReporteBandejaEntradaConPlazo"] != null
                     ? ConfigurationManager.AppSettings["NombreReporteBandejaEntradaConPlazo"].ToString() : "Reporte Bandeja Entrada ConPlazo";
            }
        }
        public static string NombreReporteBandejaEntradaRecibidos
        {
            get
            {
                return ConfigurationManager.AppSettings.AllKeys.Any(k => k.Contains("NombreReporteBandejaEntradaRecibidos")) &&
                     ConfigurationManager.AppSettings["NombreReporteBandejaEntradaRecibidos"] != null
                     ? ConfigurationManager.AppSettings["NombreReporteBandejaEntradaRecibidos"].ToString() : "Reporte Bandeja Entrada Recibidos";
            }
        }

        public static string WebApiUrl
        {
            get
            {
                return ConfigurationManager.AppSettings.AllKeys.Any(k => k.Contains("WebApiUrl")) &&
                     ConfigurationManager.AppSettings["WebApiUrl"] != null
                     ? ConfigurationManager.AppSettings["WebApiUrl"].ToString() : "https://localhost:44307/api/";
            }
        }

        public static string urlbase
        {
            get
            {
                return ConfigurationManager.AppSettings.AllKeys.Any(k => k.Contains("urlbase")) &&
                     ConfigurationManager.AppSettings["urlbase"] != null
                     ? ConfigurationManager.AppSettings["urlbase"].ToString() : "https://localhost:59481/";
            }
        }


        public static string FormatoFechaNombreReporte
        {
            get
            {
                return ConfigurationManager.AppSettings.AllKeys.Any(k => k.Contains("FormatoFechaNombreReporte")) &&
                     ConfigurationManager.AppSettings["FormatoFechaNombreReporte"] != null
                     ? ConfigurationManager.AppSettings["FormatoFechaNombreReporte"].ToString() : "yyyy-MM-dd";
            }
        }
    }
}