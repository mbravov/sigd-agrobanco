﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Presentacion.Controllers;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Agrobanco.SIGD.Presentacion.Areas.Mantenimiento.Controllers
{
    [AutenticacionUsuario]
    public class UsuarioSIEDController : Controller
    {
        // GET: Mantenimiento/UsuarioSIED
        public ActionResult Index()
        {
            return View(ObtenerUsuarioSIED());
        }


        private UsuarioSIED ObtenerUsuarioSIED()
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new Service().HttpGetJson<RespuestaOperacionServicio>(
            ConfigurationUtilities.WebApiUrl + "/UsuarioSIED/ObtenerUsuarioSIED", GetToken());
            ViewBag.Usuarios = _service.HttpGetJson<List<TrabajadorDTO>>(UrlApi.ObtenerUrl("/trabajador/GetTrabajadores"), GetToken());

            var item = Helpers.ConvertToObject<UsuarioSIED>(respuesta.data);
            if(item == null)
            {
                item = new UsuarioSIED();
            }
            return item;
        }


        [HttpPost]
        public async Task<ActionResult> Grabar(UsuarioSIED obj)
        {

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            if(obj.Accion == "editar")
            {
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "UsuarioSIED/Editar", GetToken());
            }
            else if(obj.Accion == "grabar")
            {
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "UsuarioSIED/Grabar", GetToken());
            }

            return Json(respuesta);
        }


        public string GetToken()
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            return strRptToken;
        }


    }
}