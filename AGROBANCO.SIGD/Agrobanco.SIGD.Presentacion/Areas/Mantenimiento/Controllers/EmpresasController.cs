﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Controllers;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Areas.Mantenimiento.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class EmpresasController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult> Ver(int id)
        {

            return View("_Ver", await ObtenerEmpresa(id));
        }
        [HttpGet]
        public async Task<ActionResult> Editar(int id)
        {

            return View("_Editar", await ObtenerEmpresa(id));
        }

        [HttpGet]
        public async Task<ActionResult> Nuevo()
        {
            Empresa asunto = new Empresa();

            DocumentoController documentoController = new DocumentoController();
            var parametros = await documentoController.CargarParametrosComun();
            ViewBag.Estados = parametros.ListaEstados;
            asunto.FechaCreacion = DateTime.Now;
            asunto.Estado = 1;
            return View("_Nuevo", asunto);
        }
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            int nroPagina = 1;
            MantenimientoEmpresaListadoViewModel viewmodel = new MantenimientoEmpresaListadoViewModel();
            viewmodel.ResultadoBusqueda = new ResultadoBusquedaModel<Empresa>();

            //Obtener resultado busqueda            
            var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarEmpresa(nroPagina, "", ConfigurationUtilities.PageSize);
            viewmodel.ResultadoBusqueda = onePageListaDocumentos;
            viewmodel.ResultadoBusqueda.ListaPaginado = onePageListaDocumentos.ToPageList();

            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();

            var parametros = await documentoController.CargarParametrosComun();
            ViewBag.Estados = parametros.ListaEstados;

            return View(viewmodel);
        }
        [HttpPost]
        public async Task<ActionResult> Index(int? page, string searchString, string FechaRegistroDesde, string FechaRegistroHasta)
        {
            int nroPagina = page == null ? 1 : page.Value;
            ViewBag.searchString = searchString;
            ViewBag.FiltrosActuales = Helpers.ClearString(searchString);

            //Obtener resultado busqueda            
            var onePageLista = await new ProxyApi(base.GetToken()).ProxyListarEmpresa(nroPagina, searchString, ConfigurationUtilities.PageSize);
            onePageLista.ListaPaginado = onePageLista.ToPageList();

            return PartialView("_Lista", onePageLista);
        }


        private async Task<Empresa> ObtenerEmpresa(int id)
        {
            RespuestaOperacionServicio itemOp = new Service().HttpGetJson<RespuestaOperacionServicio>(
                ConfigurationUtilities.WebApiUrl + "/empresa/ObtenerEmpresa?id=" + id, GetToken());

            var itemEmpresa = Helpers.ConvertToObject<Empresa>(itemOp.data);
            DocumentoController documentoController = new DocumentoController();
            ParametrosComunDTO parametros = await documentoController.CargarParametrosComun();
            var itemEstado = parametros.ListaEstados.Find(x => x.Valor == itemEmpresa.Estado.ToString());
            if (itemEstado != null)
                itemEmpresa.EstadoDescripcion = itemEstado.Campo;

            ViewBag.Estados = parametros.ListaEstados;

            return itemEmpresa;
        }

        [HttpPost]
        public async Task<ActionResult> Grabar(FormCollection form)
        {
            Empresa request = new Empresa();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Helpers.ConvertToModelV3(form, request);

            if (request.CodEmpresa == 0)//registrar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Empresa/Registrar", GetToken());
            }
            else//actualizar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Empresa/Actualizar", GetToken());
            }
            return Json(respuesta);
        }
        [HttpPost]
        public async Task<ActionResult> Eliminar(int id)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Empresa request = new Empresa();
            request.CodEmpresa = id;

            if (request.CodEmpresa > 0)//registrar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Empresa/Eliminar", GetToken());
            }
            return Json(respuesta);
        }

        public async Task<FileResult> ExportarPDF(string searchString)
        {
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "PDF";
            string fileNameReport = "ReportePDF";
            List<Empresa> lista = (await new ProxyApi(base.GetToken()).ProxyListarEmpresa(1, searchString, int.MaxValue)).Lista;
            return PDFFileResult(viewReport, lista, fileNameReport, true);
        }
        public void ExportarExcel(string searchString)
        {

            List<Empresa> lista = (new ProxyApi(base.GetToken()).ProxyListarEmpresa_Sync(1, searchString, int.MaxValue)).Lista;
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Lista de Empresas";
            string fileNameReport = "ReporteExcel";

            GenerarExcelResponse<List<Empresa>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 8;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "CÓDIGO";
                ws.Cell(rowIndex, 2).Value = "DESCRIPCIÓN";
                ws.Cell(rowIndex, 3).Value = "DIRECCION";
                ws.Cell(rowIndex, 4).Value = "ESTADO";
                ws.Cell(rowIndex, 5).Value = "REGISTRADO POR";
                ws.Cell(rowIndex, 6).Value = "FECHA CREACION";
                ws.Cell(rowIndex, 7).Value = "ACTUALIZADO POR";
                ws.Cell(rowIndex, 8).Value = "FECHA ACTUALIZACION";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = item.CodEmpresa;
                    ws.Cell(rowIndex, 2).Value = item.Descripcion;
                    ws.Cell(rowIndex, 3).Value = item.Direccion;
                    ws.Cell(rowIndex, 4).Value = (item.Estado == 1 ? "Activo" : "Inactivo");
                    ws.Cell(rowIndex, 5).Value = item.UsuarioCreacion;
                    ws.Cell(rowIndex, 6).Value = item.FechaCreacion;
                    ws.Cell(rowIndex, 7).Value = item.UsuarioActualizacion;
                    ws.Cell(rowIndex, 8).Value = item.FechaActualizacion;
                    //ws.Cell(rowIndex, 7).Value = item.Derivado + "\r" + item.Jefe;
                    //ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                }
                //ws.Columns().AdjustToContents();
                ws.Columns().AdjustToContents();
                ws.Columns("C").Width = 50;
            });
        }

    }
}
