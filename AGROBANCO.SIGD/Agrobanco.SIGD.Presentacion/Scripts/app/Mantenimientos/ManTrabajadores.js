﻿$(document).ready(function () {

    var codigoTrabajador = '';
    var confirmDelete = true;
    var urlbase = $("#hdUrlBase").val();

    var trabajador = (function () {
       
        var descripcionEdit = $("#descripcionEdit");
        //var btnGuardar = $("#btnGuardar");
        var btnCancelarEdicion = $("#btnCancelarEdicion");
        var btnCancelar = $("#btnCancelar");
        //var btnEditar = $("#btnEditar");
        var CodArea = $("#CodArea");
        var FechaCreacion = $("#FechaCreacion");

        //Visualizar
        var codigover = $("#codigover");
        var nombrever = $("#nombrever");
        var vpaterno = $("#paternover");
        var maternover = $("#maternover");
        var cargover = $("#cargover");
        var ubicacionver = $("#ubicacionver");
        var anexover = $("#anexover");
        var emailver = $("#emailver");
        var areaver = $("#areaver");
        var tipodocumentover = $("#tipodocumento");
        var nrodocumentover = $("#nrodocumento");
        var celulaver = $("#celulaver");
        var apecompletos = $("#apecompletos");
        var carjefever = $("#carjefever");
        var carFirmaver = $("#carFirmaver");
        var webusrver = $("#webusrver");


        var fechaver = $("#fechaver");
        var usuariover = $("#usuariover");
        var fechaactualizacionver = $("#fechaactualizacionver");
        var usuarioactver = $("#usuarioactver");
        var estadover = $("#estadover");
        var SelectAreas = $(".cbAreas");
        var SelectTipoDocumento = $(".cbTipoDocumento");

        // Nuevo

        var nnombre = $("#nnombre");
        var npaterno = $("#npaterno");
        var nmaterno = $("#nmaterno");
        var ncargo = $("#ncargo");
        var nubicacion = $("#nubicacion");
        var nanexo = $("#nanexo");
        var nemail = $("#nemail");
        var cboJefe = $("#cboJefe");
        var cboFirma = $("#cboFirma");
        var nnrodocumento = $("#nnrodocumento")
        var ncelular = $("#ncelular");
        var nwebusr = $("#nwebusr");

        // Editar              

        var CodTrabEdit = $("#CodTrabEdit");
        var nnombrEdit = $("#nnombrEdit");
        var npaternoEdit = $("#npaternoEdit");
        var nmaternoEdit = $("#nmaternoEdit");
        var nrodocumentoEdit = $("#nrodocumentoEdit");
        var ncelularEdit = $("#ncelularEdit");
        var ncargoEdit = $("#ncargoEdit");
        var nubicacionEdit = $("#nubicacionEdit");
        var nanexoEdit = $("#nanexoEdit")
        var nemailEdit = $("#nemailEdit");
        var webusredit = $("#webusredit");
        var estadogene = "";
        var codjefegene = "";
        var codareagene = "";
        var nomGeneAea = "";
      
        obtenerAreasllenarSelect(SelectAreas);
        cargarSelectTipoDocIdentidad(SelectTipoDocumento);
        
        var EstadoActivo = 1;
        var EstadoInactivo = 0;
        var identiJefe =01;
        var idenFirma = 0;

        var init = function () {
            
            btnCancelarEdicion.on("click", btnCancelarEdicionClick);
            btnCancelar.on("click", btnCancelarClick);
           
            obtenerEstadosSelect("#cbEstadoNuevo");
            $('#cbEstadoNuevo').val(EstadoActivo);
            $('#cbEstadoNuevo').formSelect();

            obtenerJefe("#cboJefe");
            $('#cboJefe').val(identiJefe);
            $('#cboJefe').formSelect();

            obtenerAfirmacion("#cboFirma");
            $('#cboFirma').val(idenFirma);
            $('#cboFirma').formSelect();
            
            FechaCreacion.val();
        };


        var btnCancelarClick = function () {       
            nnombre.val(""); 
            npaterno.val("");
            nmaterno.val("");
            ncargo.val("");
            nubicacion.val("");
            nanexo.val("");
            nemail.val("");
            SelectAreas.val("");
            SelectTipoDocumento.val("");
            cboJefe.val("");
            cboFirma.val("");
            ncelular.val("");
            nnrodocumento.val("");
            nwebusr.val("");
            $('#cbAreas').val("");
            $('#cbTipoDocumento').val("");        
            $('#ptv-NuevoTIpoDocumento').modal('close');
            return false;
        };


        var btnCancelarEdicionClick = function () {
            $('#ptv-EditarTIpoDocumento').modal('close');
            return false;
        };

       

        $(document).on("click", "#idEditarTrabajador", function () {
            $('label').addClass("active");
            $('#cbEstadoEdit option').remove();
            $('#cboAreaEdit option').remove();
            $('#cboJefeEdit option').remove();
            $('#cboFirmaEdit option').remove();
            $('#cbotipdocumentoEdit option').remove();
            
            obtenerEstadosSelect("#cbEstadoEdit");
            obtenerAreasllenarSelect("#cboAreaEdit");
            cargarSelectTipoDocIdentidad("#cbotipdocumentoEdit");
            obtenerJefe("#cboJefeEdit");
            obtenerAfirmacion("#cboFirmaEdit");

            codigoTrabajador = $(this).data('id');

            $.ajax({
                url: urlbase + "/Mantenimiento/Trabajadores/ObtenerTrabajadorID?id=" + codigoTrabajador,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    CodTrabEdit.val(result.iCodTrabajador);
                    nnombrEdit.val(result.vNombres);
                    npaternoEdit.val(result.vApePaterno);
                    nmaternoEdit.val(result.vApeMaterno);
                    nrodocumentoEdit.val(result.vNumDocumento);
                    ncelularEdit.val(result.vCelular);
                    ncargoEdit.val(result.vCargo);
                    nubicacionEdit.val(result.vUbicacion);
                    nanexoEdit.val(result.iAnexo);
                    nemailEdit.val(result.vEmail);
                    webusredit.val(result.WEBUSR);
                    estadogene = (result.siEstado);
                    codjefegene = (result.siEsJefe);
                    codareagene = (result.iCodArea);
                    nomGeneAea = (result.vArea);

                    $('#cbotipdocumentoEdit').val(result.iCodTipDocIdentidad);
                    $('#cboJefeEdit').val(result.siEsJefe);
                    $('#cboFirmaEdit').val(result.siFirma);
                    $('#cboAreaEdit').val(result.iCodArea);
                    $('#cbEstadoEdit').val(result.siEstado);

                    
                    $('#cbEstadoEdit').formSelect();
                    $('#cboAreaEdit').formSelect();
                    $('#cboJefeEdit').formSelect();
                    $('#cboFirmaEdit').formSelect();
                },
                error: function (e, b, c) {
                    MsgError("Hubo problemas en la aplicación.");
                    console.log(e.responseText);
                }
            });

        });

        $(document).on("click", "#idVerTabajador", function () {

            codigoTrabajador = $(this).data('id');
            
            $.ajax({
                url: urlbase + "/Mantenimiento/Trabajadores/ObtenerTrabajadorID?id=" + codigoTrabajador,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                                       
                    codigover.text(codigoTrabajador);                    
                    nombrever.text(result.vNombres);
                    vpaterno.text(result.vApeMaterno);
                    maternover.text(result.vApePaterno);
                    cargover.text(result.vCargo);
                    ubicacionver.text(result.vUbicacion);
                    anexover.text(result.iAnexo);
                    emailver.text(result.vEmail);
                    areaver.text(result.vArea);
                    tipodocumentover.text(result.vTipoDocumento);
                    nrodocumentover.text(result.vNumDocumento);
                    fechaver.text(result.sFecCreacion);
                    usuariover.text(result.vUsuCreacion);
                    celulaver.text(result.vCelular);
                    apecompletos.text(result.apeCompletos);
                    webusrver.text(result.WEBUSR);
                    carjefever.text((result.siEsJefe == "0") ? "NO" : "SI");
                    carFirmaver.text((result.siFirma == "0") ? "NO" : "SI");
                    fechaactualizacionver.text((result.sFecActualizacion == null) ? "" : result.sFecActualizacion);
                    usuarioactver.text((result.vUsuActualizacion == null) ? "" : result.vUsuActualizacion);
                    estadover.text((result.siEstado == 1) ? "Activo" : "Inactivo");
                },
                error: function (e, b, c) {
                    MsgError("Hubo problemas en la aplicación.");
                    console.log(e.responseText);
                }
            });


        });


        $(SelectAreas).change(function () {
            trabajador.CodArea = this.value;
            trabajador.DescripcionArea = $("#" + this.id + " option[value='" + this.value + "']").text();         
            return false;
        });



        $(document).on("click", "#btnEditar", function () {
            var obj = $(this);

            if ($('#frmEditarTrabajador')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmEditarTrabajador')[0].checkValidity()) {
                return true;
            }
                        
            var data = new Object();
            data.iCodTrabajador = CodTrabEdit.val();
            data.vNumDocumento = nrodocumentoEdit.val();
            data.vNombres = nnombrEdit.val();
            data.vApePaterno = npaternoEdit.val();
            data.vApeMaterno = nmaternoEdit.val();
            data.vCargo = ncargoEdit.val();
            data.vUbicacion = nubicacionEdit.val();
            data.iAnexo = nanexoEdit.val();
            data.vEmail = nemailEdit.val();
            data.siEstado = $('#cbEstadoEdit').val();
            data.siEsJefe = $('#cboJefeEdit').val();
            data.siFirma = $('#cboFirmaEdit').val();
            data.iCodArea = $('#cboAreaEdit').val();
            data.iCodTipDocIdentidad = $('#cbotipdocumentoEdit').val();
            data.vCelular = ncelularEdit.val();
            data.WEBUSR = webusredit.val();
            data.iCodPerfil = 0;
            data.vCodPerfil = "0";   
            data.iOperador = 2;
            //codareagene = (result.iCodArea);

            if (codjefegene == 1 && data.siEsJefe == 0) {
                //miConfirm('El área ' + nomGeneAea + ' quedará sin jefe asignado, ¿Desea continuar?', function (ok) {
                miConfirm('El área ' + nomGeneAea + ' quedará sin jefe asignado, ¿Desea continuar?', function (ok) {
                    if (ok === true) {
                        EjecutarAjaxPost(obj, "/Mantenimiento/Trabajadores/Actualizar", data,
                            function (result) {


                                if (result.data === -3) {

                                    MsgError("El webuser " + webusredit.val() + " ya se encuentra registrado");
                                    return;

                                }

                                if (result.data === -2) {

                                    MsgError("El área " + $('#cboAreaEdit :selected').text() + " ya tiene un jefe asignado");
                                    return;

                                }
                                if (result.Resultado === 1) {
                                    MsgInformacion("Se registró correctamente", function () {
                                        window.location.href = urlbase + "/Mantenimiento/Trabajadores";
                                    });
                                }
                                else {
                                    MsgError("Hubo problemas en el registro.");
                                }
                            },
                            function (e, b, c) {

                                MsgError("Hubo problemas en la aplicación.");
                            }
                        );
                    }

                });
                return false;

            }
            else {

                if (estadogene == 1 && data.siEstado == 0 && codjefegene == 1 && codareagene == data.iCodArea) {
                    miConfirm('El área ' + $('#cboAreaEdit :selected').text() + ' quedará sin jefe asignado, ¿Desea continuar?', function (ok) {
                        if (ok === true) {
                            EjecutarAjaxPost(obj, "/Mantenimiento/Trabajadores/Actualizar", data,
                                function (result) {


                                    if (result.data === -3) {

                                        MsgError("El webuser " + webusredit.val() + " ya se encuentra registrado");
                                        return;

                                    }

                                    if (result.data === -2) {

                                        MsgError("El área " + $('#cboAreaEdit :selected').text() + " ya tiene un jefe asignado");
                                        return;

                                    }
                                    if (result.Resultado === 1) {
                                        MsgInformacion("Se registró correctamente", function () {
                                            window.location.href = urlbase + "/Mantenimiento/Trabajadores";
                                        });
                                    }
                                    else {
                                        MsgError("Hubo problemas en el registro.");
                                    }
                                },
                                function (e, b, c) {

                                    MsgError("Hubo problemas en la aplicación.");
                                }
                            );
                        }

                    });
                    return false;

                }
                else {

                    miConfirm('¿Está seguro de editar el registro?', function (ok) {
                        if (ok === true) {
                            EjecutarAjaxPost(obj, "/Mantenimiento/Trabajadores/Actualizar", data,
                                function (result) {
                                    debugger

                                    if (result.data === -3) {

                                        
                                        MsgError("El webuser " + webusredit.val() + " ya se encuentra registrado");
                                        return;

                                    }

                                    if (result.data === -2) {

                                        MsgError("El área " + $('#cboAreaEdit :selected').text() + " ya tiene un jefe asignado");
                                        return;

                                    }
                                    if (result.Resultado === 1) {
                                        MsgInformacion("Se registró correctamente", function () {
                                            window.location.href = urlbase + "/Mantenimiento/Trabajadores";
                                        });
                                    }
                                    else {
                                        MsgError("Hubo problemas en el registro.");
                                    }
                                },
                                function (e, b, c) {
                                    debugger
                                    MsgError("Hubo problemas en la aplicación.");
                                }
                            );
                        }
                    });
                    return false;
                }
            
            }

           
            
                     

        
        });




        $(document).on("click", "#btnGuardar", function () {
                      
            var obj = $(this);        

            if ($('#frmNuevoTrabajador')[0].checkValidity()) {
                event.preventDefault();
            }
            if (!$('#frmNuevoTrabajador')[0].checkValidity()) {
                return true;
            }
            
            var data = new Object();
            data.vNumDocumento = nnrodocumento.val();
            data.vNombres = nnombre.val();
            data.vApePaterno = npaterno.val();
            data.vApeMaterno = nmaterno.val();
            data.vCargo = ncargo.val();
            data.vUbicacion = nubicacion.val();
            data.iAnexo = nanexo.val();
            data.vEmail = nemail.val();
            data.siEstado = $('#cbEstadoNuevo').val();
            data.siEsJefe = $('#cboJefe').val();
            data.siFirma = $('#cboFirma').val();
            data.iCodArea = $('#cbAreas').val();
            data.iCodTipDocIdentidad = $('#cbTipoDocumento').val();
            data.vCelular = ncelular.val();
            //data.WEBUSR = nwebusr.val();     
            data.WEBUSR = nwebusr.val();
            data.iCodPerfil = 0;
            data.vCodPerfil = "0";
            data.iOperador = 1;
                       
            miConfirm('¿Está seguro de grabar el registro?', function (ok) {
                if (ok === true) {
                    EjecutarAjaxPost(obj, "/Mantenimiento/Trabajadores/Registrar", data,
                        function (result) {
                            

                            if (result.data === -3) {
                                
                                MsgError("El webuser " + nwebusr.val() + " ya se encuentra registrado");
                                return;

                            }

                            if (result.data === -2) {

                                MsgError("El área " + $('#cbAreas :selected').text() + " ya tiene un jefe asignado");
                                return;
                                
                            }
                            if (result.Resultado === 1) {
                                MsgInformacion("Se registró correctamente", function () {
                                    window.location.href = urlbase + "/Mantenimiento/Trabajadores";
                                });
                            }
                            else {
                                MsgError("Hubo problemas en el registro.");
                            }
                        },
                        function (e, b, c) {
                            
                            MsgError("Hubo problemas en la aplicación.");
                        }
                    );
                }
            });
            return false;
        });



       
        $(document).on("click", "#idEliminarTrabajador", function () {
            
            var obj = $(this);
            var Trabajadores = new Object();
            
            Trabajadores.iCodTrabajador = $(this).data('id');
            Trabajadores.siEsJefe = "";
            var vArea = "";           

            $.ajax({
                url: urlbase + "/Mantenimiento/Trabajadores/ObtenerTrabajadorID?id=" + Trabajadores.iCodTrabajador,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    
                    //codigover.text(codigoTrabajador);
                    Trabajadores.siEsJefe = result.siEsJefe;
                    vArea = result.vArea;

                    if (Trabajadores.siEsJefe == 1) {

                        miConfirm('El área ' + vArea + ' quedará sin jefe asignado, ¿Desea continuar?', function (ok) {
                            if (ok === true) {
                                EjecutarAjaxPost(obj, "/Mantenimiento/Trabajadores/Eliminar", Trabajadores,
                                    function (result) {
                                        if (result.Resultado === 1) {
                                            MsgInformacion("Se eliminó el registro correctamente", function () {
                                                window.location.href = urlbase + "/Mantenimiento/Trabajadores";

                                            });
                                        }
                                        else {
                                            MsgError("Hubo problemas en el registro.");
                                        }
                                    },
                                    function (e, b, c) {
                                        MsgError("Hubo problemas en la aplicación.");
                                    }
                                );
                            }
                        });
                    }
                    else if (Trabajadores.siEsJefe == 0) {
                        miConfirm('¿Está seguro de inactivar el registro?', function (ok) {
                            if (ok === true) {
                                EjecutarAjaxPost(obj, "/Mantenimiento/Trabajadores/Eliminar", Trabajadores,
                                    function (result) {
                                        if (result.Resultado === 1) {
                                            MsgInformacion("Se eliminó el registro correctamente", function () {
                                                window.location.href = urlbase + "/Mantenimiento/Trabajadores";

                                            });
                                        }
                                        else {
                                            MsgError("Hubo problemas en el registro.");
                                        }
                                    },
                                    function (e, b, c) {
                                        MsgError("Hubo problemas en la aplicación.");
                                    }
                                );
                            }
                        });
                    }

                },
                error: function (e, b, c) {
                    MsgError("Hubo problemas en la aplicación.");
                    console.log(e.responseText);
                }
            });
                  
           
            return false;
        });
    
        return {
            init: init
        };

    })();
    trabajador.init();
});