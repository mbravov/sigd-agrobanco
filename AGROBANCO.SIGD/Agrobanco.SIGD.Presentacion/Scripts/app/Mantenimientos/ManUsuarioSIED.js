﻿$(document).ready(function () {

    var urlbase = $("#hdUrlBase").val();

    var manusuariosied = (function () {
        var selectUsuarioSIED = $("#cbUsuario");
        var nombre = $("#nombre");
        var apaterno = $("#apaterno");
        var amaterno = $("#amaterno");
        var email = $("#email");
        var nrodoc = $("#nrodoc");
        var codtrabajador = $("#codtrabajador");
        var btnGuardar = $("#btnGuardar");
        var accion = $(".frmUsuarioSIEDAccion");

        var init = function () {
            btnGuardar.on("click", btnGuardarClick);

            $(selectUsuarioSIED).change(function () {
                $(".lblactive").addClass("active");
                nombre.val($(this).find(':selected').data('nombre'));
                apaterno.val($(this).find(':selected').data('apepaterno'));
                amaterno.val($(this).find(':selected').data('apematerno'));
                email.val($(this).find(':selected').data('email'));
                nrodoc.val($(this).find(':selected').data('numdoc'));
                codtrabajador.val($(this).find(':selected').data('codtrabajador'));
                return false;
            });
        };

        var btnGuardarClick = function (event, data) {
            var ejecucionExitosa = "";


            if ($('#frmUsuarioSIED')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmUsuarioSIED')[0].checkValidity()) {
                return true;
        }

            var obj = $(this);
            var data = new Object();
            data.WebUsr = $('#cbUsuario').val();
            data.Nombre = nombre.val();
            data.ApePaterno = apaterno.val();
            data.ApeMaterno = amaterno.val();
            data.Email = email.val();
            data.NroDoc = nrodoc.val();
            data.Accion = accion.val();
            data.CodTrabajador = codtrabajador.val();
            if (accion.val() == "grabar") {
                ejecucionExitosa = "Se grabó el registro correctamente";
            } else {
                ejecucionExitosa = "Se actualizó el registro correctamente";
            }


            miConfirm('¿Está seguro de ' + accion.val() + ' el registro?', function (ok) {
                if (ok === true) {
                    EjecutarAjaxPost(obj, "/usuarioSIED/Grabar", data,
                        function (result) {
                            if (result.Resultado === 1) {
                                MsgInformacion(ejecucionExitosa, function () {
                                    window.location.href = urlbase + "/Mantenimiento/UsuarioSIED";
                                });
                            }
                            else {
                                MsgError("Hubo problemas en el registro.");
                            }
                        },
                        function (e, b, c) {
                            MsgError("Hubo problemas en la aplicación.");
                        }
                    );
                }
            });     
        };

        return {
            init: init
        };

    })();
    manusuariosied.init();
});