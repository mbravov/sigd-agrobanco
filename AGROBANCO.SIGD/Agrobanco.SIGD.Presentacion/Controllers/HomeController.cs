﻿using Agrobanco.SIGD.Presentacion.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Controllers
{
    
    [ErrorFilter]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }        

    }
}