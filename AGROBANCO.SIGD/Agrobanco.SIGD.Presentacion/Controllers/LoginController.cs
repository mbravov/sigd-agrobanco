﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Controllers
{

    public class LoginController : Controller
    {
        // GET: Bienvenidos
        public ActionResult Index()
        {
            var sesion = HelperSeguridad.ObtenerSessionUsuario();

            if (sesion != null)
                return RedirectToAction("Index", "Home");

            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FormCollection coleccion)
        {
            Session.RemoveAll();
            ModelState.Clear();

            Usuario oBEUsuario = new Usuario();
            oBEUsuario.UserName = coleccion["username"].ToUpper().Trim();
            oBEUsuario.Password = coleccion["password"];

            if (!string.IsNullOrWhiteSpace(oBEUsuario.UserName) && !string.IsNullOrWhiteSpace(oBEUsuario.Password))
            {
                ELoginRequest oBELoginReq = new ELoginRequest();
                oBELoginReq.Username = oBEUsuario.UserName;                
                //string urlAutenticate = UrlApi.ObtenerUrl("/Login/authenticatedummy");
                string urlAutenticate = Configuracion.SeguridadUrlAutenticateDummy;
                if (ConfigurationUtilities.IgnorarPw == false)
                {
                    urlAutenticate = Configuracion.SeguridadUrlAutenticate;
                    //urlAutenticate = UrlApi.ObtenerUrl("/Login/authenticate");
                    oBEUsuario.Password = Comun.Security.MD5Hash(oBEUsuario.Password);
                }
                oBELoginReq.Password = oBEUsuario.Password;

                Service _service = new Service();

                string strBodyLogin = JsonComponent.Serialize<ELoginRequest>(oBELoginReq);
                try
                {
                    string strRptToken = _service.HttpPostJson(urlAutenticate, strBodyLogin);

                    if (!string.IsNullOrWhiteSpace(strRptToken))
                    {
                        strRptToken = strRptToken.Remove(0, 1).ToString();
                        strRptToken = strRptToken.Remove(strRptToken.Length - 1, 1).ToString();
                        strRptToken = "Bearer " + strRptToken;
                        Session["tokenUser"] = strRptToken;
                        //EUsuario UsuEObj = _service.HttpGetJson<EUsuario>(UrlApi.ObtenerUrl("/Usuario/") + oBELoginReq.Username, strRptToken);
                        EUsuario UsuEObj = _service.HttpGetJson<EUsuario>(Configuracion.SeguridadUrlGetUsuario + oBELoginReq.Username, strRptToken);
                        if (UsuEObj.MaeTrabajador == null)
                        {
                            oBEUsuario.Password = "";
                            oBEUsuario.Mensaje = "Usuario inactivo o no registrado";
                        }
                        else
                        {
                            Filters.HelperSeguridad.GrabarSessionUsuario(UsuEObj);
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        oBEUsuario.Mensaje = "Usuario inactivo o no registrado";
                    }
                }
                catch (Exception ex)
                {
                    if (((System.Net.HttpWebResponse)((System.Net.WebException)ex).Response).StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        string msg = ex.Message;
                        oBEUsuario.Password = "";
                        oBEUsuario.Mensaje = "Usuario inactivo o no registrado";
                    }
                    else
                    {
                        string msg = ex.Message;
                        oBEUsuario.Mensaje = "Error al conectarse con el sistema";
                    }
                    StackTrace stackTrace = new StackTrace();
                    var method = stackTrace.GetFrame(0).GetMethod().Name;
                    string detalleError = method + " - " + DateTime.Now + "\n" + "\n" +
                     ex.Message + "\n" +
                     "-------------------------------------------------------------------------------------------------------------";

                        GenerarLog objLog = new GenerarLog();
                    objLog.GenerarArchivoLog(detalleError);
                }

            }

            ViewBag.Error = oBEUsuario.Mensaje;

            return View("Index");
        }
        public ActionResult CerrarSesion()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Index", "Login");

        }
    }
}