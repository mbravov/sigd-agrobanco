﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class MaestrosController : Controller
    {
        [HttpPost]
        public async Task<ActionResult> GetFechaPlazo(int plazo, DateTime fechaInicio)
        {
            string url = string.Format("Documento/GetPlazoDoc/{0}/{1}", plazo, fechaInicio.ToString("yyyy-MM-dd"));
            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            DateTime fechaPlazo = new DateTime();
            try
            {
                respuesta = await Service.GetAsync<RespuestaOperacionServicio>(url, token);

                if (respuesta.Resultado == 1)
                {
                    fechaPlazo = Helpers.ConvertToObject<DateTime>(respuesta.data);
                }

                respuesta.data = fechaPlazo;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(respuesta);
        }

        [HttpGet]
        public async Task<ActionResult> ObtenerEmpresas()
        {
            string url = "Empresa/GetEmpresa/";
            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            List<Empresa> empresas = new List<Empresa>();

            respuesta = await Service.GetAsync<RespuestaOperacionServicio>(url, token);

            if (respuesta.Resultado == 1)
            {
                empresas = Helpers.ConvertToObject<List<Empresa>>(respuesta.data);
            }

            respuesta.data = empresas;

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> ObtenerTrabajadoresPorEmpresa(string codEmpresa)
        {
            string url = string.Format("Empresa/GetTrabajadorEmp/{0}", codEmpresa);
            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            List<Representante> representantes = new List<Representante>();

            respuesta = await Service.GetAsync<RespuestaOperacionServicio>(url, token);

            if (respuesta.Resultado == 1)
            {
                representantes = Helpers.ConvertToObject<List<Representante>>(respuesta.data);
            }

            respuesta.data = representantes;

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> ObtenerAreas()
        {
            string url = "Area/Get/";
            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            List<Area> areas = new List<Area>();

            respuesta = await Service.GetAsync<RespuestaOperacionServicio>(url, token);

            if (respuesta.Resultado == 1)
            {
                areas = Helpers.ConvertToObject<List<Area>>(respuesta.data);
            }

            respuesta.data = areas;

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> ObtenerAreasSIED()
        {
            string url = "Area/GetSIED/";
            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            List<Area> areas = new List<Area>();

            respuesta = await Service.GetAsync<RespuestaOperacionServicio>(url, token);

            if (respuesta.Resultado == 1)
            {
                areas = Helpers.ConvertToObject<List<Area>>(respuesta.data);
            }

            respuesta.data = areas;

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> ObtenerAsuntos()
        {
            string url = "Asunto/GetAsunto/";
            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            List<Asunto> asuntos = new List<Asunto>();

            respuesta = await Service.GetAsync<RespuestaOperacionServicio>(url, token);

            if (respuesta.Resultado == 1)
            {
                asuntos = Helpers.ConvertToObject<List<Asunto>>(respuesta.data);
            }

            respuesta.data = asuntos;

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> ObtenerTipDocIdentidad()
        {
            string url = "Trabajador/ListarTipDocIdentidad/";
            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            List<Trabajador> trabajador = new List<Trabajador>();

            respuesta = await Service.GetAsync<RespuestaOperacionServicio>(url, token);
            if (respuesta.Resultado == 1)
            {
                trabajador = Helpers.ConvertToObject<List<Trabajador>>(respuesta.data);
            }
            respuesta.data = trabajador;
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }


    }
}