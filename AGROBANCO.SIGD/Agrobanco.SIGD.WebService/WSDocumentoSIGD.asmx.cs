﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Comun.SFTP;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.Entidades.WebService;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using Agrobanco.SIGD.WebService.ConsumoAPI;
using Agrobanco.SIGD.WebService.Entidades;
using Agrobanco.SIGD.WebService.Util;
using Newtonsoft.Json;
using Renci.SshNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using static Agrobanco.SIGD.Comun.Enumerados;
using static Agrobanco.SIGD.WebService.Util.ConstantesWS;

namespace Agrobanco.SIGD.WSDocumentoSIGD
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://royalsystems.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSDocumentoSIGD : System.Web.Services.WebService
    {

        [WebMethod]
        public string WM_RegistrarTransaccion(TramiteDocumentario tramiteDocumentario)
        {
            string webUsr = String.Empty;
            int anio = DateTime.Now.Year;
            DateTime fechaActual = DateTime.Today;
            string rpta = string.Empty;
            string validacionRegistro = string.Empty;

            try {
                    TramiteDocumentario TramiteDocumentario = new TramiteDocumentario
                    {
                        tipoEntidad = tramiteDocumentario.tipoEntidad,
                        entidad = tramiteDocumentario.entidad,
                        numeroDocumento = tramiteDocumentario.numeroDocumento,
                        fechaDocumento = tramiteDocumentario.fechaDocumento,
                        dirigidoA = tramiteDocumentario.dirigidoA,
                        referencia = tramiteDocumentario.referencia,
                        asunto = tramiteDocumentario.asunto,
                        prioridad = tramiteDocumentario.prioridad,
                        plazo = tramiteDocumentario.plazo,
                        indicacion = tramiteDocumentario.indicacion,
                        otraIndicacion = tramiteDocumentario.otraIndicacion,
                        anexos = tramiteDocumentario.anexos
                    };

                    validacionRegistro = validacionInicial(TramiteDocumentario);

                if (validacionRegistro.Length > 0)
                {
                    return validacionRegistro;
                }

                    webUsr = PeticionesAPI.GetUsuarioSIED().WebUsr; //"AGRSIED"
                    TrabajadorDTO objTrab = new TrabajadorDTO { WEBUSR = webUsr };
                    objTrab = PeticionesAPI.ObtenerTabajadorPorWEBUSR(webUsr);

                    DocumentoRequest documentoRequest = new DocumentoRequest
                    {
                        Anio = fechaActual.Year,
                        Origen = Convert.ToInt16(TipoOrigen.EXTERNO),
                        NumDocumento = TramiteDocumentario.numeroDocumento,
                        EstadoDocumento = Convert.ToInt16(EstadoDocumento.PENDIENTE),
                        FechaDocumento = Convert.ToDateTime(validarFechaDocumento(TramiteDocumentario.fechaDocumento)),
                        Asunto = TramiteDocumentario.asunto,
                        Prioridad = TramiteDocumentario.prioridad,
                        Referencia = TramiteDocumentario.referencia,
                        CodArea = objTrab.iCodArea,
                        CodTrabajador = objTrab.iCodTrabajador,
                        FechaRecepcion = fechaActual,
                        Plazo = validarPlazo(TramiteDocumentario.plazo),
                       // FechaPlazo = Convert.ToDateTime(TramiteDocumentario.Plazo),
                        Observaciones = TramiteDocumentario.otraIndicacion,
                        CodTipoDocumento = null,
                        CodRepresentante = 0,
                        UsuarioCreacion = webUsr,
                        FechaCreacion = fechaActual,
                        LoginCreacion = webUsr,
                        LstDocAdjunto = new List<DocumentoAdjunto>(),
                        LstDocAnexo = new List<DocumentoAnexo>(),
                        TipoEntidad = TramiteDocumentario.tipoEntidad,
                        Entidad = TramiteDocumentario.entidad,
                        DirigidoA = TramiteDocumentario.dirigidoA,
                        Indicacion = TramiteDocumentario.indicacion
                    };

                    Context.Response.AddHeader("Host", Context.Request.Headers["Host"]);
                    Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    validacionRegistro = validarRegistroDocumento(TramiteDocumentario);

                    if (validacionRegistro == "")
                     {
                        TramiteDocumentario.anexos?.ForEach(x => {

                            var nuevoPath = recortarPath(x.nombreFisico);
                            documentoRequest.LstDocAnexo.Add(new DocumentoAnexo {
                                AlmacenArchivoSIED = x.almacenArchivo,
                                DescripcionSIED = x.descripcion,
                                NombreFisicoSIED = x.nombreFisico,
                                FileArray = SFTP.DowloadFile(nuevoPath),
                                NombreDocumento = nombreArchivo(nuevoPath) });
                        });
                       
                        rpta = RegistrarDocumentoRecepcion(documentoRequest);

                        return rpta;
                     }
                    else
                    {
                       return validacionRegistro;
                    }
             }
             catch (Exception e)
             {
                 StackTrace stackTrace = new StackTrace();
                 var method = stackTrace.GetFrame(0).GetMethod().Name;
                 GenerarLog objLOG = new GenerarLog();
                 string detalleError = method + " - " +
                     DateTime.Now + "\n" + "\n" +
                     e.Message + "\n" +
                     "-------------------------------------------------------------------------------------------------------------";
                 objLOG.GenerarArchivoLog(detalleError);
                 return e.Message;
             }
        }

        private string recortarPath(string path)
        {
            string nuevoPath = path;
            var pathaRecortar = ConfigurationManager.AppSettings["empresasRUC"].ToString();

            if (path.Contains(pathaRecortar))
            {
                nuevoPath = path.Replace(pathaRecortar, "");
            }

            return nuevoPath;
        }

        private string validacionInicial(TramiteDocumentario tramiteDocumentario)
        {
            string mensaje = "";
            string mensajeObligatoriedad = " es obligatorio, mayor detalle revisar la documentación del servicio de Recepción.";
            string mensajeLongitud = " no respeta la longitud máxima, mayor detalle revisar la documentación del servicio de Recepción.";
            string mensajeTipoDato = " no respeta el tipo de dato, mayor detalle revisar la documentación del servicio de Recepción.";
            string mensajeFueradeIntervalo = " no respeta los valores permitidos, mayor detalle revisar la documentación del servicio de Recepción.";

            string[] tipoEntidadPermitido = { "1", "2", "3", "4" };
            string[] PrioridadPermitido = { "1", "2", "3" };
            string[] IndicacionPermitido = { "1", "2", "3", "4","5", "6", "7" };

            //TipoEntidad
            if (String.IsNullOrEmpty(tramiteDocumentario.tipoEntidad))
            {
                mensaje = "El campo TipoEntidad "+ mensajeObligatoriedad;
                return mensaje;
            }

            if (tramiteDocumentario.tipoEntidad.Length > 10)
            {
                mensaje = "El campo TipoEntidad "+ mensajeLongitud;
                return mensaje;
            }

            if(!tipoEntidadPermitido.Contains(tramiteDocumentario.tipoEntidad))
            {
                mensaje = "El campo TipoEntidad " + mensajeFueradeIntervalo;
                return mensaje;
            }

            //Entidad
            if (String.IsNullOrEmpty(tramiteDocumentario.entidad))
            {
                mensaje = "El campo Entidad " + mensajeObligatoriedad;
                return mensaje;
            }

            if (tramiteDocumentario.entidad.Length > 11)
            {
                mensaje = "El campo Entidad " + mensajeLongitud;
                return mensaje;
            }


            //NumeroDocumento
            if (String.IsNullOrEmpty(tramiteDocumentario.numeroDocumento))
            {
                mensaje = "El campo NumeroDocumento " + mensajeObligatoriedad;
                return mensaje;
            }

            if (tramiteDocumentario.numeroDocumento.Length > 50)
            {
                mensaje = "El campo NumeroDocumento " + mensajeLongitud;
                return mensaje;
            }

            //FechaDocumento
            if (String.IsNullOrEmpty(tramiteDocumentario.fechaDocumento))
            {
                mensaje = "El campo FechaDocumento " + mensajeObligatoriedad;
                return mensaje;
            }

            if (tramiteDocumentario.fechaDocumento.Length > 10)
            {
                mensaje = "El campo FechaDocumento " + mensajeLongitud;
                return mensaje;
            }

            if (validateFormatoFecha(tramiteDocumentario.fechaDocumento))
            {
                var splitDDMMYYY = tramiteDocumentario.fechaDocumento.Split('/');
                if (!isValidDate(Convert.ToInt32(remover0(splitDDMMYYY[0])), Convert.ToInt32(remover0(splitDDMMYYY[1])), Convert.ToInt32(splitDDMMYYY[2])))
                {
                    mensaje = "El campo FechaDocumento " + mensajeTipoDato;
                    return mensaje;
                }

            }
            else
            {
                mensaje = "El campo FechaDocumento " + mensajeTipoDato;
                return mensaje;
            }


            //DirigidoA
            if (tramiteDocumentario.dirigidoA.Length > 10)
            {
                mensaje = "El campo DirigidoA " + mensajeLongitud;
                return mensaje;
            }

            //Referencia
            if (tramiteDocumentario.referencia.Length > 100)
            {
                mensaje = "El campo Referencia " + mensajeLongitud;
                return mensaje;
            }

            //Asunto
            if (tramiteDocumentario.asunto.Length > 200)
            {
                mensaje = "El campo Asunto " + mensajeLongitud;
                return mensaje;
            }



            //Prioridad
            if (String.IsNullOrEmpty(tramiteDocumentario.prioridad))
            {
                mensaje = "El campo Prioridad " + mensajeObligatoriedad;
                return mensaje;
            }

            if (tramiteDocumentario.prioridad.Length > 2)
            {
                mensaje = "El campo Prioridad " + mensajeLongitud;
                return mensaje;
            }
            if (!PrioridadPermitido.Contains(tramiteDocumentario.prioridad))
            {
                mensaje = "El campo Prioridad " + mensajeFueradeIntervalo;
                return mensaje;
            }

            //Plazo
            if (String.IsNullOrEmpty(tramiteDocumentario.plazo.ToString()))
            {
                mensaje = "El campo Plazo " + mensajeObligatoriedad;
                return mensaje;
            }

            if (!IsNumeric(tramiteDocumentario.plazo))
            {
                mensaje = "El campo Plazo " + mensajeTipoDato;
                return mensaje;
            }

            //Indicacion
            if (String.IsNullOrEmpty(tramiteDocumentario.indicacion))
            {
                mensaje = "El campo Indicacion " + mensajeObligatoriedad;
                return mensaje;
            }

            if (tramiteDocumentario.indicacion.Length > 2)
            {
                mensaje = "El campo Indicacion " + mensajeLongitud;
                return mensaje;
            }

            if (!IndicacionPermitido.Contains(tramiteDocumentario.indicacion))
            {
                mensaje = "El campo Indicacion " + mensajeFueradeIntervalo;
                return mensaje;
            }

            //OtrasIndicaciones
            if (tramiteDocumentario.otraIndicacion.Length > 200)
            {
                mensaje = "El campo OtrasIndicaciones " + mensajeLongitud;
                return mensaje;
            }


            //OtrasIndicaciones
            if (tramiteDocumentario.anexos.Count == 0)
            {
                mensaje = "El campo Anexos " + mensajeObligatoriedad;
                return mensaje;
            }

            tramiteDocumentario.anexos?.ForEach(x => {
                if (x.almacenArchivo.Length > 5)
                {
                    mensaje = "El campo AlmacenArchivo " + mensajeLongitud;
                    return;
                }

                if (String.IsNullOrEmpty(x.descripcion))
                {
                    mensaje = "El campo Descripcion " + mensajeObligatoriedad;
                    return;
                }

                if (x.descripcion.Length > 200)
                {
                    mensaje = "El campo Descripcion " + mensajeLongitud;
                    return;
                }

                if (String.IsNullOrEmpty(x.nombreFisico))
                {
                    mensaje = "El campo NombreFIsico " + mensajeObligatoriedad;
                    return;
                }

                if (x.nombreFisico.Length > 200)
                {
                    mensaje = "El campo NombreFIsico " + mensajeLongitud;
                    return;
                }
            });

            return mensaje;
        }

        private int remover0(string valor)
        {
            string[] valores = { "01", "02", "03", "04", "05", "06", "07" , "08" , "09" };
            if (valores.Contains(valor))
            {
                return Convert.ToInt32(valor.Substring(1, 1));
            }
            else
            {
                return Convert.ToInt32(valor);
            }
        }

        public static Boolean EsFecha(String fecha)
        {
            try
            {
                DateTime.Parse(fecha);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool validateFormatoFecha(string valor)
        {
            try
            {
                DateTime parsed;
                bool valid = DateTime.TryParseExact(valor, "dd/mm/yyyy",
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.None, out parsed);
                return valid;
            }
            catch(Exception e)
            {
                return false;
            }
      
        }

        bool isLeap(int year)
        {

            // Return true if year is a
            // multiple of 4 and not
            // multiple of 100. OR year
            // is multiple of 400.
            return (((year % 4 == 0) &&
                     (year % 100 != 0)) ||
                     (year % 400 == 0));
        }

        bool isValidDate(int d,
                            int m,
                            int y)
        {
             int MAX_VALID_YR = 9999;
             int MIN_VALID_YR = 1800;
            // If year, month and day
            // are not in given range
            if (y > MAX_VALID_YR ||
                y < MIN_VALID_YR)
                return false;
            if (m < 1 || m > 12)
                return false;
            if (d < 1 || d > 31)
                return false;

            // Handle February month
            // with leap year
            if (m == 2)
            {
                if (isLeap(y))
                    return (d <= 29);
                else
                    return (d <= 28);
            }

            // Months of April, June,
            // Sept and Nov must have
            // number of days less than
            // or equal to 30.
            if (m == 4 || m == 6 ||
                m == 9 || m == 11)
                return (d <= 30);

            return true;
        }

        #region Metodos privados
        private int validarCodRepresentante(string entidad)
        {
            int CodRepresentante = 0;
            if (String.IsNullOrWhiteSpace(entidad)) { CodRepresentante = 0; }

            if (CodRepresentante == 0)
            {
                CodRepresentante = Convert.ToInt32(ConfigurationManager.AppSettings["config:RemitenteNoValido"]);
            }
            return CodRepresentante;
        }

        private short validarPlazo(string plazo)
        {
            return Convert.ToInt16(IsNumeric(String.IsNullOrWhiteSpace(plazo) ? "0" : plazo) == false ? "0" : (String.IsNullOrWhiteSpace(plazo) ? "0" : plazo));
        }

        private string RegistrarDocumentoRecepcion(DocumentoRequest objDoc)
        {
            try
            {
                string rpta = string.Empty;

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var jsonParameters = JsonConvert.SerializeObject(objDoc);
                    var url = string.Concat(ConfigurationManager.AppSettings["WebApiRecepcionUrl"], "registrar");
                    var content = new StringContent(jsonParameters, System.Text.Encoding.UTF8, "application/json");
                    var result = client.PostAsync(url, content).Result;

                    if (result.IsSuccessStatusCode)
                    {
                        rpta = result.Content.ReadAsStringAsync().Result;
                    }

                }
                return rpta;
            }
            catch(Exception e)
            {
                throw e;
            }
          
        }

        private bool validarAnexos(string nombre)
        {
            var esAnexo = false;
            string fileName = nombre;
            int fileExtPos = fileName.LastIndexOf(".");
            if (fileExtPos >= 0)
                fileName = fileName.Substring(0, fileExtPos);

            var carEspecial = fileName.IndexOf("_");
            if (carEspecial != -1)
            {
                for (var i = carEspecial + 1; i < fileName.Length; i++)
                {
                    if (IsNumeric(fileName[i].ToString()))
                    {
                        esAnexo = true;
                    }
                    else
                    {
                        esAnexo = false;
                        break;
                    }
                }
            }
            return esAnexo;
        }

        private bool IsNumeric(string value) => value.All(char.IsNumber);

        private string nombreArchivo(string path)
        {
            var nombreArchivo = string.Empty;
            var carEspecial = path.LastIndexOf("/");
            if (carEspecial != -1)
            {
                for (var i = carEspecial + 1; i < path.Length; i++)
                {
                    nombreArchivo = nombreArchivo + path[i];
                }
            }
            return nombreArchivo;
        }

        private string validarRegistroDocumento(TramiteDocumentario TramiteDocumentario)
        {
            var msgValdacion = string.Empty;

            if (TramiteDocumentario.anexos != null)
            {
                if (TramiteDocumentario.anexos.Count() == 0)
                {
                    msgValdacion = Convert.ToInt16(AdjuntoPrincipalSinAdjunto.codigoError).ToString();
                    return msgValdacion;
                }


                foreach(var anexo in TramiteDocumentario.anexos)
                {
                    var nuevoPath = recortarPath(anexo.nombreFisico);
                    var sizeFile = SFTP.getSizeFile(nuevoPath);
                    msgValdacion = sizeFile == 0 ? Convert.ToInt16(AdjuntoPrincipalNoExiste.codigoError).ToString() : "";
                    if (msgValdacion.Length > 0)
                    {
                        break;
                    }

                    msgValdacion = sizeFile >= 5 * 1024 * 1024 ? Convert.ToInt16(AdjuntoPrincipalPeso.codigoError).ToString() : "";

                    if (msgValdacion.Length > 0)
                    {
                        break;
                    }
                }
            }

            return msgValdacion;
        }

        private string validarFechaDocumento(string date)
        {
            DateTime d;

            bool chValidity = DateTime.TryParseExact(
               date,
               "dd/MM/yyyy",
               CultureInfo.InvariantCulture,
               DateTimeStyles.None,
               out d);
            if (!chValidity)
            {
                return DateTime.Today.ToShortDateString();
            }
            else
            {
                return date;
            }
        }

        private string validarPrioridad(string valor)
        {
            IParametroBL objServicioParam = new ParametroBL();
            var lst = objServicioParam.ListarParametrosPorGrupo(Convert.ToInt16(GrupoParametros.PRIORIDAD));
            var lstExist = lst.Where(c => c.CodParametro.ToString() == valor).ToList();
            if (lstExist.Count > 0)
            {
                return valor;
            }
            else
            {
                return Convert.ToInt16(EnumPrioridad.NORMAL).ToString();
            }
        }

        private string validarIndicacion(string valor)
        {
            IParametroBL objServicioParam = new ParametroBL();
            var lst = objServicioParam.ListarParametrosPorGrupo(Convert.ToInt16(GrupoParametros.INDICACIONES));
            var lstExist = lst.Where(c => c.CodParametro.ToString() == valor).ToList();
            if (lstExist.Count > 0)
            {
                var campo = lstExist.Where(y => y.CodParametro.ToString() == valor).ToList();
                return campo[0].CodParametro.ToString();
            }
            else
            {
                var campo = lst.Where(z => z.CodParametro == (Convert.ToInt16(Indicacion.OTROS))).ToList();
                return campo[0].CodParametro.ToString();
            }
        }
        #endregion


    }
}
