﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Agrobanco.SIGD.LogicaNegocio.ImplementationsDb2;
using Agrobanco.SIGD.Entidades.EntidadesDb2;

namespace Agrobanco.SIGD.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/usuario")]

    public class UsuarioController : ApiController
    {
        // GET: api/Usuario
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Usuario/5
        public EUsuario Get(string id)
        {
            EUsuario ObjEUsu = new EUsuario();
            ECntrlweb ObjE = new ECntrlweb();
            ObjE.webusr = id;
            try
            {
                NUsuario ObjN = new NUsuario();

                //ObjEUsu = ObjN.ObtenerUsuario(ObjE);
                ObjEUsu = ObjN.ObtenerUsuarioNegocio(ObjE);
            }
            catch (Exception ex)
            {
                return ObjEUsu;
            }

            return ObjEUsu;
        }

        // POST: api/Usuario
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Usuario/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Usuario/5
        public void Delete(int id)
        {
        }
    }
}
