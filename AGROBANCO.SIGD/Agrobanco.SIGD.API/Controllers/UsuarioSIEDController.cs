﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.Comun;
using System.Web.Http;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.API.Filter;
using System.Diagnostics;

namespace Agrobanco.SIGD.API.Controllers
{
    [Authorize]
    [ErrorFilterApi]
    [System.Web.Http.RoutePrefix("api/UsuarioSIED")]

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsuarioSIEDController : ApiController
    {


        /// <summary>
        /// Servicio API que obtiene Datos de areas
        /// </summary>
        /// <returns>Retorna la Entidad Trabajador Por ID</returns>   
        [HttpGet]
        [Route("ObtenerUsuarioSIED")]
        public RespuestaOperacionServicio ObtenerUsuarioSIED()
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IUsuarioSIED objServicio = new UsuarioSIEDBL();
                rpta.data = objServicio.ObtenerUsuarioSIED();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        [HttpPost]
        [Route("Grabar")]
        public RespuestaOperacionServicio Grabar([FromBody]UsuarioSIEDDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                obj.UsuarioCreacion = nameuser;
                obj.LoginCreacion = nameuser;
                IUsuarioSIED objServicio = new UsuarioSIEDBL();
                rpta.data = objServicio.GrabarUsuarioSIED(obj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        [HttpPost]
        [Route("Editar")]
        public RespuestaOperacionServicio Editar([FromBody]UsuarioSIEDDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                obj.UsuarioCreacion = nameuser;
                obj.LoginCreacion = nameuser;
                obj.UsuarioActualizacion = nameuser;
                obj.LoginActualizacion = nameuser;

                IUsuarioSIED objServicio = new UsuarioSIEDBL();
                rpta.data = objServicio.EditarUsuarioSIED(obj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }

    }
}