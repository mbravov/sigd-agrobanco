﻿using System;
using System.Net;
using System.Threading;
using System.Web.Http;
using Agrobanco.SIGD.LogicaNegocio.ImplementationsDb2;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.API.Security;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.API.Filter;

namespace Agrobanco.SIGD.API.Controllers
{
    [ErrorFilterApi]
    [AllowAnonymous]
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        [HttpPost]
        [Route("authenticate")]
        public IHttpActionResult Authenticate(ELoginRequest login)
        {
            if (login == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            NCntrlweb UsuNObj = new NCntrlweb();
            ECntrlweb UsuERpt = new ECntrlweb();


            UsuERpt = UsuNObj.LoginUsuario(login);

            if (!string.IsNullOrWhiteSpace(UsuERpt.webusr) && !string.IsNullOrWhiteSpace(UsuERpt.webacp))
            {
                var token = TokenGenerator.GenerateTokenJwt(login.Username, login.Password);
                return Ok(token);
            }

            // Unauthorized access 
            return Unauthorized();
        }

        [HttpPost]
        [Route("authenticatev2")]
        public IHttpActionResult Authenticatev2(ELoginRequest login)
        {
            if (login == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            
            ECntrlweb userRequest = new ECntrlweb();            
            userRequest.webusr = login.Username;
            
            NUsuario ObjN = new NUsuario();
            
            EUsuario userResponse = ObjN.ObtenerUsuarioNegocio(userRequest);
            
            if (!string.IsNullOrWhiteSpace(userResponse.MaeTrabajador.WEBUSR))
            {
                var token = TokenGenerator.GenerateTokenJwt(userResponse.MaeTrabajador.WEBUSR, userResponse.MaeTrabajador.WEBUSR);
                return Ok(token);
            }

            // Unauthorized access 
            return Unauthorized();
        }

        [HttpPost]
        [Route("authenticatedummy")]
        public IHttpActionResult AuthenticateDummy(ELoginRequest login)
        {
            if (login == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            NCntrlweb UsuNObj = new NCntrlweb();
            ECntrlweb UsuERpt = new ECntrlweb();

            if (Configuracion.IgnorarLoginDB2AS400)
                UsuERpt = UsuNObj.GetPerfilUsuarioDummy(login);
            else
                UsuERpt = UsuNObj.ConsultarUsuario(login);

            if (!string.IsNullOrWhiteSpace(UsuERpt.webusr) )
            {
                var token = TokenGenerator.GenerateTokenJwt(login.Username, login.Password);
                return Ok(token);
            }

            // Unauthorized access 
            return Unauthorized();
        }

        // GET api/<controller>/5
        public string Get()
        {
            return "";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}