﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.WebService
{
    public class TramiteDocumentario
    {
        public string tipoEntidad { get; set; }
        public string entidad { get; set; }
        public string numeroDocumento { get; set; }
        public string fechaDocumento { get; set; }
        public string dirigidoA { get; set; }
        public string referencia { get; set; }
        public string asunto { get; set; }
        public string prioridad { get; set; }
        public string plazo { get; set; }
        public string indicacion { get; set; }
        public string otraIndicacion { get; set; }
        public List<itemAnexo> anexos { get; set; }
    }
}
