﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.WebService
{
    public class itemAnexo
    {
        public string almacenArchivo { get; set; }
        public string descripcion { get; set; }
        public string nombreFisico { get; set; }
    }
}
