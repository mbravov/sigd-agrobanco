﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.EntidadesDb2
{
    public class ESegModulo
    {
        public string vCodModulo { get; set; }
        public string vCodSistema { get; set; }
        public string vNombre { get; set; }
        public string vDescripcion { get; set; }
        public int? iNivel { get; set; }
        public string vCodModPadre { get; set; }
        public string vURL { get; set; }
        public string vEstiloIcono { get; set; }
        
    }
}
