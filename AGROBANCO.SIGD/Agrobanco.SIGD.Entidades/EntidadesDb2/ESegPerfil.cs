﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.EntidadesDb2
{
    public class ESegPerfil
    {
        public string vCodPerfil { get; set; }
        public string vCodSistema { get; set; }
        public string vNombre { get; set; }
        public string vDescripcion { get; set; }
    }
}
