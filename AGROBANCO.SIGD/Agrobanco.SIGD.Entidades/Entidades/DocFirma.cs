﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class DocFirma
    {
        public int CodDocFirma { get; set; }
        public int CodDocumento { get;set; }
        public int CodAdjunto { get; set; }
        public int CodLaserFiche { get; set; }
        public int? CodDerivacion { get; set; }
        public int CodTrabajador { get; set; }
        public int CodArea { get; set; }
        public int posx { get; set; }
        public int posy { get; set; }
        public DateTime FechaFirma { get; set; }
        public String Comentario { get; set; }

        #region Propiedades Auditoria
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }

        #endregion
    }
}
