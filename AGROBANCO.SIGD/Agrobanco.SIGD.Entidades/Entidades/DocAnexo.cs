﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class DocAnexo
    {

        #region Campos Tabla

        public int CodDocAnexo { get; set; }
        public int CodLaserfiche { get; set; }
        public int CodDocumentoMovimiento { get; set; }
        public int CodDocumento { get; set; }
        public string FileArray { get; set; }
        public string Path { get; set; }
        public short Estado { get; set; }

        //Otros campos
        public string NombreDocumento { get; set; }
        public int secuencia { get; set; }
        public int TipoArchivo { get; set; }
        //Propiedades de Auditoría
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }

        public string AlmacenArchivoSIED { get; set; }
        public string DescripcionSIED { get; set; }
        public string NombreFisicoSIED { get; set; }

        #endregion
    }
}
