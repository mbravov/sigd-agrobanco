﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
    public class ResultadoBusquedaDTO<T>
    {
        public List<T> Lista { get; set; }
        public int TotalRegistros { get; set; }
        public int PaginaActual { get; set; }
        public string Error { get; set; }
        public int Resultado { get; set; }
    }
}
