﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
   public class RepresentanteDTO
    {
        public int CodRepresentante { get; set; }
        public string Nombre { get; set; }
        public short Estado { get; set; }
    }
}
