﻿using System;
using System.Collections.Generic;

namespace Agrobanco.SIGD.Entidades
{
    public class DocumentoRequest
    {

        public int Anio { get; set; }
        public short Origen { get; set; }
        public string NumDocumento { get; set; }
        public short EstadoDocumento { get; set; }
        public DateTime FechaDocumento { get; set; }
        public string Asunto { get; set; }
        public string Prioridad { get; set; }
        public string Referencia { get; set; }
        public int CodArea { get; set; }
        public int CodTrabajador { get; set; }
        public DateTime FechaRecepcion { get; set; }
        public short Plazo { get; set; }
        public DateTime FechaPlazo { get; set; }
        public string Observaciones { get; set; }
        public short? CodTipoDocumento { get; set; }
        public int CodRepresentante { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public List<DocumentoAdjunto> LstDocAdjunto { get; set; }
        public List<DocumentoAnexo> LstDocAnexo { get; set; }
        public string TipoEntidad { get; set; }
        public string Entidad { get; set; }
        public string DirigidoA { get; set; }
        public string Indicacion { get; set; }
    }

    public class DocumentoAdjunto
    {
        public string FileArray { get; set; }
    }
    public class DocumentoAnexo
    {
        public string FileArray { get; set; }
        public string NombreDocumento { get; set; }
        public string AlmacenArchivoSIED { get; set; }
        public string DescripcionSIED { get; set; }
        public string NombreFisicoSIED { get; set; }
    }
}