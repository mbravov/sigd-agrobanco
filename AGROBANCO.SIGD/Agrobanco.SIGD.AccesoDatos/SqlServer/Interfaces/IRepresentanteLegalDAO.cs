﻿using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
    public interface IRepresentanteLegalDAO
    {
        RepresentanteLegalDTO ObtenerRepresentanteLegal();
        int GrabarRepresentanteLegal(RepresentanteLegalDTO obj);
        int EditarRepresentanteLegal(RepresentanteLegalDTO obj);
    }
}
