﻿using Agrobanco.SIGD.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
    public interface IAreaDAO
    {
        /// <summary>
        /// Interface para el Listado de Áreas
        /// </summary>
        /// <returns>Retorna Lista genérica de Áreas</returns>     
        List<AreaDTO> ListarArea();
        List<AreaDTO> ListarAreaSIED();
        List<AreaDTO> ListarAreasPaginado(AreaDTO objParams, out int totalRegistros);

        AreaDTO ObtenerAreasPorId(AreaDTO objParams);

        int RegistrarArea(AreaDTO objArea);

        int ActualizarArea(AreaDTO objArea);

        int EliminarArea(AreaDTO objArea);
        
    }
}
