﻿using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
    public interface IDocumentoDAO
    {
        /// <summary>
        /// Interface para Obtener Documentos 
        /// </summary>
        /// <returns>Retorna una Entidad Documento</returns>  
        Documento GetDocumento(Documento EObj);
        /// <summary>
        /// Interface para Obtener Documentos 
        /// </summary>
        /// <returns>Retorna una Entidad Documento</returns>  
        Documento GetDocumentoRecepcionSIED(Documento EObj);
        /// <summary>
        /// Interface para Obtener Documentos 
        /// </summary>
        /// <returns>Retorna una Entidad Documento</returns>  
        Documento GetDocumentoEnvioSIED(Documento EObj);
        /// <summary>
        /// Interface para Obtener Documentos 
        /// </summary>
        /// <returns>Retorna una Entidad Documento</returns>  
        EnvioSIED ObtenerDocumentoSIED_SeviceEnvio(Documento EObj);
        /// <summary>
        /// Interface para Obtener Documentos  Derivador x trabajador
        /// </summary>
        /// <returns>Retorna una Entidad Documento</returns>  
        DocDerivacion GetDocumentoDerivadoxTrabajador(DocDerivacion EObj);
        /// <summary>
        /// Interface para Obtener Documentos  Derivador x ID
        /// </summary>
        /// <returns>Retorna una Entidad Documento</returns>  
        DocDerivacion GetDocumentoDerivadoPorId(int CodDocumentoDerivacion);
        /// <summary>
        /// Interface para Obtener Documentos 
        /// </summary>
        /// <returns>Retorna una Entidad Documento Anexos</returns>  
        List<DocAnexo> GetDocumentoAnexos(DocAnexo EObj);
        /// <summary>
        /// Interface para Obtener Documentos 
        /// </summary>
        /// <returns>Retorna una Entidad Documento Anexos</returns>  
        Documento GetDetalleCorreoDerivacion(int EObj);
        /// <summary>
        /// Interface para Obtener Documentos 
        /// </summary>
        /// <returns>Retorna una Entidad Documento Anexos</returns>  
        Documento GetDetalleCorreoFinalizar(int EObj);
        Documento GetDetalleCorreoAnularDocumentoEnvioSIED(int id);
        /// <summary>
        /// Interface para Obtener Documentos Adjuntos
        /// </summary>
        /// <returns>Retorna una lista de Documentos Adjuntos asociados a un codigo Documento</returns>  
        List<DocAdjunto> GetDocumentosAdjuntos(DocAdjunto objParams);
        /// <summary>
        /// Método Público para obtener el seguimiento del documento
        /// </summary>
        /// <returns>Retorna Lista genérica del Seguimiento del Documento</returns>  
        List<DocumentoSeguimientoDTO> GetSeguimientoDocumento(int iCodDocumento);
        /// <summary>
        /// Método Público para obtener el seguimiento del documento
        /// </summary>
        /// <returns>Retorna Lista genérica del Seguimiento del Documento</returns>  
        List<DocumentoSeguimientoDTO> GetCabeceraSeguimiento(int iCodDocumento);
        /// <summary>
        /// Método Público para obtener el seguimiento del documento
        /// </summary>
        /// <returns>Retorna Lista genérica del Seguimiento del Documento</returns>  
        List<DocumentoSeguimientoDTO> GetCabeceraSeguimientoInterno(int iCodDocumento);
        /// <summary>
        /// Método Público para obtener el seguimiento del documento
        /// </summary>
        /// <returns>Retorna Lista genérica del Seguimiento del Documento</returns>  
        List<DocumentoSeguimientoDTO> GetDetalleSeguimiento(int iCodDocumento);
        /// <summary>
        /// Método Público para obtener los comentarios del documento
        /// </summary>
        /// <returns>Retorna Lista genérica de Comentarios de Documento</returns>  
        List<ComentarioDocumentoDTO> GetComentariosDocumento(int iCodDocumento);
        /// <summary>
        /// Interface para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        List<DocumentoBandejaDTO> ListarBandejaMesaPartes(DocumentoBandejaParamsDTO objParams);

        List<TipoDocumentoDTO> ListarTiposDocumentos(TipoDocumentoDTO objParams, out int totalRegistros);

        List<String> ListarEmailPorAreaJefe(string codigos);

        List<String> ListarEmailPorTrabajador(string codigos);
        List<String> ListarEmailResponsablePorCodigoDocumento(string codigos);

        List<DocumentoBandejaDTO> ListarBandejaEntrada(DocumentoBandejaEntradaDTO objParams, out int totalRegistros);
        List<DocumentoBandejaDTO> ListarBandejaRecepcionSIEDFONAFE(DocumentoBandejaEntradaDTO objParams, out int totalRegistros);
        List<DocumentoBandejaDTO> ListarBandejaEnvioSied(DocumentoBandejaEntradaDTO objParams, out int totalRegistros);

        TipoDocumentoDTO ObtenerTipoDocumento(TipoDocumentoDTO objParams);

        TipoDocumentoDTO ObtenerTipoDocumentoPorDescripcion(TipoDocumentoDTO objParams);

        int EliminarTipoDocumento(TipoDocumentoDTO objDocumento);
        /// <summary>
        /// Interface para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>     

        int RegistrarDocumento(Documento objMovDocumento, out string outCorrelativoDoc); 
        int RegistrarDocumentoRecepcionSIED(Documento objMovDocumento, out string outCorrelativoDoc);
        int RegistrarDocumentoBandejaEntrada(Documento objMovDocumento, out string outCorrelativoDoc);
        int RegistrarDocumentoBandejaEnvioSIED(Documento objMovDocumento, out string outCorrelativoDoc);
        int RegistrarTipoDocumento(TipoDocumentoDTO objMovDocumento);

        int ActualizarTipoDocumento(TipoDocumentoDTO objMovDocumento);
        /// <summary>
        /// Interface para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>     

        /// <summary>
        /// Interface para el Anular Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Anular Documento</returns>     
        int AnularDocumento(ComentarioDocumentoDTO objDocumento);
        int AnularDocumentoEnvioSIED(ComentarioDocumentoDTO objDocumento);
        int AnularDocumentoRecepcionSIED(ComentarioDocumentoDTO objDocumento);
        /// <summary>
        /// Interface para el Finalizar Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Finalizar Documento</returns>     
        int FinalizarDocumento(ComentarioDocumentoDTO objDocumento, out string mensaje);
        int FinalizarDocumentoBandejaEntrada(ComentarioDocumentoDTO objDocumento, out string mensaje);
        /// <summary>
        /// Interface para devolver un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de devolucion de Documento</returns>     
        int DevolverDocumento(ComentarioDocumentoDTO objDocumento);
        /// <summary>
        /// Interface para devolver un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de devolucion de Documento</returns>     
        int DevolverDocumentoInterno(ComentarioDocumentoDTO objDocumento);
        /// <summary>
        /// Interface para derivar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de derivacion de Documento</returns>     

        int RegistrarDerivarDocumento(DerivarDocumentoDTO objDocumento);

        int RegistrarAvance(RegistrarAvanceDTO registrarAvanceDTO);

        /// <summary>
        /// Interface para la actualización de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Actualización del Documento</returns>     
        int ActualizarDocumento(Documento objMovDocumento);
        int ActualizarDocumentoInterno(Documento objMovDocumento);
        int ActualizarDocumentoInternoEnvioSIED(Documento objMovDocumento);
        /// <summary>
        /// Interface para Calcular la fecha de plazo del documento
        /// </summary>
        /// <returns>Retorna el Indicador de Fecha de Plazo del Documento</returns>     
        DateTime CalcularFechaPlazoDocumento(int cantidadPlazo, DateTime fechaInicio);

        /// <summary>
        /// Interface para la validar el finalziar
        /// </summary>
        /// <returns>Retorna el mensaje de validacion</returns>     
        int ValidarFinalizarDocumento(ComentarioDocumentoDTO finalizar, out string mensaje, out string codDerivados);

        /// <summary>
        /// Interface para la validar el finalziar
        /// </summary>
        /// <returns>Retorna el mensaje de validacion</returns>     
        int ValidarDevolverDocumento(ComentarioDocumentoDTO finalizar, out string mensaje, out string codDerivados);

        DerivarDocumentoDTO GetEstadoDocumentoMovimiento(int CodDocumento, int operador);

        /// <summary>
        /// Interface para Obtener Documentos 
        /// </summary>
        /// <returns>Retorna una Entidad Documento</returns>  
        Documento GetDetalleCorreoAvance(int EObj);


        int GetFlagSIED(int codDocumento);

        /// <summary>
        /// Método Público para Subir Documento digital
        /// </summary>
        /// <returns>Retorna el Indicador</returns> 
        int SubirDocumentoLaserFicher(Documento objDocumento,out Documento obj);


        /// <summary>
        /// Interface para el Anular Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Anular Documento</returns>     
        int ValidarFirmante(int codDocumento, int codTrabajador);


        /// <summary>
        /// Interface para Actualizar el estado de un Documento en Firma
        /// </summary>
        /// <returns>Retorna el Indicador de Anular Documento</returns>     
        int DocumentoAtencionFirma(int codDocumento, int atencion);

        /// <summary>
        /// Interface que Obtiene el estado de firma de documento
        /// </summary>
        /// <returns>Retorna el Indicador de Anular Documento</returns>     
        int ObtenerEstadoFirmaDocumento(int codDocumento);

        /// <summary>
        /// Interface que Obtiene la cantidad de firmas digitales pro documento
        /// </summary>
        /// <returns>Retorna la cantidad de firmas</returns>     
        int ObtenerCantidadFirmas(int codDocumento, out int posx, out int posy);

        /// <summary>
        /// Interface que Obtiene el codigo laserfiche por codigo de documento
        /// </summary>
        /// <returns>Retorna id laserfiche</returns> 
        int ObtenerCodLaserFIchePorCodDocumento(int id);
        List<int> ObtenerCodLaserFIcheAnexosPorCodDocumento(int id);
        /// <summary>
        /// Interface que Obtiene La lista de Documentos Derivados por Codigo de documento
        /// </summary>
        /// <returns>Retorna id laserfiche</returns> 
        List<DocDerivacion> GetDocumentosDerivadosPorCodDocumento(int CodDocumento, int TipoAccion);

        int FinalizarDocumentoEnvioSIED(Documento objDocumento);

        int RegistrarCargo(RegistroCargoDTO cargo);

        int RegistrarCargoSIED(RegistroCargoDTO cargo);

        int registrarcargoRecepcionSIED(RegistroCargoDTO cargo);

        int ObtenerTotalDerivaciones(int codDocumento, int tipoAcceso);

        int InsertarMovComentario(DocComentario objDocComentario, SqlCommand cmd);

        int EstablecerDocumentoPrincipalSIED(int EObj);
    }
}
