﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
   public  interface IReportesDAO
    {

     List<ReportesDTO.ReporteMesaParteDTO> ReporteMesaPartesDAO(ReportesDTO.ReporteMesaParteDTO objParametros, out int totalRegistros);

     List<ReportesDTO.ReporteMesaParteDTO> ReporteAreaFechaEstadoDAO(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros);

     List<ReportesDTO.ReporteMesaParteDTO> ReporteSeguimientoDocumentosDAO(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros);

     List<ReportesDTO.ReporteMesaParteDTO> ReporteListarAñoDAO();
        
    List<ReportesDTO.ReporteAtencionDocumentoDTO> ReporteAtencionDocumento(ReportesDTO.ReporteAtencionDocumentoDTO objParametros, out int totalRegistros);
    }
}
