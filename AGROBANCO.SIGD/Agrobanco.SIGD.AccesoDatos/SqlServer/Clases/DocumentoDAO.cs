﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Agrobanco.SIGD.Comun;
using System.IO;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.AccesoDatos.ImplementationsDb2;
using System.Configuration;
using static Agrobanco.SIGD.Comun.Enumerados;
using Agrobanco.SIGD.Presentacion.Utilities;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class DocumentoDAO : IDocumentoDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public DocumentoDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        #endregion

        #region Métodos Públicos


        /// <summary>
        /// Método Público para el Obtener el correlativo inicial de los anexos tipo: Adjuntos
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public int GetCorrelativoAnexosInicial(int CodDocumento, TipoArchivos tipoArchivo)
        {
            int secuencia = 1;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerCorrelativoAnxInicial]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = CodDocumento;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter("@iTipoArchivo", SqlDbType.Int);
                    param.Value = Convert.ToInt32(tipoArchivo);
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.Direction = ParameterDirection.Output;
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@onFlagOK";
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                if (!Convert.IsDBNull(reader["iSecuencia"])) secuencia = Convert.ToInt32(reader["iSecuencia"]);
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return secuencia;
        }


        /// <summary>
        /// Método Público para el Obtener el correlativo por ID laserfiche
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public string GetCorrelativoPorIdLaserfiche(string id)
        {
            string nRpta = "0";
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_ObtenerCorrelativoPorIdLaserfiche]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@IdLaserfiche", SqlDbType.Int);
                        param.Value = Convert.ToInt32(id);
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.VarChar;
                        param.Size = 50;
                        param.ParameterName = "@vCorrelativo";
                        cmd.Parameters.Add(param);


                        cmd.ExecuteNonQuery();

                        nRpta =(cmd.Parameters["@vCorrelativo"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = "-1";
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener ID Laserfiche");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }

            return nRpta;
        }

        
        /// <summary>
        /// Método Público para el Obtener el Cod dOcumento por ID laserfiche
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public int GetCodDocumentoPorCodLaserFIche(int id)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_ObtenerCodDocumentoPorCodLaserFIche]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@IdLaserfiche", SqlDbType.Int);
                        param.Value = id;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@CodDocumento";
                        cmd.Parameters.Add(param);


                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32((cmd.Parameters["@CodDocumento"].Value));
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener Cod Documento");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }

            return nRpta;
        }


        /// <summary>
        /// Método Público para el Obtener el Cod dOcumento por ID laserfiche
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public int ObtenerCodLaserFIchePorCodDocumento(int id)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_ObtenerCodLaserFichePorCodDocumento]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@CodDocumento", SqlDbType.Int);
                        param.Value = id;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@IdLaserfiche"; 
                        cmd.Parameters.Add(param);


                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32((cmd.Parameters["@IdLaserfiche"].Value));
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener IdLaserfiche");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }

            return nRpta;
        }


        /// <summary>
        /// Método Público para el Obtener el Cod dOcumento por ID laserfiche
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public List<int> ObtenerCodLaserFIcheAnexosPorCodDocumento(int id)
        {
            List<int> list = new List<int>();
            int codlaserfiche = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_ObtenerCodLaserFIcheAnexosPorCodDocumento]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@CodDocumento", SqlDbType.Int);
                        param.Value = id;
                        cmd.Parameters.Add(param);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            try
                            {
                                while (reader.Read())
                                {
                                    
                                    if (!Convert.IsDBNull(reader["iCodLaserfiche"])) codlaserfiche = Convert.ToInt32(reader["iCodLaserfiche"]);
                                    list.Add(codlaserfiche);
                                }

                            }
                            catch (Exception ex)
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                //Generando Archivo Log
                                new LogWriter(ex.Message);
                                throw new Exception("Error al Obtener datos del trabajador");
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener IdLaserfiche");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }

            return list;
        }

        /// <summary>
        /// Método Público para actualizar el codigo Laserfiche para adjuntos
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public int ActualizarCodigoLaserFicheAdjunto(int codLaserFicheOld, int codLaserFicheNew,Documento objDoc, SqlCommand cmd)
        {
            int nRpta = 0;

          cmd.CommandText = "[dbo].[UP_MOV_ActualizarCodLaserficheAdjuntos]";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.Clear();

          SqlParameter param = new SqlParameter("@codLaserFicheNew", SqlDbType.Int);
          param.Value = codLaserFicheNew;
          cmd.Parameters.Add(param);

          param = new SqlParameter();
          param.Direction = ParameterDirection.Input;
          param.SqlDbType = SqlDbType.Int;
          param.Value = codLaserFicheOld;
          param.ParameterName = "@codLaserFicheOld";
          cmd.Parameters.Add(param);

          param = new SqlParameter();
          param.Direction = ParameterDirection.Input;
          param.SqlDbType = SqlDbType.VarChar;
          param.Value = objDoc.UsuarioActualizacion;
          param.ParameterName = "@vUsuActualizacion";
          cmd.Parameters.Add(param);

          param = new SqlParameter();
          param.Direction = ParameterDirection.Input;
          param.SqlDbType = SqlDbType.VarChar;
          param.Value = objDoc.LoginActualizacion;
          param.ParameterName = "@vLgnActualizacion";
          cmd.Parameters.Add(param);

          param = new SqlParameter();
          param.Direction = ParameterDirection.Input;
          param.SqlDbType = SqlDbType.VarChar;
          param.Value = objDoc.RolActualizacion;
          param.ParameterName = "@vRolActualizacion";
          cmd.Parameters.Add(param);

          param = new SqlParameter();
          param.Direction = ParameterDirection.Output;
          param.SqlDbType = SqlDbType.Int;
          param.ParameterName = "@onFlagOK";
          cmd.Parameters.Add(param);


          cmd.ExecuteNonQuery();

          nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return nRpta;
        }

        /// <summary>
        /// Método Público para el Obtener el último movimiento de un documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public int GetUltimoMovimiento(int CodDocumento)
        {
            int iCodDocMovimiento = 1;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerUltimoMovimiento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = CodDocumento;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.Direction = ParameterDirection.Output;
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@onFlagOK";
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                if (!Convert.IsDBNull(reader["iCodDocMovimiento"])) iCodDocMovimiento = Convert.ToInt32(reader["iCodDocMovimiento"]);
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return iCodDocMovimiento;
        }

        /// <summary>
        /// Método Público para el Obtener Datos de un Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public Documento GetDocumento(Documento EObj)
        {
            Documento objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = EObj.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new Documento();
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodSecuencial"])) objEntidad.Secuencial = Convert.ToInt32(reader["iCodSecuencial"]);
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.Correlativo = Convert.ToString(reader["vCorrelativo"]);
                                if (!Convert.IsDBNull(reader["iAnio"])) objEntidad.Anio = Convert.ToInt32(reader["iAnio"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = short.Parse(reader["siOrigen"].ToString());
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.NumDocumento = Convert.ToString(reader["vNumDocumento"]);
                                if (!Convert.IsDBNull(reader["siEstDocumento"])) objEntidad.EstadoDocumento = short.Parse(reader["siEstDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["dFecDocumento"])) objEntidad.FechaDocumento = DateTime.Parse(reader["dFecDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["vFecDocumento"])) objEntidad.strFechaDocumento = Convert.ToString(reader["vFecDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["dFecRegistro"])) objEntidad.FechaRegistro = DateTime.Parse(reader["dFecRegistro"].ToString());
                                if (!Convert.IsDBNull(reader["vAsunto"])) objEntidad.Asunto = Convert.ToString(reader["vAsunto"]);
                                if (!Convert.IsDBNull(reader["siPrioridad"])) objEntidad.Prioridad = short.Parse(reader["siPrioridad"].ToString());
                                if (!Convert.IsDBNull(reader["vReferencia"])) objEntidad.Referencia = Convert.ToString(reader["vReferencia"]);
                                if (!Convert.IsDBNull(reader["dFecRecepcion"])) objEntidad.FechaRecepcion = DateTime.Parse(reader["dFecRecepcion"].ToString());
                                if (!Convert.IsDBNull(reader["siPlazo"])) objEntidad.Plazo = short.Parse(reader["siPlazo"].ToString());
                                if (!Convert.IsDBNull(reader["dFecPlazo"])) objEntidad.FechaPlazo = DateTime.Parse(reader["dFecPlazo"].ToString());
                                if (!Convert.IsDBNull(reader["vObservaciones"])) objEntidad.Observaciones = Convert.ToString(reader["vObservaciones"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocumento"])) objEntidad.CodTipoDocumento = short.Parse(reader["iCodTipDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["vTipoDocumento"])) objEntidad.TipoDocumento = Convert.ToString(reader["vTipoDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodRepresentante"])) objEntidad.CodRepresentante = Convert.ToInt32(reader["iCodRepresentante"]);
                                if (!Convert.IsDBNull(reader["vRepresentante"])) objEntidad.Representante = Convert.ToString(reader["vRepresentante"]);
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["vArea"])) objEntidad.Area = Convert.ToString(reader["vArea"]);
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.CodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNombreTrab"])) objEntidad.NombreTrab = Convert.ToString(reader["vNombreTrab"]);
                                if (!Convert.IsDBNull(reader["vApePaternoTrab"])) objEntidad.ApePaternoTrab = Convert.ToString(reader["vApePaternoTrab"]);
                                if (!Convert.IsDBNull(reader["vApeMaternoTrab"])) objEntidad.ApeMaternoTrab = Convert.ToString(reader["vApeMaternoTrab"]);
                                if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.CodEmpresa = short.Parse(reader["iCodEmpresa"].ToString());
                                if (!Convert.IsDBNull(reader["vDescripcionEmpresa"])) objEntidad.DescripcionEmpresa = Convert.ToString(reader["vDescripcionEmpresa"]);
                                if (!Convert.IsDBNull(reader["vUsuarioRegistro"])) objEntidad.UsuarioRegistro = Convert.ToString(reader["vUsuarioRegistro"]);
                                if (!Convert.IsDBNull(reader["CodResponsable"])) objEntidad.CodResponsalbe = Convert.ToInt32(reader["CodResponsable"]);
                                if (!Convert.IsDBNull(reader["siFirma"])) objEntidad.siFirma = Convert.ToInt32(reader["siFirma"]);
                                if (!Convert.IsDBNull(reader["iCodDocAdjunto"])) objEntidad.CodDocAdjunto = Convert.ToInt32(reader["iCodDocAdjunto"]);
                                if (!Convert.IsDBNull(reader["vDocumentoFirmado"])) objEntidad.DocumentoFirmado = Convert.ToInt32(reader["vDocumentoFirmado"]);
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                if (!Convert.IsDBNull(reader["iCodUltimoMovimiento"])) objEntidad.CodUltimoMovimiento = Convert.ToInt32(reader["iCodUltimoMovimiento"]);


                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }


        /// <summary>
        /// Método Público para el Obtener Datos de un Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public int GetOrigen(Documento EObj)
        {
            int origen = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerOrigen]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = EObj.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                if (!Convert.IsDBNull(reader["siOrigen"])) origen = Convert.ToInt32(reader["siOrigen"]);
                                  break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el origen del documento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return origen;
        }

        public int GetFlagSIED(int codDocumento)
        {
            int siFlagSIED = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerFlagSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = codDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                if (!Convert.IsDBNull(reader["siFlagSIED"])) siFlagSIED = Convert.ToInt32(reader["siFlagSIED"]);
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el origen del documento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return siFlagSIED;
        }

        /// <summary>
        /// Método Público para el Obtener Datos de un Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public Documento GetDocumentoRecepcionSIED(Documento EObj)
        {
            Documento objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentoRecepcionSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = EObj.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new Documento();
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodSecuencial"])) objEntidad.Secuencial = Convert.ToInt32(reader["iCodSecuencial"]);
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.Correlativo = Convert.ToString(reader["vCorrelativo"]);
                                if (!Convert.IsDBNull(reader["iAnio"])) objEntidad.Anio = Convert.ToInt32(reader["iAnio"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = short.Parse(reader["siOrigen"].ToString());
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.NumDocumento = Convert.ToString(reader["vNumDocumento"]);
                                if (!Convert.IsDBNull(reader["siEstDocumento"])) objEntidad.EstadoDocumento = short.Parse(reader["siEstDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["dFecDocumento"])) objEntidad.FechaDocumento = DateTime.Parse(reader["dFecDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["vFecDocumento"])) objEntidad.strFechaDocumento = Convert.ToString(reader["vFecDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["dFecRegistro"])) objEntidad.FechaRegistro = DateTime.Parse(reader["dFecRegistro"].ToString());
                                if (!Convert.IsDBNull(reader["vAsunto"])) objEntidad.Asunto = Convert.ToString(reader["vAsunto"]);
                                if (!Convert.IsDBNull(reader["siPrioridadSIED"])) objEntidad.PrioridadSIED = reader["siPrioridadSIED"].ToString();
                                if (!Convert.IsDBNull(reader["vReferencia"])) objEntidad.Referencia = Convert.ToString(reader["vReferencia"]);
                                if (!Convert.IsDBNull(reader["dFecRecepcion"])) objEntidad.FechaRecepcion = DateTime.Parse(reader["dFecRecepcion"].ToString());
                                if (!Convert.IsDBNull(reader["siPlazo"])) objEntidad.Plazo = short.Parse(reader["siPlazo"].ToString());
                                if (!Convert.IsDBNull(reader["dFecPlazo"])) objEntidad.FechaPlazo = DateTime.Parse(reader["dFecPlazo"].ToString());
                                if (!Convert.IsDBNull(reader["vObservaciones"])) objEntidad.Observaciones = Convert.ToString(reader["vObservaciones"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocumento"])) objEntidad.CodTipoDocumento = short.Parse(reader["iCodTipDocumento"].ToString());
                                //if (!Convert.IsDBNull(reader["vTipoDocumento"])) objEntidad.TipoDocumento = Convert.ToString(reader["vTipoDocumento"]);
                                if (!Convert.IsDBNull(reader["vTipoEntidadSIED"])) objEntidad.TipoEntidadSIED = Convert.ToString(reader["vTipoEntidadSIED"]);
                                if (!Convert.IsDBNull(reader["vEntidadSIED"])) objEntidad.EntidadSIED = Convert.ToString(reader["vEntidadSIED"]);
                                if (!Convert.IsDBNull(reader["iCodRepresentante"])) objEntidad.CodRepresentante = Convert.ToInt32(reader["iCodRepresentante"]);
                                //if (!Convert.IsDBNull(reader["vRepresentante"])) objEntidad.Representante = Convert.ToString(reader["vRepresentante"]);
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["vArea"])) objEntidad.Area = Convert.ToString(reader["vArea"]);
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.CodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNombreTrab"])) objEntidad.NombreTrab = Convert.ToString(reader["vNombreTrab"]);
                                if (!Convert.IsDBNull(reader["vApePaternoTrab"])) objEntidad.ApePaternoTrab = Convert.ToString(reader["vApePaternoTrab"]);
                                if (!Convert.IsDBNull(reader["vApeMaternoTrab"])) objEntidad.ApeMaternoTrab = Convert.ToString(reader["vApeMaternoTrab"]);
                                //if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.CodEmpresa = short.Parse(reader["iCodEmpresa"].ToString());
                                //if (!Convert.IsDBNull(reader["vDescripcionEmpresa"])) objEntidad.DescripcionEmpresa = Convert.ToString(reader["vDescripcionEmpresa"]);
                                if (!Convert.IsDBNull(reader["vUsuarioRegistro"])) objEntidad.UsuarioRegistro = Convert.ToString(reader["vUsuarioRegistro"]);
                                if (!Convert.IsDBNull(reader["CodResponsable"])) objEntidad.CodResponsalbe = Convert.ToInt32(reader["CodResponsable"]);
                                if (!Convert.IsDBNull(reader["siFirma"])) objEntidad.siFirma = Convert.ToInt32(reader["siFirma"]);
                                if (!Convert.IsDBNull(reader["iCodDocAdjunto"])) objEntidad.CodDocAdjunto = Convert.ToInt32(reader["iCodDocAdjunto"]);
                                if (!Convert.IsDBNull(reader["vDocumentoFirmado"])) objEntidad.DocumentoFirmado = Convert.ToInt32(reader["vDocumentoFirmado"]);
                                if (!Convert.IsDBNull(reader["comentarioAnulacion"])) objEntidad.Comentario = Convert.ToString(reader["comentarioAnulacion"]);
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                if (!Convert.IsDBNull(reader["iCodUltimoMovimiento"])) objEntidad.CodUltimoMovimiento = Convert.ToInt32(reader["iCodUltimoMovimiento"]);

                                if (!Convert.IsDBNull(reader["iCodRepresentante"])) objEntidad.CodRepresentante = Convert.ToInt32(reader["iCodRepresentante"]);
                                if (!Convert.IsDBNull(reader["vDirigidoASIED"])) objEntidad.DirigidoASIED = Convert.ToString(reader["vDirigidoASIED"]);
                                if (!Convert.IsDBNull(reader["vReferencia"])) objEntidad.Referencia = Convert.ToString(reader["vReferencia"]);
                                if (!Convert.IsDBNull(reader["vIndicacionSIED"])) objEntidad.IndicacionSIED = Convert.ToString(reader["vIndicacionSIED"]);

                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }

        public int EstablecerDocumentoPrincipalSIED(int codDocumento)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_EstablecerDocumentoPrincipalSIED]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodLaserfiche", SqlDbType.Int);
                        param.Value = codDocumento;
                        cmd.Parameters.Add(param);


                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);


                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al anular el documento");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para el Obtener Datos de un Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public Documento GetDocumentoEnvioSIED(Documento EObj)
        {
            Documento objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentEnvioSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = EObj.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new Documento();
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodSecuencial"])) objEntidad.Secuencial = Convert.ToInt32(reader["iCodSecuencial"]);
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.Correlativo = Convert.ToString(reader["vCorrelativo"]);
                                if (!Convert.IsDBNull(reader["iAnio"])) objEntidad.Anio = Convert.ToInt32(reader["iAnio"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = short.Parse(reader["siOrigen"].ToString());
                                 if (!Convert.IsDBNull(reader["siEstDocumento"])) objEntidad.EstadoDocumento = short.Parse(reader["siEstDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["dFecDocumento"])) objEntidad.FechaDocumento = DateTime.Parse(reader["dFecDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["vFecDocumento"])) objEntidad.strFechaDocumento = Convert.ToString(reader["vFecDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["dFecRegistro"])) objEntidad.FechaRegistro = DateTime.Parse(reader["dFecRegistro"].ToString());
                                if (!Convert.IsDBNull(reader["vAsunto"])) objEntidad.Asunto = Convert.ToString(reader["vAsunto"]);
                                if (!Convert.IsDBNull(reader["iCodTipAsuntoSIED"])) objEntidad.CodAsuntoSIED = Convert.ToString(reader["iCodTipAsuntoSIED"]);
                                if (!Convert.IsDBNull(reader["siPrioridad"])) objEntidad.Prioridad = short.Parse(reader["siPrioridad"].ToString());
                                if (!Convert.IsDBNull(reader["vReferencia"])) objEntidad.Referencia = Convert.ToString(reader["vReferencia"]);
                                if (!Convert.IsDBNull(reader["dFecRecepcion"])) objEntidad.FechaRecepcionSIED = DateTime.Parse(reader["dFecRecepcion"].ToString());
                                if (!Convert.IsDBNull(reader["siPlazo"])) objEntidad.Plazo = short.Parse(reader["siPlazo"].ToString());
                                if (!Convert.IsDBNull(reader["dFecPlazo"])) objEntidad.FechaPlazo = DateTime.Parse(reader["dFecPlazo"].ToString());
                                if (!Convert.IsDBNull(reader["vObservaciones"])) objEntidad.Observaciones = Convert.ToString(reader["vObservaciones"]);
                                if (!Convert.IsDBNull(reader["vTipoDocumento"])) objEntidad.TipoDocumento = Convert.ToString(reader["vTipoDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocumentoSIED"])) objEntidad.CodTipoDocumentoSIED = Convert.ToString(reader["iCodTipDocumentoSIED"]);
                                 if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["vArea"])) objEntidad.Area = Convert.ToString(reader["vArea"]);
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.CodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNombreTrab"])) objEntidad.NombreTrab = Convert.ToString(reader["vNombreTrab"]);
                                if (!Convert.IsDBNull(reader["vApePaternoTrab"])) objEntidad.ApePaternoTrab = Convert.ToString(reader["vApePaternoTrab"]);
                                if (!Convert.IsDBNull(reader["vApeMaternoTrab"])) objEntidad.ApeMaternoTrab = Convert.ToString(reader["vApeMaternoTrab"]);
                                if (!Convert.IsDBNull(reader["vUsuarioRegistro"])) objEntidad.UsuarioRegistro = Convert.ToString(reader["vUsuarioRegistro"]);
                                if (!Convert.IsDBNull(reader["CodResponsable"])) objEntidad.CodResponsalbe = Convert.ToInt32(reader["CodResponsable"]);
                                if (!Convert.IsDBNull(reader["siFirma"])) objEntidad.siFirma = Convert.ToInt32(reader["siFirma"]);
                                if (!Convert.IsDBNull(reader["iCodDocAdjunto"])) objEntidad.CodDocAdjunto = Convert.ToInt32(reader["iCodDocAdjunto"]);
                                if (!Convert.IsDBNull(reader["comentarioAnulacion"])) objEntidad.Comentario = Convert.ToString(reader["comentarioAnulacion"]);

                                if (!Convert.IsDBNull(reader["iCodRespuestaSIED"])) objEntidad.CodRespuestaSIED = Convert.ToString(reader["iCodRespuestaSIED"]);
                                if (!Convert.IsDBNull(reader["iCodDocumentoRespuestaSIED"])) objEntidad.CodDocumentoRespuestaSIED = Convert.ToInt32(reader["iCodDocumentoRespuestaSIED"]);
                                if (!Convert.IsDBNull(reader["iCodCuoRespuestaSIED"])) objEntidad.CodCUORespuestaSIED = Convert.ToString(reader["iCodCuoRespuestaSIED"]);
                                if (!Convert.IsDBNull(reader["vMensajeSIED"])) objEntidad.MensajeSIED = Convert.ToString(reader["vMensajeSIED"]);
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                               

                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }

        
        /// <summary>
        /// Método Público para el Obtener Datos de un Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public EnvioSIED ObtenerDocumentoSIED_SeviceEnvio(Documento EObj)
        {
            EnvioSIED objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentEnvioSIED_ServiceSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = EObj.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new EnvioSIED();
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.numeroDocumento = Convert.ToString(reader["vCorrelativo"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocumentoSIED"])) objEntidad.tipoDocumento = Convert.ToString(reader["iCodTipDocumentoSIED"]);
                                if (!Convert.IsDBNull(reader["vAsunto"])) objEntidad.asunto = Convert.ToString(reader["vAsunto"]);
                                if (!Convert.IsDBNull(reader["iCodTipAsuntoSIED"])) objEntidad.categoriaAsunto = Convert.ToInt16(reader["iCodTipAsuntoSIED"]);

                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.origen = Convert.ToInt32(reader["siOrigen"]);
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }


        /// <summary>
        /// Método Público para el Listado de Bandeja Mesa Partes
        /// </summary>
        /// <returns>Retorna Lista genérica de Bandeja Mesa Partes</returns>  
        public List<DocAdjunto> GetDocumentosAdjuntos(DocAdjunto objParams)
        {
            DocAdjunto objEntidad = null;
            List<DocAdjunto> lstDocumento = new List<DocAdjunto>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentosAdjuntos]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = objParams.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocAdjunto();
                                if (!Convert.IsDBNull(reader["iCodDocAdjunto"])) objEntidad.CodDocAdjunto = Convert.ToInt32(reader["iCodDocAdjunto"]);
                                if (!Convert.IsDBNull(reader["iCodLaserfiche"])) objEntidad.CodLaserfiche = Convert.ToInt32(reader["iCodLaserfiche"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);

                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                //SIED
                                if (!Convert.IsDBNull(reader["vAlmacenArchivoSIED"])) objEntidad.AlmacenArchivoSIED = Convert.ToString(reader["vAlmacenArchivoSIED"]);
                                if (!Convert.IsDBNull(reader["vDescripcionSIED"])) objEntidad.DescripcionSIED = Convert.ToString(reader["vDescripcionSIED"]);
                                if (!Convert.IsDBNull(reader["vNombreFisicoSIED"])) objEntidad.NombreFisicoSIED = Convert.ToString(reader["vNombreFisicoSIED"]);
                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }
        /// <summary>
        /// Método Público para el Listado de Documento Anexos
        /// </summary>
        /// <returns>Retorna Lista genérica de Documento Anexos</returns>  
        public List<DocAnexo> GetDocumentosDerivados(DocAnexo objParams)
        {
            DocAnexo objEntidad = null;
            List<DocAnexo> lstDocumento = new List<DocAnexo>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentosAnexos]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = objParams.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocAnexo();
                                if (!Convert.IsDBNull(reader["iCodDocAnexo"])) objEntidad.CodDocAnexo = Convert.ToInt32(reader["iCodDocAnexo"]);
                                if (!Convert.IsDBNull(reader["iCodLaserfiche"])) objEntidad.CodLaserfiche = Convert.ToInt32(reader["iCodLaserfiche"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["iCodDocMovimiento"])) objEntidad.CodDocumentoMovimiento = Convert.ToInt32(reader["iCodDocMovimiento"]);
                                if (!Convert.IsDBNull(reader["vNombre"])) objEntidad.NombreDocumento = (reader["vNombre"].ToString());
                                if (!Convert.IsDBNull(reader["iSecuencia"])) objEntidad.secuencia = short.Parse(reader["iSecuencia"].ToString());
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);

                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }
        /// <summary>
        /// Método Público para obtener el seguimiento del documento
        /// </summary>
        /// <returns>Retorna Lista genérica del Seguimiento del Documento</returns>  
        public List<DocumentoSeguimientoDTO> GetSeguimientoDocumento(int iCodDocumento)
        {
            DocumentoSeguimientoDTO objEntidad = null;
            List<DocumentoSeguimientoDTO> lstDocumento = new List<DocumentoSeguimientoDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_Listar_SeguimientoDocumento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = iCodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocumentoSeguimientoDTO();
                                if (!Convert.IsDBNull(reader["iCodMovAnterior"])) objEntidad.CodMovAnterior = Convert.ToInt32(reader["iCodMovAnterior"]);
                                if (!Convert.IsDBNull(reader["iCodDocMovimiento"])) objEntidad.CodDocMovimiento = Convert.ToInt32(reader["iCodDocMovimiento"]);
                                if (!Convert.IsDBNull(reader["iCodDocDerivacion"])) objEntidad.CodDocDerivacion = Convert.ToInt32(reader["iCodDocDerivacion"]);


                                if (!Convert.IsDBNull(reader["Colaborador"])) objEntidad.Colaborador = Convert.ToString(reader["Colaborador"]);
                                if (!Convert.IsDBNull(reader["Area"])) objEntidad.Area = Convert.ToString(reader["Area"]);
                                if (!Convert.IsDBNull(reader["FechaRecepcion"])) objEntidad.FechaRecepcion = Convert.ToString(reader["FechaRecepcion"]);
                                if (!Convert.IsDBNull(reader["TipoReg"])) objEntidad.TipoReg = Convert.ToString(reader["TipoReg"]);
                                if (!Convert.IsDBNull(reader["Comentario"])) objEntidad.Comentario = Convert.ToString(reader["Comentario"]);
                                if (!Convert.IsDBNull(reader["iEstadoDocumento"])) objEntidad.EstadoDocumento = Convert.ToInt32(reader["iEstadoDocumento"]);
                                if (!Convert.IsDBNull(reader["EstadoDescripcion"])) objEntidad.EstadoDescripcion = Convert.ToString(reader["EstadoDescripcion"]);
                                if (!Convert.IsDBNull(reader["ColaboradorCreacion"])) objEntidad.ColaboradorCreacion = Convert.ToString(reader["ColaboradorCreacion"]);
                                if (!Convert.IsDBNull(reader["iTipoMovimiento"])) objEntidad.TipoMovimiento = Convert.ToInt32(reader["iTipoMovimiento"]);



                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }

        /// <summary>
        /// Método Público para obtener el seguimiento del documento
        /// </summary>
        /// <returns>Retorna Lista genérica del Seguimiento del Documento</returns>  
        public List<DocumentoSeguimientoDTO> GetCabeceraSeguimiento(int iCodDocumento)
        {
            DocumentoSeguimientoDTO objEntidad = null;
            List<DocumentoSeguimientoDTO> lstDocumento = new List<DocumentoSeguimientoDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_Listar_CabeceraSeguimiento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = iCodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocumentoSeguimientoDTO();
                                if (!Convert.IsDBNull(reader["NOMBREDESDE"])) objEntidad.NombreDesde = Convert.ToString(reader["NOMBREDESDE"]);
                                if (!Convert.IsDBNull(reader["AREADESDE"])) objEntidad.AreaDesde = Convert.ToString(reader["AREADESDE"]);
                                if (!Convert.IsDBNull(reader["NOMBREPARA"])) objEntidad.NombrePara = Convert.ToString(reader["NOMBREPARA"]);
                                if (!Convert.IsDBNull(reader["AREAPARA"])) objEntidad.AreaPara = Convert.ToString(reader["AREAPARA"]);
                                if (!Convert.IsDBNull(reader["FECHA"])) objEntidad.FechaRecepcion = Convert.ToString(reader["FECHA"]);
                                if (!Convert.IsDBNull(reader["iEstadoDocumento"])) objEntidad.EstadoDocumento = Convert.ToInt16(reader["iEstadoDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodDocMovimiento"])) objEntidad.CodDocMovimiento = Convert.ToInt16(reader["iCodDocMovimiento"]);
                                if (!Convert.IsDBNull(reader["iCodDocAuxiliar"])) objEntidad.CodDocAuxiliar = Convert.ToInt16(reader["iCodDocAuxiliar"]);
                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener la cabecera del seguimiento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }

        /// <summary>
        /// Método Público para obtener el seguimiento del documento
        /// </summary>
        /// <returns>Retorna Lista genérica del Seguimiento del Documento</returns>  
        public List<DocumentoSeguimientoDTO> GetCabeceraSeguimientoInterno(int iCodDocumento)
        {
            DocumentoSeguimientoDTO objEntidad = null;
            List<DocumentoSeguimientoDTO> lstDocumento = new List<DocumentoSeguimientoDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = iCodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocumentoSeguimientoDTO();
                                if (!Convert.IsDBNull(reader["NOMBREDESDE"])) objEntidad.NombreDesde = Convert.ToString(reader["NOMBREDESDE"]);
                                if (!Convert.IsDBNull(reader["AREADESDE"])) objEntidad.AreaDesde = Convert.ToString(reader["AREADESDE"]);
                                 if (!Convert.IsDBNull(reader["AREAPARA"])) objEntidad.AreaPara = Convert.ToString(reader["AREAPARA"]);
                                if (!Convert.IsDBNull(reader["FECHA"])) objEntidad.FechaRecepcion = Convert.ToString(reader["FECHA"]);
                                if (!Convert.IsDBNull(reader["iEstadoDocumento"])) objEntidad.EstadoDocumento = Convert.ToInt16(reader["iEstadoDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodDerivacionesInternas"])) objEntidad.CodDerivacionesInternas = Convert.ToString(reader["iCodDerivacionesInternas"]);
                                  lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener la cabecera del seguimiento interno");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }
        /// <summary>
        /// Método Público para obtener el seguimiento del documento
        /// </summary>
        /// <returns>Retorna Lista genérica del Seguimiento del Documento</returns>  
        public List<DocumentoSeguimientoDTO> GetDetalleSeguimiento(int iCodDocumento)
        {
            DocumentoSeguimientoDTO objEntidad = null;
            List<DocumentoSeguimientoDTO> lstDocumento = new List<DocumentoSeguimientoDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_Listar_DetalleSeguimiento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = iCodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocumentoSeguimientoDTO();
                                if (!Convert.IsDBNull(reader["NOMBREDESDE"])) objEntidad.NombreDesde = Convert.ToString(reader["NOMBREDESDE"]);
                                if (!Convert.IsDBNull(reader["AREADESDE"])) objEntidad.AreaDesde = Convert.ToString(reader["AREADESDE"]);
                                if (!Convert.IsDBNull(reader["NOMBREPARA"])) objEntidad.NombrePara = Convert.ToString(reader["NOMBREPARA"]);
                                if (!Convert.IsDBNull(reader["AREAPARA"])) objEntidad.AreaPara = Convert.ToString(reader["AREAPARA"]);
                                if (!Convert.IsDBNull(reader["FECHA"])) objEntidad.FechaRecepcion = Convert.ToString(reader["FECHA"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.siEstado = Convert.ToInt32(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["iEstadoDocumento"])) objEntidad.EstadoDocumento = Convert.ToInt16(reader["iEstadoDocumento"]);
                                if (!Convert.IsDBNull(reader["EstadoDocumento"])) objEntidad.EstadoDocumentoInterno = Convert.ToInt16(reader["EstadoDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodDocMovimiento"])) objEntidad.CodDocMovimiento = Convert.ToInt16(reader["iCodDocMovimiento"]);
                                if (!Convert.IsDBNull(reader["iCodDocDerivacion"])) objEntidad.CodDocDerivacion = Convert.ToInt16(reader["iCodDocDerivacion"]);
                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener la cabecera del seguimiento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }

        /// <summary>
        /// Método Público para obtener el seguimiento del documento
        /// </summary>
        /// <returns>Retorna Lista genérica del Seguimiento del Documento</returns>  
        public List<ComentarioDocumentoDTO> GetComentariosDocumento(int iCodDocumento)
        {
            ComentarioDocumentoDTO objEntidad = null;
            List<ComentarioDocumentoDTO> lstDocumento = new List<ComentarioDocumentoDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_Listar_Comentarios]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = iCodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new ComentarioDocumentoDTO();
                                if (!Convert.IsDBNull(reader["iCodDocComentario"])) objEntidad.CodComentario = Convert.ToInt16(reader["iCodDocComentario"]);
                                if (!Convert.IsDBNull(reader["vComentario"])) objEntidad.Comentario = Convert.ToString(reader["vComentario"]);
                                if (!Convert.IsDBNull(reader["iCodDocMovimiento"])) objEntidad.CodMovimiento = Convert.ToInt16(reader["iCodDocMovimiento"]);
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt16(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocumentoDerivacion = Convert.ToInt16(reader["iCodDocumentoDerivacion"]);
                                if (!Convert.IsDBNull(reader["COMENTADOPOR"])) objEntidad.ComentadoPor = Convert.ToString(reader["COMENTADOPOR"]);
                                if (!Convert.IsDBNull(reader["iEstadoDocumento"])) objEntidad.EstadoDocumento = Convert.ToInt16(reader["iEstadoDocumento"]);
                                if (!Convert.IsDBNull(reader["siEstadoDerivacion"])) objEntidad.EstadoDerivacion = Convert.ToInt16(reader["siEstadoDerivacion"]);
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener los comentarios");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }


        /// <summary>
        /// Método Público para el Listado de Documento Anexos
        /// </summary>
        /// <returns>Retorna Lista genérica de Documento Anexos</returns>  
        public List<DocAnexo> GetDocumentoAnexos(DocAnexo objParams)
        {
            DocAnexo objEntidad = null;
            List<DocAnexo> lstDocumento = new List<DocAnexo>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentosAnexos]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = objParams.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocAnexo();
                                if (!Convert.IsDBNull(reader["iCodDocAnexo"])) objEntidad.CodDocAnexo = Convert.ToInt32(reader["iCodDocAnexo"]);
                                if (!Convert.IsDBNull(reader["iCodLaserfiche"])) objEntidad.CodLaserfiche = Convert.ToInt32(reader["iCodLaserfiche"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["iCodDocMovimiento"])) objEntidad.CodDocumentoMovimiento = Convert.ToInt32(reader["iCodDocMovimiento"]);
                                if (!Convert.IsDBNull(reader["vNombre"])) objEntidad.NombreDocumento = (reader["vNombre"].ToString());
                                if (!Convert.IsDBNull(reader["iSecuencia"])) objEntidad.secuencia = short.Parse(reader["iSecuencia"].ToString());
                                if (!Convert.IsDBNull(reader["iTipoArchivo"])) objEntidad.TipoArchivo = short.Parse(reader["iTipoArchivo"].ToString());
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                //SIED
                                if (!Convert.IsDBNull(reader["vAlmacenArchivoSIED"])) objEntidad.AlmacenArchivoSIED = Convert.ToString(reader["vAlmacenArchivoSIED"]);
                                if (!Convert.IsDBNull(reader["vDescripcionSIED"])) objEntidad.DescripcionSIED = Convert.ToString(reader["vDescripcionSIED"]);
                                if (!Convert.IsDBNull(reader["vNombreFisicoSIED"])) objEntidad.NombreFisicoSIED = Convert.ToString(reader["vNombreFisicoSIED"]);
                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }
        

        /// <summary>
        /// Método Público para obtener el detalle de la Derivacion
        /// </summary>
        /// <returns>Retorna objeto Derivacion</returns>  
        public Documento GetDetalleCorreoDerivacion(int objParams)
        {
            Documento objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDetalleCorreoDerivacion]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = objParams;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new Documento();
                                if (!Convert.IsDBNull(reader["siPlazo"])) objEntidad.Plazo = Convert.ToInt16(reader["siPlazo"]);
                                if (!Convert.IsDBNull(reader["dFecPlazo"])) objEntidad.FechaPlazo = Convert.ToDateTime(reader["dFecPlazo"]);
                                if (!Convert.IsDBNull(reader["siPrioridad"])) objEntidad.Prioridad = short.Parse(reader["siPrioridad"].ToString());
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = Convert.ToInt16(reader["siOrigen"]);
                                if (!Convert.IsDBNull(reader["REPRESENTANTE"])) objEntidad.Representante = (reader["REPRESENTANTE"].ToString());
                                if (!Convert.IsDBNull(reader["EMPRESA"])) objEntidad.DescripcionEmpresa = (reader["EMPRESA"].ToString());
                                if (!Convert.IsDBNull(reader["DERIVADOPOR"])) objEntidad.DerivadoPor =(reader["DERIVADOPOR"].ToString());
                                if (!Convert.IsDBNull(reader["TipoAccion"])) objEntidad.TipoAccion = (reader["TipoAccion"].ToString());
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }

        /// <summary>
        /// Método Público para obtener el detalle de la Finalizacion
        /// </summary>
        /// <returns>Retorna objeto Derivacion</returns>  
        public Documento GetDetalleCorreoFinalizar(int objParams)
        {
            Documento objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDetalleCorreoFinalizar]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = objParams;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new Documento();

                                if (!Convert.IsDBNull(reader["dFecPlazo"])) objEntidad.FechaPlazo = Convert.ToDateTime(reader["dFecPlazo"]);
                                if (!Convert.IsDBNull(reader["siPrioridad"])) objEntidad.Prioridad = short.Parse(reader["siPrioridad"].ToString());
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = Convert.ToInt16(reader["siOrigen"]);
                                if (!Convert.IsDBNull(reader["REPRESENTANTE"])) objEntidad.Representante = (reader["REPRESENTANTE"].ToString());
                                if (!Convert.IsDBNull(reader["EMPRESA"])) objEntidad.DescripcionEmpresa = (reader["EMPRESA"].ToString());
                                if (!Convert.IsDBNull(reader["AREA"])) objEntidad.Area = (reader["AREA"].ToString());
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.Correlativo = (reader["vCorrelativo"].ToString());
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.NumDocumento = (reader["vNumDocumento"].ToString());

                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }
        
        /// <summary>
        /// Método Público para obtener el detalle de la Finalizacion
        /// </summary>
        /// <returns>Retorna objeto Derivacion</returns>  
        public Documento GetDetalleCorreoAnularDocumentoEnvioSIED(int objParams)
        {
            Documento objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDetalleCorreoAnularEnvioSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = objParams;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new Documento();

                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.Correlativo = (reader["vCorrelativo"].ToString());
                                if (!Convert.IsDBNull(reader["siPrioridad"])) objEntidad.Prioridad = short.Parse(reader["siPrioridad"].ToString());
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = Convert.ToInt16(reader["siOrigen"]);
                                if (!Convert.IsDBNull(reader["vNombres"])) objEntidad.NombreTrab = (reader["vNombres"].ToString());
                                if (!Convert.IsDBNull(reader["vApePaterno"])) objEntidad.ApePaternoTrab = (reader["vApePaterno"].ToString());
                                if (!Convert.IsDBNull(reader["vApeMaterno"])) objEntidad.ApeMaternoTrab = (reader["vApeMaterno"].ToString());

                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }


        /// <summary>
        /// Método Público para el Listado de Bandeja Mesa Partes
        /// </summary>
        /// <returns>Retorna Lista genérica de Bandeja Mesa Partes</returns>  
        public List<DocumentoBandejaDTO> ListarBandejaMesaPartes(DocumentoBandejaParamsDTO objParams)
        {
            DocumentoBandejaDTO objEntidad = null;
            List<DocumentoBandejaDTO> lstDocumento = new List<DocumentoBandejaDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_Listar_Documento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@pDescripcion";
                    param.Value = objParams.Descripcion;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.DateTime;
                    param.ParameterName = "@FechaInicio";
                    param.Value = objParams.FechaInicio;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.DateTime;
                    param.ParameterName = "@FechaFin";
                    param.Value = objParams.FechaFin;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocumentoBandejaDTO();
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["Estado"])) objEntidad.Estado = short.Parse(reader["Estado"].ToString());
                                if (!Convert.IsDBNull(reader["EstadoDocumento"])) objEntidad.EstadoDocumento = short.Parse(reader["EstadoDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["Prioridad"])) objEntidad.Prioridad = short.Parse(reader["Prioridad"].ToString());
                                if (!Convert.IsDBNull(reader["Recibido"])) objEntidad.Recibido = Convert.ToString(reader["Recibido"]);
                                if (!Convert.IsDBNull(reader["Recepcion"])) objEntidad.Recepcion = Convert.ToString(reader["Recepcion"]);
                                if (!Convert.IsDBNull(reader["Reg"])) objEntidad.Reg = Convert.ToInt32(reader["Reg"]);
                                if (!Convert.IsDBNull(reader["Documento"])) objEntidad.Documento = Convert.ToString(reader["Documento"]);
                                if (!Convert.IsDBNull(reader["DocumentoAsunto"])) objEntidad.DocumentoAsunto = Convert.ToString(reader["DocumentoAsunto"]);
                                if (!Convert.IsDBNull(reader["Remitente"])) objEntidad.Remitente = Convert.ToString(reader["Remitente"]);
                                if (!Convert.IsDBNull(reader["Derivado"])) objEntidad.Derivado = Convert.ToString(reader["Derivado"]);
                                if (!Convert.IsDBNull(reader["Plazo"])) objEntidad.Plazo = Convert.ToString(reader["Plazo"]);
                                if (!Convert.IsDBNull(reader["ExisteAdjunto"])) objEntidad.ExisteAdjunto = Convert.ToString(reader["ExisteAdjunto"]);
                                if (!Convert.IsDBNull(reader["Correlativo"])) objEntidad.Correlativo = Convert.ToString(reader["Correlativo"]);
                                if (!Convert.IsDBNull(reader["CodEmpresa"])) objEntidad.CodEmpresa = Convert.ToInt32(reader["CodEmpresa"]);
                                if (!Convert.IsDBNull(reader["NombreEmpresa"])) objEntidad.NombreEmpresa = Convert.ToString(reader["NombreEmpresa"]);
                                if (!Convert.IsDBNull(reader["CodRepresentante"])) objEntidad.CodRepresentante = Convert.ToInt32(reader["CodRepresentante"]);
                                if (!Convert.IsDBNull(reader["NombreTrabajador"])) objEntidad.NombreTrabajador = Convert.ToString(reader["NombreTrabajador"]);
                                if (!Convert.IsDBNull(reader["vEstadoDescripcion"])) objEntidad.EstadoDescripcion = Convert.ToString(reader["vEstadoDescripcion"]);
                                if (!Convert.IsDBNull(reader["vEstadoPrioridad"])) objEntidad.EstadoPrioridad = Convert.ToString(reader["vEstadoPrioridad"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion= Convert.ToString(reader["vUsuCreacion"]);

                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }


        /// <summary>
        /// Método Público para el Listado de Bandeja de entrada
        /// </summary>
        /// <returns>Retorna Lista genérica de Bandeja entrada</returns>  
        public List<DocumentoBandejaDTO> ListarBandejaEntrada(DocumentoBandejaEntradaDTO objParams, out int totalRegistros)
        {
            DocumentoBandejaDTO objEntidad = null;
            totalRegistros = 0;
            List<DocumentoBandejaDTO> lstDocumento = new List<DocumentoBandejaDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_Listar_BandejaEntrada]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vTextoBusqueda";
                    param.Value = objParams.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Date;
                    param.ParameterName = "@dFechaDesde";
                    param.Value = objParams.FechaDesde;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Date;
                    param.ParameterName = "@dFechaHasta";
                    param.Value = objParams.FechaHasta;
                    cmd.Parameters.Add(param);


                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iTipoBandeja";
                    param.Value = objParams.TipoBandeja;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodTrabajador";
                    param.Value = objParams.CodTrabajador;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodPerfil";
                    param.Value = objParams.CodPerfil;
                    cmd.Parameters.Add(param);


                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iTamPagina";
                    param.Value = objParams.TamanioPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocumentoBandejaDTO();
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["Estado"])) objEntidad.Estado = short.Parse(reader["Estado"].ToString());
                                if (!Convert.IsDBNull(reader["EstadoDocumento"])) objEntidad.EstadoDocumento = short.Parse(reader["EstadoDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["Prioridad"])) objEntidad.Prioridad = short.Parse(reader["Prioridad"].ToString());
                                if (!Convert.IsDBNull(reader["Recibido"])) objEntidad.Recibido = Convert.ToString(reader["Recibido"]);
                                if (!Convert.IsDBNull(reader["Recepcion"])) objEntidad.Recepcion = Convert.ToString(reader["Recepcion"]);
                                if (!Convert.IsDBNull(reader["Reg"])) objEntidad.Reg = Convert.ToInt32(reader["Reg"]);
                                if (!Convert.IsDBNull(reader["Documento"])) objEntidad.Documento = Convert.ToString(reader["Documento"]);
                                if (!Convert.IsDBNull(reader["DocumentoAsunto"])) objEntidad.DocumentoAsunto = Convert.ToString(reader["DocumentoAsunto"]);
                                if (!Convert.IsDBNull(reader["Remitente"])) objEntidad.Remitente = Convert.ToString(reader["Remitente"]);
                                if (!Convert.IsDBNull(reader["Derivado"])) objEntidad.Derivado = Convert.ToString(reader["Derivado"]);
                                if (!Convert.IsDBNull(reader["Plazo"])) objEntidad.Plazo = Convert.ToString(reader["Plazo"]);
                                if (!Convert.IsDBNull(reader["ExisteAdjunto"])) objEntidad.ExisteAdjunto = Convert.ToString(reader["ExisteAdjunto"]);
                                if (!Convert.IsDBNull(reader["Correlativo"])) objEntidad.Correlativo = Convert.ToString(reader["Correlativo"]);
                                if (!Convert.IsDBNull(reader["CodEmpresa"])) objEntidad.CodEmpresa = Convert.ToInt32(reader["CodEmpresa"]);
                                if (!Convert.IsDBNull(reader["NombreEmpresa"])) objEntidad.NombreEmpresa = Convert.ToString(reader["NombreEmpresa"]);
                                if (!Convert.IsDBNull(reader["CodRepresentante"])) objEntidad.CodRepresentante = Convert.ToInt32(reader["CodRepresentante"]);
                                if (!Convert.IsDBNull(reader["NombreTrabajador"])) objEntidad.NombreTrabajador = Convert.ToString(reader["NombreTrabajador"]);
                                if (!Convert.IsDBNull(reader["vEstadoDescripcion"])) objEntidad.EstadoDescripcion = Convert.ToString(reader["vEstadoDescripcion"]);
                                if (!Convert.IsDBNull(reader["vEstadoPrioridad"])) objEntidad.EstadoPrioridad = Convert.ToString(reader["vEstadoPrioridad"]);
                                if (!Convert.IsDBNull(reader["TipoDocIng"])) objEntidad.TipoDocIng = Convert.ToString(reader["TipoDocIng"]);
                                if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocDerivacion = Convert.ToInt32(reader["iCodDocumentoDerivacion"]);
                                if (!Convert.IsDBNull(reader["HabilitarSeguimiento"])) objEntidad.HabilitarSeguimiento = Convert.ToBoolean(reader["HabilitarSeguimiento"]);
                                if (!Convert.IsDBNull(reader["HabilitarDerivar"])) objEntidad.HabilitarDerivar = Convert.ToBoolean(reader["HabilitarDerivar"]);
                                if (!Convert.IsDBNull(reader["HabilitarDevolver"])) objEntidad.HabilitarDevolver = Convert.ToBoolean(reader["HabilitarDevolver"]);
                                if (!Convert.IsDBNull(reader["HabilitarFinalizar"])) objEntidad.HabilitarFinalizar = Convert.ToBoolean(reader["HabilitarFinalizar"]);
                                if (!Convert.IsDBNull(reader["HabilitarRegistrarAvance"])) objEntidad.HabilitarRegistrarAvance = Convert.ToBoolean(reader["HabilitarRegistrarAvance"]);
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = Convert.ToInt16(reader["siOrigen"]);
                                if (!Convert.IsDBNull(reader["FILTRO"])) objEntidad.FILTRO = Convert.ToString(reader["FILTRO"]);
                                if (!Convert.IsDBNull(reader["iCodDocumentoFirma"])) objEntidad.CodDocFirma = Convert.ToInt32(String.IsNullOrEmpty(reader["iCodDocumentoFirma"].ToString())==true?0: reader["iCodDocumentoFirma"]);
                                if (!Convert.IsDBNull(reader["siFlagSIED"])) objEntidad.siFlagSIED = Convert.ToString(reader["siFlagSIED"]);
                                if (!Convert.IsDBNull(reader["vEntidadSIED"])) objEntidad.EntidadSIED = Convert.ToString(reader["vEntidadSIED"]);
                                if (!Convert.IsDBNull(reader["vTipoEntidadSIED"])) objEntidad.TipoEntidadSIED = Convert.ToString(reader["vTipoEntidadSIED"]);

                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return lstDocumento;
        }


        /// <summary>
        /// Método Público para el Listado de Bandeja de entrada
        /// </summary>
        /// <returns>Retorna Lista genérica de Bandeja entrada</returns>  
        public List<DocumentoBandejaDTO> ListarBandejaRecepcionSIEDFONAFE(DocumentoBandejaEntradaDTO objParams, out int totalRegistros)
        {
            DocumentoBandejaDTO objEntidad = null;
            totalRegistros = 0;
            List<DocumentoBandejaDTO> lstDocumento = new List<DocumentoBandejaDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_Listar_BandejaRecepcionSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vTextoBusqueda";
                    param.Value = objParams.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Date;
                    param.ParameterName = "@dFechaDesde";
                    param.Value = objParams.FechaDesde;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Date;
                    param.ParameterName = "@dFechaHasta";
                    param.Value = objParams.FechaHasta;
                    cmd.Parameters.Add(param);


                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iTipoBandeja";
                    param.Value = objParams.TipoBandeja;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodTrabajador";
                    param.Value = objParams.CodTrabajador;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodPerfil";
                    param.Value = objParams.CodPerfil;
                    cmd.Parameters.Add(param);


                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iTamPagina";
                    param.Value = objParams.TamanioPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocumentoBandejaDTO();
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["Estado"])) objEntidad.Estado = short.Parse(reader["Estado"].ToString());
                                if (!Convert.IsDBNull(reader["EstadoDocumento"])) objEntidad.EstadoDocumento = short.Parse(reader["EstadoDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["Prioridad"])) objEntidad.Prioridad = short.Parse(reader["Prioridad"].ToString());
                                if (!Convert.IsDBNull(reader["Recibido"])) objEntidad.Recibido = Convert.ToString(reader["Recibido"]);
                                if (!Convert.IsDBNull(reader["Recepcion"])) objEntidad.Recepcion = Convert.ToString(reader["Recepcion"]);
                                if (!Convert.IsDBNull(reader["Reg"])) objEntidad.Reg = Convert.ToInt32(reader["Reg"]);
                                if (!Convert.IsDBNull(reader["Documento"])) objEntidad.Documento = Convert.ToString(reader["Documento"]);
                                if (!Convert.IsDBNull(reader["DocumentoAsunto"])) objEntidad.DocumentoAsunto = Convert.ToString(reader["DocumentoAsunto"]);
                                if (!Convert.IsDBNull(reader["Remitente"])) objEntidad.Remitente = Convert.ToString(reader["Remitente"]);
                                if (!Convert.IsDBNull(reader["Derivado"])) objEntidad.Derivado = Convert.ToString(reader["Derivado"]);
                                if (!Convert.IsDBNull(reader["Plazo"])) objEntidad.Plazo = Convert.ToString(reader["Plazo"]);
                                if (!Convert.IsDBNull(reader["ExisteAdjunto"])) objEntidad.ExisteAdjunto = Convert.ToString(reader["ExisteAdjunto"]);
                                if (!Convert.IsDBNull(reader["Correlativo"])) objEntidad.Correlativo = Convert.ToString(reader["Correlativo"]);
                                if (!Convert.IsDBNull(reader["CodEmpresa"])) objEntidad.CodEmpresa = Convert.ToInt32(reader["CodEmpresa"]);
                                if (!Convert.IsDBNull(reader["NombreEmpresa"])) objEntidad.NombreEmpresa = Convert.ToString(reader["NombreEmpresa"]);
                                if (!Convert.IsDBNull(reader["CodRepresentante"])) objEntidad.CodRepresentante = Convert.ToInt32(reader["CodRepresentante"]);
                                if (!Convert.IsDBNull(reader["NombreTrabajador"])) objEntidad.NombreTrabajador = Convert.ToString(reader["NombreTrabajador"]);
                                if (!Convert.IsDBNull(reader["vEstadoDescripcion"])) objEntidad.EstadoDescripcion = Convert.ToString(reader["vEstadoDescripcion"]);
                                if (!Convert.IsDBNull(reader["vEstadoPrioridad"])) objEntidad.EstadoPrioridad = Convert.ToString(reader["vEstadoPrioridad"]);
                                if (!Convert.IsDBNull(reader["TipoDocIng"])) objEntidad.TipoDocIng = Convert.ToString(reader["TipoDocIng"]);
                                if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocDerivacion = Convert.ToInt32(reader["iCodDocumentoDerivacion"]);
                                if (!Convert.IsDBNull(reader["HabilitarSeguimiento"])) objEntidad.HabilitarSeguimiento = Convert.ToBoolean(reader["HabilitarSeguimiento"]);
                                if (!Convert.IsDBNull(reader["HabilitarDerivar"])) objEntidad.HabilitarDerivar = Convert.ToBoolean(reader["HabilitarDerivar"]);
                                if (!Convert.IsDBNull(reader["HabilitarDevolver"])) objEntidad.HabilitarDevolver = Convert.ToBoolean(reader["HabilitarDevolver"]);
                                if (!Convert.IsDBNull(reader["HabilitarFinalizar"])) objEntidad.HabilitarFinalizar = Convert.ToBoolean(reader["HabilitarFinalizar"]);
                                if (!Convert.IsDBNull(reader["HabilitarRegistrarAvance"])) objEntidad.HabilitarRegistrarAvance = Convert.ToBoolean(reader["HabilitarRegistrarAvance"]);
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = Convert.ToInt16(reader["siOrigen"]);
                                if (!Convert.IsDBNull(reader["FILTRO"])) objEntidad.FILTRO = Convert.ToString(reader["FILTRO"]);
                                if (!Convert.IsDBNull(reader["iCodDocumentoFirma"])) objEntidad.CodDocFirma = Convert.ToInt32(String.IsNullOrEmpty(reader["iCodDocumentoFirma"].ToString()) == true ? 0 : reader["iCodDocumentoFirma"]);
                                if (!Convert.IsDBNull(reader["vEntidadSIED"])) objEntidad.EntidadSIED = Convert.ToString(reader["vEntidadSIED"]);
                                if (!Convert.IsDBNull(reader["vTipoEntidadSIED"])) objEntidad.TipoEntidadSIED = Convert.ToString(reader["vTipoEntidadSIED"]);

                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);

                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return lstDocumento;
        }

        /// <summary>
        /// Método Público para el Listado de Bandeja de envio SIED
        /// </summary>
        /// <returns>Retorna Lista genérica de Bandeja envio SIED</returns>  
        public List<DocumentoBandejaDTO> ListarBandejaEnvioSied(DocumentoBandejaEntradaDTO objParams, out int totalRegistros)
        {
            DocumentoBandejaDTO objEntidad = null;
            totalRegistros = 0;
            List<DocumentoBandejaDTO> lstDocumento = new List<DocumentoBandejaDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_Listar_BandejaEnvioSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@pDescripcion";
                    param.Value = objParams.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Date;
                    param.ParameterName = "@FechaInicio";
                    param.Value = objParams.FechaDesde;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Date;
                    param.ParameterName = "@FechaFin";
                    param.Value = objParams.FechaHasta;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodTrabajador";
                    param.Value = objParams.CodTrabajador;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iTamPagina";
                    param.Value = objParams.TamanioPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocumentoBandejaDTO();
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["siPrioridad"])) objEntidad.Prioridad = short.Parse(reader["siPrioridad"].ToString());
                                if (!Convert.IsDBNull(reader["siEstDocumento"])) objEntidad.EstadoDocumento = short.Parse(reader["siEstDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["Recepcion"])) objEntidad.Recepcion = Convert.ToString(reader["Recepcion"]);
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.Correlativo = Convert.ToString(reader["vCorrelativo"]);
                                if (!Convert.IsDBNull(reader["vAsunto"])) objEntidad.DocumentoAsunto = Convert.ToString(reader["vAsunto"]);
                                if (!Convert.IsDBNull(reader["Remitente"])) objEntidad.Remitente = Convert.ToString(reader["Remitente"]);
                                if (!Convert.IsDBNull(reader["DrigidoA"])) objEntidad.EmpresasSied = Convert.ToString(reader["DrigidoA"]);
                                if (!Convert.IsDBNull(reader["Plazo"])) objEntidad.Plazo = Convert.ToString(reader["Plazo"]);
                                if (!Convert.IsDBNull(reader["TipoDocumento"])) objEntidad.Documento = Convert.ToString(reader["TipoDocumento"]);
                                if (!Convert.IsDBNull(reader["vEstadoDescripcion"])) objEntidad.EstadoDescripcion = Convert.ToString(reader["vEstadoDescripcion"]);
                                if (!Convert.IsDBNull(reader["vEstadoPrioridad"])) objEntidad.EstadoPrioridad = Convert.ToString(reader["vEstadoPrioridad"]);
                                if (!Convert.IsDBNull(reader["Origen"])) objEntidad.Origen = Convert.ToInt32(reader["Origen"]);
                                if (!Convert.IsDBNull(reader["HabilitarDerivar"])) objEntidad.HabilitarDerivar = Convert.ToBoolean(reader["HabilitarDerivar"]);
                                if (!Convert.IsDBNull(reader["iCodDocumentoRespuestaSIED"])) objEntidad.CodDocumentoRespuestaSIED = Convert.ToInt32(reader["iCodDocumentoRespuestaSIED"]);
                                if (!Convert.IsDBNull(reader["iCodRespuestaSIED"])) objEntidad.CodRespuestaSIED = Convert.ToString(reader["iCodRespuestaSIED"]);

                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return lstDocumento;
        }

        /// <summary>
        /// Método Público para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        /// <summary>
        /// Método Público para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        //public int RegistrarDocumento(Documento objMovDocumento, out string outCorrelativoDoc)
        //{
        //    int nRpta = 0;
        //    outCorrelativoDoc = "";
        //    using (SqlConnection conn = new SqlConnection(cadenaConexion))
        //    {
        //        conn.Open();

        //        using (SqlCommand cmd = conn.CreateCommand())
        //        {
        //            using (SqlTransaction tx = conn.BeginTransaction())
        //            {
        //                try
        //                {
        //                    cmd.Transaction = tx;
        //                    string outCorrelativo = "";
        //                    //Generando Secuencial
        //                    var onCodSecuencial = GenerarSecuencial(objMovDocumento, cmd, out outCorrelativo);

        //                    if (onCodSecuencial >= 0)
        //                    {
        //                        //Insertando Documentos
        //                        objMovDocumento.Secuencial = onCodSecuencial;
        //                        objMovDocumento.Correlativo = outCorrelativo;

        //                        /*SETEAR CODIGO TRABAJADOR*/
        //                        DocDerivadoDAO objDocDerivado = new DocDerivadoDAO(cadenaConexion);
        //                        MaeTrabajadorDAO obj = new MaeTrabajadorDAO(cadenaConexion);
        //                        EMaeTrabajador filtro = new EMaeTrabajador();
        //                        filtro.iCodArea = objMovDocumento.CodArea;
        //                        filtro.siEsJefe = 1;
        //                        var trabajadores = obj.GetTrabajadorPorArea(filtro);
        //                        int codTrabajador = 0;

        //                        codTrabajador = trabajadores.FirstOrDefault().iCodTrabajador;
        //                        if (codTrabajador == 0)
        //                            throw new Exception("La área debe tener asignado un jefe");
        //                        objMovDocumento.CodTrabajador = codTrabajador;

        //                        var onFlagOk = InsertarDocumento(objMovDocumento, cmd);

        //                        var codDocMovimiento = 0;
        //                        int idDocumento = int.Parse(onFlagOk.ToString());
        //                        //Insertando Movimiento
        //                        if (onFlagOk >= 0)
        //                        {
        //                            objMovDocumento.objDocMovimiento = new DocMovimiento();
        //                            objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
        //                            objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
        //                            objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
        //                            objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
        //                            objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
        //                            //objMovDocumento.objDocMovimiento.Nivel = 1;
        //                            objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
        //                            //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
        //                            objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
        //                            //objMovDocumento.objDocMovimiento.FechaFirma = null;
        //                            objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
        //                            objMovDocumento.objDocMovimiento.CodTrabajador = objMovDocumento.CodTrabajador;
        //                            objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;

        //                            objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
        //                            objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
        //                            objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
        //                            objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
        //                            objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

        //                            onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
        //                            codDocMovimiento = onFlagOk;
        //                            if (onFlagOk >= 0)
        //                            {
        //                                //Evaluando si hay comentarios
        //                                if (objMovDocumento.objDocMovimiento.objDocComentario != null)
        //                                {
        //                                    objMovDocumento.objDocMovimiento.objDocComentario.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
        //                                    objMovDocumento.objDocMovimiento.objDocComentario.CodDocMovimiento = objMovDocumento.objDocMovimiento.CodDocMovimiento;
        //                                    objMovDocumento.objDocMovimiento.objDocComentario.HostCreacion = objMovDocumento.HostCreacion;
        //                                    objMovDocumento.objDocMovimiento.objDocComentario.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
        //                                    objMovDocumento.objDocMovimiento.objDocComentario.LoginCreacion = objMovDocumento.LoginCreacion;
        //                                    objMovDocumento.objDocMovimiento.objDocComentario.RolCreacion = objMovDocumento.RolCreacion;
        //                                    onFlagOk = InsertarMovComentario(objMovDocumento.objDocMovimiento.objDocComentario, cmd);
        //                                }
        //                            }
        //                            //Insertando CC
        //                            if (onFlagOk >= 0 && objMovDocumento.CodAreaCC.Length > 0)
        //                            {
        //                                DocDerivacion objDocDerivacion;
        //                                string[] arrCodAreaCC = objMovDocumento.CodAreaCC.Split('|');
        //                                for (int i = 0; i < arrCodAreaCC.Length; i++)
        //                                {
        //                                    objDocDerivacion = new DocDerivacion();
        //                                    objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
        //                                    objDocDerivacion.CodArea = short.Parse(arrCodAreaCC[i]);
        //                                    objDocDerivacion.CodResponsable = codTrabajador;
        //                                    objDocDerivacion.TipoAcceso = 2;
        //                                    objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
        //                                    objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
        //                                    objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
        //                                    objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
        //                                    objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
        //                                    objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

        //                                    onFlagOk = objDocDerivado.RegistrarDocumentoDerivacion(objDocDerivacion);
        //                                }
        //                            }
        //                            //Evaluando si hay Documentos Adjuntos
        //                            if (onFlagOk >= 0)
        //                            {
        //                                //Insertando Adjunto de Documentos
        //                                LaserFiche objLaserFiche = new LaserFiche();
        //                                string filename = string.Empty;
        //                                int codLaserFiche = 0;
        //                                if (objMovDocumento.LstDocAdjunto != null && objMovDocumento.LstDocAdjunto.Count > 0)
        //                                {
        //                                    foreach (var item in objMovDocumento.LstDocAdjunto)
        //                                    {
        //                                        //Subiendo el Archivo al Repositorio Laserfiche

        //                                        var bytes = Convert.FromBase64String(item.FileArray);
        //                                        MemoryStream file = new MemoryStream(bytes);

        //                                        codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, string.Empty, objMovDocumento.Correlativo.ToString(), false, out filename);

        //                                        if (codLaserFiche > 0)
        //                                        {
        //                                            //Insertando MovDocAdjuntos
        //                                            item.CodDocumento = objMovDocumento.CodDocumento;
        //                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
        //                                            item.HostCreacion = objMovDocumento.HostCreacion;
        //                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
        //                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
        //                                            item.RolCreacion = objMovDocumento.RolCreacion;
        //                                            item.CodLaserfiche = codLaserFiche;

        //                                            onFlagOk = InsertarDocAdjunto(item, cmd);
        //                                        }
        //                                    }
        //                                }

        //                                //ANEXOS
        //                                if (objMovDocumento.LstDocAnexo != null && objMovDocumento.LstDocAnexo.Count > 0)
        //                                {
        //                                    int cc = 1;
        //                                    foreach (var item in objMovDocumento.LstDocAnexo)
        //                                    {
        //                                        //Subiendo el Archivo al Repositorio Laserfiche
        //                                        var bytes = Convert.FromBase64String(item.FileArray);
        //                                        MemoryStream file = new MemoryStream(bytes);

        //                                        var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
        //                                        codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, nombreAnexo, objMovDocumento.Correlativo.ToString(), true, out filename);

        //                                        if (codLaserFiche > 0)
        //                                        {
        //                                            //Insertando MovDocAdjuntos
        //                                            item.CodDocumentoMovimiento = codDocMovimiento;
        //                                            item.CodDocumento = objMovDocumento.CodDocumento;
        //                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
        //                                            item.HostCreacion = objMovDocumento.HostCreacion;
        //                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
        //                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
        //                                            item.RolCreacion = objMovDocumento.RolCreacion;
        //                                            item.CodLaserfiche = codLaserFiche;
        //                                            item.NombreDocumento = nombreAnexo;
        //                                            item.secuencia = cc;

        //                                            onFlagOk = InsertarDocAnexo(item, cmd);
        //                                        }
        //                                        cc++;
        //                                    }

        //                                }
        //                            }
        //                        }

        //                        if (onFlagOk >= 0)
        //                        {
        //                            nRpta = idDocumento;
        //                            outCorrelativoDoc = outCorrelativo;
        //                            tx.Commit();
        //                        }
        //                        else { tx.Rollback(); }
        //                    }
        //                    else
        //                    {
        //                        tx.Rollback();
        //                    }



        //                }
        //                catch (Exception ex)
        //                {
        //                    nRpta = -1;
        //                    outCorrelativoDoc = "";
        //                    tx.Rollback();
        //                    conn.Close();
        //                    conn.Dispose();
        //                    cmd.Dispose();
        //                    //Generando Archivo Log
        //                    new LogWriter(ex.Message);
        //                    throw new Exception("Error al Registrar el Documento Externo");
        //                }
        //                finally
        //                {
        //                    conn.Close();
        //                    conn.Dispose();
        //                    cmd.Dispose();


        //                }
        //            }
        //        }
        //    }

        //    return nRpta;
        //}

        /// <summary>
        /// Método Público para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        public int RegistrarDocumento(Documento objMovDocumento, out string outCorrelativoDoc)
        {
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            string guidTmp = "";
            try
            {
                guidTmp = GuardarArchivosDocumentoTemporales(objMovDocumento);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
                throw new Exception("No se puede establecer conexión con el servidor de archivos.");
            }
            int nRpta = 0;
            int onFlagOk = -1;
            var codDocMovimiento = 0;
            outCorrelativoDoc = "";
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            string outCorrelativo = "";
                            //Generando Secuencial
                            var onCodSecuencial = GenerarSecuencial(objMovDocumento, cmd, out outCorrelativo);

                            if (onCodSecuencial >= 0)
                            {
                                //Insertando Documentos
                                objMovDocumento.Secuencial = onCodSecuencial;
                                objMovDocumento.Correlativo = outCorrelativo;

                                /*SETEAR CODIGO TRABAJADOR*/
                                DocDerivadoDAO objDocDerivado = new DocDerivadoDAO(cadenaConexion);
                                MaeTrabajadorDAO obj = new MaeTrabajadorDAO(cadenaConexion);
                                EMaeTrabajador filtro = new EMaeTrabajador();
                                filtro.iCodArea = objMovDocumento.CodArea;
                                filtro.siEsJefe = 1;
                                var trabajadores = obj.GetTrabajadorPorArea(filtro);
                                int codTrabajador = 0;

                                /* foreach (var item in trabajadores)
                                 {
                                     DSegAsignacion db2 = new DSegAsignacion();
                                     Entidades.EntidadesDb2.ESegAsignacion eSeg = new Entidades.EntidadesDb2.ESegAsignacion();
                                     eSeg.webusr = item.WEBUSR;

                                     var asignacion = db2.ListaSegAsignacion(eSeg).FirstOrDefault();

                                     if (asignacion.vCodPerfil == "JEFE")
                                     {
                                         codTrabajador = item.iCodTrabajador;
                                         break;
                                     }
                                 }*/

                                codTrabajador = trabajadores.FirstOrDefault().iCodTrabajador;
                                if (codTrabajador == 0)
                                    throw new Exception("La área debe tener asignado un jefe");
                                objMovDocumento.CodTrabajador = codTrabajador;

                                onFlagOk = InsertarDocumento(objMovDocumento, cmd);

                                int idDocumento = int.Parse(onFlagOk.ToString());
                                //Insertando Movimiento
                                if (onFlagOk >= 0)
                                {
                                    objMovDocumento.objDocMovimiento = new DocMovimiento();
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                    objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                    objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                    objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                    //objMovDocumento.objDocMovimiento.Nivel = 1;
                                    objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                    //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                    objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                    //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.CodTrabajador = objMovDocumento.CodTrabajador;
                                    objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;
                                    objMovDocumento.objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Registro);

                                    objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                    objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                    objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                    objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                    objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

                                    onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
                                    codDocMovimiento = onFlagOk;
                                    if (onFlagOk >= 0)
                                    {
                                        //Evaluando si hay comentarios
                                        if (objMovDocumento.objDocMovimiento.objDocComentario != null)
                                        {
                                            objMovDocumento.objDocMovimiento.objDocComentario.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.CodDocMovimiento = objMovDocumento.objDocMovimiento.CodDocMovimiento;
                                            objMovDocumento.objDocMovimiento.objDocComentario.HostCreacion = objMovDocumento.HostCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.RolCreacion = objMovDocumento.RolCreacion;
                                            onFlagOk = InsertarMovComentario(objMovDocumento.objDocMovimiento.objDocComentario, cmd);
                                        }
                                    }
                                    //Insertando CC
                                    if (onFlagOk >= 0 && objMovDocumento.CodAreaCC != null)
                                    {
                                        DocDerivacion objDocDerivacion;
                                        string[] arrCodAreaCC = objMovDocumento.CodAreaCC.Split('|');
                                        for (int i = 0; i < arrCodAreaCC.Length; i++)
                                        {
                                            filtro = new EMaeTrabajador();
                                            filtro.iCodArea = short.Parse(arrCodAreaCC[i]);
                                            filtro.siEsJefe = 1;
                                            var trabajadoresCC = obj.GetTrabajadorPorArea(filtro);
                                            int codTrabajadorCC = 0;

                                            codTrabajadorCC = trabajadoresCC.FirstOrDefault().iCodTrabajador;

                                            objDocDerivacion = new DocDerivacion();
                                            objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                            objDocDerivacion.CodArea = short.Parse(arrCodAreaCC[i]);
                                            objDocDerivacion.CodResponsable = codTrabajadorCC;
                                            objDocDerivacion.TipoAcceso = (short)Convert.ToInt32(EnumTipoAcceso.ConCopia);
                                            objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
                                            objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
                                            objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = objDocDerivado.RegistrarDocumentoDerivacion(objDocDerivacion);
                                        }
                                    }

                                    //se mueve el guardar en laserfiche, grabamos adjuntos dentro de la transacción.
                                }

                                if (objMovDocumento.LstDocAdjunto != null)
                                    foreach (var item in objMovDocumento.LstDocAdjunto)
                                    {
                                        if (item.CodLaserfiche > 0)
                                        {
                                            item.CodDocumento = objMovDocumento.CodDocumento;
                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            item.HostCreacion = objMovDocumento.HostCreacion;
                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
                                            item.RolCreacion = objMovDocumento.RolCreacion;
                                            onFlagOk = InsertarDocAdjunto(item, cmd);
                                        }
                                    }
                                if (objMovDocumento.LstDocAnexo != null)
                                {
                                    int cc = 1;
                                    foreach (var item in objMovDocumento.LstDocAnexo)
                                    {
                                        item.CodDocumentoMovimiento = codDocMovimiento;
                                        if (item.CodLaserfiche > 0)
                                        {
                                            var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                                            item.NombreDocumento = nombreAnexo;
                                            item.CodDocumento = objMovDocumento.CodDocumento;
                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            item.HostCreacion = objMovDocumento.HostCreacion;
                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
                                            item.RolCreacion = objMovDocumento.RolCreacion;
                                            item.TipoArchivo = Convert.ToInt32(TipoArchivos.Anexos);
                                            onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                        }
                                        cc++;
                                    }
                                }


                                if (onFlagOk >= 0)
                                {
                                    nRpta = idDocumento;
                                    outCorrelativoDoc = outCorrelativo;
                                    tx.Commit();
                                }
                                else { tx.Rollback(); rollback = true; }

                            }
                            else
                            {
                                tx.Rollback();
                                rollback = true;
                            }



                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            outCorrelativoDoc = "";
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();


                        }
                    }
                }
            }

            if (!rollback)
            {
                MoverArchivosTemporales(objMovDocumento);
            }
            else
            {
                LimpiarCarpeta(objMovDocumento);
            }

            return nRpta;
        }
        
        /// <summary>
        /// Método Público para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        public int RegistrarDocumentoRecepcionSIED(Documento objMovDocumento, out string outCorrelativoDoc)
        {
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            string guidTmp = "";
            try
            {
                guidTmp = GuardarArchivosDocumentoTemporales(objMovDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            int nRpta = 0;
            int onFlagOk = -1;
            var codDocMovimiento = 0;
            outCorrelativoDoc = "";
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            string outCorrelativo = "";
                            //Generando Secuencial
                            var onCodSecuencial = GenerarSecuencial(objMovDocumento, cmd, out outCorrelativo);

                            if (onCodSecuencial >= 0)
                            {
                                //Insertando Documentos
                                objMovDocumento.Secuencial = onCodSecuencial;
                                objMovDocumento.Correlativo = outCorrelativo;
                                EMaeTrabajador filtro = new EMaeTrabajador();
                                MaeTrabajadorDAO obj = new MaeTrabajadorDAO(cadenaConexion);
                                DocDerivadoDAO objDocDerivado = new DocDerivadoDAO(cadenaConexion);
                                /*SETEAR CODIGO TRABAJADOR*/
                                /*DocDerivadoDAO objDocDerivado = new DocDerivadoDAO(cadenaConexion);
                                ;
                                filtro.iCodArea = objMovDocumento.CodArea;
                                filtro.siEsJefe = 1;
                                var trabajadores = obj.GetTrabajadorPorArea(filtro);
                                int codTrabajador = 0;

                                codTrabajador = trabajadores.FirstOrDefault().iCodTrabajador;*/
                                /*if (codTrabajador == 0)
                                    throw new Exception("La área debe tener asignado un jefe");
                                objMovDocumento.CodTrabajador = codTrabajador;*/

                                onFlagOk = InsertarDocumentoRecepcionSIED(objMovDocumento, cmd);

                                int idDocumento = int.Parse(onFlagOk.ToString());
                                //Insertando Movimiento
                                if (onFlagOk >= 0)
                                {
                                    objMovDocumento.objDocMovimiento = new DocMovimiento();
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                    objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                    objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                    objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                    //objMovDocumento.objDocMovimiento.Nivel = 1;
                                    objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                    //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                    objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                    //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.CodTrabajador = objMovDocumento.CodTrabajador;
                                    objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;
                                    objMovDocumento.objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Registro);

                                    objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                    objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                    objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                    objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                    objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

                                    onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
                                    codDocMovimiento = onFlagOk;
                                    if (onFlagOk >= 0)
                                    {
                                        //Evaluando si hay comentarios
                                        if (objMovDocumento.objDocMovimiento.objDocComentario != null)
                                        {
                                            objMovDocumento.objDocMovimiento.objDocComentario.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.CodDocMovimiento = objMovDocumento.objDocMovimiento.CodDocMovimiento;
                                            objMovDocumento.objDocMovimiento.objDocComentario.HostCreacion = objMovDocumento.HostCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.RolCreacion = objMovDocumento.RolCreacion;
                                            onFlagOk = InsertarMovComentario(objMovDocumento.objDocMovimiento.objDocComentario, cmd);
                                        }
                                    }
                                    //Insertando CC
                                    if (onFlagOk >= 0 && objMovDocumento.CodAreaCC != null)
                                    {
                                        DocDerivacion objDocDerivacion;
                                        string[] arrCodAreaCC = objMovDocumento.CodAreaCC.Split('|');
                                        for (int i = 0; i < arrCodAreaCC.Length; i++)
                                        {
                                            filtro = new EMaeTrabajador();
                                            filtro.iCodArea = short.Parse(arrCodAreaCC[i]);
                                            filtro.siEsJefe = 1;
                                            var trabajadoresCC = obj.GetTrabajadorPorArea(filtro);
                                            int codTrabajadorCC = 0;

                                            codTrabajadorCC = trabajadoresCC.FirstOrDefault().iCodTrabajador;

                                            objDocDerivacion = new DocDerivacion();
                                            objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                            objDocDerivacion.CodArea = short.Parse(arrCodAreaCC[i]);
                                            objDocDerivacion.CodResponsable = codTrabajadorCC;
                                            objDocDerivacion.TipoAcceso = (short)Convert.ToInt32(EnumTipoAcceso.ConCopia);
                                            objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
                                            objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
                                            objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = objDocDerivado.RegistrarDocumentoDerivacion(objDocDerivacion);
                                        }
                                    }

                                    //se mueve el guardar en laserfiche, grabamos adjuntos dentro de la transacción.
                                }

                                if (objMovDocumento.LstDocAdjunto != null)
                                    foreach (var item in objMovDocumento.LstDocAdjunto)
                                    {
                                        if (item.CodLaserfiche > 0)
                                        {
                                            item.CodDocumento = objMovDocumento.CodDocumento;
                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            item.HostCreacion = objMovDocumento.HostCreacion;
                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
                                            item.RolCreacion = objMovDocumento.RolCreacion;
                                            onFlagOk = InsertarDocAdjunto(item, cmd);
                                        }
                                    }
                                if (objMovDocumento.LstDocAnexo != null)
                                {
                                    int cc = 1;
                                foreach (var item in objMovDocumento.LstDocAnexo)
                                    {
                                        item.CodDocumentoMovimiento = codDocMovimiento;
                                        if (item.CodLaserfiche > 0)
                                        {
                                            var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                                            item.NombreDocumento = nombreAnexo;
                                            item.CodDocumento = objMovDocumento.CodDocumento;
                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            item.HostCreacion = objMovDocumento.HostCreacion;
                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
                                            item.RolCreacion = objMovDocumento.RolCreacion;
                                            item.TipoArchivo = Convert.ToInt32(TipoArchivos.Anexos);
                                            onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                        }
                                    cc++;
                                }
                            }


                            if (onFlagOk >= 0)
                                {
                                    nRpta = idDocumento;
                                    outCorrelativoDoc = outCorrelativo;
                                    tx.Commit();
                                }
                                else { tx.Rollback(); rollback = true; }

                            }
                            else
                            {
                                tx.Rollback();
                                rollback = true;
                            }



                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            outCorrelativoDoc = "";
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();


                        }
                    }
                }
            }

            if (!rollback)
            {
                MoverArchivosTemporales(objMovDocumento);
            }
            else
            {
                LimpiarCarpeta(objMovDocumento);
            }

            return nRpta;
        }
        /// <summary>
        /// Método Público para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        public int RegistrarDocumentoBandejaEntrada(Documento objMovDocumento, out string outCorrelativoDoc)
        {
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            string guidTmp = "";
            try
            {
                guidTmp = GuardarArchivosDocumentoTemporales(objMovDocumento);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
                throw new Exception("No se puede establecer conexión con el servidor de archivos.");
            }
            int nRpta = 0;
            int onFlagOk = -1;
            var codDocMovimiento = 0;
            outCorrelativoDoc = "";
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            string outCorrelativo = "";
                            //Generando Secuencial
                            var onCodSecuencial = GenerarSecuencial(objMovDocumento, cmd, out outCorrelativo);

                            if (onCodSecuencial >= 0)
                            {
                                //Insertando Documentos
                                objMovDocumento.Secuencial = onCodSecuencial;
                                objMovDocumento.Correlativo = outCorrelativo;
                                EMaeTrabajador filtro = new EMaeTrabajador();
                                MaeTrabajadorDAO obj = new MaeTrabajadorDAO(cadenaConexion);
                                DocDerivadoDAO objDocDerivado = new DocDerivadoDAO(cadenaConexion);
                                /*SETEAR CODIGO TRABAJADOR*/
                                /*                                    
                                   filtro.iCodArea = objMovDocumento.CodArea;
                                   filtro.siEsJefe = 1;
                                   var trabajadores = obj.GetTrabajadorPorArea(filtro);
                                   int codTrabajador = 0;*/

                                /* foreach (var item in trabajadores)
                                 {
                                     DSegAsignacion db2 = new DSegAsignacion();
                                     Entidades.EntidadesDb2.ESegAsignacion eSeg = new Entidades.EntidadesDb2.ESegAsignacion();
                                     eSeg.webusr = item.WEBUSR;

                                     var asignacion = db2.ListaSegAsignacion(eSeg).FirstOrDefault();

                                     if (asignacion.vCodPerfil == "JEFE")
                                     {
                                         codTrabajador = item.iCodTrabajador;
                                         break;
                                     }
                                 }*/

                                /* codTrabajador = trabajadores.FirstOrDefault().iCodTrabajador;
                                 if (codTrabajador == 0)
                                     throw new Exception("La área debe tener asignado un jefe");
                                 objMovDocumento.CodTrabajador = codTrabajador;*/

                                onFlagOk = InsertarDocumento(objMovDocumento, cmd);

                                int idDocumento = int.Parse(onFlagOk.ToString());
                                //Insertando Movimiento
                                if (onFlagOk >= 0)
                                {
                                    objMovDocumento.objDocMovimiento = new DocMovimiento();
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                    objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                    objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                    objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                    //objMovDocumento.objDocMovimiento.Nivel = 1;
                                    objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                    //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                    objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                    //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.CodTrabajador = objMovDocumento.CodTrabajador;
                                    objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;
                                    objMovDocumento.objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Registro);

                                    objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                    objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                    objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                    objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                    objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

                                    onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
                                    codDocMovimiento = onFlagOk;
                                    if (onFlagOk >= 0)
                                    {
                                        //Evaluando si hay comentarios
                                        if (objMovDocumento.objDocMovimiento.objDocComentario != null)
                                        {
                                            objMovDocumento.objDocMovimiento.objDocComentario.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.CodDocMovimiento = objMovDocumento.objDocMovimiento.CodDocMovimiento;
                                            objMovDocumento.objDocMovimiento.objDocComentario.HostCreacion = objMovDocumento.HostCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.RolCreacion = objMovDocumento.RolCreacion;
                                            onFlagOk = InsertarMovComentario(objMovDocumento.objDocMovimiento.objDocComentario, cmd);
                                        }
                                    }
                                    //Insertando CC
                                    if (onFlagOk >= 0 && objMovDocumento.CodAreaCC != null)
                                    {
                                        DocDerivacion objDocDerivacion;
                                        string[] arrCodAreaCC = objMovDocumento.CodAreaCC.Split('|');
                                        for (int i = 0; i < arrCodAreaCC.Length; i++)
                                        {
                                            filtro = new EMaeTrabajador();
                                            filtro.iCodArea = short.Parse(arrCodAreaCC[i]);
                                            filtro.siEsJefe = 1;
                                            var trabajadoresCC = obj.GetTrabajadorPorArea(filtro);
                                            int codTrabajadorCC = 0;

                                            codTrabajadorCC = trabajadoresCC.FirstOrDefault().iCodTrabajador;

                                            objDocDerivacion = new DocDerivacion();
                                            objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                            objDocDerivacion.CodArea = short.Parse(arrCodAreaCC[i]);
                                            objDocDerivacion.CodResponsable = codTrabajadorCC;
                                            objDocDerivacion.TipoAcceso = (short)Convert.ToInt32(EnumTipoAcceso.ConCopia);
                                            objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
                                            objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
                                            objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = objDocDerivado.RegistrarDocumentoDerivacion(objDocDerivacion);
                                        }
                                    }
                                    //Insertando Derivador
                                   
                                    if (onFlagOk >= 0 && objMovDocumento.CodAreaBE != null)
                                    {
                                        DocDerivacion objDocDerivacion;
                                        string[] arrCodAreaBE = objMovDocumento.CodAreaBE.Split('|');
                                        for (int i = 0; i < arrCodAreaBE.Length; i++)
                                        {
                                            filtro = new EMaeTrabajador();
                                            filtro.iCodArea = short.Parse(arrCodAreaBE[i]);
                                            filtro.siEsJefe = 1;
                                            var trabajadoresBE = obj.GetTrabajadorPorArea(filtro);
                                            int codTrabajadorBE = 0;

                                            codTrabajadorBE = trabajadoresBE.FirstOrDefault().iCodTrabajador;

                                            objDocDerivacion = new DocDerivacion();
                                            objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                            objDocDerivacion.CodArea = short.Parse(arrCodAreaBE[i]);
                                            objDocDerivacion.CodResponsable = codTrabajadorBE;
                                            objDocDerivacion.TipoAcceso = (short)Convert.ToInt32(EnumTipoAcceso.Derivado);
                                            objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
                                            objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
                                            objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = objDocDerivado.RegistrarDocumentoDerivacion(objDocDerivacion);
                                            //INSERTANDO MOVIMIENTOS

                                            objMovDocumento.objDocMovimiento = new DocMovimiento();
                                            objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;
                                            objMovDocumento.objDocMovimiento.CodDocDerivacion = onFlagOk;
                                            objMovDocumento.objDocMovimiento.siEstadoDerivacion = Convert.ToInt16(EstadoDocumento.PENDIENTE);
                                            objMovDocumento.objDocMovimiento.EstadoDocumento = objMovDocumento.EstadoDocumento;
                                            objMovDocumento.objDocMovimiento.CodArea = short.Parse(arrCodAreaBE[i]);
                                            objMovDocumento.objDocMovimiento.CodAreaDerivacion = short.Parse(arrCodAreaBE[i]);
                                            objMovDocumento.objDocMovimiento.CodResponsableDerivacion = codTrabajadorBE;
                                            objMovDocumento.objDocMovimiento.CodTrabajador = codTrabajadorBE;
                                            objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                            objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                            objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                            objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                            objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                            objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                            objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                            objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
                                            var codMovimiento = onFlagOk;
                                            onFlagOk = InsertarCodigosInternos(objMovDocumento.CodAreaBE, codMovimiento, cmd);
                                        }
                                    }
                                    

                                    //se mueve el guardar en laserfiche, grabamos adjuntos dentro de la transacción.
                                }

                                if (objMovDocumento.LstDocAdjunto != null)
                                    foreach (var item in objMovDocumento.LstDocAdjunto)
                                    {
                                        if (item.CodLaserfiche > 0)
                                        {
                                            item.CodDocumento = objMovDocumento.CodDocumento;
                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            item.HostCreacion = objMovDocumento.HostCreacion;
                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
                                            item.RolCreacion = objMovDocumento.RolCreacion;
                                            onFlagOk = InsertarDocAdjunto(item, cmd);
                                        }
                                    }
                                if (objMovDocumento.LstDocAnexo != null)
                                {
                                    int cc = 1;
                                    foreach (var item in objMovDocumento.LstDocAnexo)
                                    {
                                        item.CodDocumentoMovimiento = codDocMovimiento;
                                        if (item.CodLaserfiche > 0)
                                        {
                                            var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                                            item.NombreDocumento = nombreAnexo;
                                            item.CodDocumento = objMovDocumento.CodDocumento;
                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            item.HostCreacion = objMovDocumento.HostCreacion;
                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
                                            item.RolCreacion = objMovDocumento.RolCreacion;
                                            item.TipoArchivo = Convert.ToInt32(TipoArchivos.Anexos);
                                            onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                        }
                                        cc++;
                                    }
                                }


                                if (onFlagOk >= 0)
                                {
                                    nRpta = idDocumento;
                                    outCorrelativoDoc = outCorrelativo;
                                    tx.Commit();
                                }
                                else { tx.Rollback(); rollback = true; }

                            }
                            else
                            {
                                tx.Rollback();
                                rollback = true;
                            }



                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            outCorrelativoDoc = "";
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();


                        }
                    }
                }
            }

            if (!rollback)
            {
                MoverArchivosTemporales(objMovDocumento);
            }
            else
            {
                LimpiarCarpeta(objMovDocumento);
            }

            return nRpta;
        }

        
        public int RegistrarDocumentoBandejaEnvioSIED(Documento objMovDocumento, out string outCorrelativoDoc)
        {
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            string guidTmp = "";
            try
            {
                guidTmp = GuardarArchivosDocumentoTemporales(objMovDocumento);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
                throw new Exception("No se puede establecer conexión con el servidor de archivos.");
            }
            int nRpta = 0;
            int onFlagOk = -1;
            var codDocMovimiento = 0;
            outCorrelativoDoc = "";
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            string outCorrelativo = "";
                            //Generando Secuencial
                            var onCodSecuencial = GenerarSecuencialSIED(objMovDocumento, cmd, out outCorrelativo);

                            if (onCodSecuencial >= 0)
                            {
                                //Insertando Documentos
                                objMovDocumento.Secuencial = onCodSecuencial;
                                objMovDocumento.Correlativo = outCorrelativo;
                                EMaeTrabajador filtro = new EMaeTrabajador();
                                MaeTrabajadorDAO obj = new MaeTrabajadorDAO(cadenaConexion);
                                DocDerivadoDAO objDocDerivado = new DocDerivadoDAO(cadenaConexion);
                                onFlagOk = InsertarDocumentoSIED(objMovDocumento, cmd);

                                int idDocumento = int.Parse(onFlagOk.ToString());
                                //Insertando Movimiento
                                if (onFlagOk >= 0)
                                {
                                    objMovDocumento.objDocMovimiento = new DocMovimiento();
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                    objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                    objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                    objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                    //objMovDocumento.objDocMovimiento.Nivel = 1;
                                    objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                    //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                    objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                    //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.CodTrabajador = objMovDocumento.CodTrabajador;
                                    objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;
                                    objMovDocumento.objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Registro);

                                    objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                    objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                    objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                    objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                    objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

                                    onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
                                    codDocMovimiento = onFlagOk;
                                    if (onFlagOk >= 0)
                                    {
                                        //Evaluando si hay comentarios
                                        if (objMovDocumento.objDocMovimiento.objDocComentario != null)
                                        {
                                            objMovDocumento.objDocMovimiento.objDocComentario.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.CodDocMovimiento = objMovDocumento.objDocMovimiento.CodDocMovimiento;
                                            objMovDocumento.objDocMovimiento.objDocComentario.HostCreacion = objMovDocumento.HostCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.RolCreacion = objMovDocumento.RolCreacion;
                                            onFlagOk = InsertarMovComentario(objMovDocumento.objDocMovimiento.objDocComentario, cmd);
                                        }
                                    }

                                    //Insertando Derivador

                                    if (onFlagOk >= 0 && objMovDocumento.CodAreaBE != null)
                                    {
                                        DocDerivacion objDocDerivacion;
                                        ///string[] arrCodAreaBE = objMovDocumento.CodAreaBE.Split('|');
                                        for (int i = 0; i < objMovDocumento.lstEmpresaSied.Count; i++)
                                        {

                                            objDocDerivacion = new DocDerivacion();
                                            objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                         //   objDocDerivacion.CodArea = short.Parse(arrCodAreaBE[i]);
                                         //   objDocDerivacion.CodResponsable = codTrabajadorBE;
                                            objDocDerivacion.TipoAcceso = (short)Convert.ToInt32(EnumTipoAcceso.EmpresaSIED);
                                            objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
                                            objDocDerivacion.CodEntidadSied = Convert.ToInt32(objMovDocumento.lstEmpresaSied[i].codEmpresa);
                                            objDocDerivacion.NombreEntidadSIED = objMovDocumento.lstEmpresaSied[i].nomEmpresaCorto;
                                            objDocDerivacion.FormatoDocSied = objMovDocumento.lstEmpresaSied[i].CodFormatoDocumentoEmpresas;
                                            objDocDerivacion.rucSied = objMovDocumento.lstEmpresaSied[i].rucEmpresa;
                                            objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
                                            objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = objDocDerivado.RegistrarDocumentoDerivacionSIED(objDocDerivacion);
                                            //INSERTANDO MOVIMIENTO
                                            objMovDocumento.objDocMovimiento = new DocMovimiento();
                                            objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;
                                            objMovDocumento.objDocMovimiento.CodDocDerivacion = onFlagOk;
                                            objMovDocumento.objDocMovimiento.siEstadoDerivacion = Convert.ToInt16(EstadoDocumento.PENDIENTE);
                                            objMovDocumento.objDocMovimiento.EstadoDocumento = objMovDocumento.EstadoDocumento;
                                            //objMovDocumento.objDocMovimiento.CodArea = short.Parse(arrCodAreaBE[i]);
                                            //objMovDocumento.objDocMovimiento.CodAreaDerivacion = short.Parse(arrCodAreaBE[i]);
                                            //objMovDocumento.objDocMovimiento.CodResponsableDerivacion = codTrabajadorBE;
                                            //objMovDocumento.objDocMovimiento.CodTrabajador = codTrabajadorBE;
                                            objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                            objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                            objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                            objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                            objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                            objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                            objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                            objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = InsertarMovDocumentoSIED(objMovDocumento.objDocMovimiento, cmd);
                                            var codMovimiento = onFlagOk;
                                            //onFlagOk = InsertarCodigosInternos(objMovDocumento.CodAreaBE, codMovimiento, cmd);
                                        }
                                    }


                                    //se mueve el guardar en laserfiche, grabamos adjuntos dentro de la transacción.
                                }

                                if (objMovDocumento.LstDocAdjunto != null)
                                    foreach (var item in objMovDocumento.LstDocAdjunto)
                                    {
                                        if (item.CodLaserfiche > 0)
                                        {
                                            item.CodDocumento = objMovDocumento.CodDocumento;
                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            item.HostCreacion = objMovDocumento.HostCreacion;
                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
                                            item.RolCreacion = objMovDocumento.RolCreacion;
                                            onFlagOk = InsertarDocAdjunto(item, cmd);
                                        }
                                    }
                                if (objMovDocumento.LstDocAnexo != null)
                                {
                                    int cc = 1;
                                    foreach (var item in objMovDocumento.LstDocAnexo)
                                    {
                                        item.CodDocumentoMovimiento = codDocMovimiento;
                                        if (item.CodLaserfiche > 0)
                                        {
                                            var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                                            item.NombreDocumento = nombreAnexo;
                                            item.CodDocumento = objMovDocumento.CodDocumento;
                                            item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            item.HostCreacion = objMovDocumento.HostCreacion;
                                            item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            item.LoginCreacion = objMovDocumento.LoginCreacion;
                                            item.RolCreacion = objMovDocumento.RolCreacion;
                                            item.TipoArchivo = Convert.ToInt32(TipoArchivos.Anexos);
                                            onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                        }
                                        cc++;
                                    }
                                }


                                if (onFlagOk >= 0)
                                {
                                    nRpta = idDocumento;
                                    outCorrelativoDoc = outCorrelativo;
                                    tx.Commit();
                                }
                                else { tx.Rollback(); rollback = true; }

                            }
                            else
                            {
                                tx.Rollback();
                                rollback = true;
                            }



                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            outCorrelativoDoc = "";
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();


                        }
                    }
                }
            }

            if (!rollback)
            {
                MoverArchivosTemporales(objMovDocumento);
            }
            else
            {
                LimpiarCarpeta(objMovDocumento);
            }

            return nRpta;
        }


        private void LimpiarCarpeta(Documento objMovDocumento)
        {
            if (!ExisteArchivo(objMovDocumento))
                return;

            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                if (objMovDocumento.LstDocAdjunto != null)
                    foreach (var item in objMovDocumento.LstDocAdjunto)
                        if (item.CodLaserfiche > 0)
                            lfhelper.DeleteDocumentIfExistv2(item.CodLaserfiche);
                if (objMovDocumento.LstDocAnexo != null)
                    foreach (var item in objMovDocumento.LstDocAnexo)
                        if (item.CodLaserfiche > 0)
                            lfhelper.DeleteDocumentIfExistv2(item.CodLaserfiche);
            }
        }
        private bool ExisteArchivo(Documento objMovDocumento)
        {
            bool existe = false;
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["IgnorarLaserFiche"]) == false)
            {
                if (objMovDocumento.LstDocAdjunto != null && objMovDocumento.LstDocAdjunto.Count > 0)
                    existe = objMovDocumento.LstDocAdjunto.Count > 0;
                if (objMovDocumento.LstDocAnexo != null && objMovDocumento.LstDocAnexo.Count > 0)
                    existe = objMovDocumento.LstDocAnexo.Count > 0;
            }
            return existe;
        }
        private string GuardarArchivosDocumentoTemporales(Documento objMovDocumento)
        {
            string guidTmp = Guid.NewGuid().ToString().Replace("-", "");
            try
            {
                if (!ExisteArchivo(objMovDocumento))
                    return "";
                objMovDocumento.Correlativo = guidTmp;
                string directoryPath = @String.Format(@"\{0}\SGDTEMP",
                                ApplicationKeys.LaserFicheFolderBase);

                using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
                {
                    lfhelper.GetOrCreateFolderInfo(directoryPath);
                    if (objMovDocumento.LstDocAdjunto != null && objMovDocumento.LstDocAdjunto.Count > 0)
                    {
                        foreach (var item in objMovDocumento.LstDocAdjunto)
                        {
                            string fileName = "";
                            byte[] bytes = Convert.FromBase64String(item.FileArray);
                            string documentPath = @String.Format(@"{0}\{1}.{2}",
                                directoryPath,
                                objMovDocumento.Correlativo,
                                Constantes.LaserficheFileExtensionDefault);

                            MemoryStream file = new MemoryStream(bytes);
                            item.CodLaserfiche = lfhelper.RegisterFileWithoutMetaData(
                                file,
                                documentPath,
                                ApplicationKeys.Laserfiche_REG,
                                out fileName,
                                contenType: TypesConstants.GetTypeMIME(Constantes.LaserficheFileExtensionDefault)
                                );
                        }
                    }
                    //ANEXOS
                    if (objMovDocumento.LstDocAnexo != null && objMovDocumento.LstDocAnexo.Count > 0)
                    {
                        int cc = 1;
                        foreach (var item in objMovDocumento.LstDocAnexo)
                        {
                            string fileName = "";
                            byte[] bytes = Convert.FromBase64String(item.FileArray);
                            var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                            string documentPath = String.Format(@"\{0}\{1}", directoryPath, nombreAnexo);

                            MemoryStream file = new MemoryStream(bytes);
                            item.CodLaserfiche = lfhelper.RegisterFileWithoutMetaData(
                                file,
                                documentPath,
                                ApplicationKeys.Laserfiche_REG,
                                out fileName,
                                contenType: TypesConstants.GetTypeMIME(Constantes.LaserficheFileExtensionDefault)
                                );
                            //Insertando Anexos
                            item.secuencia = cc;

                            cc++;
                        }
                    }

                    lfhelper.Dispose();
                }
                objMovDocumento.Correlativo = "";
            }
            catch (Exception e)
            {
                throw e;
            }
            return guidTmp;
        }

        private void MoverArchivosTemporales(Documento objMovDocumento)
        {
            if (!ExisteArchivo(objMovDocumento))
                return;
            string directoryPath = @String.Format(@"\{0}\{1}",
                            ApplicationKeys.LaserFicheFolderBase,
                            objMovDocumento.Correlativo);
            string directoryPathAnexos = @String.Format(@"\{0}\Anexos", directoryPath);

            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                lfhelper.GetOrCreateFolderInfo(directoryPath);
                lfhelper.GetOrCreateFolderInfo(directoryPathAnexos);
                if (objMovDocumento.LstDocAdjunto != null && objMovDocumento.LstDocAdjunto.Count > 0)
                {
                    foreach (var item in objMovDocumento.LstDocAdjunto)
                    {
                        string documentPath = @String.Format(@"{0}\{1}.{2}",
                            directoryPath,
                            objMovDocumento.Correlativo,
                            Constantes.LaserficheFileExtensionDefault);
                        lfhelper.MoveDocument(item.CodLaserfiche, documentPath);
                    }
                }
                //ANEXOS
                if (objMovDocumento.LstDocAnexo != null && objMovDocumento.LstDocAnexo.Count > 0)
                {
                    int cc = 1;
                    foreach (var item in objMovDocumento.LstDocAnexo)
                    {
                        var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                        string documentPath = String.Format(@"\{0}\{1}", directoryPathAnexos, nombreAnexo);
                        lfhelper.MoveDocument(item.CodLaserfiche, documentPath);
                        cc++;
                    }
                }
                lfhelper.Dispose();
            }
        }


        /// <summary>
        /// Método Público para el Anular un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        public int AnularDocumento(ComentarioDocumentoDTO anular)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_AnularDocumento]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                        param.Value = anular.CodDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vUsuAnulador", SqlDbType.VarChar);
                        param.Value = anular.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vComentario", SqlDbType.VarChar);
                        param.Value = anular.Comentario;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);


                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al anular el documento");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }

            return nRpta;
        }
        
         public int AnularDocumentoEnvioSIED(ComentarioDocumentoDTO anular)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_AnularDocumento]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                        param.Value = anular.CodDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vUsuAnulador", SqlDbType.VarChar);
                        param.Value = anular.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vComentario", SqlDbType.VarChar);
                        param.Value = anular.Comentario;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);


                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al anular el documento");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }

            return nRpta;
        }
        
        public int AnularDocumentoRecepcionSIED(ComentarioDocumentoDTO anular)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_AnularDocumento]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                        param.Value = anular.CodDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vUsuAnulador", SqlDbType.VarChar);
                        param.Value = anular.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vComentario", SqlDbType.VarChar);
                        param.Value = anular.Comentario;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);


                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al anular el documento");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }

            return nRpta;
        }
        public int FinalizarAux(int codDocumento, string comentario, string usuario, SqlCommand cmd, out string mensaje, out List<RegistrarDerivacionFinalizarDTO> listaDerivacionesPendientes)
        {
            listaDerivacionesPendientes = new List<RegistrarDerivacionFinalizarDTO>();
            int nRpta = 0;
            mensaje = "";

            cmd.CommandText = "[dbo].[UP_MOV_FinalizarDocumento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = codDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vUsuario", SqlDbType.VarChar);
            param.Value = usuario;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vComentario", SqlDbType.VarChar);
            param.Value = comentario;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 5000;
            param.ParameterName = "@onMensaje";
            cmd.Parameters.Add(param);

            //cmd.ExecuteNonQuery();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    var objEntidad = new RegistrarDerivacionFinalizarDTO();
                    if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                    if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocDerivacion = Convert.ToInt32(reader["iCodDocumentoDerivacion"]);
                    if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                    if (!Convert.IsDBNull(reader["iCodResponsable"])) objEntidad.CodResponsable = Convert.ToInt32(reader["iCodResponsable"]);

                    listaDerivacionesPendientes.Add(objEntidad);
                }
            }

            nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
            mensaje = Convert.ToString(cmd.Parameters["@onMensaje"].Value.ToString());

            return nRpta;
        }

        public int DevolverInternoAux(int codDocumento, string comentario, string usuario, SqlCommand cmd, out string mensaje, out List<RegistrarDerivacionFinalizarDTO> listaDerivacionesPendientes)
        {
            listaDerivacionesPendientes = new List<RegistrarDerivacionFinalizarDTO>();
            int nRpta = 0;
            mensaje = "";

            cmd.CommandText = "[dbo].[UP_MOV_DevolverDocumentoInterno]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = codDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vUsuario", SqlDbType.VarChar);
            param.Value = usuario;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 5000;
            param.ParameterName = "@onMensaje";
            cmd.Parameters.Add(param);

            //cmd.ExecuteNonQuery();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    var objEntidad = new RegistrarDerivacionFinalizarDTO();
                    if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                    if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocDerivacion = Convert.ToInt32(reader["iCodDocumentoDerivacion"]);
                    if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                    if (!Convert.IsDBNull(reader["iCodResponsable"])) objEntidad.CodResponsable = Convert.ToInt32(reader["iCodResponsable"]);

                    listaDerivacionesPendientes.Add(objEntidad);
                }
            }

            nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
            mensaje = Convert.ToString(cmd.Parameters["@onMensaje"].Value.ToString());

            return nRpta;
        }

        public int ValidarFinalizarAux(int codDocumento, string comentario, string usuario, SqlCommand cmd, out string mensaje, out string codDerivados)
        {
            int nRpta = 0;
            mensaje = "";
            codDerivados = "";
            cmd.CommandText = "[dbo].[UP_MOV_ValidarFinalizarDocumento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = codDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vUsuario", SqlDbType.VarChar);
            param.Value = usuario;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vComentario", SqlDbType.VarChar);
            param.Value = comentario;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 5000;
            param.ParameterName = "@onMensaje";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 5000;
            param.ParameterName = "@onCodDocDerivados";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
            mensaje = Convert.ToString(cmd.Parameters["@onMensaje"].Value.ToString());
            codDerivados = Convert.ToString(cmd.Parameters["@onCodDocDerivados"].Value.ToString());

            return nRpta;
        }

        
        public int ValidarDevolverInternoAux(int codDocumento, string comentario, string usuario, SqlCommand cmd, out string mensaje, out string codDerivados)
        {
            int nRpta = 0;
            mensaje = "";
            codDerivados = "";
            cmd.CommandText = "[dbo].[UP_MOV_ValidarDevolverDocumento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = codDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 5000;
            param.ParameterName = "@onMensaje";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 5000;
            param.ParameterName = "@onCodDocDerivados";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
            mensaje = Convert.ToString(cmd.Parameters["@onMensaje"].Value.ToString());
            codDerivados = Convert.ToString(cmd.Parameters["@onCodDocDerivados"].Value.ToString());

            return nRpta;
        }
        /// <summary>
        /// Método Público para finalizar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de finalizacion del Documento</returns>  
        public int FinalizarDocumento(ComentarioDocumentoDTO finalizar, out string mensaje)
        {
            mensaje = "";
            int nRpta = 0;
            int onFlagOk = -1;
            bool rollback = false;
            string guidTmp = "";
            string correlativo = "";
            int codDocMovimiento;
            int correlativoAdjuntoInicial = GetCorrelativoAnexosInicial(finalizar.CodDocumento, TipoArchivos.Adjuntos);
            Documento documento;
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            try
            {
                if (finalizar.LstDocAnexo != null)
                    guidTmp = docAnexoDAO.GuardarArchivosTemporalesAnexo(finalizar.LstDocAnexo, "ADJ", correlativoAdjuntoInicial);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
                throw new Exception("No se puede establecer conexión con el servidor de archivos.");
            }
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                var flagSied = GetFlagSIED(finalizar.CodDocumento);



                using (SqlCommand cmd = conn.CreateCommand())
                {
                    if(flagSied == Convert.ToInt32(FlagSIED.RECEPCION))
                    {
                        documento = GetDocumentoRecepcionSIED(new Documento()
                        {
                            CodDocumento = finalizar.CodDocumento
                        });
                    }
                    else
                    {
                        documento = GetDocumento(new Documento()
                        {
                            CodDocumento = finalizar.CodDocumento
                        });
                    }
                    
                }
                correlativo = documento.Correlativo;

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;

                            List<RegistrarDerivacionFinalizarDTO> listaDerivacionesPendientes;
                            onFlagOk = FinalizarAux(finalizar.CodDocumento,
                                finalizar.Comentario, finalizar.UsuarioCreacion, cmd, out mensaje, out listaDerivacionesPendientes);

                            listaDerivacionesPendientes.ForEach(x =>
                            {
                                x.UsuarioCreacion = finalizar.UsuarioCreacion;
                                x.HostCreacion = finalizar.HostCreacion;
                                x.InstanciaCreacion = finalizar.InstanciaCreacion;
                                x.LoginCreacion = finalizar.LoginCreacion;
                                x.RolCreacion = finalizar.RolCreacion;
                                x.Comentario = Constantes.MENSAJE_ATENIDO_AUTOMATICO;
                            });
                            if (listaDerivacionesPendientes.Count > 0)
                            {
                                onFlagOk = AtenderDerivacionesPendientes(listaDerivacionesPendientes, documento, cmd);
                            }
                            nRpta = onFlagOk;
                            if (onFlagOk > 0)
                            {
                                var objDocMovimiento = new DocMovimiento();
                                objDocMovimiento.CodArea = documento.CodArea;
                                objDocMovimiento.FechaRecepcion = documento.FechaRecepcion;
                                objDocMovimiento.FechaLectura = documento.FechaDocumento;
                                objDocMovimiento.Plazo = documento.Plazo;
                                objDocMovimiento.FechaPlazo = documento.FechaPlazo;
                                //objMovDocumento.objDocMovimiento.Nivel = 1;
                                objDocMovimiento.IndicadorFirma = 0;
                                //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                objDocMovimiento.IndicadorVisado = 0;
                                //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                objDocMovimiento.CodTrabajador = documento.CodTrabajador;
                                objDocMovimiento.CodDocumento = documento.CodDocumento;
                                objDocMovimiento.CodMovAnterior = documento.CodUltimoMovimiento;
                                objDocMovimiento.EstadoDocumento = Convert.ToInt16(EstadoDocumento.ATENDIDO);
                                objDocMovimiento.UsuarioCreacion = finalizar.UsuarioCreacion;
                                objDocMovimiento.HostCreacion = finalizar.HostCreacion;
                                objDocMovimiento.InstanciaCreacion = finalizar.InstanciaCreacion;
                                objDocMovimiento.LoginCreacion = finalizar.LoginCreacion;
                                objDocMovimiento.RolCreacion = finalizar.RolCreacion;
                                objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Finalizar);
                                onFlagOk = InsertarMovDocumento(objDocMovimiento, cmd);
                                codDocMovimiento = onFlagOk;
                                if (onFlagOk > 0)
                                {
                                    //Evaluando si hay comentarios
                                    objDocMovimiento.objDocComentario = new DocComentario();
                                    objDocMovimiento.objDocComentario.Comentario = finalizar.Comentario;
                                    if (objDocMovimiento.objDocComentario != null)
                                    {
                                        objDocMovimiento.objDocComentario.CodDocMovimiento = objDocMovimiento.CodDocMovimiento;
                                        objDocMovimiento.objDocComentario.CodDocumento = objDocMovimiento.CodDocumento;
                                        objDocMovimiento.objDocComentario.UsuarioCreacion = finalizar.UsuarioCreacion;
                                        objDocMovimiento.objDocComentario.HostCreacion = finalizar.HostCreacion;
                                        objDocMovimiento.objDocComentario.InstanciaCreacion = finalizar.InstanciaCreacion;
                                        objDocMovimiento.objDocComentario.LoginCreacion = finalizar.LoginCreacion;
                                        objDocMovimiento.objDocComentario.RolCreacion = finalizar.RolCreacion;
                                        //objDocMovimiento.objDocComentario.CodDocDerivacion = finalizar.CodDocDerivacion;
                                        onFlagOk = InsertarMovComentario(objDocMovimiento.objDocComentario, cmd);
                                    }
                                    if (finalizar.LstDocAnexo != null)
                                    {
                                        int cc = correlativoAdjuntoInicial;
                                        foreach (DocAnexo item in finalizar.LstDocAnexo)
                                        {
                                            item.CodDocumentoMovimiento = codDocMovimiento;
                                            if (item.CodLaserfiche > 0)
                                            {
                                                item.CodDocumento = finalizar.CodDocumento;
                                                item.UsuarioCreacion = finalizar.UsuarioCreacion;
                                                item.HostCreacion = finalizar.HostCreacion;
                                                item.InstanciaCreacion = finalizar.InstanciaCreacion;
                                                item.LoginCreacion = finalizar.LoginCreacion;
                                                item.RolCreacion = finalizar.RolCreacion;
                                                item.secuencia = cc;
                                                item.TipoArchivo = Convert.ToInt32(TipoArchivos.Adjuntos);
                                                onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                            }
                                        }
                                    }
                                }
                            }
                            if (onFlagOk > 0)
                            {
                                nRpta = finalizar.CodDocumento;
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }

            if (!rollback)
            {
                docAnexoDAO.MoverArchivosAnexosTemporales(finalizar.LstDocAnexo, correlativo, "Adjuntos", "ADJ", correlativoAdjuntoInicial);
            }
            else
            {
                docAnexoDAO.LimpiarCarpetaAnexos(finalizar.LstDocAnexo);
            }
            return nRpta;

        }


        /// <summary>
        /// Método Público para finalizar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de finalizacion del Documento</returns>  
        public int FinalizarDocumentoBandejaEntrada(ComentarioDocumentoDTO finalizar, out string mensaje)
        {
            mensaje = "";
            int nRpta = 0;
            int onFlagOk = -1;
            bool rollback = false;
            string guidTmp = "";
            string correlativo = "";
            int codDocMovimiento;
            int correlativoAdjuntoInicial = GetCorrelativoAnexosInicial(finalizar.CodDocumento, TipoArchivos.Adjuntos);
            Documento documento;
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            try
            {
                if (finalizar.LstDocAnexo != null)
                    guidTmp = docAnexoDAO.GuardarArchivosTemporalesAnexo(finalizar.LstDocAnexo, "ADJ", correlativoAdjuntoInicial);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
                throw new Exception("No se puede establecer conexión con el servidor de archivos.");
            }
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    var flagSied = GetFlagSIED(finalizar.CodDocumento);

                    if(flagSied == Convert.ToInt32(FlagSIED.RECEPCION))
                    {
                        documento = GetDocumentoRecepcionSIED(new Documento()
                        {
                            CodDocumento = finalizar.CodDocumento
                        });
                    }
                    else
                    {
                        documento = GetDocumento(new Documento()
                        {
                            CodDocumento = finalizar.CodDocumento
                        });
                    }

                }
                correlativo = documento.Correlativo;

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;

                            List<RegistrarDerivacionFinalizarDTO> listaDerivacionesPendientes;
                            onFlagOk = FinalizarAux(finalizar.CodDocumento,
                                finalizar.Comentario, finalizar.UsuarioCreacion, cmd, out mensaje, out listaDerivacionesPendientes);

                            listaDerivacionesPendientes.ForEach(x =>
                            {
                                x.UsuarioCreacion = finalizar.UsuarioCreacion;
                                x.HostCreacion = finalizar.HostCreacion;
                                x.InstanciaCreacion = finalizar.InstanciaCreacion;
                                x.LoginCreacion = finalizar.LoginCreacion;
                                x.RolCreacion = finalizar.RolCreacion;
                                x.Comentario = Constantes.MENSAJE_ATENIDO_AUTOMATICO_INTERNO;
                            });
                            if (listaDerivacionesPendientes.Count > 0)
                            {
                                onFlagOk = AtenderDerivacionesPendientes(listaDerivacionesPendientes, documento, cmd);
                            }
                            nRpta = onFlagOk;
                            if (onFlagOk > 0)
                            {
                                var objDocMovimiento = new DocMovimiento();
                                objDocMovimiento.CodArea = documento.CodArea;
                                objDocMovimiento.FechaRecepcion = documento.FechaRecepcion;
                                objDocMovimiento.FechaLectura = documento.FechaDocumento;
                                objDocMovimiento.Plazo = documento.Plazo;
                                objDocMovimiento.FechaPlazo = documento.FechaPlazo;
                                //objMovDocumento.objDocMovimiento.Nivel = 1;
                                objDocMovimiento.IndicadorFirma = 0;
                                //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                objDocMovimiento.IndicadorVisado = 0;
                                //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                objDocMovimiento.CodTrabajador = documento.CodTrabajador;
                                objDocMovimiento.CodDocumento = documento.CodDocumento;
                                objDocMovimiento.CodMovAnterior = documento.CodUltimoMovimiento;
                                objDocMovimiento.EstadoDocumento = Convert.ToInt16(EstadoDocumento.ATENDIDO);
                                objDocMovimiento.UsuarioCreacion = finalizar.UsuarioCreacion;
                                objDocMovimiento.HostCreacion = finalizar.HostCreacion;
                                objDocMovimiento.InstanciaCreacion = finalizar.InstanciaCreacion;
                                objDocMovimiento.LoginCreacion = finalizar.LoginCreacion;
                                objDocMovimiento.RolCreacion = finalizar.RolCreacion;
                                objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Finalizar);
                                onFlagOk = InsertarMovDocumento(objDocMovimiento, cmd);
                                codDocMovimiento = onFlagOk;
                                if (onFlagOk > 0)
                                {
                                    //Evaluando si hay comentarios
                                    objDocMovimiento.objDocComentario = new DocComentario();
                                    objDocMovimiento.objDocComentario.Comentario = finalizar.Comentario;
                                    if (objDocMovimiento.objDocComentario != null)
                                    {
                                        objDocMovimiento.objDocComentario.CodDocMovimiento = objDocMovimiento.CodDocMovimiento;
                                        objDocMovimiento.objDocComentario.CodDocumento = objDocMovimiento.CodDocumento;
                                        objDocMovimiento.objDocComentario.UsuarioCreacion = finalizar.UsuarioCreacion;
                                        objDocMovimiento.objDocComentario.HostCreacion = finalizar.HostCreacion;
                                        objDocMovimiento.objDocComentario.InstanciaCreacion = finalizar.InstanciaCreacion;
                                        objDocMovimiento.objDocComentario.LoginCreacion = finalizar.LoginCreacion;
                                        objDocMovimiento.objDocComentario.RolCreacion = finalizar.RolCreacion;
                                        //objDocMovimiento.objDocComentario.CodDocDerivacion = finalizar.CodDocDerivacion;
                                        onFlagOk = InsertarMovComentario(objDocMovimiento.objDocComentario, cmd);
                                    }
                                    if (finalizar.LstDocAnexo != null)
                                    {
                                        int cc = correlativoAdjuntoInicial;
                                        foreach (DocAnexo item in finalizar.LstDocAnexo)
                                        {
                                            item.CodDocumentoMovimiento = codDocMovimiento;
                                            if (item.CodLaserfiche > 0)
                                            {
                                                item.CodDocumento = finalizar.CodDocumento;
                                                item.UsuarioCreacion = finalizar.UsuarioCreacion;
                                                item.HostCreacion = finalizar.HostCreacion;
                                                item.InstanciaCreacion = finalizar.InstanciaCreacion;
                                                item.LoginCreacion = finalizar.LoginCreacion;
                                                item.RolCreacion = finalizar.RolCreacion;
                                                item.secuencia = cc;
                                                item.TipoArchivo = Convert.ToInt32(TipoArchivos.Adjuntos);
                                                onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                            }
                                        }
                                    }
                                }
                            }
                            if (onFlagOk > 0)
                            {
                                nRpta = finalizar.CodDocumento;
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }

            if (!rollback)
            {
                docAnexoDAO.MoverArchivosAnexosTemporales(finalizar.LstDocAnexo, correlativo, "Adjuntos", "ADJ", correlativoAdjuntoInicial);
            }
            else
            {
                docAnexoDAO.LimpiarCarpetaAnexos(finalizar.LstDocAnexo);
            }
            return nRpta;

        }


        private int AtenderDerivacionesPendientes(List<RegistrarDerivacionFinalizarDTO> listaDerivacionesPendientes, Documento documento, SqlCommand cmd)
        {
            int onFlagOk = 0;
            foreach (var reg in listaDerivacionesPendientes)
            {

                onFlagOk = CambiarEstadoDocumentoDerivado(reg.CodDocDerivacion.Value,
                    Convert.ToInt16(EstadoDocumento.ATENDIDO), reg.UsuarioCreacion, cmd);

                if (onFlagOk > 0)
                {
                    var objDocMovimiento = new DocMovimiento();
                    objDocMovimiento.CodArea = documento.CodArea;
                    objDocMovimiento.FechaRecepcion = documento.FechaRecepcion;
                    objDocMovimiento.FechaLectura = documento.FechaDocumento;
                    objDocMovimiento.Plazo = documento.Plazo;
                    objDocMovimiento.FechaPlazo = documento.FechaPlazo;
                    //objMovDocumento.objDocMovimiento.Nivel = 1;
                    objDocMovimiento.IndicadorFirma = 0;
                    //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                    objDocMovimiento.IndicadorVisado = 0;
                    //objMovDocumento.objDocMovimiento.FechaFirma = null;
                    objDocMovimiento.CodTrabajador = documento.CodTrabajador;
                    objDocMovimiento.CodDocumento = documento.CodDocumento;
                    objDocMovimiento.CodMovAnterior = documento.CodUltimoMovimiento;
                    objDocMovimiento.EstadoDocumento = documento.EstadoDocumento;//Convert.ToInt16(EstadoDocumento.EN_PROCESO);
                    objDocMovimiento.UsuarioCreacion = reg.UsuarioCreacion;
                    objDocMovimiento.HostCreacion = reg.HostCreacion;
                    objDocMovimiento.InstanciaCreacion = reg.InstanciaCreacion;
                    objDocMovimiento.LoginCreacion = reg.LoginCreacion;
                    objDocMovimiento.RolCreacion = reg.RolCreacion;
                    objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.FinalizarAutomatico);
                    objDocMovimiento.Estado = 1;

                    objDocMovimiento.CodDocDerivacion = reg.CodDocDerivacion;
                    objDocMovimiento.CodAreaDerivacion = reg.CodArea;
                    objDocMovimiento.CodResponsableDerivacion = reg.CodResponsable;
                    objDocMovimiento.siEstadoDerivacion = Convert.ToInt16(EstadoDocumento.ATENDIDO);


                    onFlagOk = InsertarMovDocumento(objDocMovimiento, cmd);
                    var codDocMovimiento = onFlagOk;
                    if (onFlagOk > 0)
                    {
                        //Evaluando si hay comentarios
                        objDocMovimiento.objDocComentario = new DocComentario();
                        objDocMovimiento.objDocComentario.Comentario = reg.Comentario;
                        if (objDocMovimiento.objDocComentario != null)
                        {
                            objDocMovimiento.objDocComentario.CodDocMovimiento = objDocMovimiento.CodDocMovimiento;
                            objDocMovimiento.objDocComentario.CodDocumento = objDocMovimiento.CodDocumento;
                            objDocMovimiento.objDocComentario.UsuarioCreacion = reg.UsuarioCreacion;
                            objDocMovimiento.objDocComentario.HostCreacion = reg.HostCreacion;
                            objDocMovimiento.objDocComentario.InstanciaCreacion = reg.InstanciaCreacion;
                            objDocMovimiento.objDocComentario.LoginCreacion = reg.LoginCreacion;
                            objDocMovimiento.objDocComentario.RolCreacion = reg.RolCreacion;
                            objDocMovimiento.objDocComentario.CodDocDerivacion = reg.CodDocDerivacion;
                            onFlagOk = InsertarMovComentario(objDocMovimiento.objDocComentario, cmd);
                        }

                    }
                }
            }
            return onFlagOk;
        }

        private int DevolverDerivacionesPendientes(List<RegistrarDerivacionFinalizarDTO> listaDerivacionesPendientes, Documento documento, SqlCommand cmd)
        {
            int onFlagOk = 0;
            foreach (var reg in listaDerivacionesPendientes)
            {

                onFlagOk = CambiarEstadoDocumentoDerivado(reg.CodDocDerivacion.Value,
                    Convert.ToInt16(EstadoDocumento.DEVUELTO), reg.UsuarioCreacion, cmd);

                if (onFlagOk > 0)
                {
                    var objDocMovimiento = new DocMovimiento();
                    objDocMovimiento.CodArea = documento.CodArea;
                    objDocMovimiento.FechaRecepcion = documento.FechaRecepcion;
                    objDocMovimiento.FechaLectura = documento.FechaDocumento;
                    objDocMovimiento.Plazo = documento.Plazo;
                    objDocMovimiento.FechaPlazo = documento.FechaPlazo;
                    //objMovDocumento.objDocMovimiento.Nivel = 1;
                    objDocMovimiento.IndicadorFirma = 0;
                    //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                    objDocMovimiento.IndicadorVisado = 0;
                    //objMovDocumento.objDocMovimiento.FechaFirma = null;
                    objDocMovimiento.CodTrabajador = documento.CodTrabajador;
                    objDocMovimiento.CodDocumento = documento.CodDocumento;
                    objDocMovimiento.CodMovAnterior = documento.CodUltimoMovimiento;
                    objDocMovimiento.EstadoDocumento = Convert.ToInt16(EstadoDocumento.DEVUELTO);//Convert.ToInt16(EstadoDocumento.EN_PROCESO);
                    objDocMovimiento.UsuarioCreacion = reg.UsuarioCreacion;
                    objDocMovimiento.HostCreacion = reg.HostCreacion;
                    objDocMovimiento.InstanciaCreacion = reg.InstanciaCreacion;
                    objDocMovimiento.LoginCreacion = reg.LoginCreacion;
                    objDocMovimiento.RolCreacion = reg.RolCreacion;
                    objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.DevolverAutomatico);
                    objDocMovimiento.Estado = 1;

                    objDocMovimiento.CodDocDerivacion = reg.CodDocDerivacion;
                    objDocMovimiento.CodAreaDerivacion = reg.CodArea;
                    objDocMovimiento.CodResponsableDerivacion = reg.CodResponsable;
                    objDocMovimiento.siEstadoDerivacion = Convert.ToInt16(EstadoDocumento.DEVUELTO);


                    onFlagOk = InsertarMovDocumento(objDocMovimiento, cmd);
                    var codDocMovimiento = onFlagOk;
                    if (onFlagOk > 0)
                    {
                        //Evaluando si hay comentarios
                        objDocMovimiento.objDocComentario = new DocComentario();
                        objDocMovimiento.objDocComentario.Comentario = reg.Comentario;
                        if (objDocMovimiento.objDocComentario != null)
                        {
                            objDocMovimiento.objDocComentario.CodDocMovimiento = objDocMovimiento.CodDocMovimiento;
                            objDocMovimiento.objDocComentario.CodDocumento = objDocMovimiento.CodDocumento;
                            objDocMovimiento.objDocComentario.UsuarioCreacion = reg.UsuarioCreacion;
                            objDocMovimiento.objDocComentario.HostCreacion = reg.HostCreacion;
                            objDocMovimiento.objDocComentario.InstanciaCreacion = reg.InstanciaCreacion;
                            objDocMovimiento.objDocComentario.LoginCreacion = reg.LoginCreacion;
                            objDocMovimiento.objDocComentario.RolCreacion = reg.RolCreacion;
                            objDocMovimiento.objDocComentario.CodDocDerivacion = reg.CodDocDerivacion;
                            onFlagOk = InsertarMovComentario(objDocMovimiento.objDocComentario, cmd);
                        }

                    }
                }
            }
            return onFlagOk;
        }

        /// <summary>
        /// Método Público para finalizar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de finalizacion del Documento</returns>  
        public int ValidarFinalizarDocumento(ComentarioDocumentoDTO finalizar, out string mensaje, out string codDerivados)
        {
            mensaje = "";
            int nRpta = 0;
            int onFlagOk = -1;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    onFlagOk = ValidarFinalizarAux(finalizar.CodDocumento,
                        finalizar.Comentario, finalizar.UsuarioCreacion, cmd, out mensaje,out codDerivados);
                    nRpta = onFlagOk;

                }
            }
            return nRpta;

        }
        
        /// <summary>
        /// Método Público para finalizar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de finalizacion del Documento</returns>  
        public int ValidarFirmante(int codDocumento, int codTrabajador)
        {
            int nRpta = 0;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                    cmd.CommandText = "[dbo].[UP_MOV_ValidarFirmante]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();

                    SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                    param.Value = codDocumento;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter("@iCodTrabajador", SqlDbType.VarChar);
                    param.Value = codTrabajador;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.Direction = ParameterDirection.Output;
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@onFirma";
                    cmd.Parameters.Add(param);


                    cmd.ExecuteNonQuery();

                    nRpta = Convert.ToInt32(cmd.Parameters["@onFirma"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al ValidarFirmante");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return nRpta;

        }


        /// <summary>
        /// Método Público para Actualizar el estado de un Documento en Firma
        /// </summary>
        /// <returns>Retorna el Indicador de Atencion de Documento Firma</returns>  
        public int DocumentoAtencionFirma(int codDocumento, int Atencion)
        {
            int nRpta = 0;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_ActualizarEstadoFirmaDocumento]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@CodDocumento", SqlDbType.Int);
                        param.Value = codDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@Atencion", SqlDbType.Int);
                        param.Value = Atencion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onRpta";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onRpta"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al DocumentoAtencionFirma");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return nRpta;

        }

        
        /// <summary>
        /// Método Público para Obtener el estado de Firma Documento
        /// </summary>
        /// <returns>Retorna el estado Documento Firma</returns>  
        public int ObtenerEstadoFirmaDocumento(int codDocumento)
        {
            int nRpta = 0;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_ObtenerEstadoFirmaDocumento]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@CodDocumento", SqlDbType.Int);
                        param.Value = codDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onRpta";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onRpta"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al ObtenerEstadoFirmaDocumento");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return nRpta;

        }

        /// <summary>
        /// Interface que Obtiene la cantidad de firmas digitales pro documento
        /// </summary>
        /// <returns>Retorna la cantidad de firmas</returns>     
        public int ObtenerCantidadFirmas(int codDocumento, out int posx, out int posy)
        {
            int nRpta = 0;
                
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_ObtenerCantidadFirmasDocumento]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@CodDocumento", SqlDbType.Int);
                        param.Value = codDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onCantFirma";
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onPosx";
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onPosy";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onCantFirma"].Value.ToString());
                        posx = Convert.ToInt32(cmd.Parameters["@onPosx"].Value.ToString());
                        posy = Convert.ToInt32(cmd.Parameters["@onPosy"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al ObtenerCantidadFirmas");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return nRpta;

        }
        /// <summary>
        /// Método Público para finalizar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de finalizacion del Documento</returns>  
        public int ValidarDevolverDocumento(ComentarioDocumentoDTO finalizar, out string mensaje, out string codDerivados)
        {
            mensaje = "";
            int nRpta = 0;
            int onFlagOk = -1;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    onFlagOk = ValidarDevolverInternoAux(finalizar.CodDocumento,
                        finalizar.Comentario, finalizar.UsuarioCreacion, cmd, out mensaje, out codDerivados);
                    nRpta = onFlagOk;

                }
            }
            return nRpta;

        }


        /// <summary>
        /// Método Público para la devolucion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de devolucion del Documento</returns>  
        public int DevolverDocumento(ComentarioDocumentoDTO anular)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_DevolverDocumento]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                        param.Value = anular.CodDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@iCodDocumentoDerivacion", SqlDbType.Int);
                        param.Value = anular.CodDocDerivacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vUsuario", SqlDbType.VarChar);
                        param.Value = anular.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vComentario", SqlDbType.VarChar);
                        param.Value = anular.Comentario;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);


                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al devolver el documento");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para Devolver un Documento Interno
        /// </summary>
        /// <returns>Retorna el Indicador de finalizacion del Documento</returns>  
        public int DevolverDocumentoInterno(ComentarioDocumentoDTO devolver)
        {
            string mensaje = "";
            int nRpta = 0;
            int onFlagOk = -1;
            bool rollback = false;
            string correlativo = "";
            int codDocMovimiento;
            Documento documento;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                var flagSied = GetFlagSIED(devolver.CodDocumento);
                
                if(flagSied == Convert.ToInt32(FlagSIED.RECEPCION))
                {
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        documento = GetDocumentoRecepcionSIED(new Documento()
                        {
                            CodDocumento = devolver.CodDocumento
                        });
                    }
                }
                else
                {
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        documento = GetDocumento(new Documento()
                        {
                            CodDocumento = devolver.CodDocumento
                        });
                    }
                }

               
                correlativo = documento.Correlativo;

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;

                            List<RegistrarDerivacionFinalizarDTO> listaDerivacionesPendientes;
                            onFlagOk = DevolverInternoAux(devolver.CodDocumento,
                                devolver.Comentario, devolver.UsuarioCreacion, cmd, out mensaje, out listaDerivacionesPendientes);

                            listaDerivacionesPendientes.ForEach(x =>
                            {
                                x.UsuarioCreacion = devolver.UsuarioCreacion;
                                x.HostCreacion = devolver.HostCreacion;
                                x.InstanciaCreacion = devolver.InstanciaCreacion;
                                x.LoginCreacion = devolver.LoginCreacion;
                                x.RolCreacion = devolver.RolCreacion;
                                x.Comentario = Constantes.MENSAJE_DEVUELTO_AUTOMATICO_DEVUELTO;
                            });
                            if (listaDerivacionesPendientes.Count > 0)
                            {
                                onFlagOk = DevolverDerivacionesPendientes(listaDerivacionesPendientes, documento, cmd);
                            }
                            nRpta = onFlagOk;
                            if (onFlagOk > 0)
                            {
                                var objDocMovimiento = new DocMovimiento();
                                objDocMovimiento.CodArea = documento.CodArea;
                                objDocMovimiento.FechaRecepcion = documento.FechaRecepcion;
                                objDocMovimiento.FechaLectura = documento.FechaDocumento;
                                objDocMovimiento.Plazo = documento.Plazo;
                                objDocMovimiento.FechaPlazo = documento.FechaPlazo;
                                //objMovDocumento.objDocMovimiento.Nivel = 1;
                                objDocMovimiento.IndicadorFirma = 0;
                                //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                objDocMovimiento.IndicadorVisado = 0;
                                //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                objDocMovimiento.CodTrabajador = documento.CodTrabajador;
                                objDocMovimiento.CodDocumento = documento.CodDocumento;
                                objDocMovimiento.CodMovAnterior = documento.CodUltimoMovimiento;
                                objDocMovimiento.EstadoDocumento = Convert.ToInt16(EstadoDocumento.DEVUELTO);
                                objDocMovimiento.UsuarioCreacion = devolver.UsuarioCreacion;
                                objDocMovimiento.HostCreacion = devolver.HostCreacion;
                                objDocMovimiento.InstanciaCreacion = devolver.InstanciaCreacion;
                                objDocMovimiento.LoginCreacion = devolver.LoginCreacion;
                                objDocMovimiento.RolCreacion = devolver.RolCreacion;
                                objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Devolver);
                                onFlagOk = InsertarMovDocumento(objDocMovimiento, cmd);
                                codDocMovimiento = onFlagOk;
                                if (onFlagOk > 0)
                                {
                                    //Evaluando si hay comentarios
                                    objDocMovimiento.objDocComentario = new DocComentario();
                                    objDocMovimiento.objDocComentario.Comentario = devolver.Comentario;
                                    if (objDocMovimiento.objDocComentario != null)
                                    {
                                        objDocMovimiento.objDocComentario.CodDocMovimiento = objDocMovimiento.CodDocMovimiento;
                                        objDocMovimiento.objDocComentario.CodDocumento = objDocMovimiento.CodDocumento;
                                        objDocMovimiento.objDocComentario.UsuarioCreacion = devolver.UsuarioCreacion;
                                        objDocMovimiento.objDocComentario.HostCreacion = devolver.HostCreacion;
                                        objDocMovimiento.objDocComentario.InstanciaCreacion = devolver.InstanciaCreacion;
                                        objDocMovimiento.objDocComentario.LoginCreacion = devolver.LoginCreacion;
                                        objDocMovimiento.objDocComentario.RolCreacion = devolver.RolCreacion;
                                        //objDocMovimiento.objDocComentario.CodDocDerivacion = finalizar.CodDocDerivacion;
                                        onFlagOk = InsertarMovComentario(objDocMovimiento.objDocComentario, cmd);
                                    }
                                }
                            }
                            if (onFlagOk > 0)
                            {
                                nRpta = devolver.CodDocumento;
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al  Devolver documentos internos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }

            return nRpta;

        }

        /// <summary>
        /// Método Público para la Actualizacion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Actualizacion del Documento</returns>  
        public int ActualizarDocumento(Documento objMovDocumento)
        {
            int nRpta = 0;
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
             DocComentario objDocComentario = new DocComentario();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    {
                        using (SqlTransaction tx = conn.BeginTransaction())
                        {
                            try
                            {
                                cmd.Transaction = tx;
                                EMaeTrabajador filtro = new EMaeTrabajador();
                                MaeTrabajadorDAO obj = new MaeTrabajadorDAO(cadenaConexion);
                                DocDerivacion objDocDerivacion = new DocDerivacion();
                                DocDerivadoDAO objDocDerivado = new DocDerivadoDAO(cadenaConexion);

                                filtro.iCodArea = objMovDocumento.CodArea;
                                filtro.siEsJefe = 1;
                                var trabajadoresArea = obj.GetTrabajadorPorArea(filtro);

                                int codigoTrabajador = 0;
                                codigoTrabajador = trabajadoresArea.FirstOrDefault().iCodTrabajador;
                                if (codigoTrabajador == 0)
                                    throw new Exception("La área debe tener asignado un jefe");
                                objMovDocumento.CodTrabajador = codigoTrabajador;

                                var onFlagOk = ActualizarDocumento(objMovDocumento, cmd);
                                int idDocumento = int.Parse(onFlagOk.ToString());
                                var codDocMovimiento = 0;
                                //Insertando Movimiento
                                if (onFlagOk >= 0)
                                {
                                    objMovDocumento.objDocMovimiento = new DocMovimiento();
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                    objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                    objMovDocumento.objDocMovimiento.FechaDerivacion = objMovDocumento.FechaDerivacion;
                                    objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                    objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                    objMovDocumento.objDocMovimiento.FechaDerivacion = objMovDocumento.FechaDerivacion;
                                    //objMovDocumento.objDocMovimiento.Nivel = 1;
                                    objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                    //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                    objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                    //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                    objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                    objMovDocumento.objDocMovimiento.CodTrabajador = objMovDocumento.CodTrabajador;
                                    objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;

                                    objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                    objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                    objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                    objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                    objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;
                                    objMovDocumento.objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Registro);
                                    onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
                                    codDocMovimiento = onFlagOk;

                                    objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                    onFlagOk = objDocDerivado.ElimarDocumentoDerivacionCC(objDocDerivacion);

                                    if (onFlagOk >= 0)
                                    {
                                        //Evaluando si hay comentarios
                                        if (objMovDocumento.Comentario != null)
                                        {
                                            objMovDocumento.objDocMovimiento.objDocComentario = new DocComentario();
                                            objMovDocumento.objDocMovimiento.objDocComentario.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.Comentario = objMovDocumento.Comentario;
                                            objMovDocumento.objDocMovimiento.objDocComentario.CodDocumento = objMovDocumento.CodDocumento;
                                            objMovDocumento.objDocMovimiento.objDocComentario.CodDocMovimiento = objMovDocumento.objDocMovimiento.CodDocMovimiento;
                                            objMovDocumento.objDocMovimiento.objDocComentario.HostCreacion = objMovDocumento.HostCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objMovDocumento.objDocMovimiento.objDocComentario.RolCreacion = objMovDocumento.RolCreacion;
                                            onFlagOk = InsertarMovComentario(objMovDocumento.objDocMovimiento.objDocComentario, cmd);
                                        }
                                    }


                                    //Insertando CC
                                    if (onFlagOk >= 0 && objMovDocumento.CodAreaCC != null)
                                    {
                                        string[] arrCodAreaCC = objMovDocumento.CodAreaCC.Split('|');
                                        for (int i = 0; i < arrCodAreaCC.Length; i++)
                                        {
                                            filtro = new EMaeTrabajador();
                                            filtro.iCodArea = short.Parse(arrCodAreaCC[i]);
                                            filtro.siEsJefe = 1;
                                            var trabajadores = obj.GetTrabajadorPorArea(filtro);
                                            int codTrabajador = 0;

                                            codTrabajador = trabajadores.FirstOrDefault().iCodTrabajador;


                                            objDocDerivacion = new DocDerivacion();
                                            objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                            objDocDerivacion.CodArea = short.Parse(arrCodAreaCC[i]);
                                            objDocDerivacion.CodResponsable = codTrabajador;
                                            objDocDerivacion.TipoAcceso = (short)Convert.ToInt32(EnumTipoAcceso.ConCopia);
                                            objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
                                            objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
                                            objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = objDocDerivado.RegistrarDocumentoDerivacion(objDocDerivacion);
                                        }
                                    }

                                    if (!Convert.ToBoolean(ConfigurationManager.AppSettings["IgnorarLaserFiche"]))
                                    {
                                        //Evaluando si hay Documentos Adjuntos
                                        if (onFlagOk >= 0)
                                        {
                                            LaserFiche objLaserFiche = new LaserFiche();
                                            //Insertando Adjunto de Documentos
                                            string filename = string.Empty;
                                            int codLaserFiche = 0;
                                            if (objMovDocumento.LstDocAdjunto != null && objMovDocumento.LstDocAdjunto.Count > 0)
                                            {
                                                foreach (var item in objMovDocumento.LstDocAdjunto)
                                                {
                                                    //Subiendo el Archivo al Repositorio Laserfiche
                                                    var bytes = Convert.FromBase64String(item.FileArray);
                                                    MemoryStream file = new MemoryStream(bytes);

                                                    codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, string.Empty, objMovDocumento.Correlativo.ToString(), false, out filename);

                                                    if (codLaserFiche > 0)
                                                    {
                                                        //Insertando MovDocAdjuntos
                                                        item.CodDocumento = objMovDocumento.CodDocumento;
                                                        item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                                        item.HostCreacion = objMovDocumento.HostCreacion;
                                                        item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                                        item.LoginCreacion = objMovDocumento.LoginCreacion;
                                                        item.RolCreacion = objMovDocumento.RolCreacion;
                                                        item.CodLaserfiche = codLaserFiche;

                                                        onFlagOk = InsertarDocAdjunto(item, cmd);
                                                    }
                                                }
                                            }

                                            //ANEXOS
                                            if (objMovDocumento.LstDocAnexo != null && objMovDocumento.LstDocAnexo.Count > 0)
                                            {
                                                int cc = 1;
                                                List<LaserFiche> objListLaserFiche = new List<LaserFiche>();
                                                DocAnexo objDocAnexo = new DocAnexo();
                                                objDocAnexo.CodDocumento = objMovDocumento.CodDocumento;
                                                List<DocAnexo> lst = GetDocumentoAnexos(objDocAnexo);
                                                if (lst.Count > 0)
                                                {
                                                    cc = lst[lst.Count - 1].secuencia + 1;
                                                }

                                                foreach (var item in objMovDocumento.LstDocAnexo)
                                                {
                                                    //Subiendo el Archivo al Repositorio Laserfiche
                                                    var bytes = Convert.FromBase64String(item.FileArray);
                                                    MemoryStream file = new MemoryStream(bytes);

                                                    var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                                                    codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, nombreAnexo, objMovDocumento.Correlativo.ToString(), true, out filename);

                                                    if (codLaserFiche > 0)
                                                    {
                                                        //Insertando MovDocAdjuntos
                                                        item.CodDocumentoMovimiento = codDocMovimiento;
                                                        item.CodDocumento = objMovDocumento.CodDocumento;
                                                        item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                                        item.HostCreacion = objMovDocumento.HostCreacion;
                                                        item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                                        item.LoginCreacion = objMovDocumento.LoginCreacion;
                                                        item.RolCreacion = objMovDocumento.RolCreacion;
                                                        item.CodLaserfiche = codLaserFiche;
                                                        item.NombreDocumento = nombreAnexo;
                                                        item.secuencia = cc;
                                                        item.TipoArchivo = Convert.ToInt32(TipoArchivos.Anexos);
                                                        onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                                    }
                                                    cc++;
                                                }
                                            }
                                            //ELIMINAR ANEXOS ADJUNTOS
                                            if (objMovDocumento.CodAnexos != null)
                                            {
                                                string[] arrCodigosLaserFiche = objMovDocumento.CodAnexos.Split(',');
                                                for (int i = 0; i < arrCodigosLaserFiche.Length; i++)
                                                {
                                                    DocAnexo objAnexo = new DocAnexo();
                                                    objAnexo.CodLaserfiche = Convert.ToInt32(arrCodigosLaserFiche[i]);
                                                    onFlagOk = docAnexoDAO.EliminarDocAnexo(objAnexo, cmd);
                                                    objLaserFiche.EliminarDocumento(Convert.ToInt32(arrCodigosLaserFiche[i]));
                                                }
                                            }
                                        }
                                    }
                                }

                                if (onFlagOk >= 0)
                                {
                                    nRpta = idDocumento;
                                    tx.Commit();
                                }
                                //else { tx.Rollback(); }
                            }
                            catch (Exception ex)
                            {
                                nRpta = -1;
                                tx.Rollback();
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                //Generando Archivo Log
                                new LogWriter(ex.Message);
                                throw new Exception("Error al Registrar el Documento Externo");
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                            }
                        }
                    }
                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para la Actualizacion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Actualizacion del Documento</returns>  
        public int ActualizarDocumentoInterno(Documento objMovDocumento)
        {
            int nRpta = 0;
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    {
                        try
                        {
                            //cmd.Transaction = tx;
                            EMaeTrabajador filtro = new EMaeTrabajador();
                            MaeTrabajadorDAO obj = new MaeTrabajadorDAO(cadenaConexion);
                            DocDerivacion objDocDerivacion = new DocDerivacion();
                            DocDerivadoDAO objDocDerivado = new DocDerivadoDAO(cadenaConexion);

                           /* filtro.iCodArea = objMovDocumento.CodArea;
                            filtro.siEsJefe = 1;
                            var trabajadoresArea = obj.GetTrabajadorPorArea(filtro);

                            int codigoTrabajador = 0;
                            codigoTrabajador = trabajadoresArea.FirstOrDefault().iCodTrabajador;
                            if (codigoTrabajador == 0)
                                throw new Exception("La área debe tener asignado un jefe");
                            objMovDocumento.CodTrabajador = codigoTrabajador;*/

                            var onFlagOk = ActualizarDocumento(objMovDocumento, cmd);
                            int idDocumento = int.Parse(onFlagOk.ToString());
                            var codDocMovimiento = 0;
                            //Insertando Movimiento
                            if (onFlagOk >= 0)
                            {
                                objMovDocumento.objDocMovimiento = new DocMovimiento();
                                objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                objMovDocumento.objDocMovimiento.FechaDerivacion = objMovDocumento.FechaDerivacion;
                                objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                objMovDocumento.objDocMovimiento.FechaDerivacion = objMovDocumento.FechaDerivacion;
                                //objMovDocumento.objDocMovimiento.Nivel = 1;
                                objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                objMovDocumento.objDocMovimiento.CodTrabajador = objMovDocumento.CodTrabajador;
                                objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;

                                objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;
                                objMovDocumento.objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Registro);
                                onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
                                codDocMovimiento = onFlagOk;

                                objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                onFlagOk = objDocDerivado.ElimarDocumentoDerivacionCC(objDocDerivacion);
                                //Insertando CC
                                if (onFlagOk >= 0 && objMovDocumento.CodAreaCC != null)
                                {
                                    string[] arrCodAreaCC = objMovDocumento.CodAreaCC.Split('|');
                                    for (int i = 0; i < arrCodAreaCC.Length; i++)
                                    {
                                        filtro = new EMaeTrabajador();
                                        filtro.iCodArea = short.Parse(arrCodAreaCC[i]);
                                        filtro.siEsJefe = 1;
                                        var trabajadores = obj.GetTrabajadorPorArea(filtro);
                                        int codTrabajador = 0;

                                        codTrabajador = trabajadores.FirstOrDefault().iCodTrabajador;


                                        objDocDerivacion = new DocDerivacion();
                                        objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                        objDocDerivacion.CodArea = short.Parse(arrCodAreaCC[i]);
                                        objDocDerivacion.CodResponsable = codTrabajador;
                                        objDocDerivacion.TipoAcceso = (short)Convert.ToInt32(EnumTipoAcceso.ConCopia);
                                        objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
                                        objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                        objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
                                        objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                        objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
                                        objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

                                        onFlagOk = objDocDerivado.RegistrarDocumentoDerivacion(objDocDerivacion);
                                    }
                                }
                                //inactivando antiguos Derivados
                                if (onFlagOk >= 0 && objMovDocumento.CodDerivadosInactivar != null)
                                    {
                                        
                                        string[] arrCodAreaBE = objMovDocumento.CodDerivadosInactivar.Split('|');
                                        for (int i = 0; i < arrCodAreaBE.Length; i++)
                                        {
  
                                            objDocDerivacion = new DocDerivacion();
                                            objDocDerivacion.CodDocumentoDerivacion = short.Parse(arrCodAreaBE[i]);
                                            objDocDerivacion.UsuarioActualizacion = objMovDocumento.UsuarioCreacion;

                                        onFlagOk = this.EliminarDocumentoDerivado(objDocDerivacion, cmd);
                                        }
                                    }
                                //Insertando Derivador
                                    if (onFlagOk >= 0 && objMovDocumento.CodAreaBE != null)
                                    {
                                        
                                        string[] arrCodAreaBE = objMovDocumento.CodAreaBE.Split('|');
                                        for (int i = 0; i < arrCodAreaBE.Length; i++)
                                        {
                                            filtro = new EMaeTrabajador();
                                            filtro.iCodArea = short.Parse(arrCodAreaBE[i]);
                                            filtro.siEsJefe = 1;
                                            var trabajadoresBE = obj.GetTrabajadorPorArea(filtro);
                                            int codTrabajadorBE = 0;

                                            codTrabajadorBE = trabajadoresBE.FirstOrDefault().iCodTrabajador;

                                            objDocDerivacion = new DocDerivacion();
                                            objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                            objDocDerivacion.CodArea = short.Parse(arrCodAreaBE[i]);
                                            objDocDerivacion.CodResponsable = codTrabajadorBE;
                                            objDocDerivacion.TipoAcceso = (short)Convert.ToInt32(EnumTipoAcceso.Derivado);
                                            objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
                                            objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
                                            objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = objDocDerivado.RegistrarDocumentoDerivacion(objDocDerivacion);

                                            objMovDocumento.objDocMovimiento = new DocMovimiento();
                                            objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;
                                            objMovDocumento.objDocMovimiento.CodDocDerivacion = onFlagOk;
                                            objMovDocumento.objDocMovimiento.siEstadoDerivacion = Convert.ToInt16(EstadoDocumento.PENDIENTE);
                                            objMovDocumento.objDocMovimiento.EstadoDocumento = objMovDocumento.EstadoDocumento;
                                            objMovDocumento.objDocMovimiento.CodArea = short.Parse(arrCodAreaBE[i]);
                                            objMovDocumento.objDocMovimiento.CodAreaDerivacion = short.Parse(arrCodAreaBE[i]);
                                            objMovDocumento.objDocMovimiento.CodResponsableDerivacion = codTrabajadorBE;
                                            objMovDocumento.objDocMovimiento.CodTrabajador = codTrabajadorBE;
                                            objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                            objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                            objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                            objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                            objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                            objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                            objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                            objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                            objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                            objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                            objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

                                            onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
                                            var codMovimiento = onFlagOk;
                                            onFlagOk = InsertarCodigosInternos(objMovDocumento.CodAreaBE, codMovimiento, cmd);

                                    }
                                }
                               

                                if (!Convert.ToBoolean(ConfigurationManager.AppSettings["IgnorarLaserFiche"]))
                                {
                                    //Evaluando si hay Documentos Adjuntos
                                    if (onFlagOk >= 0)
                                    {
                                        LaserFiche objLaserFiche = new LaserFiche();
                                        //Insertando Adjunto de Documentos
                                        string filename = string.Empty;
                                        int codLaserFiche = 0;
                                        if (objMovDocumento.LstDocAdjunto != null && objMovDocumento.LstDocAdjunto.Count > 0)
                                        {
                                            foreach (var item in objMovDocumento.LstDocAdjunto)
                                            {
                                                //Subiendo el Archivo al Repositorio Laserfiche
                                                var bytes = Convert.FromBase64String(item.FileArray);
                                                MemoryStream file = new MemoryStream(bytes);

                                                codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, string.Empty, objMovDocumento.Correlativo.ToString(), false, out filename);

                                                if (codLaserFiche > 0)
                                                {
                                                    //Insertando MovDocAdjuntos
                                                    item.CodDocumento = objMovDocumento.CodDocumento;
                                                    item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                                    item.HostCreacion = objMovDocumento.HostCreacion;
                                                    item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                                    item.LoginCreacion = objMovDocumento.LoginCreacion;
                                                    item.RolCreacion = objMovDocumento.RolCreacion;
                                                    item.CodLaserfiche = codLaserFiche;

                                                    onFlagOk = InsertarDocAdjunto(item, cmd);
                                                }
                                            }
                                        }

                                        //ANEXOS
                                        if (objMovDocumento.LstDocAnexo != null && objMovDocumento.LstDocAnexo.Count > 0)
                                        {
                                            int cc = 1;
                                            List<LaserFiche> objListLaserFiche = new List<LaserFiche>();
                                            DocAnexo objDocAnexo = new DocAnexo();
                                            objDocAnexo.CodDocumento = objMovDocumento.CodDocumento;
                                            List<DocAnexo> lst = GetDocumentoAnexos(objDocAnexo);
                                            if (lst.Count > 0)
                                            {
                                                cc = lst[lst.Count - 1].secuencia + 1;
                                            }

                                            foreach (var item in objMovDocumento.LstDocAnexo)
                                            {
                                                //Subiendo el Archivo al Repositorio Laserfiche
                                                var bytes = Convert.FromBase64String(item.FileArray);
                                                MemoryStream file = new MemoryStream(bytes);

                                                var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                                                codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, nombreAnexo, objMovDocumento.Correlativo.ToString(), true, out filename);

                                                if (codLaserFiche > 0)
                                                {
                                                    //Insertando MovDocAdjuntos
                                                    item.CodDocumentoMovimiento = codDocMovimiento;
                                                    item.CodDocumento = objMovDocumento.CodDocumento;
                                                    item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                                    item.HostCreacion = objMovDocumento.HostCreacion;
                                                    item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                                    item.LoginCreacion = objMovDocumento.LoginCreacion;
                                                    item.RolCreacion = objMovDocumento.RolCreacion;
                                                    item.CodLaserfiche = codLaserFiche;
                                                    item.NombreDocumento = nombreAnexo;
                                                    item.secuencia = cc;
                                                    item.TipoArchivo = Convert.ToInt32(TipoArchivos.Anexos);
                                                    onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                                }
                                                cc++;
                                            }
                                        }
                                        //ELIMINAR ANEXOS ADJUNTOS
                                        if (objMovDocumento.CodAnexos != null)
                                        {
                                            string[] arrCodigosLaserFiche = objMovDocumento.CodAnexos.Split(',');
                                            for (int i = 0; i < arrCodigosLaserFiche.Length; i++)
                                            {
                                                DocAnexo objAnexo = new DocAnexo();
                                                objAnexo.CodLaserfiche = Convert.ToInt32(arrCodigosLaserFiche[i]);
                                                onFlagOk = docAnexoDAO.EliminarDocAnexo(objAnexo, cmd);
                                                objLaserFiche.EliminarDocumento(Convert.ToInt32(arrCodigosLaserFiche[i]));
                                            }
                                        }
                                    }
                                }
                            }

                            if (onFlagOk >= 0)
                            {
                                nRpta = idDocumento;
                                //tx.Commit();
                            }
                            //else { tx.Rollback(); }
                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            //tx.Rollback();
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }

            return nRpta;
        }
        

        /// <summary>
        /// Método Público para la Actualizacion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Actualizacion del Documento</returns>  
        public int ActualizarDocumentoInternoEnvioSIED(Documento objMovDocumento)
        {
            int nRpta = 0;
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    {
                        try
                        {
                            //cmd.Transaction = tx;
                            EMaeTrabajador filtro = new EMaeTrabajador();
                            MaeTrabajadorDAO obj = new MaeTrabajadorDAO(cadenaConexion);
                            DocDerivacion objDocDerivacion = new DocDerivacion();
                            DocDerivadoDAO objDocDerivado = new DocDerivadoDAO(cadenaConexion);

                            var onFlagOk = ActualizarDocumentoEnvioSIED(objMovDocumento, cmd);
                            int idDocumento = int.Parse(onFlagOk.ToString());
                            var codDocMovimiento = 0;
                            //Insertando Movimiento
                            if (onFlagOk >= 0)
                            {
                                objMovDocumento.objDocMovimiento = new DocMovimiento();
                                objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                objMovDocumento.objDocMovimiento.FechaDerivacion = objMovDocumento.FechaDerivacion;
                                objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                objMovDocumento.objDocMovimiento.FechaDerivacion = objMovDocumento.FechaDerivacion;
                                //objMovDocumento.objDocMovimiento.Nivel = 1;
                                objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
                                objMovDocumento.objDocMovimiento.CodTrabajador = objMovDocumento.CodTrabajador;
                                objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;

                                objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;
                                objMovDocumento.objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Registro);
                                onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
                            

                                codDocMovimiento = onFlagOk;

                                objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                //inactivando antiguos Derivados
                                if (onFlagOk >= 0 && objMovDocumento.CodDerivadosInactivar != null)
                                {

                                    string[] arrCodAreaBE = objMovDocumento.CodDerivadosInactivar.Split('|');
                                    for (int i = 0; i < arrCodAreaBE.Length; i++)
                                    {

                                        objDocDerivacion = new DocDerivacion();
                                        objDocDerivacion.CodDocumentoDerivacion = short.Parse(arrCodAreaBE[i]);
                                        objDocDerivacion.UsuarioActualizacion = objMovDocumento.UsuarioCreacion;

                                        onFlagOk = this.EliminarDocumentoDerivadoEnvioSIED(objDocDerivacion, cmd);
                                    }
                                }
                                //Insertando Derivador
                                if (onFlagOk >= 0 && objMovDocumento.CodAreaBE != null)
                                {

                                    string[] arrCodAreaBE = objMovDocumento.CodAreaBE.Split('|');
                                    for (int i = 0; i < arrCodAreaBE.Length; i++)
                                    {

                                        objDocDerivacion = new DocDerivacion();
                                        objDocDerivacion.CodDocumento = objMovDocumento.CodDocumento;
                                        objDocDerivacion.TipoAcceso = (short)Convert.ToInt32(EnumTipoAcceso.EmpresaSIED);
                                        objDocDerivacion.FecDerivacion = objMovDocumento.FechaDocumento;
                                        objDocDerivacion.CodEntidadSied = Convert.ToInt32(objMovDocumento.lstEmpresaSied[i].codEmpresa);
                                        objDocDerivacion.NombreEntidadSIED = objMovDocumento.lstEmpresaSied[i].nomEmpresaCorto;
                                        objDocDerivacion.FormatoDocSied = objMovDocumento.lstEmpresaSied[i].CodFormatoDocumentoEmpresas;
                                        objDocDerivacion.rucSied = objMovDocumento.lstEmpresaSied[i].rucEmpresa;
                                        objDocDerivacion.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                        objDocDerivacion.HostCreacion = objMovDocumento.HostCreacion;
                                        objDocDerivacion.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                        objDocDerivacion.LoginCreacion = objMovDocumento.LoginCreacion;
                                        objDocDerivacion.RolCreacion = objMovDocumento.RolCreacion;

                                        onFlagOk = objDocDerivado.RegistrarDocumentoDerivacionSIED(objDocDerivacion);
                                        //INSERTANDO MOVIMIENTO
                                        objMovDocumento.objDocMovimiento = new DocMovimiento();
                                        objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;
                                        objMovDocumento.objDocMovimiento.CodDocDerivacion = onFlagOk;
                                        objMovDocumento.objDocMovimiento.siEstadoDerivacion = Convert.ToInt16(EstadoDocumento.PENDIENTE);
                                        objMovDocumento.objDocMovimiento.EstadoDocumento = objMovDocumento.EstadoDocumento;
                                        objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
                                        objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
                                        objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
                                        objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
                                        objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
                                        objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
                                        objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                        objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
                                        objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                        objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
                                        objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

                                        onFlagOk = InsertarMovDocumentoSIED(objMovDocumento.objDocMovimiento, cmd);
                                        
                                    }
                                }


                                if (!Convert.ToBoolean(ConfigurationManager.AppSettings["IgnorarLaserFiche"]))
                                {
                                    //Evaluando si hay Documentos Adjuntos
                                    if (onFlagOk >= 0)
                                    {
                                        LaserFiche objLaserFiche = new LaserFiche();
                                        //Insertando Adjunto de Documentos
                                        string filename = string.Empty;
                                        int codLaserFiche = 0;
                                        if (objMovDocumento.LstDocAdjunto != null && objMovDocumento.LstDocAdjunto.Count > 0)
                                        {
                                            foreach (var item in objMovDocumento.LstDocAdjunto)
                                            {
                                                //Subiendo el Archivo al Repositorio Laserfiche
                                                var bytes = Convert.FromBase64String(item.FileArray);
                                                MemoryStream file = new MemoryStream(bytes);

                                                codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, string.Empty, objMovDocumento.Correlativo.ToString(), false, out filename);

                                                if (codLaserFiche > 0)
                                                {
                                                    //Insertando MovDocAdjuntos
                                                    item.CodDocumento = objMovDocumento.CodDocumento;
                                                    item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                                    item.HostCreacion = objMovDocumento.HostCreacion;
                                                    item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                                    item.LoginCreacion = objMovDocumento.LoginCreacion;
                                                    item.RolCreacion = objMovDocumento.RolCreacion;
                                                    item.CodLaserfiche = codLaserFiche;

                                                    onFlagOk = InsertarDocAdjunto(item, cmd);
                                                }
                                            }
                                        }

                                        //ANEXOS
                                        if (objMovDocumento.LstDocAnexo != null && objMovDocumento.LstDocAnexo.Count > 0)
                                        {
                                            int cc = 1;
                                            List<LaserFiche> objListLaserFiche = new List<LaserFiche>();
                                            DocAnexo objDocAnexo = new DocAnexo();
                                            objDocAnexo.CodDocumento = objMovDocumento.CodDocumento;
                                            List<DocAnexo> lst = GetDocumentoAnexos(objDocAnexo);
                                            if (lst.Count > 0)
                                            {
                                                cc = lst[lst.Count - 1].secuencia + 1;
                                            }

                                            foreach (var item in objMovDocumento.LstDocAnexo)
                                            {
                                                //Subiendo el Archivo al Repositorio Laserfiche
                                                var bytes = Convert.FromBase64String(item.FileArray);
                                                MemoryStream file = new MemoryStream(bytes);

                                                var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                                                codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, nombreAnexo, objMovDocumento.Correlativo.ToString(), true, out filename);

                                                if (codLaserFiche > 0)
                                                {
                                                    //Insertando MovDocAdjuntos
                                                    item.CodDocumentoMovimiento = codDocMovimiento;
                                                    item.CodDocumento = objMovDocumento.CodDocumento;
                                                    item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
                                                    item.HostCreacion = objMovDocumento.HostCreacion;
                                                    item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
                                                    item.LoginCreacion = objMovDocumento.LoginCreacion;
                                                    item.RolCreacion = objMovDocumento.RolCreacion;
                                                    item.CodLaserfiche = codLaserFiche;
                                                    item.NombreDocumento = nombreAnexo;
                                                    item.secuencia = cc;
                                                    item.TipoArchivo = Convert.ToInt32(TipoArchivos.Anexos);
                                                    onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                                }
                                                cc++;
                                            }
                                        }
                                        //ELIMINAR ANEXOS ADJUNTOS
                                        if (objMovDocumento.CodAnexos != null)
                                        {
                                            string[] arrCodigosLaserFiche = objMovDocumento.CodAnexos.Split(',');
                                            for (int i = 0; i < arrCodigosLaserFiche.Length; i++)
                                            {
                                                DocAnexo objAnexo = new DocAnexo();
                                                objAnexo.CodLaserfiche = Convert.ToInt32(arrCodigosLaserFiche[i]);
                                                onFlagOk = docAnexoDAO.EliminarDocAnexo(objAnexo, cmd);
                                                objLaserFiche.EliminarDocumento(Convert.ToInt32(arrCodigosLaserFiche[i]));
                                            }
                                        }
                                    }
                                }
                            }

                            if (onFlagOk >= 0)
                            {
                                nRpta = idDocumento;
                                //tx.Commit();
                            }
                            //else { tx.Rollback(); }
                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            //tx.Rollback();
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }

            return nRpta;
        }
        ///// <summary>
        ///// Método Público para la Actualizacion de un Documento
        ///// </summary>
        ///// <returns>Retorna el Indicador de Actualizacion del Documento</returns>  
        //public int ActualizarDocumento2(Documento objMovDocumento)
        //{
        //    DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
        //    int nRpta = 0;
        //    int onFlagOk = 0;
        //    var codDocMovimiento = 0;

        //    using (SqlConnection conn = new SqlConnection(cadenaConexion))
        //    {
        //        conn.Open();

        //        using (SqlCommand cmd = conn.CreateCommand())
        //        {
        //            using (SqlTransaction tx = conn.BeginTransaction())
        //            {
        //                try
        //                {
        //                    cmd.Transaction = tx;

        //                    onFlagOk = ActualizarDocumento(objMovDocumento, cmd);
        //                    int idDocumento = int.Parse(onFlagOk.ToString());

        //                    //Insertando Movimiento
        //                    if (onFlagOk >= 0)
        //                    {
        //                        objMovDocumento.objDocMovimiento = new DocMovimiento();
        //                        objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
        //                        objMovDocumento.objDocMovimiento.FechaRecepcion = objMovDocumento.FechaRecepcion;
        //                        objMovDocumento.objDocMovimiento.FechaLectura = objMovDocumento.FechaDocumento;
        //                        objMovDocumento.objDocMovimiento.FechaDerivacion = objMovDocumento.FechaDerivacion;
        //                        objMovDocumento.objDocMovimiento.Plazo = objMovDocumento.Plazo;
        //                        objMovDocumento.objDocMovimiento.FechaPlazo = objMovDocumento.FechaPlazo;
        //                        objMovDocumento.objDocMovimiento.FechaDerivacion = objMovDocumento.FechaDerivacion;
        //                        //objMovDocumento.objDocMovimiento.Nivel = 1;
        //                        objMovDocumento.objDocMovimiento.IndicadorFirma = 0;
        //                        //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
        //                        objMovDocumento.objDocMovimiento.IndicadorVisado = 0;
        //                        //objMovDocumento.objDocMovimiento.FechaFirma = null;
        //                        objMovDocumento.objDocMovimiento.CodArea = objMovDocumento.CodArea;
        //                        objMovDocumento.objDocMovimiento.CodTrabajador = objMovDocumento.CodTrabajador;
        //                        objMovDocumento.objDocMovimiento.CodDocumento = objMovDocumento.CodDocumento;

        //                        objMovDocumento.objDocMovimiento.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
        //                        objMovDocumento.objDocMovimiento.HostCreacion = objMovDocumento.HostCreacion;
        //                        objMovDocumento.objDocMovimiento.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
        //                        objMovDocumento.objDocMovimiento.LoginCreacion = objMovDocumento.LoginCreacion;
        //                        objMovDocumento.objDocMovimiento.RolCreacion = objMovDocumento.RolCreacion;

        //                        onFlagOk = InsertarMovDocumento(objMovDocumento.objDocMovimiento, cmd);
        //                        codDocMovimiento = onFlagOk;



        //                    }

        //                    if (onFlagOk >= 0)
        //                    {
        //                        nRpta = idDocumento;
        //                        tx.Commit();
        //                    }
        //                    else { tx.Rollback(); }
        //                }
        //                catch (Exception ex)
        //                {
        //                    nRpta = -1;
        //                    tx.Rollback();
        //                    conn.Close();
        //                    conn.Dispose();
        //                    cmd.Dispose();
        //                    //Generando Archivo Log
        //                    new LogWriter(ex.Message);
        //                    throw new Exception("Error al Registrar el Documento Externo");
        //                }
        //                finally
        //                {
        //                    conn.Close();
        //                    conn.Dispose();
        //                    cmd.Dispose();
        //                }
        //            }
        //        }
        //    }


        //    //grabando en laserfiche

        //    //Evaluando si hay Documentos Adjuntos
        //    if (onFlagOk >= 0)
        //    {
        //        LaserFiche objLaserFiche = new LaserFiche();
        //        //Insertando Adjunto de Documentos
        //        string filename = string.Empty;
        //        int codLaserFiche = 0;
        //        if (objMovDocumento.LstDocAdjunto != null && objMovDocumento.LstDocAdjunto.Count > 0)
        //        {
        //            foreach (var item in objMovDocumento.LstDocAdjunto)
        //            {
        //                //Subiendo el Archivo al Repositorio Laserfiche
        //                var bytes = Convert.FromBase64String(item.FileArray);
        //                MemoryStream file = new MemoryStream(bytes);

        //                codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, string.Empty, objMovDocumento.Correlativo.ToString(), false, out filename);

        //                if (codLaserFiche > 0)
        //                {
        //                    //Insertando MovDocAdjuntos
        //                    item.CodDocumento = objMovDocumento.CodDocumento;
        //                    item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
        //                    item.HostCreacion = objMovDocumento.HostCreacion;
        //                    item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
        //                    item.LoginCreacion = objMovDocumento.LoginCreacion;
        //                    item.RolCreacion = objMovDocumento.RolCreacion;
        //                    item.CodLaserfiche = codLaserFiche;

        //                    //onFlagOk = InsertarDocAdjunto(item, cmd);
        //                }
        //            }
        //        }
        //        //ANEXOS
        //        if (objMovDocumento.LstDocAnexo != null && objMovDocumento.LstDocAnexo.Count > 0)
        //        {
        //            int cc = 1;
        //            List<LaserFiche> objListLaserFiche = new List<LaserFiche>();
        //            DocAnexo objDocAnexo = new DocAnexo();
        //            objDocAnexo.CodDocumento = objMovDocumento.CodDocumento;
        //            List<DocAnexo> lst = GetDocumentoAnexos(objDocAnexo);
        //            if (lst.Count > 0)
        //            {
        //                cc = lst[lst.Count - 1].secuencia + 1;
        //            }

        //            foreach (var item in objMovDocumento.LstDocAnexo)
        //            {
        //                //Subiendo el Archivo al Repositorio Laserfiche
        //                var bytes = Convert.FromBase64String(item.FileArray);
        //                MemoryStream file = new MemoryStream(bytes);

        //                var nombreAnexo = objMovDocumento.Correlativo.ToString() + "_ANX" + cc + System.IO.Path.GetExtension(item.NombreDocumento);
        //                codLaserFiche = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", objMovDocumento.NumDocumento, nombreAnexo, objMovDocumento.Correlativo.ToString(), true, out filename);

        //                if (codLaserFiche > 0)
        //                {
        //                    //Insertando MovDocAdjuntos
        //                    item.CodDocumentoMovimiento = codDocMovimiento;
        //                    item.CodDocumento = objMovDocumento.CodDocumento;
        //                    item.UsuarioCreacion = objMovDocumento.UsuarioCreacion;
        //                    item.HostCreacion = objMovDocumento.HostCreacion;
        //                    item.InstanciaCreacion = objMovDocumento.InstanciaCreacion;
        //                    item.LoginCreacion = objMovDocumento.LoginCreacion;
        //                    item.RolCreacion = objMovDocumento.RolCreacion;
        //                    item.CodLaserfiche = codLaserFiche;
        //                    item.NombreDocumento = nombreAnexo;
        //                    item.secuencia = cc;

        //                    //onFlagOk = InsertarDocAnexo(item, cmd);
        //                }
        //                cc++;
        //            }
        //        }
        //        //ELIMINAR ANEXOS ADJUNTOS
        //        if (objMovDocumento.CodAnexos != null)
        //        {
        //            string[] arrCodigosLaserFiche = objMovDocumento.CodAnexos.Split(',');
        //            for (int i = 0; i < arrCodigosLaserFiche.Length; i++)
        //            {
        //                DocAnexo objAnexo = new DocAnexo();
        //                objAnexo.CodLaserfiche = Convert.ToInt32(arrCodigosLaserFiche[i]);
        //                //onFlagOk = EliminarDocAnexo(objAnexo, cmd);
        //                objLaserFiche.EliminarDocumento(Convert.ToInt32(arrCodigosLaserFiche[i]));
        //            }
        //        }



        //        //grabando en BD los adjuntos y anexos


        //        using (SqlConnection conn = new SqlConnection(cadenaConexion))
        //        {
        //            conn.Open();

        //            using (SqlCommand cmd = conn.CreateCommand())
        //            {
        //                using (SqlTransaction tx = conn.BeginTransaction())
        //                {
        //                    try
        //                    {
        //                        cmd.Transaction = tx;


        //                        if (objMovDocumento.LstDocAdjunto != null && objMovDocumento.LstDocAdjunto.Count > 0)
        //                        {
        //                            foreach (var item in objMovDocumento.LstDocAdjunto)
        //                            {
        //                                if (item.CodLaserfiche > 0)
        //                                {
        //                                    onFlagOk = InsertarDocAdjunto(item, cmd);
        //                                }
        //                            }
        //                        }
        //                        if (objMovDocumento.LstDocAnexo != null && objMovDocumento.LstDocAnexo.Count > 0)
        //                        {
        //                            foreach (var item in objMovDocumento.LstDocAnexo)
        //                            {
        //                                if (item.CodLaserfiche > 0)
        //                                {
        //                                    onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
        //                                }
        //                            }

        //                            if (objMovDocumento.CodAnexos != null)
        //                            {
        //                                string[] arrCodigosLaserFiche = objMovDocumento.CodAnexos.Split(',');
        //                                for (int i = 0; i < arrCodigosLaserFiche.Length; i++)
        //                                {
        //                                    DocAnexo objAnexo = new DocAnexo();
        //                                    objAnexo.CodLaserfiche = Convert.ToInt32(arrCodigosLaserFiche[i]);
        //                                    onFlagOk = docAnexoDAO.EliminarDocAnexo(objAnexo, cmd);
        //                                }
        //                            }
        //                        }

        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        nRpta = -1;
        //                        tx.Rollback();
        //                        conn.Close();
        //                        conn.Dispose();
        //                        cmd.Dispose();
        //                        //Generando Archivo Log
        //                        new LogWriter(ex.Message);
        //                        throw new Exception("Error al Registrar el Documento Externo");
        //                    }
        //                    finally
        //                    {
        //                        conn.Close();
        //                        conn.Dispose();
        //                        cmd.Dispose();
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return nRpta;
        //}


        /// <summary>
        /// Interface para Calcular la fecha de plazo del documento
        /// </summary>
        /// <returns>Retorna el Indicador de Fecha de Plazo del Documento</returns>
        public DateTime CalcularFechaPlazoDocumento(int cantidadPlazo, DateTime fechaInicio)
        {
            DateTime fechaPlazo;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_CalcularFechaPlazoDocumento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@CantDiasPlazo";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = cantidadPlazo;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@FechaInicio";
                    param.SqlDbType = SqlDbType.DateTime;
                    param.Value = fechaInicio;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@FechaPlazo";
                    param.SqlDbType = SqlDbType.DateTime;
                    param.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(param);

                    try
                    {
                        cmd.ExecuteNonQuery();
                        fechaPlazo = Convert.ToDateTime(cmd.Parameters["@FechaPlazo"].Value);
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error Calcular la Fecha de Plazo del Documento");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return fechaPlazo;
        }
        
        public int ObtenerPrimerMovimientoDocumento(int codDocumento, int estadoDocumento)
        {
            int codMovimiento;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerPrimerCodMovimientoDocumento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@CodDocumento";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = codDocumento;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@EstadoDocumento";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = estadoDocumento;
                    cmd.Parameters.Add(param);


                    param = new SqlParameter();
                    param.ParameterName = "@CodDocMovimiento";
                    param.SqlDbType = SqlDbType.Int;
                    param.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(param);

                    try
                    {
                        cmd.ExecuteNonQuery();
                        codMovimiento = Convert.ToInt32(cmd.Parameters["@CodDocMovimiento"].Value);
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al obtener primer movimiento del Documento");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return codMovimiento;
        }
        
        public int ObtenerTotalDerivaciones(int codDocumento, int tipoAcceso)
        {
            int totalDerivaciones;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerTotalDerivaciones]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@iCodDocumento";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = codDocumento;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@siTipoAcceso";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = tipoAcceso;
                    cmd.Parameters.Add(param);

                    try
                    {
                        var result = cmd.ExecuteScalar();

                        totalDerivaciones = Convert.ToInt32(result);
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al obtener el total de derivaciones");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return totalDerivaciones;
        }

        #endregion

        #region Métodos Privados

        /// <summary>
        /// Método privado para la generación del correlativo del Documento
        /// </summary>
        /// <returns>Retorna el correlativo del documento</returns>  
        private int GenerarSecuencial(Documento objDocumento, SqlCommand cmd, out string outCorrelativo)
        {
            int outSecuencial = 0;

            cmd.CommandText = "[dbo].[UP_MAE_Generar_Secuencial]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iOrigen", SqlDbType.Int);
            param.Value = objDocumento.Origen;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTipoDocumento", SqlDbType.Int);
            param.Value = objDocumento.CodTipoDocumento;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = objDocumento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocumento.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocumento.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocumento.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocumento.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocumento.RolCreacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onSecuencial";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 5000;
            param.ParameterName = "@onCorrelativo";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            outSecuencial = Convert.ToInt32(cmd.Parameters["@onSecuencial"].Value.ToString());
            outCorrelativo = Convert.ToString(cmd.Parameters["@onCorrelativo"].Value.ToString());

            return outSecuencial;
        }


        /// <summary>
        /// Método privado para la generación del correlativo del Documento
        /// </summary>
        /// <returns>Retorna el correlativo del documento</returns>  
        private int GenerarSecuencialSIED(Documento objDocumento, SqlCommand cmd, out string outCorrelativo)
        {
            int outSecuencial = 0;

            cmd.CommandText = "[dbo].[UP_MAE_Generar_SecuencialSIED]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iOrigen", SqlDbType.Int);
            param.Value = objDocumento.Origen;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTipoDocumento", SqlDbType.VarChar);
            param.Value = objDocumento.CodTipoDocumentoSIED;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = objDocumento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocumento.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocumento.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocumento.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocumento.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocumento.RolCreacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onSecuencial";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 5000;
            param.ParameterName = "@onCorrelativo";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            outSecuencial = Convert.ToInt32(cmd.Parameters["@onSecuencial"].Value.ToString());
            outCorrelativo = Convert.ToString(cmd.Parameters["@onCorrelativo"].Value.ToString());

            return outSecuencial;
        }
        /// <summary>
        /// Método privado para el registro del Documento
        /// </summary>
        /// <returns>Retorna el código del documento registrado</returns>  
        private int InsertarDocumento(Documento objDocumento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocumento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodSecuencial", SqlDbType.Int);
            param.Value = objDocumento.Secuencial;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vCorrelativo", SqlDbType.VarChar);
            param.Value = objDocumento.Correlativo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iAnio", SqlDbType.Int);
            param.Value = objDocumento.Anio;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
            param.Value = objDocumento.Estado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siOrigen", SqlDbType.SmallInt);
            param.Value = objDocumento.Origen;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vNumDocumento", SqlDbType.VarChar, 100);
            param.Value = objDocumento.NumDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstDocumento", SqlDbType.SmallInt);
            param.Value = objDocumento.EstadoDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDocumento", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDocumento, default(DateTime))) param.Value = objDocumento.FechaDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRegistro", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRegistro, default(DateTime))) param.Value = objDocumento.FechaRegistro;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDerivacion, default(DateTime?))) param.Value = objDocumento.FechaDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vAsunto", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Asunto;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPrioridad", SqlDbType.SmallInt);
            param.Value = objDocumento.Prioridad;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vReferencia", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Referencia;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRecepcion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRecepcion, default(DateTime))) param.Value = objDocumento.FechaRecepcion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPlazo", SqlDbType.SmallInt);
            param.Value = objDocumento.Plazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecPlazo", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaPlazo, default(DateTime))) param.Value = objDocumento.FechaPlazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vObservaciones", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Observaciones;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTipDocumento", SqlDbType.Int);
            param.Value = objDocumento.CodTipoDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodRepresentante", SqlDbType.Int);
            param.Value = objDocumento.CodRepresentante;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = objDocumento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
            param.Value = objDocumento.CodTrabajador;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocumento.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocumento.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocumento.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocumento.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocumento.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            objDocumento.CodDocumento = onRpta;

            return onRpta;

        }

        /// <summary>
        /// Método privado para el registro del Documento
        /// </summary>
        /// <returns>Retorna el código del documento registrado</returns>  
        private int InsertarDocumentoRecepcionSIED(Documento objDocumento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocumentoRecepcionSIED]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodSecuencial", SqlDbType.Int);
            param.Value = objDocumento.Secuencial;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vCorrelativo", SqlDbType.VarChar);
            param.Value = objDocumento.Correlativo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iAnio", SqlDbType.Int);
            param.Value = objDocumento.Anio;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
            param.Value = objDocumento.Estado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siOrigen", SqlDbType.SmallInt);
            param.Value = objDocumento.Origen;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vNumDocumento", SqlDbType.VarChar, 100);
            param.Value = objDocumento.NumDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstDocumento", SqlDbType.SmallInt);
            param.Value = objDocumento.EstadoDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDocumento", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDocumento, default(DateTime))) param.Value = objDocumento.FechaDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRegistro", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRegistro, default(DateTime))) param.Value = objDocumento.FechaRegistro;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDerivacion, default(DateTime?))) param.Value = objDocumento.FechaDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vAsunto", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Asunto;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPrioridad", SqlDbType.SmallInt);
            param.Value = objDocumento.Prioridad;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vReferencia", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Referencia;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRecepcion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRecepcion, default(DateTime))) param.Value = objDocumento.FechaRecepcion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPlazo", SqlDbType.SmallInt);
            param.Value = objDocumento.Plazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecPlazo", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaPlazo, default(DateTime))) param.Value = objDocumento.FechaPlazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vObservaciones", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Observaciones;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTipDocumento", SqlDbType.Int);
            param.Value = objDocumento.CodTipoDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodRepresentante", SqlDbType.Int);
            param.Value = objDocumento.CodRepresentante;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = objDocumento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
            param.Value = objDocumento.CodTrabajador;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siFlagSied", SqlDbType.Int);
            param.Value = objDocumento.siFlagSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocumento.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocumento.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocumento.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocumento.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocumento.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vTipoEntidadSIED", SqlDbType.VarChar);
            param.Value = objDocumento.TipoEntidadSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vEntidadSIED", SqlDbType.VarChar);
            param.Value = objDocumento.EntidadSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vDirigidoASIED", SqlDbType.VarChar);
            param.Value = objDocumento.DirigidoASIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPrioridadSIED", SqlDbType.VarChar);
            param.Value = objDocumento.PrioridadSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vIndicacionSIED", SqlDbType.VarChar);
            param.Value = objDocumento.IndicacionSIED;
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            objDocumento.CodDocumento = onRpta;

            return onRpta;

        }

        /// <summary>
        /// Método privado para el registro del Documento
        /// </summary>
        /// <returns>Retorna el código del documento registrado</returns>  
        private int InsertarDocumentoSIED(Documento objDocumento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocumentoSIED]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodSecuencial", SqlDbType.Int);
            param.Value = objDocumento.Secuencial;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vCorrelativo", SqlDbType.VarChar);
            param.Value = objDocumento.Correlativo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iAnio", SqlDbType.Int);
            param.Value = objDocumento.Anio;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
            param.Value = objDocumento.Estado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siOrigen", SqlDbType.SmallInt);
            param.Value = objDocumento.Origen;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vNumDocumento", SqlDbType.VarChar, 100);
            param.Value = objDocumento.NumDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstDocumento", SqlDbType.SmallInt);
            param.Value = objDocumento.EstadoDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDocumento", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDocumento, default(DateTime))) param.Value = objDocumento.FechaDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRegistro", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRegistro, default(DateTime))) param.Value = objDocumento.FechaRegistro;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDerivacion, default(DateTime?))) param.Value = objDocumento.FechaDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vAsunto", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Asunto;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPrioridad", SqlDbType.SmallInt);
            param.Value = objDocumento.Prioridad;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vReferencia", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Referencia;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRecepcion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRecepcionSIED, default(DateTime))) param.Value = objDocumento.FechaRecepcionSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPlazo", SqlDbType.SmallInt);
            param.Value = objDocumento.Plazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecPlazo", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaPlazo, default(DateTime))) param.Value = objDocumento.FechaPlazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vObservaciones", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Observaciones;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTipDocumento", SqlDbType.Int);
            param.Value = null;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodRepresentante", SqlDbType.Int);
            param.Value = objDocumento.CodRepresentante;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = objDocumento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
            param.Value = objDocumento.CodTrabajador;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTipDocumentoSIED", SqlDbType.VarChar);
            param.Value = objDocumento.CodTipoDocumentoSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTipAsuntoSIED", SqlDbType.Int);
            param.Value = objDocumento.CodAsuntoSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siFlagSIED", SqlDbType.Int);
            param.Value = (int)Enumerados.DestinoSied.ENVIO;
            cmd.Parameters.Add(param);


            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocumento.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocumento.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocumento.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocumento.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocumento.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            objDocumento.CodDocumento = onRpta;

            return onRpta;

        }
        /// <summary>
        /// Método privado para el registro del Documento
        /// </summary>
        /// <returns>Retorna el código del documento registrado</returns>  
        private int ActualizarDocumento(Documento objDocumento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_ActualizarDocumento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocumento.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodSecuencial", SqlDbType.Int);
            param.Value = objDocumento.Secuencial;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iAnio", SqlDbType.Int);
            param.Value = objDocumento.Anio;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
            param.Value = objDocumento.Estado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siOrigen", SqlDbType.SmallInt);
            param.Value = objDocumento.Origen;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vNumDocumento", SqlDbType.VarChar, 100);
            param.Value = objDocumento.NumDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstDocumento", SqlDbType.SmallInt);
            param.Value = objDocumento.EstadoDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDocumento", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDocumento, default(DateTime))) param.Value = objDocumento.FechaDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRegistro", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRegistro, default(DateTime))) param.Value = objDocumento.FechaRegistro;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDerivacion, default(DateTime?))) param.Value = objDocumento.FechaDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vAsunto", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Asunto;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPrioridad", SqlDbType.SmallInt);
            param.Value = objDocumento.Prioridad;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vReferencia", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Referencia;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRecepcion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRecepcion, default(DateTime))) param.Value = objDocumento.FechaRecepcion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPlazo", SqlDbType.SmallInt);
            param.Value = objDocumento.Plazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecPlazo", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaPlazo, default(DateTime))) param.Value = objDocumento.FechaPlazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vObservaciones", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Observaciones;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTipDocumento", SqlDbType.Int);
            param.Value = objDocumento.CodTipoDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodRepresentante", SqlDbType.Int);
            param.Value = objDocumento.CodRepresentante;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = objDocumento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
            param.Value = objDocumento.CodTrabajador;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuActualizacion";
            param.Value = objDocumento.UsuarioActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstActualizacion";
            param.Value = objDocumento.HostActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsActualizacion";
            param.Value = objDocumento.InstanciaActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnActualizacion";
            param.Value = objDocumento.LoginActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolActualizacion";
            param.Value = objDocumento.RolActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        /// <summary>
        /// Método privado para el registro del Documento
        /// </summary>
        /// <returns>Retorna el código del documento registrado</returns>  
        private int ActualizarDocumentoEnvioSIED(Documento objDocumento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_ActualizarDocumentoEnvioSIED]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocumento.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siOrigen", SqlDbType.SmallInt);
            param.Value = objDocumento.Origen;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstDocumento", SqlDbType.SmallInt);
            param.Value = objDocumento.EstadoDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDocumento", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDocumento, default(DateTime))) param.Value = objDocumento.FechaDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRegistro", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRegistro, default(DateTime))) param.Value = objDocumento.FechaRegistro;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaDerivacion, default(DateTime?))) param.Value = objDocumento.FechaDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vAsunto", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Asunto;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPrioridad", SqlDbType.SmallInt);
            param.Value = objDocumento.Prioridad;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vReferencia", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Referencia;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRecepcion", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaRecepcionSIED, default(DateTime))) param.Value = objDocumento.FechaRecepcionSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPlazo", SqlDbType.SmallInt);
            param.Value = objDocumento.Plazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecPlazo", SqlDbType.DateTime);
            if (!object.Equals(objDocumento.FechaPlazo, default(DateTime))) param.Value = objDocumento.FechaPlazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vObservaciones", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.Observaciones;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@iCodTipAsuntoSIED", SqlDbType.VarChar);
            param.Value = objDocumento.CodAsuntoSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = objDocumento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
            param.Value = objDocumento.CodTrabajador;
            cmd.Parameters.Add(param);


            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuActualizacion";
            param.Value = objDocumento.UsuarioActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstActualizacion";
            param.Value = objDocumento.HostActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsActualizacion";
            param.Value = objDocumento.InstanciaActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnActualizacion";
            param.Value = objDocumento.LoginActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolActualizacion";
            param.Value = objDocumento.RolActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        /// <summary>
        /// Método privado para el registro de los adjuntos de un documento
        /// </summary>
        /// <returns>Retorna indicador de registro de un nuevo adjunto</returns>  
        private int InsertarDocAdjunto(DocAdjunto objDocAdjunto, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocAdjunto]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodLaserfiche", SqlDbType.Int);
            param.Value = objDocAdjunto.CodLaserfiche;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocAdjunto.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocAdjunto.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocAdjunto.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocAdjunto.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocAdjunto.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocAdjunto.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }


        /// <summary>
        /// Método privado para la actualización de los adjuntos de un documento
        /// </summary>
        /// <returns>Retorna indicador de actualizacion de un nuevo adjunto</returns>  
        private int ActualizarDocAdjunto(DocAdjunto objDocAdjunto, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_ActualizarDocAdjunto]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodLaserfiche", SqlDbType.Int);
            param.Value = objDocAdjunto.CodLaserfiche;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocAdjunto", SqlDbType.Int);
            param.Value = objDocAdjunto.CodDocAdjunto;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocAdjunto.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuActualizacion";
            param.Value = objDocAdjunto.UsuarioActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstActualizacion";
            param.Value = objDocAdjunto.HostActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsActualizacion";
            param.Value = objDocAdjunto.InstanciaActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnActualizacion";
            param.Value = objDocAdjunto.LoginActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolActualizacion";
            param.Value = objDocAdjunto.RolActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }
        /// <summary>
        /// Método privado para el registro de los Movimientos de un Documento
        /// </summary>
        /// <returns>Retorna indicador de registro de un nuevo Movimiento</returns>  
        private int InsertarMovDocumento(DocMovimiento objDocMovimiento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocMovimiento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocMovimiento.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = objDocMovimiento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
            param.Value = objDocMovimiento.CodTrabajador;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRecepcion", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaRecepcion, default(DateTime))) param.Value = objDocMovimiento.FechaRecepcion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecLectura", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaLectura, default(DateTime))) param.Value = objDocMovimiento.FechaLectura;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPlazo", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.Plazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecPlazo", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaPlazo, default(DateTime))) param.Value = objDocMovimiento.FechaPlazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siNivel", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.Nivel;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstFirma", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.IndicadorFirma;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecVisado", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaVisado, default(DateTime))) param.Value = objDocMovimiento.FechaVisado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstVisado", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.IndicadorVisado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecFirma", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaFirma, default(DateTime))) param.Value = objDocMovimiento.FechaFirma;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaDerivacion, default(DateTime?))) param.Value = objDocMovimiento.FechaDerivacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@iCodDocDerivacion", SqlDbType.Int);
            param.Value = objDocMovimiento.CodDocDerivacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@iCodAreaDerivacion", SqlDbType.Int);
            param.Value = objDocMovimiento.CodAreaDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodResponsableDerivacion", SqlDbType.Int);
            param.Value = objDocMovimiento.CodResponsableDerivacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@siEstadoDerivacion", SqlDbType.Int);
            param.Value = objDocMovimiento.siEstadoDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iTipoMovimiento", SqlDbType.Int);
            param.Value = objDocMovimiento.TipoMovimiento;
            cmd.Parameters.Add(param);



            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocMovimiento.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocMovimiento.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocMovimiento.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocMovimiento.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocMovimiento.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            objDocMovimiento.CodDocMovimiento = onRpta;
            return onRpta;

        }
        /// <summary>
        /// Método privado para el registro de los Movimientos de un Documento
        /// </summary>
        /// <returns>Retorna indicador de registro de un nuevo Movimiento</returns>  
        private int InsertarMovDocumentoSIED(DocMovimiento objDocMovimiento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocMovimiento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocMovimiento.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = null;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
            param.Value = null;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRecepcion", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaRecepcion, default(DateTime))) param.Value = objDocMovimiento.FechaRecepcion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecLectura", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaLectura, default(DateTime))) param.Value = objDocMovimiento.FechaLectura;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPlazo", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.Plazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecPlazo", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaPlazo, default(DateTime))) param.Value = objDocMovimiento.FechaPlazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siNivel", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.Nivel;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstFirma", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.IndicadorFirma;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecVisado", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaVisado, default(DateTime))) param.Value = objDocMovimiento.FechaVisado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstVisado", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.IndicadorVisado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecFirma", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaFirma, default(DateTime))) param.Value = objDocMovimiento.FechaFirma;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaDerivacion, default(DateTime?))) param.Value = objDocMovimiento.FechaDerivacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@iCodDocDerivacion", SqlDbType.Int);
            param.Value = objDocMovimiento.CodDocDerivacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@iCodAreaDerivacion", SqlDbType.Int);
            param.Value = objDocMovimiento.CodAreaDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodResponsableDerivacion", SqlDbType.Int);
            param.Value = objDocMovimiento.CodResponsableDerivacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@siEstadoDerivacion", SqlDbType.Int);
            param.Value = objDocMovimiento.siEstadoDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iTipoMovimiento", SqlDbType.Int);
            param.Value = objDocMovimiento.TipoMovimiento;
            cmd.Parameters.Add(param);



            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocMovimiento.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocMovimiento.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocMovimiento.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocMovimiento.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocMovimiento.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            objDocMovimiento.CodDocMovimiento = onRpta;
            return onRpta;

        }
        /// <summary>
        /// Método privado para la actualizacion de Movimientos de un Documento
        /// </summary>
        /// <returns>Retorna indicador de actualizacion de un Movimiento</returns>  
        private int ActualizarMovDocumento(DocMovimiento objDocMovimiento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocMovimiento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocMovimiento.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodArea", SqlDbType.Int);
            param.Value = objDocMovimiento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
            param.Value = objDocMovimiento.CodTrabajador;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecRecepcion", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaRecepcion, default(DateTime))) param.Value = objDocMovimiento.FechaRecepcion;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecLectura", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaLectura, default(DateTime))) param.Value = objDocMovimiento.FechaLectura;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siPlazo", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.Plazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecPlazo", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaPlazo, default(DateTime))) param.Value = objDocMovimiento.FechaPlazo;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siNivel", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.Nivel;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstFirma", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.IndicadorFirma;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecVisado", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaVisado, default(DateTime))) param.Value = objDocMovimiento.FechaVisado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@siEstVisado", SqlDbType.SmallInt);
            param.Value = objDocMovimiento.IndicadorVisado;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecFirma", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaFirma, default(DateTime))) param.Value = objDocMovimiento.FechaFirma;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
            if (!object.Equals(objDocMovimiento.FechaDerivacion, default(DateTime?))) param.Value = objDocMovimiento.FechaDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocMovimiento.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocMovimiento.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocMovimiento.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocMovimiento.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocMovimiento.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            objDocMovimiento.CodDocMovimiento = onRpta;
            return onRpta;

        }
        /// <summary>
        /// Método privado para la insercion de codigos a origen interno de los movimientos
        /// </summary>
        /// <returns>Retorna indicador </returns>  
        private int InsertarCodigosInternos(String vCodigosInternos,int codDocMovimiento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_ActualizarMovimientosInternos]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocMovimiento", SqlDbType.Int);
            param.Value = codDocMovimiento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vCodigosInternos", SqlDbType.VarChar, 1000);
            param.Value = vCodigosInternos;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }




        /// <summary>
        /// Método privado para el registro de los Comentarios de un Documento
        /// </summary>
        /// <returns>Retorna indicador de registro de un nuevo Comentario</returns>  
        public int InsertarMovComentario(DocComentario objDocComentario, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocComentario]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@vComentario", SqlDbType.VarChar, 1000);
            param.Value = objDocComentario.Comentario;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocMovimiento", SqlDbType.Int);
            param.Value = objDocComentario.CodDocMovimiento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocComentario.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocDerivacion", SqlDbType.Int);
            param.Value = objDocComentario.CodDocDerivacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocComentario.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocComentario.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocComentario.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocComentario.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocComentario.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }
        /// <summary>
        /// Método privado para la actualizacion de los Comentarios de un Documento
        /// </summary>
        /// <returns>Retorna indicador de registro de un nuevo Comentario</returns>  
        private int ActualizaMovComentario(DocComentario objDocComentario, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocComentario]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@vComentario", SqlDbType.VarChar, 1000);
            param.Value = objDocComentario.Comentario;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocMovimiento", SqlDbType.Int);
            param.Value = objDocComentario.CodDocMovimiento;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocComentario.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocComentario.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocComentario.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocComentario.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocComentario.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        /// <summary>
        /// Método privado para la derivacion de un documento
        /// </summary>
        /// <returns>Retorna indicador de derivacion de un documento</returns>  
        public int DerivarDocumento(DerivarDocumentoDTO objDocumento, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_DerivarDocumento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodArea", SqlDbType.VarChar, 1000);
            param.Value = objDocumento.CodArea;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocumento.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
            param.Value = objDocumento.CodTrabajadores;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iTipoAccion", SqlDbType.Int);
            param.Value = objDocumento.CodAccion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocumento.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocumento.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocumento.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuActualizacion";
            param.Value = objDocumento.UsuarioActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnActualizacion";
            param.Value = objDocumento.LoginActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolActualizacion";
            param.Value = objDocumento.RolActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }


        public int RegistrarDerivarDocumento(DerivarDocumentoDTO objDocumento)
        {
            int nRpta = 0;
            bool rollback = false;
            DerivarDocumentoDTO objDerivarDTO = new DerivarDocumentoDTO();
            DocComentario objDocComentario = new DocComentario();
            objDerivarDTO.CodDocMovimientoAnterior = GetUltimoMovimiento(objDocumento.CodDocumento);
            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            objDerivarDTO.CodDocumento = objDocumento.CodDocumento;
                            objDerivarDTO.UsuarioCreacion = objDocumento.UsuarioCreacion;
                            objDerivarDTO.LoginCreacion = objDocumento.LoginCreacion;
                            objDerivarDTO.RolCreacion = objDocumento.RolCreacion;
                            objDerivarDTO.UsuarioActualizacion = objDocumento.UsuarioActualizacion;
                            objDerivarDTO.LoginActualizacion = objDocumento.LoginActualizacion;
                            objDerivarDTO.RolActualizacion = objDocumento.RolActualizacion;
                            for (int x = 0; x < objDocumento.DerivarDocumento.Count; x++)
                            {
                                objDerivarDTO.CodArea = objDocumento.DerivarDocumento[x].CodArea[0];
                                objDerivarDTO.CodAccion = objDocumento.DerivarDocumento[x].CodAccion[0];
                                for (int y = 0; y < objDocumento.DerivarDocumento[x].CodTrabajadores.Count; y++)
                                {
                                    objDerivarDTO.CodTrabajadores = objDocumento.DerivarDocumento[x].CodTrabajadores[y];
                                    nRpta = DerivarDocumento(objDerivarDTO, cmd);

                                    if (nRpta >= 0)
                                    {
                                        objDocComentario.CodDocMovimiento = nRpta;
                                        objDocComentario.UsuarioCreacion = objDocumento.UsuarioCreacion;
                                        objDocComentario.CodDocumento = objDocumento.CodDocumento;
                                        objDocComentario.HostCreacion = objDocumento.HostCreacion;
                                        objDocComentario.InstanciaCreacion = objDocumento.InstanciaCreacion;
                                        objDocComentario.LoginCreacion = objDocumento.LoginCreacion;
                                        objDocComentario.RolCreacion = objDocumento.RolCreacion;
                                        objDocComentario.Comentario = objDocumento.Comentario;
                                        nRpta = InsertarMovComentario(objDocComentario, cmd);
                                    }
                                }

                            }

                            if (nRpta >= 0)
                            {
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }
                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Derivar Documento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();


                        }
                    }

                    return nRpta;
                }

            }
        }

        /// <summary>
        /// Método privado para el cambio de estado del documento
        /// </summary>
        /// <returns>Retorna indicador de realizó el cambio de estado</returns>  
        public int CambiarEstadoDocumento(int codDocumento, short nuevoEstado, string usuario, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_CambiarEstadoDocumento]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int, 1000);
            param.Value = codDocumento;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@newEstadoDocumento", SqlDbType.TinyInt);
            param.Value = nuevoEstado;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuario";
            param.Value = usuario;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        /// <summary>
        /// Método privado para la actualizacion de campos del documento sied
        /// </summary>
        /// <returns>Retorna indicador de realizó la actualizacion</returns>  
        public int ActualizarDocumentoEnvioSIEDService(Documento obj, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_ActualizarDocumentoEnvioSIEDService]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int, 1000);
            param.Value = obj.CodDocumento;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@iCodRespuestaSIED", SqlDbType.VarChar);
            param.Value = obj.CodRespuestaSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocumentoRespuestaSIED", SqlDbType.Int);
            param.Value = obj.CodDocumentoRespuestaSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vCodCUORespuestaSIED", SqlDbType.VarChar);
            param.Value = obj.CodCUORespuestaSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vMensajeSIED", SqlDbType.VarChar);
            param.Value = obj.MensajeSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar);
            param.Value = obj.CodRespuestaSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@vRolActualizacion", SqlDbType.VarChar);
            param.Value = obj.CodRespuestaSIED;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        /// <summary>
        /// Método privado para el cambio de estado del documento derivado
        /// </summary>
        /// <returns>Retorna indicador de realizó el cambio de estado</returns>  
        public int CambiarEstadoDocumentoDerivado(int codDocumentoDerivacion, short nuevoEstado, string usuario, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_CambiarEstadoDocumentoDerivacion]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumentoDerivacion", SqlDbType.Int);
            param.Value = codDocumentoDerivacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter("@newEstadoDocumento", SqlDbType.TinyInt);
            param.Value = nuevoEstado;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuario";
            param.Value = usuario;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        /// <summary>
        /// Método privado para el cambio de estado del documento derivado
        /// </summary>
        /// <returns>Retorna indicador de realizó el cambio de estado</returns>  
        public int EliminarDocumentoDerivado(DocDerivacion obj, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_EliminarDocumentoDerivacion]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumentoDerivacion", SqlDbType.Int);
            param.Value = obj.CodDocumentoDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuario";
            param.Value = obj.UsuarioActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        /// <summary>
        /// Método privado para el cambio de estado del documento derivado
        /// </summary>
        /// <returns>Retorna indicador de realizó el cambio de estado</returns>  
        public int EliminarDocumentoDerivadoEnvioSIED(DocDerivacion obj, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_EliminarDocumentoDerivacionEnvioSIED]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodDocumentoDerivacion", SqlDbType.Int);
            param.Value = obj.CodDocumentoDerivacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuario";
            param.Value = obj.UsuarioActualizacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        public int FinalizarDocumentoEnvioSIED(Documento objDocumento)
        {
            int onFlagOk = -1;

            var doc = GetDocumentoEnvioSIED(objDocumento);
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            onFlagOk = CambiarEstadoDocumento(objDocumento.CodDocumento,
                                    Convert.ToInt16(EstadoDocumento.ATENDIDO), objDocumento.UsuarioCreacion, cmd);


                            if (onFlagOk > 0)
                            {
                                onFlagOk = ActualizarDocumentoEnvioSIEDService(objDocumento, cmd);

                                if (onFlagOk > 0)
                                {
                                var objDocMovimiento = new DocMovimiento();
                                objDocMovimiento.CodArea = doc.CodArea;
                                objDocMovimiento.FechaRecepcion = doc.FechaRecepcion;
                                objDocMovimiento.FechaLectura = doc.FechaDocumento;
                                objDocMovimiento.Plazo = doc.Plazo;
                                objDocMovimiento.FechaPlazo = doc.FechaPlazo;
                                objDocMovimiento.IndicadorFirma = 0;
                                objDocMovimiento.IndicadorVisado = 0;
                                objDocMovimiento.CodTrabajador = doc.CodTrabajador;
                                objDocMovimiento.CodDocumento = doc.CodDocumento;
                                objDocMovimiento.CodMovAnterior = doc.CodUltimoMovimiento;
                                objDocMovimiento.EstadoDocumento = Convert.ToInt16(EstadoDocumento.ATENDIDO);
                                objDocMovimiento.UsuarioCreacion = objDocumento.UsuarioCreacion;
                                objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Finalizar);

                                onFlagOk = InsertarMovDocumentoSIED(objDocMovimiento, cmd);
                                }
                               
                            }
                            if (onFlagOk >= 0)
                            {
                                tx.Commit();
                            }
                            else { tx.Rollback();  }

                        }
                        catch (Exception ex)
                        {
                            onFlagOk = -1;
                            tx.Rollback();
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error FinalizarDocumentoEnvioSIED");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return onFlagOk;
        }
        

        public int RegistrarAvance(RegistrarAvanceDTO registrarAvanceDTO)
        {
            int nRpta = 0;
            int onFlagOk = -1;
            bool rollback = false;
            string guidTmp = "";
            string correlativo = "";
            int codDocMovimiento;
            Documento documento;
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            int correlativoAdjuntoInicial = GetCorrelativoAnexosInicial(registrarAvanceDTO.CodDocumento, TipoArchivos.Adjuntos);

            DocDerivacion derivacion = null;

            if (registrarAvanceDTO.CodDocDerivacion.HasValue)
                derivacion = new DocDerivadoDAO(cadenaConexion).GetDocumentoDerivadoPorId(registrarAvanceDTO.CodDocDerivacion.Value);
            try
            {
                if (registrarAvanceDTO.LstDocAnexo != null)
                    guidTmp = docAnexoDAO.GuardarArchivosTemporalesAnexo(registrarAvanceDTO.LstDocAnexo, "ADJ", correlativoAdjuntoInicial);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
                throw new Exception("No se puede establecer conexión con el servidor de archivos.");
            }
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                var flagSied = GetFlagSIED(registrarAvanceDTO.CodDocumento);

                if (flagSied == Convert.ToInt32(FlagSIED.RECEPCION))
                {
                    documento = GetDocumentoRecepcionSIED(new Documento()
                    {
                        CodDocumento = registrarAvanceDTO.CodDocumento
                    });
                }
                else
                {
                    documento = GetDocumento(new Documento()
                    {
                        CodDocumento = registrarAvanceDTO.CodDocumento
                    });
                }
                
                correlativo = documento.Correlativo;

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;

                            if (registrarAvanceDTO.Origen == Convert.ToInt16(TipoOrigen.INTERNO))
                            {
                                if (documento.EstadoDocumento == Convert.ToInt16(EstadoDocumento.PENDIENTE))
                                {
                                    onFlagOk = CambiarEstadoDocumento(registrarAvanceDTO.CodDocumento,
                                    Convert.ToInt16(EstadoDocumento.EN_PROCESO), registrarAvanceDTO.UsuarioCreacion, cmd);


                                    if (registrarAvanceDTO.CodDocDerivacion.HasValue)
                                    {
                                        onFlagOk = CambiarEstadoDocumentoDerivado(registrarAvanceDTO.CodDocDerivacion.Value,
                                                                   Convert.ToInt16(EstadoDocumento.ATENDIDO), registrarAvanceDTO.UsuarioCreacion, cmd);
                                    }
                                }
                                else
                                {
                                    if (registrarAvanceDTO.CodDocDerivacion.HasValue)
                                    {
                                        onFlagOk = CambiarEstadoDocumentoDerivado(registrarAvanceDTO.CodDocDerivacion.Value,
                                    Convert.ToInt16(EstadoDocumento.ATENDIDO), registrarAvanceDTO.UsuarioCreacion, cmd);

                                    }
                                    else
                                    {
                                        onFlagOk = CambiarEstadoDocumento(registrarAvanceDTO.CodDocumento,
                                        Convert.ToInt16(EstadoDocumento.EN_PROCESO), registrarAvanceDTO.UsuarioCreacion, cmd);
                                    }
                                }
                            }
                            else
                            {
                                if (registrarAvanceDTO.CodDocDerivacion.HasValue)
                                {
                                    onFlagOk = CambiarEstadoDocumentoDerivado(registrarAvanceDTO.CodDocDerivacion.Value,
                                Convert.ToInt16(EstadoDocumento.ATENDIDO), registrarAvanceDTO.UsuarioCreacion, cmd);

                                }
                                else
                                {
                                    onFlagOk = CambiarEstadoDocumento(registrarAvanceDTO.CodDocumento,
                                    Convert.ToInt16(EstadoDocumento.EN_PROCESO), registrarAvanceDTO.UsuarioCreacion, cmd);
                                }
                            }

                            if (onFlagOk > 0)
                            {
                                var objDocMovimiento = new DocMovimiento();
                                objDocMovimiento.CodArea = documento.CodArea;
                                objDocMovimiento.FechaRecepcion = documento.FechaRecepcion;
                                objDocMovimiento.FechaLectura = documento.FechaDocumento;
                                objDocMovimiento.Plazo = documento.Plazo;
                                objDocMovimiento.FechaPlazo = documento.FechaPlazo;
                                //objMovDocumento.objDocMovimiento.Nivel = 1;
                                objDocMovimiento.IndicadorFirma = 0;
                                //objMovDocumento.objDocMovimiento.FechaVisado = DateTime.Parse(null);
                                objDocMovimiento.IndicadorVisado = 0;
                                //objMovDocumento.objDocMovimiento.FechaFirma = null;
                                objDocMovimiento.CodTrabajador = documento.CodTrabajador;
                                objDocMovimiento.CodDocumento = documento.CodDocumento;
                                objDocMovimiento.CodMovAnterior = documento.CodUltimoMovimiento;
                                objDocMovimiento.EstadoDocumento = Convert.ToInt16(EstadoDocumento.EN_PROCESO);
                                objDocMovimiento.UsuarioCreacion = registrarAvanceDTO.UsuarioCreacion;
                                objDocMovimiento.HostCreacion = registrarAvanceDTO.HostCreacion;
                                objDocMovimiento.InstanciaCreacion = registrarAvanceDTO.InstanciaCreacion;
                                objDocMovimiento.LoginCreacion = registrarAvanceDTO.LoginCreacion;
                                objDocMovimiento.RolCreacion = registrarAvanceDTO.RolCreacion;
                                objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.RegistrarAvance);

                                if (registrarAvanceDTO.CodDocDerivacion.HasValue)
                                {
                                    objDocMovimiento.CodDocDerivacion = registrarAvanceDTO.CodDocDerivacion;
                                    objDocMovimiento.CodAreaDerivacion = derivacion.CodArea;
                                    objDocMovimiento.CodResponsableDerivacion = derivacion.CodResponsable;
                                    objDocMovimiento.siEstadoDerivacion = Convert.ToInt16(EstadoDocumento.ATENDIDO);
                                }

                                onFlagOk = InsertarMovDocumento(objDocMovimiento, cmd);
                                codDocMovimiento = onFlagOk;
                                if (onFlagOk > 0)
                                {
                                    //Evaluando si hay comentarios
                                    objDocMovimiento.objDocComentario = new DocComentario();
                                    objDocMovimiento.objDocComentario.Comentario = registrarAvanceDTO.Comentario;
                                    if (objDocMovimiento.objDocComentario != null)
                                    {
                                        objDocMovimiento.objDocComentario.CodDocMovimiento = objDocMovimiento.CodDocMovimiento;
                                        objDocMovimiento.objDocComentario.CodDocumento = objDocMovimiento.CodDocumento;
                                        objDocMovimiento.objDocComentario.UsuarioCreacion = registrarAvanceDTO.UsuarioCreacion;
                                        objDocMovimiento.objDocComentario.HostCreacion = registrarAvanceDTO.HostCreacion;
                                        objDocMovimiento.objDocComentario.InstanciaCreacion = registrarAvanceDTO.InstanciaCreacion;
                                        objDocMovimiento.objDocComentario.LoginCreacion = registrarAvanceDTO.LoginCreacion;
                                        objDocMovimiento.objDocComentario.RolCreacion = registrarAvanceDTO.RolCreacion;
                                        objDocMovimiento.objDocComentario.CodDocDerivacion = registrarAvanceDTO.CodDocDerivacion;
                                        onFlagOk = InsertarMovComentario(objDocMovimiento.objDocComentario, cmd);
                                    }
                                    if (registrarAvanceDTO.LstDocAnexo != null)
                                    {
                                        int cc = correlativoAdjuntoInicial;
                                        foreach (DocAnexo item in registrarAvanceDTO.LstDocAnexo)
                                        {
                                            item.CodDocumentoMovimiento = codDocMovimiento;
                                            if (item.CodLaserfiche > 0)
                                            {
                                                item.CodDocumento = registrarAvanceDTO.CodDocumento;
                                                item.UsuarioCreacion = registrarAvanceDTO.UsuarioCreacion;
                                                item.HostCreacion = registrarAvanceDTO.HostCreacion;
                                                item.InstanciaCreacion = registrarAvanceDTO.InstanciaCreacion;
                                                item.LoginCreacion = registrarAvanceDTO.LoginCreacion;
                                                item.RolCreacion = registrarAvanceDTO.RolCreacion;
                                                item.secuencia = cc;
                                                item.TipoArchivo = Convert.ToInt32(TipoArchivos.Adjuntos);
                                                onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                                cc++;
                                            }
                                        }
                                    }
                                }

                            }
                            if (onFlagOk >= 0)
                            {
                                nRpta = registrarAvanceDTO.CodDocumento;
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }

            if (!rollback)
            {
                docAnexoDAO.MoverArchivosAnexosTemporales(registrarAvanceDTO.LstDocAnexo, correlativo, "Adjuntos", "ADJ", correlativoAdjuntoInicial);
            }
            else
            {
                docAnexoDAO.LimpiarCarpetaAnexos(registrarAvanceDTO.LstDocAnexo);
            }
            return nRpta;
        }
                
        public DocDerivacion GetDocumentoDerivadoxTrabajador(DocDerivacion EObj)
        {
            DocDerivacion objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorTrabajador]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = EObj.CodDocumento;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodTrabajador";
                    param.Value = EObj.CodResponsable;
                    cmd.Parameters.Add(param);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocDerivacion();
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = short.Parse(reader["iCodArea"].ToString());
                                if (!Convert.IsDBNull(reader["iCodResponsable"])) objEntidad.CodResponsable = Convert.ToInt32(reader["iCodResponsable"]);
                                if (!Convert.IsDBNull(reader["siEstadoDerivacion"])) objEntidad.EstadoDerivacion = short.Parse(reader["siEstadoDerivacion"].ToString());
                                if (!Convert.IsDBNull(reader["siTipoAcceso"])) objEntidad.TipoAcceso = short.Parse(reader["siTipoAcceso"].ToString());
                                if (!Convert.IsDBNull(reader["dFecDerivacion"])) objEntidad.FecDerivacion = DateTime.Parse(reader["dFecDerivacion"].ToString());
                                if (!Convert.IsDBNull(reader["ResponsableDerivacion"])) objEntidad.Responsable = Convert.ToString(reader["ResponsableDerivacion"].ToString());
                                if (!Convert.IsDBNull(reader["vNombres"])) objEntidad.NombreResponsable = Convert.ToString(reader["vNombres"].ToString());
                                if (!Convert.IsDBNull(reader["vApeMaterno"])) objEntidad.ApellidoMatResponsable = Convert.ToString(reader["vApeMaterno"].ToString());
                                if (!Convert.IsDBNull(reader["vApePaterno"])) objEntidad.ApellidoPatResponsable = Convert.ToString(reader["vApePaterno"].ToString());
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);

                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }

        /// <summary>
        /// Método Público para el Obtener Datos de un Documento Derivado
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public DocDerivacion GetDocumentoDerivadoPorId(int CodDocumentoDerivacion)
        {
            DocDerivacion objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorId]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumentoDerivacion";
                    param.Value = CodDocumentoDerivacion;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocDerivacion();
                                if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocumentoDerivacion = Convert.ToInt32(reader["iCodDocumentoDerivacion"]);
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["iCodResponsable"])) objEntidad.CodResponsable = Convert.ToInt32(reader["iCodResponsable"]);
                                if (!Convert.IsDBNull(reader["siEstadoDerivacion"])) objEntidad.EstadoDerivacion = Convert.ToInt16(reader["siEstadoDerivacion"]);
                                if (!Convert.IsDBNull(reader["siTipoAcceso"])) objEntidad.TipoAcceso = Convert.ToInt16(reader["siTipoAcceso"]);
                                if (!Convert.IsDBNull(reader["dFecDerivacion"])) objEntidad.FecDerivacion = DateTime.Parse(reader["dFecDerivacion"].ToString());

                                if (!Convert.IsDBNull(reader["NombreArea"])) objEntidad.NombreArea = Convert.ToString(reader["NombreArea"]);
                                if (!Convert.IsDBNull(reader["NombreResponsable"])) objEntidad.NombreResponsable = Convert.ToString(reader["NombreResponsable"]);
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                if (!Convert.IsDBNull(reader["TipoAccion"])) objEntidad.TipoAccion = Convert.ToString(reader["TipoAccion"]);
                                if (!Convert.IsDBNull(reader["iCodDocumentoFirma"])) objEntidad.CodDocumentoFirma = Convert.ToInt32(reader["iCodDocumentoFirma"]);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }


        /// <summary>
        /// Método Público para el Obtener Datos de un Documento Derivado
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public List<DocDerivacion> GetDocumentosDerivadosPorCodDocumento(int CodDocumento,int TipoAccion)
        {
            DocDerivacion objEntidad = null;
            List<DocDerivacion> lstDocumento = new List<DocDerivacion>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = CodDocumento;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@siTipoAcceso";
                    param.Value = TipoAccion;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocDerivacion();
                                if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocumentoDerivacion = Convert.ToInt32(reader["iCodDocumentoDerivacion"]);
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["iCodResponsable"])) objEntidad.CodResponsable = Convert.ToInt32(reader["iCodResponsable"]);
                                if (!Convert.IsDBNull(reader["siEstadoDerivacion"])) objEntidad.EstadoDerivacion = Convert.ToInt16(reader["siEstadoDerivacion"]);
                                if (!Convert.IsDBNull(reader["siTipoAcceso"])) objEntidad.TipoAcceso = Convert.ToInt16(reader["siTipoAcceso"]);
                                if (!Convert.IsDBNull(reader["dFecDerivacion"])) objEntidad.FecDerivacion = DateTime.Parse(reader["dFecDerivacion"].ToString());
                                //sied
                                if (!Convert.IsDBNull(reader["vNombreEntidadSIED"])) objEntidad.NombreEntidadSIED = Convert.ToString(reader["vNombreEntidadSIED"]);
                                if (!Convert.IsDBNull(reader["vRucSIED"])) objEntidad.rucSied = Convert.ToString(reader["vRucSIED"]);
                                if (!Convert.IsDBNull(reader["iFormatoDocumentoSIED"])) objEntidad.FormatoDocSied = Convert.ToString(reader["iFormatoDocumentoSIED"]);
                                if (!Convert.IsDBNull(reader["iCodEntidadSIED"])) objEntidad.CodEntidadSied = Convert.ToInt16(reader["iCodEntidadSIED"]);
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                if (!Convert.IsDBNull(reader["iTipoAccion"])) objEntidad.TipoAccion = Convert.ToString(reader["iTipoAccion"]);
                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocumento;
        }


        /// <summary>
        /// Método Público para el Listado de Tipos de documentos
        /// </summary>
        /// <returns>Retorna Lista tipo documento</returns>  
        public TipoDocumentoDTO ObtenerTipoDocumento(TipoDocumentoDTO objParams)
        {
            TipoDocumentoDTO objEntidad = null;
           
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Obtener_Tipo_Documento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodTipoDocumento";
                    param.Value = objParams.CodTipoDocumento;
                    cmd.Parameters.Add(param);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new TipoDocumentoDTO();
                                if (!Convert.IsDBNull(reader["iCodTipDocumento"])) objEntidad.CodTipoDocumento = Convert.ToInt32(reader["iCodTipDocumento"]);
                                if (!Convert.IsDBNull(reader["vAbreviatura"])) objEntidad.Abreviatura = (reader["vAbreviatura"].ToString());
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.sFechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.sFechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = (reader["vDescripcion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = (reader["vUsuCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = (reader["vUsuActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de  tipo documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return objEntidad;
        }

        /// <summary>
        /// Método Público para el Listado de Tipos de documentos
        /// </summary>
        /// <returns>Retorna Lista tipo documento</returns>  
        public TipoDocumentoDTO ObtenerTipoDocumentoPorDescripcion(TipoDocumentoDTO objParams)
        {
            TipoDocumentoDTO objEntidad = null;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ObtenerTipoDocumentoPorDescripcion]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vDescripcion";
                    param.Value = objParams.Descripcion;
                    cmd.Parameters.Add(param);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new TipoDocumentoDTO();
                                if (!Convert.IsDBNull(reader["iCodTipDocumento"])) objEntidad.CodTipoDocumento = Convert.ToInt32(reader["iCodTipDocumento"]);
                                if (!Convert.IsDBNull(reader["vAbreviatura"])) objEntidad.Abreviatura = (reader["vAbreviatura"].ToString());
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.sFechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.sFechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = (reader["vDescripcion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = (reader["vUsuCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = (reader["vUsuActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de  tipo documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return objEntidad;
        }

        /// <summary>
        /// Método Público para el Listado de Tipos de documentos
        /// </summary>
        /// <returns>Retorna Lista tipo documento</returns>  
        public List<TipoDocumentoDTO> ListarTiposDocumentos(TipoDocumentoDTO objParams, out int totalRegistros)
        {
            TipoDocumentoDTO objEntidad = null;
            totalRegistros = 0;
            List<TipoDocumentoDTO> lstDocumento = new List<TipoDocumentoDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_Tipo_DocumentoPaginado]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@texto";
                    param.Value = objParams.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@NumeroPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@tamaniopagina";
                    param.Value = objParams.TamanioPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new TipoDocumentoDTO();
                                if (!Convert.IsDBNull(reader["iCodTipDocumento"])) objEntidad.CodTipoDocumento = Convert.ToInt32(reader["iCodTipDocumento"]);
                                if (!Convert.IsDBNull(reader["vAbreviatura"])) objEntidad.Abreviatura = (reader["vAbreviatura"].ToString());
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = (reader["vDescripcion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = (reader["vUsuCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = (reader["vUsuActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());

                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                                lstDocumento.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de  tipo documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return lstDocumento;
        }

        public List<String> ListarEmailPorAreaJefe(String codigos)
        {

            List<String> lstEmail = new List<String>();
            string[] arrCodigos = codigos.Split('|');
            for (int i = 0; i < arrCodigos.Length; i++)
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {           
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_Email_PorAreaJefe]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter param;
                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@iCodArea";
                        param.Value = arrCodigos[i];
                        cmd.Parameters.Add(param);


                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            try
                            {
                                while (reader.Read())
                                {
                                    var correo = "";
                                    if (!Convert.IsDBNull(reader["vEmail"])) correo = (reader["vEmail"]).ToString();

                                    lstEmail.Add(correo);
                                }

                            }
                            catch (Exception ex)
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                new LogWriter(ex.Message);
                                throw new Exception("Error al Obtener el Listado de  Email");
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                            }
                        }

                        //totalRegistros = (int)param.Value;
                    }
              }
            }
            return lstEmail;
        }


        public List<String> ListarEmailPorTrabajador(String codigos)
        {

            List<String> lstEmail = new List<String>();
            string[] arrCodigos = codigos.Split('|');
            for (int i = 0; i < arrCodigos.Length; i++)
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_Email_PorTrabajador]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter param;
                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@CodTrabajador";
                        param.Value = arrCodigos[i];
                        cmd.Parameters.Add(param);


                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            try
                            {
                                while (reader.Read())
                                {
                                    var correo = "";
                                    if (!Convert.IsDBNull(reader["vEmail"])) correo = (reader["vEmail"]).ToString();

                                    lstEmail.Add(correo);
                                }

                            }
                            catch (Exception ex)
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                new LogWriter(ex.Message);
                                throw new Exception("Error al Obtener el Listado de  Email");
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                            }
                        }

                        //totalRegistros = (int)param.Value;
                    }
                }
            }
            return lstEmail;
        }
        
         public List<String> ListarEmailResponsablePorCodigoDocumento(String codigos)
        {

            List<String> lstEmail = new List<String>();
            string[] arrCodigos = codigos.Split('|');
            for (int i = 0; i < arrCodigos.Length; i++)
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_EmailResponsable_PorCodDocumento]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter param;
                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@CodDocumento";
                        param.Value = arrCodigos[i];
                        cmd.Parameters.Add(param);


                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            try
                            {
                                while (reader.Read())
                                {
                                    var correo = "";
                                    if (!Convert.IsDBNull(reader["vEmail"])) correo = (reader["vEmail"]).ToString();

                                    lstEmail.Add(correo);
                                }

                            }
                            catch (Exception ex)
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                new LogWriter(ex.Message);
                                throw new Exception("Error al Obtener el Listado de  Email");
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                            }
                        }

                        //totalRegistros = (int)param.Value;
                    }
                }
            }
            return lstEmail;
        }


        public int RegistrarTipoDocumento(TipoDocumentoDTO objDocumento)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            nRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_RegistrarTipoDocumento]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();

                            SqlParameter param = new SqlParameter("@vAbreviatura", SqlDbType.VarChar);
                            param.Value = objDocumento.Abreviatura;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vDescripcion", SqlDbType.VarChar);
                            param.Value = objDocumento.Descripcion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@Estado", SqlDbType.VarChar);
                            param.Value = objDocumento.Estado;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vUsuCreacion";
                            param.Value = objDocumento.UsuarioCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vHstCreacion";
                            param.Value = objDocumento.HostCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vLgnCreacion";
                            param.Value = objDocumento.LoginCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vRolCreacion";
                            param.Value = objDocumento.RolCreacion;
                            cmd.Parameters.Add(param);


                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (nRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }

        public int ActualizarTipoDocumento(TipoDocumentoDTO objDocumento)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_ActualizarTipoDocumento]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();


                            SqlParameter param = new SqlParameter("@vAbreviatura", SqlDbType.VarChar);
                            param.Value = objDocumento.Abreviatura;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@iCodTipoDocumento", SqlDbType.VarChar);
                            param.Value = objDocumento.CodTipoDocumento;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vDescripcion", SqlDbType.VarChar);
                            param.Value = objDocumento.Descripcion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@Estado", SqlDbType.VarChar);
                            param.Value = objDocumento.Estado;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vUsuActualizacion";
                            param.Value = objDocumento.UsuarioActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vHstActualizacion";
                            param.Value = objDocumento.HostActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vInsActualizacion";
                            param.Value = objDocumento.InstanciaActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vLgnActualizacion";
                            param.Value = objDocumento.LoginActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@vRolActualizacion";
                            param.Value = objDocumento.RolActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }

        public int EliminarTipoDocumento(TipoDocumentoDTO objDocumento)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_EliminarTipoDocumento]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();


                            SqlParameter param = new SqlParameter("@iCodTipoDocumento", SqlDbType.VarChar);
                            param.Value = objDocumento.CodTipoDocumento;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar);
                            param.Value = objDocumento.UsuarioActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar);
                            param.Value = objDocumento.LoginActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Documento Externo");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }



        /// <summary>
        /// Método Público para el Obtener estado inicial del documento
        /// </summary>
        /// <returns>Retorna idEstado Documento</returns>  
        public DerivarDocumentoDTO GetEstadoDocumentoMovimiento(int CodDocumento, int operador)
        {
            
            DerivarDocumentoDTO objRes = new DerivarDocumentoDTO();
            //DerivarDocumentoDTO lstDocMovi = new List<DerivarDocumentoDTO>();

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerEstadoDocumentoMovimiento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = CodDocumento;
                    cmd.Parameters.Add(param);
                    
                   
                    param = new SqlParameter();                    
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iOperador";
                    param.Value = operador;
                    cmd.Parameters.Add(param);

                    
                    //param = new SqlParameter();
                    //param.Direction = ParameterDirection.Output;
                    //param.SqlDbType = SqlDbType.Int;
                    //param.ParameterName = "@onFlagOK";
                    //cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                                                                                                                                   
                            while (reader.Read())
                            {
                                objRes = new DerivarDocumentoDTO();
                                if (!Convert.IsDBNull(reader["iEstadoDocumento"])) objRes.iEstadoDocumento = Convert.ToInt32(reader["iEstadoDocumento"]);
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objRes.CodTrabajadores = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["iCodTrabajadorInter"])) objRes.iCodTrabajadorInter = Convert.ToInt32(reader["iCodTrabajadorInter"]);
                                if (!Convert.IsDBNull(reader["siOrigen"])) objRes.siOrigen = Convert.ToInt32(reader["siOrigen"]);
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objRes.Correlativo = Convert.ToString(reader["vCorrelativo"]);
                                break;

                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener estado del documento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objRes;
        }

        /// <summary>
        /// Método Público para Subir Documento digital
        /// </summary>
        /// <returns>Retorna el Indicador</returns> 
        public int SubirDocumentoLaserFicher(Documento objDocumento,out Documento objDocCorreo)
        {
          int rpta = 0;var onFlagOk = 0;var codDocMovimiento = 0;
          string filename = "";
          var codLaserFicheOld = objDocumento.LstDocAdjunto.FirstOrDefault().CodLaserfiche;
          var codDocumento = GetCodDocumentoPorCodLaserFIche(codLaserFicheOld);
          objDocumento.CodDocumento = codDocumento;
          var correlativo = GetCorrelativoPorIdLaserfiche(codLaserFicheOld.ToString()); 
          LaserFiche objLaserFiche = new LaserFiche();  
          objLaserFiche.EliminarDocumento(codLaserFicheOld);
          var bytes = Convert.FromBase64String(objDocumento.LstDocAdjunto.FirstOrDefault().FileArray);
          MemoryStream file = new MemoryStream(bytes);
          var codLaserFicheNew = objLaserFiche.CargarDocumentoLaserFichev3(file, "pdf", "test", string.Empty, string.Empty, correlativo, false, out filename);
          Documento objDoc2 = new Documento();
          int origen = GetOrigen(objDocumento);
            if((origen != Convert.ToInt16(TipoOrigen.INTERNO)) ||  (origen != Convert.ToInt16(TipoOrigen.EXTERNO)))
            {
                objDoc2 = GetDocumentoEnvioSIED(objDocumento);
            }
            else
            {
                objDoc2 = GetDocumento(objDocumento);
            }

         
          objDocCorreo = objDoc2;
            if (codLaserFicheNew > 0)
            {
                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        using (SqlTransaction tx = conn.BeginTransaction())
                        {
                            try
                            {
                                cmd.Transaction = tx;
                                rpta = ActualizarCodigoLaserFicheAdjunto(codLaserFicheOld, codLaserFicheNew, objDocumento, cmd);
                               
                                DocMovimiento objDocMovimiento = new DocMovimiento(); 
                                if (rpta > 0)
                                {
                                    DocFirma objDocFirma = new DocFirma();
                                    objDocFirma.CodDocumento = objDoc2.CodDocumento;
                                    objDocFirma.CodAdjunto = objDoc2.CodDocAdjunto;
                                    objDocFirma.CodLaserFiche = codLaserFicheNew;
                                    objDocFirma.CodDerivacion = objDocumento.CodDocDerivacion;
                                    objDocFirma.CodTrabajador = objDocumento.CodTrabajador;
                                    objDocFirma.CodArea = objDocumento.CodArea;
                                    objDocFirma.UsuarioCreacion = objDocumento.UsuarioCreacion;
                                    objDocFirma.LoginCreacion = objDocumento.LoginCreacion;
                                    objDocFirma.Comentario = objDocumento.reason;
                                    objDocFirma.posx = objDocumento.posx;
                                    objDocFirma.posy = objDocumento.posy;
                                    rpta = InsertarDocFirma(objDocFirma, cmd);

                                    if(rpta > 0)
                                    {
                                         objDocMovimiento = new DocMovimiento();
                                         objDocMovimiento.CodArea = objDoc2.CodArea;
                                         objDocMovimiento.FechaRecepcion = objDoc2.FechaRecepcion;
                                         objDocMovimiento.FechaLectura = objDoc2.FechaDocumento;
                                         objDocMovimiento.Plazo = objDoc2.Plazo;
                                         objDocMovimiento.FechaPlazo = objDoc2.FechaPlazo;
                                         objDocMovimiento.IndicadorFirma = 1;
                                         objDocMovimiento.IndicadorVisado = 0;
                                         objDocMovimiento.CodTrabajador = objDoc2.CodTrabajador;
                                         objDocMovimiento.CodDocumento = objDoc2.CodDocumento;
                                         objDocMovimiento.TipoMovimiento = Convert.ToInt32(EnumTipoMovimiento.Registro);
                                         objDocMovimiento.CodDocDerivacion = objDocumento.CodDocDerivacion;
                                        if (objDocumento.CodDocDerivacion != null)
                                        {
                                            objDocMovimiento.CodAreaDerivacion = objDocumento.CodArea;
                                            objDocMovimiento.CodResponsableDerivacion = objDocumento.CodTrabajador;
                                            objDocMovimiento.siEstadoDerivacion = Convert.ToInt32(EstadoDocumento.PENDIENTE);
                                        }
                                         objDocMovimiento.UsuarioCreacion = objDocumento.UsuarioCreacion;
                                         objDocMovimiento.HostCreacion = objDocumento.HostCreacion;
                                         objDocMovimiento.InstanciaCreacion = objDocumento.InstanciaCreacion;
                                         objDocMovimiento.LoginCreacion = objDocumento.LoginCreacion;
                                         objDocMovimiento.RolCreacion = objDocumento.RolCreacion;

                                         onFlagOk = InsertarMovDocumento(objDocMovimiento, cmd);
                                         codDocMovimiento = onFlagOk;

                                        if (onFlagOk > 0)
                                        {
                                            DocComentario objDocComentario = new DocComentario();
                                            objDocComentario.CodDocumento = objDocumento.CodDocumento;
                                            objDocComentario.Comentario = objDocumento.reason;
                                            objDocComentario.UsuarioCreacion = objDocumento.UsuarioCreacion; 
                                            objDocComentario.CodDocMovimiento = codDocMovimiento;
                                            objDocComentario.CodDocDerivacion = objDocumento.CodDocDerivacion;
                                            objDocComentario.HostCreacion = objDocumento.HostCreacion;
                                            objDocComentario.InstanciaCreacion = objDocumento.InstanciaCreacion;
                                            objDocComentario.LoginCreacion = objDocumento.LoginCreacion;
                                            objDocComentario.RolCreacion = objDocumento.RolCreacion;
                                            onFlagOk = InsertarMovComentario(objDocComentario, cmd);
                                        }
                                        if(objDocumento.CodDocDerivacion != null)
                                        {
                                            if(objDoc2.Origen == Convert.ToInt16(TipoOrigen.INTERNOSIED) || objDoc2.Origen == Convert.ToInt16(TipoOrigen.EXTERNOSIED)){
                                                int codDocDerivacion = objDocumento.CodDocDerivacion ?? default(int); 
                                                onFlagOk = CambiarEstadoDocumentoDerivado(codDocDerivacion, Convert.ToInt16(EstadoDocumento.ATENDIDO), objDocumento.UsuarioCreacion, cmd);
                                           }
                                        }
                                        

                                    }
                                    if(onFlagOk > 0)
                                    {
                                        tx.Commit();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                rpta = -1;
                                tx.Rollback();
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                //Generando Archivo Log
                                new LogWriter(ex.Message);
                                throw new Exception("Error al SubirDocumentoLaserFicher");
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                            }
                        }
                    }
                }
            }
          return rpta;
        }

        /// <summary>
        /// Método Para insertar Doc Firmna Digital
        /// </summary>
        /// <returns>Retorna el Indicador</returns> 
        public int InsertarDocFirma(DocFirma objDocFirma,SqlCommand cmd)
        {
            int nRpta = 0;
 
             cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocFirma]";
             cmd.CommandType = CommandType.StoredProcedure;
             cmd.Parameters.Clear();

             SqlParameter param = new SqlParameter("@CodDocumento", SqlDbType.Int);
             param.Value = objDocFirma.CodDocumento;
             cmd.Parameters.Add(param);

             param = new SqlParameter("@CodAdjunto", SqlDbType.Int);
             param.Value = objDocFirma.CodAdjunto;
             cmd.Parameters.Add(param);

             param = new SqlParameter("@CodLaserFiche", SqlDbType.Int);
             param.Value = objDocFirma.CodLaserFiche;
             cmd.Parameters.Add(param);

             param = new SqlParameter("@CodDerivacion", SqlDbType.Int);
             param.Value = objDocFirma.CodDerivacion;
             cmd.Parameters.Add(param);

             param = new SqlParameter("@CodTrabajador", SqlDbType.Int);
             param.Value = objDocFirma.CodTrabajador;
             cmd.Parameters.Add(param);

             param = new SqlParameter("@CodArea", SqlDbType.Int);
             param.Value = objDocFirma.CodArea;
             cmd.Parameters.Add(param);

             param = new SqlParameter("@Comentario", SqlDbType.VarChar);
             param.Size = 1000;
             param.Value = objDocFirma.Comentario;
             cmd.Parameters.Add(param);

             param = new SqlParameter("@Posx", SqlDbType.Int);
             param.Value = objDocFirma.posx;
             cmd.Parameters.Add(param);

             param = new SqlParameter("@Posy", SqlDbType.Int);
             param.Value = objDocFirma.posy;
             cmd.Parameters.Add(param);

             param = new SqlParameter();
             param.SqlDbType = SqlDbType.VarChar;
             param.ParameterName = "@vUsuCreacion";
             param.Value = objDocFirma.UsuarioCreacion;
             cmd.Parameters.Add(param);

             param = new SqlParameter();
             param.SqlDbType = SqlDbType.VarChar;
             param.ParameterName = "@vHstCreacion";
             param.Value = objDocFirma.HostCreacion;
             cmd.Parameters.Add(param);

             param = new SqlParameter();
             param.SqlDbType = SqlDbType.VarChar;
             param.ParameterName = "@vLgnCreacion";
             param.Value = objDocFirma.LoginCreacion;
             cmd.Parameters.Add(param);

             param = new SqlParameter();
             param.SqlDbType = SqlDbType.VarChar;
             param.ParameterName = "@vRolCreacion";
             param.Value = objDocFirma.RolCreacion;
             cmd.Parameters.Add(param);


             param = new SqlParameter();
             param.Direction = ParameterDirection.Output;
             param.SqlDbType = SqlDbType.Int;
             param.ParameterName = "@onFlagOK";
             cmd.Parameters.Add(param);

             cmd.ExecuteNonQuery();

                nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                return nRpta;
            
        }


        /// <summary>
        /// Método Público para obtener el detalle de avance
        /// </summary>
        /// <returns>Retorna objeto documento</returns>  
        public Documento GetDetalleCorreoAvance(int objParams)
        {
            Documento objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDetalleCorreoAvance]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = objParams;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new Documento();
                                if (!Convert.IsDBNull(reader["siPlazo"])) objEntidad.Plazo = Convert.ToInt16(reader["siPlazo"]);
                                if (!Convert.IsDBNull(reader["dFecPlazo"])) objEntidad.FechaPlazo = Convert.ToDateTime(reader["dFecPlazo"]);
                                if (!Convert.IsDBNull(reader["siPrioridad"])) objEntidad.Prioridad = short.Parse(reader["siPrioridad"].ToString());
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = Convert.ToInt16(reader["siOrigen"]);
                                if (!Convert.IsDBNull(reader["REPRESENTANTE"])) objEntidad.Representante = (reader["REPRESENTANTE"].ToString());
                                if (!Convert.IsDBNull(reader["EMPRESA"])) objEntidad.DescripcionEmpresa = (reader["EMPRESA"].ToString());
                                if (!Convert.IsDBNull(reader["DERIVADOPOR"])) objEntidad.DerivadoPor = (reader["DERIVADOPOR"].ToString());

                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }

        public int RegistrarCargo(RegistroCargoDTO cargo)
        {
            int nRpta = 0;
            int onFlagOk = -1;
            bool rollback = false;
            string guidTmp = "";
            string correlativo = "";
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            Documento documento;

            try
            {
                if (cargo.LstDocAnexo != null)
                    guidTmp = docAnexoDAO.GuardarArchivosTemporalesAnexo(cargo.LstDocAnexo, "CARGO", 1);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
                throw new Exception("No se puede establecer conexión con el servidor de archivos.");
            }

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                documento = GetDocumento(new Documento()
                {
                    CodDocumento = cargo.CodDocumento
                });
                correlativo = documento.Correlativo;

                int codMovimiento = ObtenerPrimerMovimientoDocumento(cargo.CodDocumento, Convert.ToInt32(EstadoDocumento.PENDIENTE));

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;

                            if (cargo.LstDocAnexo != null)
                            {
                                int cc = 1;
                                foreach (DocAnexo item in cargo.LstDocAnexo)
                                {
                                    item.CodDocumentoMovimiento = codMovimiento;
                                    if (item.CodLaserfiche > 0)
                                    {
                                        item.CodDocumento = cargo.CodDocumento;
                                        item.UsuarioCreacion = cargo.UsuarioCreacion;
                                        item.HostCreacion = cargo.HostCreacion;
                                        item.InstanciaCreacion = cargo.InstanciaCreacion;
                                        item.LoginCreacion = cargo.LoginCreacion;
                                        item.RolCreacion = cargo.RolCreacion;
                                        item.secuencia = cc;
                                        item.TipoArchivo = Convert.ToInt32(TipoArchivos.Cargo);
                                        onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                        cc++;
                                    }
                                }
                            }

                            if (onFlagOk >= 0)
                            {
                                nRpta = cargo.CodDocumento;
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Cargo del documento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }

            if (!rollback)
            {
                docAnexoDAO.MoverArchivosAnexosTemporales(cargo.LstDocAnexo, correlativo, "Adjuntos", "CARGO", 1);
            }
            else
            {
                docAnexoDAO.LimpiarCarpetaAnexos(cargo.LstDocAnexo);
            }
            return nRpta;
        }

        
       public int RegistrarCargoSIED(RegistroCargoDTO cargo)
        {
            int nRpta = 0;
            int onFlagOk = -1;
            bool rollback = false;
            string guidTmp = "";
            string correlativo = "";
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            Documento documento;

            try
            {
                if (cargo.LstDocAnexo != null)
                    guidTmp = docAnexoDAO.GuardarArchivosTemporalesAnexo(cargo.LstDocAnexo, "CARGO", 1);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
                throw new Exception("No se puede establecer conexión con el servidor de archivos.");
            }

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                documento = GetDocumentoEnvioSIED(new Documento()
                {
                    CodDocumento = cargo.CodDocumento
                });
                correlativo = documento.Correlativo;

                int codMovimiento = ObtenerPrimerMovimientoDocumento(cargo.CodDocumento, Convert.ToInt32(EstadoDocumento.PENDIENTE));

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;

                            if (cargo.LstDocAnexo != null)
                            {
                                int cc = 1;
                                foreach (DocAnexo item in cargo.LstDocAnexo)
                                {
                                    item.CodDocumentoMovimiento = codMovimiento;
                                    if (item.CodLaserfiche > 0)
                                    {
                                        item.CodDocumento = cargo.CodDocumento;
                                        item.UsuarioCreacion = cargo.UsuarioCreacion;
                                        item.HostCreacion = cargo.HostCreacion;
                                        item.InstanciaCreacion = cargo.InstanciaCreacion;
                                        item.LoginCreacion = cargo.LoginCreacion;
                                        item.RolCreacion = cargo.RolCreacion;
                                        item.secuencia = cc;
                                        item.TipoArchivo = Convert.ToInt32(TipoArchivos.Cargo);
                                        onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                        cc++;
                                    }
                                }
                            }

                            if (onFlagOk >= 0)
                            {
                                nRpta = cargo.CodDocumento;
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Cargo del documento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }

            if (!rollback)
            {
                docAnexoDAO.MoverArchivosAnexosTemporales(cargo.LstDocAnexo, correlativo, "Adjuntos", "CARGO", 1);
            }
            else
            {
                docAnexoDAO.LimpiarCarpetaAnexos(cargo.LstDocAnexo);
            }
            return nRpta;
        }

        public int registrarcargoRecepcionSIED(RegistroCargoDTO cargo)
        {
            int nRpta = 0;
            int onFlagOk = -1;
            bool rollback = false;
            string guidTmp = "";
            string correlativo = "";
            DocAnexoDAO docAnexoDAO = new DocAnexoDAO();
            Documento documento;

            try
            {
                if (cargo.LstDocAnexo != null)
                    guidTmp = docAnexoDAO.GuardarArchivosTemporalesAnexo(cargo.LstDocAnexo, "CARGO", 1);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
                throw new Exception("No se puede establecer conexión con el servidor de archivos.");
            }

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                documento = GetDocumentoRecepcionSIED(new Documento()
                {
                    CodDocumento = cargo.CodDocumento
                });
                correlativo = documento.Correlativo;

                int codMovimiento = ObtenerPrimerMovimientoDocumento(cargo.CodDocumento, Convert.ToInt32(EstadoDocumento.PENDIENTE));

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;

                            if (cargo.LstDocAnexo != null)
                            {
                                int cc = 1;
                                foreach (DocAnexo item in cargo.LstDocAnexo)
                                {
                                    item.CodDocumentoMovimiento = codMovimiento;
                                    if (item.CodLaserfiche > 0)
                                    {
                                        item.CodDocumento = cargo.CodDocumento;
                                        item.UsuarioCreacion = cargo.UsuarioCreacion;
                                        item.HostCreacion = cargo.HostCreacion;
                                        item.InstanciaCreacion = cargo.InstanciaCreacion;
                                        item.LoginCreacion = cargo.LoginCreacion;
                                        item.RolCreacion = cargo.RolCreacion;
                                        item.secuencia = cc;
                                        item.TipoArchivo = Convert.ToInt32(TipoArchivos.Cargo);
                                        onFlagOk = docAnexoDAO.InsertarDocAnexo(item, cmd);
                                        cc++;
                                    }
                                }
                            }

                            if (onFlagOk >= 0)
                            {
                                nRpta = cargo.CodDocumento;
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar el Cargo del documento");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }

            if (!rollback)
            {
                docAnexoDAO.MoverArchivosAnexosTemporales(cargo.LstDocAnexo, correlativo, "Adjuntos", "CARGO", 1);
            }
            else
            {
                docAnexoDAO.LimpiarCarpetaAnexos(cargo.LstDocAnexo);
            }
            return nRpta;
        }
        
    }


}

#endregion