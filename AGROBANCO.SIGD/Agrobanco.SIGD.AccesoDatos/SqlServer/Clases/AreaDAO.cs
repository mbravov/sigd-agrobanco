﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Agrobanco.SIGD.Comun;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class AreaDAO : IAreaDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public AreaDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }
        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Método Público para el Listado de Áreas
        /// </summary>
        /// <returns>Retorna Lista genérica de Áreas</returns>  
        public List<AreaDTO> ListarArea()
        {
            AreaDTO objEntidad = null;
            List<AreaDTO> lstArea = new List<AreaDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_Area]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    try {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new AreaDTO();
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                lstArea.Add(objEntidad);
                            }
                        }
                    } catch(Exception ex){
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener el Listado de Áreas");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }                  
                }
            }
            return lstArea;
        }

        public List<AreaDTO> ListarAreaSIED()
        {
            AreaDTO objEntidad = null;
            List<AreaDTO> lstArea = new List<AreaDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_AreaSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new AreaDTO();
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                lstArea.Add(objEntidad);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener el Listado de Áreas");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstArea;
        }
        public List<AreaDTO> ListarAreasPaginado(AreaDTO objParams, out int totalRegistros)
        {
            AreaDTO objEntidad = null;
            totalRegistros = 0;
            List<AreaDTO> lstAreas = new List<AreaDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_AreasPaginado]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@texto";
                    param.Value = objParams.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@NumeroPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@tamaniopagina";
                    param.Value = objParams.TamanioPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new AreaDTO();
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["vAbreviatura"])) objEntidad.Abreviatura = (reader["vAbreviatura"].ToString());
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = (reader["vDescripcion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = (reader["vUsuCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = (reader["vUsuActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());

                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                                lstAreas.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de areas");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return lstAreas;
        }


        /// <summary>
        /// Método Público para el Listado de Areas por id
        /// </summary>
        /// <returns>Retorna Lista de areas</returns>  
        public AreaDTO ObtenerAreasPorId(AreaDTO objParams)
        {
            AreaDTO objEntidad = null;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Obtener_Areas]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodArea";
                    param.Value = objParams.CodArea;
                    cmd.Parameters.Add(param);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new AreaDTO();
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["vAbreviatura"])) objEntidad.Abreviatura = (reader["vAbreviatura"].ToString());
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.sFechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.sFechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = (reader["vDescripcion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = (reader["vUsuCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = (reader["vUsuActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de  tipo documentos");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return objEntidad;
        }


        public int RegistrarArea(AreaDTO objArea)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_RegistrarArea]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();

                            SqlParameter param = new SqlParameter("@vAbreviatura", SqlDbType.VarChar);
                            param.Value = objArea.Abreviatura;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vDescripcion", SqlDbType.VarChar);
                            param.Value = objArea.Descripcion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@Estado", SqlDbType.VarChar);
                            param.Value = objArea.Estado;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vUsuCreacion";
                            param.Value = objArea.UsuarioCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vHstCreacion";
                            param.Value = objArea.HostCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vLgnCreacion";
                            param.Value = objArea.LoginCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vRolCreacion";
                            param.Value = objArea.RolCreacion;
                            cmd.Parameters.Add(param);


                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }

        public int ActualizarArea(AreaDTO objArea)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_ActualizarArea]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();


                            SqlParameter param = new SqlParameter("@vAbreviatura", SqlDbType.VarChar);
                            param.Value = objArea.Abreviatura;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@iCodArea", SqlDbType.VarChar);
                            param.Value = objArea.CodArea;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vDescripcion", SqlDbType.VarChar);
                            param.Value = objArea.Descripcion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@Estado", SqlDbType.VarChar);
                            param.Value = objArea.Estado;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vUsuActualizacion";
                            param.Value = objArea.UsuarioActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vHstActualizacion";
                            param.Value = objArea.HostActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vInsActualizacion";
                            param.Value = objArea.InstanciaActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vLgnActualizacion";
                            param.Value = objArea.LoginActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@vRolActualizacion";
                            param.Value = objArea.RolActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }

        public int EliminarArea(AreaDTO objArea)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_EliminarArea]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();


                            SqlParameter param = new SqlParameter("@iCodArea", SqlDbType.VarChar);
                            param.Value = objArea.CodArea;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar);
                            param.Value = objArea.UsuarioActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar);
                            param.Value = objArea.LoginActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }

        #endregion
    }
}
