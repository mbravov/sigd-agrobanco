﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class ReportesDAO: IReportesDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;
        
        #region Constructor
        public ReportesDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }
        #endregion

        public List<ReportesDTO.ReporteMesaParteDTO> ReporteMesaPartesDAO(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros)
        {
            ReportesDTO.ReporteMesaParteDTO objEntidad = null;
            totalRegistros = 0;
            List<ReportesDTO.ReporteMesaParteDTO> lstReporte = new List<ReportesDTO.ReporteMesaParteDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_Reporte_MesaPartes]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodArea";
                    param.Value = objParams.parCodArea;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vIdTipoDocumento";
                    param.Value = objParams.parCodTipDoc;
                    cmd.Parameters.Add(param);


                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodEstadoDocumento";
                    param.Value = objParams.EstadoDoc;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.DateTime;
                    param.ParameterName = "@fFechaInicio";
                    param.Value = objParams.fecInicio;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.DateTime;
                    param.ParameterName = "@fFechaFin";
                    param.Value = objParams.fecFinal;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@NumeroPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@tamaniopagina";
                    param.Value = objParams.tamPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new ReportesDTO.ReporteMesaParteDTO();
                                if (!Convert.IsDBNull(reader["Nro_Documento"])) objEntidad.vNumDocumento = Convert.ToString(reader["Nro_Documento"]);
                                if (!Convert.IsDBNull(reader["Nro_Tramite"])) objEntidad.vCorrelativo =Convert.ToString(reader["Nro_Tramite"].ToString());
                                if (!Convert.IsDBNull(reader["Institucion"])) objEntidad.Institucion = Convert.ToString(reader["Institucion"].ToString());
                                if (!Convert.IsDBNull(reader["FechaDerivacion"])) objEntidad.FechaDerivacion = Convert.ToString(reader["FechaDerivacion"].ToString());
                                if (!Convert.IsDBNull(reader["SumillaAsunto"])) objEntidad.vAsunto =Convert.ToString(reader["SumillaAsunto"].ToString());
                                if (!Convert.IsDBNull(reader["Responsable"])) objEntidad.Responsable =Convert.ToString(reader["Responsable"].ToString());
                                if (!Convert.IsDBNull(reader["FirmaSello"])) objEntidad.FirmaSello =Convert.ToString(reader["FirmaSello"].ToString());
                                if (!Convert.IsDBNull(reader["CodArea"])) objEntidad.CodArea = Convert.ToString(reader["CodArea"].ToString());
                                if (!Convert.IsDBNull(reader["DesArea"])) objEntidad.DesArea = Convert.ToString(reader["DesArea"].ToString());
                                if (!Convert.IsDBNull(reader["EstadoDoc"])) objEntidad.EstadoDoc = Convert.ToString(reader["EstadoDoc"].ToString());
                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                                lstReporte.Add(objEntidad);
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return lstReporte;
        }


        public List<ReportesDTO.ReporteAtencionDocumentoDTO> ReporteAtencionDocumento(ReportesDTO.ReporteAtencionDocumentoDTO objParams, out int totalRegistros)
        {
            ReportesDTO.ReporteAtencionDocumentoDTO objEntidad = null;
            totalRegistros = 0;
            List<ReportesDTO.ReporteAtencionDocumentoDTO> lstReporte = new List<ReportesDTO.ReporteAtencionDocumentoDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_Reporte_AtencionDocumentos]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vNroTramite";
                    param.Value = objParams.vCorrelativo;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.DateTime;
                    param.ParameterName = "@fFechaInicio";
                    param.Value = objParams.fecInicio;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.DateTime;
                    param.ParameterName = "@fFechaFin";
                    param.Value = objParams.fecFinal;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodAreaResponsable";
                    param.Value = objParams.CodAreaResponsable;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodAreaDerivada";
                    param.Value = objParams.CodAreaDerivada;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodTipoDocumento";
                    param.Value = objParams.CodTipoDocumento;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vEstadoDocumento";
                    param.Value = objParams.CodEstadoDoc;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@siFirmado";
                    param.Value = objParams.siFirma;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@NumeroPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@tamaniopagina";
                    param.Value = objParams.tamPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new ReportesDTO.ReporteAtencionDocumentoDTO();
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.vCorrelativo = Convert.ToString(reader["vCorrelativo"]);
                                if (!Convert.IsDBNull(reader["Remitente"])) objEntidad.Remitente = Convert.ToString(reader["Remitente"].ToString());
                                if (!Convert.IsDBNull(reader["Destinatario"])) objEntidad.Destinatario = Convert.ToString(reader["Destinatario"].ToString());
                                if (!Convert.IsDBNull(reader["vAsunto"])) objEntidad.Asunto = Convert.ToString(reader["vAsunto"].ToString());
                                if (!Convert.IsDBNull(reader["vComentario"])) objEntidad.Comentarios = Convert.ToString(reader["vComentario"].ToString());
                                if (!Convert.IsDBNull(reader["iCodDocumentoFirma"])) objEntidad.siFirma = Convert.ToString(reader["iCodDocumentoFirma"].ToString());
                                if (!Convert.IsDBNull(reader["dFechaFirma"])) objEntidad.fechaFirma = Convert.ToDateTime(reader["dFechaFirma"]);
                                if (!Convert.IsDBNull(reader["siEstDocumento"])) objEntidad.CodEstadoDoc = Convert.ToString(reader["siEstDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["DescripcionEstado"])) objEntidad.DescripcionEstado = Convert.ToString(reader["DescripcionEstado"].ToString());
                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                                lstReporte.Add(objEntidad);
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstReporte;
        }



        public List<ReportesDTO.ReporteMesaParteDTO> ReporteAreaFechaEstadoDAO(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros)
        {
            ReportesDTO.ReporteMesaParteDTO objEntidad = null;
            totalRegistros = 0;
            List<ReportesDTO.ReporteMesaParteDTO> lstReporte = new List<ReportesDTO.ReporteMesaParteDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_Reporte_AreaFechaEstado]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodArea";
                    param.Value = objParams.parCodArea;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vIdTipoDocumento";
                    param.Value = objParams.parCodTipDoc;
                    cmd.Parameters.Add(param);


                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodEstadoDocumento";
                    param.Value = objParams.EstadoDoc;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.DateTime;
                    param.ParameterName = "@fFechaInicio";
                    param.Value = objParams.fecInicio;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.DateTime;
                    param.ParameterName = "@fFechaFin";
                    param.Value = objParams.fecFinal;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@NumeroPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@tamaniopagina";
                    param.Value = objParams.tamPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new ReportesDTO.ReporteMesaParteDTO();
                                
                                if (!Convert.IsDBNull(reader["Nro_Tramite"])) objEntidad.vCorrelativo = Convert.ToString(reader["Nro_Tramite"].ToString());
                                if (!Convert.IsDBNull(reader["FechaDocumento"])) objEntidad.FechaDocumento = Convert.ToString(reader["FechaDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["Asunto"])) objEntidad.vAsunto = Convert.ToString(reader["Asunto"].ToString());
                                if (!Convert.IsDBNull(reader["Responsable"])) objEntidad.Responsable = Convert.ToString(reader["Responsable"].ToString());
                                if (!Convert.IsDBNull(reader["FirmaSello"])) objEntidad.FirmaSello = Convert.ToString(reader["FirmaSello"].ToString());
                                if (!Convert.IsDBNull(reader["CodArea"])) objEntidad.CodArea = Convert.ToString(reader["CodArea"].ToString());
                                if (!Convert.IsDBNull(reader["DesArea"])) objEntidad.DesArea = Convert.ToString(reader["DesArea"].ToString());
                                if (!Convert.IsDBNull(reader["EstadoDoc"])) objEntidad.EstadoDoc = Convert.ToString(reader["EstadoDoc"].ToString());                            
                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                                lstReporte.Add(objEntidad);
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return lstReporte;
        }

        #region Reporte de Seguimiento de Documentos

        public List<ReportesDTO.ReporteMesaParteDTO> ReporteSeguimientoDocumentosDAO(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros)
        {
            ReportesDTO.ReporteMesaParteDTO objEntidad = null;
            totalRegistros = 0;
            List<ReportesDTO.ReporteMesaParteDTO> lstReporte = new List<ReportesDTO.ReporteMesaParteDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_Reporte_SeguimientoDocumentos]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;                 

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vNroTramite";
                    param.Value = objParams.vCorrelativo;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Char;
                    param.ParameterName = "@vAño";
                    param.Value = objParams.vAño;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vCodArea";
                    param.Value = objParams.parCodArea;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vIdTipoDocumento";
                    param.Value = objParams.parCodTipDoc;
                    cmd.Parameters.Add(param);                                                          

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@NumeroPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@tamaniopagina";
                    param.Value = objParams.tamPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new ReportesDTO.ReporteMesaParteDTO();                             

                                if (!Convert.IsDBNull(reader["Nro_Tramite"])) objEntidad.vCorrelativo = Convert.ToString(reader["Nro_Tramite"].ToString());
                                if (!Convert.IsDBNull(reader["FechaDocumento"])) objEntidad.FechaDocumento = Convert.ToString(reader["FechaDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["Remitente"])) objEntidad.vRemitente = Convert.ToString(reader["Remitente"].ToString());
                                if (!Convert.IsDBNull(reader["Destinatario"])) objEntidad.vDestinatario = Convert.ToString(reader["Destinatario"].ToString());
                                if (!Convert.IsDBNull(reader["Asunto"])) objEntidad.vAsunto = Convert.ToString(reader["Asunto"].ToString());
                                if (!Convert.IsDBNull(reader["Observacion"])) objEntidad.vObservacion = Convert.ToString(reader["Observacion"].ToString());
                                if (!Convert.IsDBNull(reader["EstadoDoc"])) objEntidad.EstadoDoc = Convert.ToString(reader["EstadoDoc"].ToString());
                                if (!Convert.IsDBNull(reader["IdEstadoDoc"])) objEntidad.idestado = Convert.ToInt32(reader["IdEstadoDoc"].ToString());


                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                                lstReporte.Add(objEntidad);
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                                        
                }
            }
            return lstReporte;
        }

        public List<ReportesDTO.ReporteMesaParteDTO> ReporteListarAñoDAO()
        {
            ReportesDTO.ReporteMesaParteDTO objEntidad = null;
            
            List<ReportesDTO.ReporteMesaParteDTO> lst = new List<ReportesDTO.ReporteMesaParteDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_ListaAños]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;                                                          
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new ReportesDTO.ReporteMesaParteDTO();
                                if (!Convert.IsDBNull(reader["AÑO"])) objEntidad.vAño = Convert.ToString(reader["AÑO"].ToString());                                
                                lst.Add(objEntidad);
                            }
                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }                    
                }
            }
            return lst;
        }


        #endregion
    }
}
