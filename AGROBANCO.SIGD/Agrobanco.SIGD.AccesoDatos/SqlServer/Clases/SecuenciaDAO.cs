﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class SecuenciaDAO : ISecuenciaDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public SecuenciaDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        #endregion

        /// <summary>
        /// Método Público para el Listado de Secuencia
        /// </summary>
        /// <returns>Retorna Lista genérica de Secuencia</returns>  
        public List<Secuencia> ListarSecuencia(FiltroMantenimientoBaseDTO filtro, out int totalRegistros)
        {
            totalRegistros = 0;
            Secuencia objEntidad = null;
            List<Secuencia> lstSecuencia = new List<Secuencia>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_SecuencialPaginado]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = null;
                    param = new SqlParameter("@texto", SqlDbType.VarChar, 100);
                    param.Value = filtro.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@NumeroPagina";
                    param.Value = filtro.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@tamaniopagina";
                    param.Value = filtro.TamanioPagina;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new Secuencia();

                                if (!Convert.IsDBNull(reader["iAnio"])) objEntidad.anio = Convert.ToInt32(reader["iAnio"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["iCorrelativo"])) objEntidad.correlativo = Convert.ToInt16(reader["iCorrelativo"]);
                                if (!Convert.IsDBNull(reader["iCodSecuencial"])) objEntidad.CodSecuencial = Convert.ToInt16(reader["iCodSecuencial"]);
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocumento"])) objEntidad.CodTipoDocumento = Convert.ToInt32(reader["iCodTipDocumento"]);
                                if (!Convert.IsDBNull(reader["vDescripcionArea"])) objEntidad.DescripcionArea = Convert.ToString(reader["vDescripcionArea"]);
                                if (!Convert.IsDBNull(reader["vDescripcionTipoDocumento"])) objEntidad.DescripcionTipoDocumento = Convert.ToString(reader["vDescripcionTipoDocumento"]);
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.ValorCorrelativo = Convert.ToString(reader["vCorrelativo"]);
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = Convert.ToInt16(reader["siOrigen"]);
                                if (!Convert.IsDBNull(reader["fUsado"])) objEntidad.flagUsado = Convert.ToInt16(reader["fUsado"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = Convert.ToDateTime(reader["dFecCreacion"]);
                                lstSecuencia.Add(objEntidad);


                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al buscar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstSecuencia;
        }
        public int EliminarSecuencia(SecuenciaDTO obj)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_EliminarSecuencia]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();

                            SqlParameter param = new SqlParameter("@iCodSecuencial", SqlDbType.Int);
                            param.Value = obj.CodSecuencial;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar);
                            param.Value = obj.UsuarioActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar);
                            param.Value = obj.LoginActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar Secuencia");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }

        public int ActualizarSecuencia(SecuenciaDTO obj)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_ActualizarSecuencia]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();

                            SqlParameter param = new SqlParameter("@iCodSecuencial", SqlDbType.Int);
                            param.Value = obj.CodSecuencial;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@iOrigen", SqlDbType.Int);
                            param.Value = obj.Origen;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@anio", SqlDbType.Int);
                            param.Value = obj.anio;
                            cmd.Parameters.Add(param);
                            
                            param = new SqlParameter("@iCodArea", SqlDbType.Int);
                            param.Value = obj.CodArea;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@iCodTipoDocumento", SqlDbType.Int);
                            param.Value = obj.CodTipoDocumento;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@correlativo", SqlDbType.Int);
                            param.Value = obj.correlativo;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@ValorCorrelativo", SqlDbType.VarChar);
                            param.Value = obj.ValorCorrelativo;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@Estado";
                            param.Value = obj.Estado;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vUsuActualizacion";
                            param.Value = obj.UsuarioActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vHstActualizacion";
                            param.Value = obj.HostActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vInsActualizacion";
                            param.Value = obj.InstanciaActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vLgnActualizacion";
                            param.Value = obj.LoginActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vRolActualizacion";
                            param.Value = obj.RolActualizacion;
                            cmd.Parameters.Add(param);


                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar Secuencia");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }

        public int RegistrarSecuencia(SecuenciaDTO obj)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_RegistrarSecuencia]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();

                            SqlParameter param = new SqlParameter("@Anio", SqlDbType.Int);
                            param.Value = obj.anio;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@Correlativo", SqlDbType.Int);
                            param.Value = obj.correlativo;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vCorrelativo", SqlDbType.VarChar);
                            param.Value = obj.ValorCorrelativo;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@iCodArea", SqlDbType.Int);
                            param.Value = obj.CodArea;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@iCodTipoDocumento", SqlDbType.Int);
                            param.Value = obj.CodTipoDocumento;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@iOrigen", SqlDbType.Int);
                            param.Value = obj.Origen;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@iEstado", SqlDbType.Int);
                            param.Value = obj.Estado;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vUsuCreacion";
                            param.Value = obj.UsuarioCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vHstCreacion";
                            param.Value = obj.HostCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vLgnCreacion";
                            param.Value = obj.LoginCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vRolCreacion";
                            param.Value = obj.RolCreacion;
                            cmd.Parameters.Add(param);


                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar Secuencia");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }

        /// <summary>
        /// Método Público obtener una secuencia
        /// </summary>
        /// <returns>Retorna objeto Secuencia</returns>  
        public SecuenciaDTO ObtenerSecuencia(SecuenciaDTO objParams)
        {
            SecuenciaDTO objEntidad = null;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ObtenerSecuencia]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodSecuencia";
                    param.Value = objParams.CodSecuencial;
                    cmd.Parameters.Add(param);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new SecuenciaDTO();
                                if (!Convert.IsDBNull(reader["iAnio"])) objEntidad.anio = Convert.ToInt32(reader["iAnio"]);
                                if (!Convert.IsDBNull(reader["iCorrelativo"])) objEntidad.correlativo = Convert.ToInt32(reader["iCorrelativo"].ToString());
                                if (!Convert.IsDBNull(reader["iCodSecuencial"])) objEntidad.CodSecuencial = short.Parse(reader["iCodSecuencial"].ToString());
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = short.Parse(reader["iCodArea"].ToString());
                                if (!Convert.IsDBNull(reader["iCodTipDocumento"])) objEntidad.CodTipoDocumento = short.Parse(reader["iCodTipDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["vCorrelativo"])) objEntidad.ValorCorrelativo = (reader["vCorrelativo"].ToString());
                                if (!Convert.IsDBNull(reader["siOrigen"])) objEntidad.Origen = short.Parse(reader["siOrigen"].ToString());
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["vDescripcionArea"])) objEntidad.DescripcionArea = (reader["vDescripcionArea"].ToString());
                                if (!Convert.IsDBNull(reader["vDescripcionTipoDocumento"])) objEntidad.DescripcionTipoDocumento = (reader["vDescripcionTipoDocumento"].ToString());
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de  tipo Secuencia");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return objEntidad;
        }

        /// <summary>
        /// Interface para Calcular la fecha de plazo del documento
        /// </summary>
        /// <returns>Retorna el Indicador de Fecha de Plazo del Documento</returns>
        public String ObtenerCorrelativo(SecuenciaDTO obj)
        {
            String valorcorrelativo;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_OBTENERCORRELATIVO]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@origen";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = obj.Origen;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@iCodArea";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = (obj.CodArea==0)?null: obj.CodArea;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@iCodTipDocumento";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = (obj.CodTipoDocumento == 0) ? null : obj.CodTipoDocumento;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Anio";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = obj.anio;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@SecCorrelativo";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = obj.correlativo;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Tipo";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = 2;
                    cmd.Parameters.Add(param);
                    
                    param = new SqlParameter("@ValorCorrelativo", SqlDbType.VarChar,100);
                    param.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(param);

                    try
                    {
                        cmd.ExecuteNonQuery();
                        valorcorrelativo = Convert.ToString(cmd.Parameters["@ValorCorrelativo"].Value);
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error obtener valor correlativo");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }

                }
            }
            return valorcorrelativo;
        }
    }
}