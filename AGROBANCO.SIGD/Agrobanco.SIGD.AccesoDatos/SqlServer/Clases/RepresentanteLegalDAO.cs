﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class RepresentanteLegalDAO : IRepresentanteLegalDAO
    {
       

        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public RepresentanteLegalDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        #endregion

        #region Métodos Públicos

        public RepresentanteLegalDTO ObtenerRepresentanteLegal()
        {
            RepresentanteLegalDTO objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ListarRepresentanteLegal]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new RepresentanteLegalDTO();
                                if (!Convert.IsDBNull(reader["iCodRepresentanteLegal"])) objEntidad.iCodRepresentanteLegal = Convert.ToInt32(reader["iCodRepresentanteLegal"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToString(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["vNombre"])) objEntidad.Nombre = Convert.ToString(reader["vNombre"]);
                                if (!Convert.IsDBNull(reader["vApePaterno"])) objEntidad.ApePaterno = Convert.ToString(reader["vApePaterno"]);
                                if (!Convert.IsDBNull(reader["vApeMaterno"])) objEntidad.ApeMaterno = Convert.ToString(reader["vApeMaterno"]);
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.Email = Convert.ToString(reader["vEmail"]);
                                if (!Convert.IsDBNull(reader["vNroDoc"])) objEntidad.NroDoc = Convert.ToString(reader["vNroDoc"]);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }

        public int GrabarRepresentanteLegal(RepresentanteLegalDTO obj)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_GrabarRepresentanteLegal]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();

                            SqlParameter param = new SqlParameter("@siEstado", SqlDbType.Int);
                            param.Value = obj.Estado;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vNombre", SqlDbType.VarChar);
                            param.Value = obj.Nombre;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vApePaterno", SqlDbType.VarChar);
                            param.Value = obj.ApePaterno;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vApeMaterno";
                            param.Value = obj.ApeMaterno;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vEmail";
                            param.Value = obj.Email;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vNroDoc";
                            param.Value = obj.NroDoc;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vUsuCreacion";
                            param.Value = obj.UsuarioCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vHstCreacion";
                            param.Value = obj.HostCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vInsCreacion";
                            param.Value = obj.InstanciaCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vLgnCreacion";
                            param.Value = obj.LoginCreacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vRolCreacion";
                            param.Value = obj.RolCreacion;
                            cmd.Parameters.Add(param);


                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {
                                int log = LogUsuarioRepresentanteLegal(obj, cmd);
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Registrar");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }
        
        public int EditarRepresentanteLegal(RepresentanteLegalDTO obj)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_ActualizarRepresentanteLegal]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();

                            SqlParameter param = new SqlParameter("@siEstado", SqlDbType.Int);
                            param.Value = obj.Estado;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vNombre", SqlDbType.VarChar);
                            param.Value = obj.Nombre;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vApePaterno", SqlDbType.VarChar);
                            param.Value = obj.ApePaterno;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vApeMaterno";
                            param.Value = obj.ApeMaterno;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vEmail";
                            param.Value = obj.Email;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vNroDoc";
                            param.Value = obj.NroDoc;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vUsuActualizacion";
                            param.Value = obj.UsuarioActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vLgnActualizacion";
                            param.Value = obj.LoginActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.SqlDbType = SqlDbType.VarChar;
                            param.ParameterName = "@vRolActualizacion";
                            param.Value = obj.RolActualizacion;
                            cmd.Parameters.Add(param);


                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {
                                int log = LogUsuarioRepresentanteLegal(obj,cmd);
                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al editar usuario sied");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }


        public int LogUsuarioRepresentanteLegal(RepresentanteLegalDTO obj, SqlCommand cmd)
        {
            int onRpta = 0;


              cmd.CommandText = "[dbo].[UP_Log_UsuarioRepresentanteLegal]";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Clear();

              SqlParameter param = new SqlParameter("@siEstado", SqlDbType.Int);
              param.Value = obj.Estado;
              cmd.Parameters.Add(param);

              param = new SqlParameter("@vNombre", SqlDbType.VarChar);
              param.Value = obj.Nombre;
              cmd.Parameters.Add(param);

              param = new SqlParameter("@vApePaterno", SqlDbType.VarChar);
              param.Value = obj.ApePaterno;
              cmd.Parameters.Add(param);

              param = new SqlParameter();
              param.SqlDbType = SqlDbType.VarChar;
              param.ParameterName = "@vApeMaterno";
              param.Value = obj.ApeMaterno;
              cmd.Parameters.Add(param);

              param = new SqlParameter();
              param.SqlDbType = SqlDbType.VarChar;
              param.ParameterName = "@vEmail";
              param.Value = obj.Email;
              cmd.Parameters.Add(param);

              param = new SqlParameter();
              param.SqlDbType = SqlDbType.VarChar;
              param.ParameterName = "@vNroDoc";
              param.Value = obj.NroDoc;
              cmd.Parameters.Add(param);

              param = new SqlParameter();
              param.SqlDbType = SqlDbType.VarChar;
              param.ParameterName = "@vUsuCreacion";
              param.Value = obj.UsuarioCreacion;
              cmd.Parameters.Add(param);

              param = new SqlParameter();
              param.SqlDbType = SqlDbType.VarChar;
              param.ParameterName = "@vHstCreacion";
              param.Value = obj.HostCreacion;
              cmd.Parameters.Add(param);

              param = new SqlParameter();
              param.SqlDbType = SqlDbType.VarChar;
              param.ParameterName = "@vInsCreacion";
              param.Value = obj.InstanciaCreacion;
              cmd.Parameters.Add(param);

              param = new SqlParameter();
              param.SqlDbType = SqlDbType.VarChar;
              param.ParameterName = "@vLgnCreacion";
              param.Value = obj.LoginCreacion;
              cmd.Parameters.Add(param);

              param = new SqlParameter();
              param.SqlDbType = SqlDbType.VarChar;
              param.ParameterName = "@vRolCreacion";
              param.Value = obj.RolCreacion;
              cmd.Parameters.Add(param);


              param = new SqlParameter();
              param.Direction = ParameterDirection.Output;
              param.SqlDbType = SqlDbType.Int;
              param.ParameterName = "@onFlagOK";
              cmd.Parameters.Add(param);

              cmd.ExecuteNonQuery();

              onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

           return onRpta;
      }

        #endregion
    }
}


