﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class MaeTrabajadorDAO : IMaeTrabajadorDAO
    {
       

        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public MaeTrabajadorDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Método Público para el Listado de Áreas
        /// </summary>
        /// <returns>Retorna Lista genérica de Áreas</returns>  
        public EMaeTrabajador GetTrabajador(EMaeTrabajador EObj)
        {
            EMaeTrabajador objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Trabajador]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@piCodTrabajador";
                    param.Value = EObj.iCodTrabajador;
                    cmd.Parameters.Add(param);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new EMaeTrabajador();
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.iCodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.vNumDocumento = Convert.ToString(reader["vNumDocumento"]);
                                if (!Convert.IsDBNull(reader["vNombres"])) objEntidad.vNombres = Convert.ToString(reader["vNombres"]);
                                if (!Convert.IsDBNull(reader["vCargo"])) objEntidad.vCargo = Convert.ToString(reader["vCargo"]);
                                if (!Convert.IsDBNull(reader["vUbicacion"])) objEntidad.vUbicacion = Convert.ToString(reader["vUbicacion"]);
                                if (!Convert.IsDBNull(reader["iAnexo"])) objEntidad.iAnexo = Convert.ToInt32(reader["iAnexo"]);
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.vEmail = Convert.ToString(reader["vEmail"]);
                                if (!Convert.IsDBNull(reader["vApePaterno"])) objEntidad.vApePaterno = Convert.ToString(reader["vApePaterno"]);
                                if (!Convert.IsDBNull(reader["vApeMaterno"])) objEntidad.vApeMaterno = Convert.ToString(reader["vApeMaterno"]);
                                if (!Convert.IsDBNull(reader["siEsJefe"])) objEntidad.siEsJefe = short.Parse(reader["siEsJefe"].ToString());
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.iCodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.iCodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"]);
                                if (!Convert.IsDBNull(reader["vCelular"])) objEntidad.vCelular = Convert.ToString(reader["vCelular"]);
                                if (!Convert.IsDBNull(reader["DESCRIPCIONAREA"])) objEntidad.DescripcionArea = Convert.ToString(reader["DESCRIPCIONAREA"]);
                                if (!Convert.IsDBNull(reader["WEBUSR"])) objEntidad.WEBUSR = Convert.ToString(reader["WEBUSR"]);
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }

        public List<TrabajadorDTO> GetTrabajadorPorArea(EMaeTrabajador EObj)
        {
            List<TrabajadorDTO> trabajadores = new List<TrabajadorDTO>();
            TrabajadorDTO objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ListarTrabajadorPorArea]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodArea";
                    param.Value = EObj.iCodArea;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@siJefe";
                    param.Value = EObj.siEsJefe;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new TrabajadorDTO();
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.iCodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.vNumDocumento = Convert.ToString(reader["vNumDocumento"]);
                                if (!Convert.IsDBNull(reader["vNombres"])) objEntidad.vNombres = Convert.ToString(reader["vNombres"]);
                                if (!Convert.IsDBNull(reader["vCargo"])) objEntidad.vCargo = Convert.ToString(reader["vCargo"]);
                                if (!Convert.IsDBNull(reader["vUbicacion"])) objEntidad.vUbicacion = Convert.ToString(reader["vUbicacion"]);
                                if (!Convert.IsDBNull(reader["iAnexo"])) objEntidad.iAnexo = Convert.ToInt32(reader["iAnexo"]);
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.vEmail = Convert.ToString(reader["vEmail"]);
                                if (!Convert.IsDBNull(reader["vApePaterno"])) objEntidad.vApePaterno = Convert.ToString(reader["vApePaterno"]);
                                if (!Convert.IsDBNull(reader["vApeMaterno"])) objEntidad.vApeMaterno = Convert.ToString(reader["vApeMaterno"]);
                                if (!Convert.IsDBNull(reader["siEsJefe"])) objEntidad.siEsJefe = short.Parse(reader["siEsJefe"].ToString());
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.iCodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.iCodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"]);
                                if (!Convert.IsDBNull(reader["vCelular"])) objEntidad.vCelular = Convert.ToString(reader["vCelular"]);
                                if (!Convert.IsDBNull(reader["WEBUSR"])) objEntidad.WEBUSR = Convert.ToString(reader["WEBUSR"]);

                                trabajadores.Add(objEntidad);
                               
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return trabajadores;
        }

        public List<TrabajadorDTO> GetTrabajadorPorAreaSIED()
        {
            List<TrabajadorDTO> trabajadores = new List<TrabajadorDTO>();
            TrabajadorDTO objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ListarTrabajadorPorAreaSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new TrabajadorDTO();
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.iCodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.vNumDocumento = Convert.ToString(reader["vNumDocumento"]);
                                if (!Convert.IsDBNull(reader["vNombres"])) objEntidad.vNombres = Convert.ToString(reader["vNombres"]);
                                if (!Convert.IsDBNull(reader["vCargo"])) objEntidad.vCargo = Convert.ToString(reader["vCargo"]);
                                if (!Convert.IsDBNull(reader["vUbicacion"])) objEntidad.vUbicacion = Convert.ToString(reader["vUbicacion"]);
                                if (!Convert.IsDBNull(reader["iAnexo"])) objEntidad.iAnexo = Convert.ToInt32(reader["iAnexo"]);
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.vEmail = Convert.ToString(reader["vEmail"]);
                                if (!Convert.IsDBNull(reader["vApePaterno"])) objEntidad.vApePaterno = Convert.ToString(reader["vApePaterno"]);
                                if (!Convert.IsDBNull(reader["vApeMaterno"])) objEntidad.vApeMaterno = Convert.ToString(reader["vApeMaterno"]);
                                if (!Convert.IsDBNull(reader["siEsJefe"])) objEntidad.siEsJefe = short.Parse(reader["siEsJefe"].ToString());
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.iCodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.iCodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"]);
                                if (!Convert.IsDBNull(reader["vCelular"])) objEntidad.vCelular = Convert.ToString(reader["vCelular"]);
                                if (!Convert.IsDBNull(reader["WEBUSR"])) objEntidad.WEBUSR = Convert.ToString(reader["WEBUSR"]);

                                trabajadores.Add(objEntidad);

                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return trabajadores;
        }

        public List<TrabajadorDTO> GetTrabajadores()
        {
            List<TrabajadorDTO> trabajadores = new List<TrabajadorDTO>();
            TrabajadorDTO objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ListarTrabajadores]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new TrabajadorDTO();
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.iCodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.vNumDocumento = Convert.ToString(reader["vNumDocumento"]);
                                if (!Convert.IsDBNull(reader["vNombres"])) objEntidad.vNombres = Convert.ToString(reader["vNombres"]);
                                if (!Convert.IsDBNull(reader["vCargo"])) objEntidad.vCargo = Convert.ToString(reader["vCargo"]);
                                if (!Convert.IsDBNull(reader["vUbicacion"])) objEntidad.vUbicacion = Convert.ToString(reader["vUbicacion"]);
                                if (!Convert.IsDBNull(reader["iAnexo"])) objEntidad.iAnexo = Convert.ToInt32(reader["iAnexo"]);
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.vEmail = Convert.ToString(reader["vEmail"]);
                                if (!Convert.IsDBNull(reader["vApePaterno"])) objEntidad.vApePaterno = Convert.ToString(reader["vApePaterno"]);
                                if (!Convert.IsDBNull(reader["vApeMaterno"])) objEntidad.vApeMaterno = Convert.ToString(reader["vApeMaterno"]);
                                if (!Convert.IsDBNull(reader["siEsJefe"])) objEntidad.siEsJefe = short.Parse(reader["siEsJefe"].ToString());
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.iCodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.iCodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"]);
                                if (!Convert.IsDBNull(reader["vCelular"])) objEntidad.vCelular = Convert.ToString(reader["vCelular"]);
                                if (!Convert.IsDBNull(reader["WEBUSR"])) objEntidad.WEBUSR = Convert.ToString(reader["WEBUSR"]);

                                trabajadores.Add(objEntidad);

                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return trabajadores;
        }

        /// <summary>
        /// Método Público para el Listado de Trabajadores Paginado
        /// </summary>
        public List<TrabajadorDTO> ListarTrabajadorPaginado(TrabajadorDTO objParams, out int totalRegistros)
        {
            TrabajadorDTO objEntidad = null;
            totalRegistros = 0;
            List<TrabajadorDTO> lstTrabajador = new List<TrabajadorDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_TrabajadorPaginado]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@texto";
                    param.Value = objParams.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@NumeroPagina";
                    param.Value = objParams.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@tamaniopagina";
                    param.Value = objParams.TamanioPagina;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new TrabajadorDTO();
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.iCodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.vNumDocumento = (reader["vNumDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["Nombres"])) objEntidad.vNombreCompleto = Convert.ToString(reader["Nombres"].ToString());
                                if (!Convert.IsDBNull(reader["vCargo"])) objEntidad.vCargo =Convert.ToString(reader["vCargo"].ToString());
                                if (!Convert.IsDBNull(reader["vArea"])) objEntidad.vArea = Convert.ToString(reader["vArea"].ToString());
                                if (!Convert.IsDBNull(reader["vTipoDocumento"])) objEntidad.vTipoDocumento = Convert.ToString(reader["vTipoDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.vEmail =Convert.ToString(reader["vEmail"].ToString());
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.dFecCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.vUsuCreacion = Convert.ToString(reader["vUsuCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.dFecActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.vUsuActualizacion =Convert.ToString(reader["vUsuActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.siEstado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["siEsJefe"])) objEntidad.siEsJefe = short.Parse(reader["siEsJefe"].ToString());
                                if (!Convert.IsDBNull(reader["siFirma"])) objEntidad.siFirma = short.Parse(reader["siFirma"].ToString());
                                if (!Convert.IsDBNull(reader["webUsr"])) objEntidad.WEBUSR = Convert.ToString(reader["webUsr"].ToString());

                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                                lstTrabajador.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de Trabajadores");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                                        
                }
            }
            return lstTrabajador;
        }

        /// <summary>
        /// Método Público para el Listado de Trabajador por id
        /// </summary>
        /// <returns>Retorna Lista de trabajador</returns>  
        public TrabajadorDTO ObtenerTabajadorPorId(TrabajadorDTO objParams)
        {
            TrabajadorDTO objEntidad = null;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Obtener_Trabajador]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodTrabajador";
                    param.Value = objParams.iCodTrabajador;
                    cmd.Parameters.Add(param);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new TrabajadorDTO();

                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.iCodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.vNumDocumento = (reader["vNumDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["Nombres"])) objEntidad.vNombreCompleto = Convert.ToString(reader["Nombres"].ToString());

                                if (!Convert.IsDBNull(reader["Nombre"])) objEntidad.vNombres = Convert.ToString(reader["Nombre"].ToString());
                                if (!Convert.IsDBNull(reader["vApePaterno"])) objEntidad.vApePaterno = Convert.ToString(reader["vApePaterno"].ToString());
                                if (!Convert.IsDBNull(reader["vApeMaterno"])) objEntidad.vApeMaterno = Convert.ToString(reader["vApeMaterno"].ToString());


                                if (!Convert.IsDBNull(reader["vCargo"])) objEntidad.vCargo = Convert.ToString(reader["vCargo"].ToString());
                                if (!Convert.IsDBNull(reader["vArea"])) objEntidad.vArea = Convert.ToString(reader["vArea"].ToString());
                                if (!Convert.IsDBNull(reader["vTipoDocumento"])) objEntidad.vTipoDocumento = Convert.ToString(reader["vTipoDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.vEmail = Convert.ToString(reader["vEmail"].ToString());
                                                                
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.sFecCreacion = DateTime.Parse(reader["dFecCreacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.vUsuCreacion = Convert.ToString(reader["vUsuCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.sFecActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.vUsuActualizacion = Convert.ToString(reader["vUsuActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.siEstado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["vUbicacion"])) objEntidad.vUbicacion = Convert.ToString(reader["vUbicacion"].ToString());
                                if (!Convert.IsDBNull(reader["iAnexo"])) objEntidad.iAnexo = Convert.ToInt32(reader["iAnexo"].ToString());

                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.iCodArea = Convert.ToInt32(reader["iCodArea"].ToString());
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.iCodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"].ToString());

                                if (!Convert.IsDBNull(reader["vCelular"])) objEntidad.vCelular = Convert.ToString(reader["vCelular"].ToString());
                                if (!Convert.IsDBNull(reader["siEsJefe"])) objEntidad.siEsJefe = Convert.ToInt16(reader["siEsJefe"].ToString());
                                if (!Convert.IsDBNull(reader["siFirma"])) objEntidad.siFirma = Convert.ToInt16(reader["siFirma"].ToString());
                                if (!Convert.IsDBNull(reader["ApeCompletos"])) objEntidad.apeCompletos = Convert.ToString(reader["ApeCompletos"].ToString());

                                if (!Convert.IsDBNull(reader["webUsr"])) objEntidad.WEBUSR = Convert.ToString(reader["webUsr"].ToString());

                                


                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de  trabajadores");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return objEntidad;
        }

        /// <summary>
        /// Método Público para el Listado de Trabajador por webusr
        /// </summary>
        /// <returns>Retorna Lista de trabajador</returns>  
        public TrabajadorDTO ObtenerTabajadorPorWEBUSR(TrabajadorDTO objParams)
        {
            TrabajadorDTO objEntidad = null;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Obtener_Trabajador_WebUsr]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param;
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@webusr";
                    param.Value = objParams.WEBUSR;
                    cmd.Parameters.Add(param);


                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        var i = 0;
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new TrabajadorDTO();

                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.iCodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.vNumDocumento = (reader["vNumDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["Nombres"])) objEntidad.vNombreCompleto = Convert.ToString(reader["Nombres"].ToString());

                                if (!Convert.IsDBNull(reader["Nombre"])) objEntidad.vNombres = Convert.ToString(reader["Nombre"].ToString());
                                if (!Convert.IsDBNull(reader["vApePaterno"])) objEntidad.vApePaterno = Convert.ToString(reader["vApePaterno"].ToString());
                                if (!Convert.IsDBNull(reader["vApeMaterno"])) objEntidad.vApeMaterno = Convert.ToString(reader["vApeMaterno"].ToString());


                                if (!Convert.IsDBNull(reader["vCargo"])) objEntidad.vCargo = Convert.ToString(reader["vCargo"].ToString());
                                if (!Convert.IsDBNull(reader["vArea"])) objEntidad.vArea = Convert.ToString(reader["vArea"].ToString());
                                if (!Convert.IsDBNull(reader["vTipoDocumento"])) objEntidad.vTipoDocumento = Convert.ToString(reader["vTipoDocumento"].ToString());
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.vEmail = Convert.ToString(reader["vEmail"].ToString());

                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.sFecCreacion = DateTime.Parse(reader["dFecCreacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.vUsuCreacion = Convert.ToString(reader["vUsuCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.sFecActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString()).ToShortDateString();
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.vUsuActualizacion = Convert.ToString(reader["vUsuActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.siEstado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["vUbicacion"])) objEntidad.vUbicacion = Convert.ToString(reader["vUbicacion"].ToString());
                                if (!Convert.IsDBNull(reader["iAnexo"])) objEntidad.iAnexo = Convert.ToInt32(reader["iAnexo"].ToString());

                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.iCodArea = Convert.ToInt32(reader["iCodArea"].ToString());
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.iCodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"].ToString());

                                if (!Convert.IsDBNull(reader["vCelular"])) objEntidad.vCelular = Convert.ToString(reader["vCelular"].ToString());
                                if (!Convert.IsDBNull(reader["siEsJefe"])) objEntidad.siEsJefe = Convert.ToInt16(reader["siEsJefe"].ToString());

                                if (!Convert.IsDBNull(reader["ApeCompletos"])) objEntidad.apeCompletos = Convert.ToString(reader["ApeCompletos"].ToString());

                                if (!Convert.IsDBNull(reader["webUsr"])) objEntidad.WEBUSR = Convert.ToString(reader["webUsr"].ToString());




                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el Listado de  trabajadores");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }

                    //totalRegistros = (int)param.Value;
                }
            }
            return objEntidad;
        }

        public int RegistrarTrabajador(TrabajadorDTO objTrabajador)
        {
            int nRpta = 0;
            bool rollback = false;
            int cantidad = 0;
            int cantidadWbUser = 0;
            cantidadWbUser = GetWuserDuplicados(objTrabajador);
            if (cantidadWbUser > 0)
            {
                nRpta = -3;
            }
            else
            {
          
            if(objTrabajador.siEsJefe == 1)
            {
                cantidad = GetJefePorArea(objTrabajador);
                
            }
            if (cantidad > 0 )
            {
                nRpta = -2;
            }
            else
            {

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        using (SqlTransaction tx = conn.BeginTransaction())
                        {
                            try
                            {
                                cmd.Transaction = tx;
                                int onRpta = -1;

                                cmd.CommandText = "[dbo].[UP_MAE_RegistrarTrabajador]";
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Clear();

                                      
                                SqlParameter param = new SqlParameter("@vNumDocumento", SqlDbType.VarChar);
                                param.Value = objTrabajador.vNumDocumento;
                                cmd.Parameters.Add(param);
                              
                                param = new SqlParameter("@vNombres", SqlDbType.VarChar);
                                param.Value = objTrabajador.vNombres;
                                cmd.Parameters.Add(param);
                            

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vApePaterno";
                                param.Value = objTrabajador.vApePaterno;
                                cmd.Parameters.Add(param);
                           
                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vApeMaterno";
                                param.Value = objTrabajador.vApeMaterno;
                                cmd.Parameters.Add(param);
                               

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vCargo";
                                param.Value = objTrabajador.vCargo;
                                cmd.Parameters.Add(param);
                               

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vUbicacion";
                                param.Value = objTrabajador.vUbicacion;
                                cmd.Parameters.Add(param);
                               
                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.Int;
                                param.ParameterName = "@iAnexo";
                                param.Value = objTrabajador.iAnexo;
                                cmd.Parameters.Add(param);
                             

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vEmail";
                                param.Value = objTrabajador.vEmail;
                                cmd.Parameters.Add(param);
                              

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.Int;
                                param.ParameterName = "@Estado";
                                param.Value = objTrabajador.siEstado;
                                cmd.Parameters.Add(param);
                            

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.Int;
                                param.ParameterName = "@Jefe";
                                param.Value = objTrabajador.siEsJefe;
                                cmd.Parameters.Add(param);

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.Int;
                                param.ParameterName = "@Firma";
                                param.Value = objTrabajador.siFirma;
                                cmd.Parameters.Add(param);

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.Int;
                                param.ParameterName = "@iCodArea";
                                param.Value = objTrabajador.iCodArea;
                                cmd.Parameters.Add(param);
                              

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.Int;
                                param.ParameterName = "@iCodTipoDocIdentidad";
                                param.Value = objTrabajador.iCodTipDocIdentidad;
                                cmd.Parameters.Add(param);
                               

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vCelular";
                                param.Value = objTrabajador.vCelular;
                                cmd.Parameters.Add(param);

                                
                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@usvWebUsr";
                                param.Value = objTrabajador.WEBUSR;
                                cmd.Parameters.Add(param);
                                

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vUsuCreacion";
                                param.Value = objTrabajador.vUsuCreacion;
                                cmd.Parameters.Add(param);
                                
                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vHstCreacion";
                                param.Value = objTrabajador.vHstCreacion;
                                cmd.Parameters.Add(param);
                              

                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vLgnCreacion";
                                param.Value = objTrabajador.vLgnCreacion;
                                cmd.Parameters.Add(param);
                             
                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.VarChar;
                                param.ParameterName = "@vRolCreacion";
                                param.Value = objTrabajador.vRolCreacion;
                                cmd.Parameters.Add(param);
                              
                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.Int;
                                param.ParameterName = "@iCodPerfil";
                                param.Value = objTrabajador.iCodPerfil;
                                cmd.Parameters.Add(param);
                         
                                param = new SqlParameter();
                                param.SqlDbType = SqlDbType.Int;
                                param.ParameterName = "@vPerfil";
                                param.Value = objTrabajador.vCodPerfil;
                                cmd.Parameters.Add(param);
                               
                                param = new SqlParameter();
                                param.Direction = ParameterDirection.Output;
                                param.SqlDbType = SqlDbType.Int;
                                param.ParameterName = "@onFlagOK";
                                cmd.Parameters.Add(param);
                               

                                cmd.ExecuteNonQuery();
                                onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                                if (onRpta >= 0)
                                {

                                    tx.Commit();
                                }
                                else { tx.Rollback(); rollback = true; }

                            }
                            catch (Exception ex)
                            {
                                nRpta = -1;
                                tx.Rollback();
                                rollback = true;
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                                //Generando Archivo Log
                                new LogWriter(ex.Message);
                                throw new Exception("Error al Registrar");
                            }
                            finally
                            {
                                conn.Close();
                                conn.Dispose();
                                cmd.Dispose();
                            }
                        }
                    }
                    return nRpta;
                }
            }

            }          

            return nRpta;

        }


      
        public int ActualizarTrabajador(TrabajadorDTO objTrabajador)
        {


            int nRpta = 0;
            bool rollback = false;
            int cantidad = 0;
            int cantidadWbUser = 0;
            cantidadWbUser = GetWuserDuplicados(objTrabajador);
            if (cantidadWbUser > 0)
            {
                nRpta = -3;
            }
            else
            {

                if (objTrabajador.siEsJefe == 1)
                {
                    cantidad = GetJefePorArea(objTrabajador);

                }
                if (cantidad > 0)
                {
                    nRpta = -2;
                }
                else
                {

                    using (SqlConnection conn = new SqlConnection(cadenaConexion))
                    {
                        conn.Open();

                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            using (SqlTransaction tx = conn.BeginTransaction())
                            {
                                try
                                {
                                    cmd.Transaction = tx;
                                    int onRpta = -1;



                                    cmd.CommandText = "[dbo].[UP_MAE_ActualizarTrabajador]";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Clear();

                                    SqlParameter param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
                                    param.Value = objTrabajador.iCodTrabajador;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter("@vNumDocumento", SqlDbType.VarChar);
                                    param.Value = objTrabajador.vNumDocumento;
                                    cmd.Parameters.Add(param);


                                    param = new SqlParameter("@vNombres", SqlDbType.VarChar);
                                    param.Value = objTrabajador.vNombres;
                                    cmd.Parameters.Add(param);


                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vApePaterno";
                                    param.Value = objTrabajador.vApePaterno;
                                    cmd.Parameters.Add(param);


                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vApeMaterno";
                                    param.Value = objTrabajador.vApeMaterno;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vCargo";
                                    param.Value = objTrabajador.vCargo;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vUbicacion";
                                    param.Value = objTrabajador.vUbicacion;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.Int;
                                    param.ParameterName = "@iAnexo";
                                    param.Value = objTrabajador.iAnexo;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vEmail";
                                    param.Value = objTrabajador.vEmail;
                                    cmd.Parameters.Add(param);


                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.Int;
                                    param.ParameterName = "@Estado";
                                    param.Value = objTrabajador.siEstado;
                                    cmd.Parameters.Add(param);


                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.Int;
                                    param.ParameterName = "@Jefe";
                                    param.Value = objTrabajador.siEsJefe;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.Int;
                                    param.ParameterName = "@Firma";
                                    param.Value = objTrabajador.siFirma;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.Int;
                                    param.ParameterName = "@iCodArea";
                                    param.Value = objTrabajador.iCodArea;
                                    cmd.Parameters.Add(param);


                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.Int;
                                    param.ParameterName = "@iCodTipoDocIdentidad";
                                    param.Value = objTrabajador.iCodTipDocIdentidad;
                                    cmd.Parameters.Add(param);



                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vCelular";
                                    param.Value = objTrabajador.vCelular;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vUsuActualizacion";
                                    param.Value = objTrabajador.vUsuActualizacion;
                                    cmd.Parameters.Add(param);


                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vLgnActualizacion";
                                    param.Value = objTrabajador.vLgnActualizacion;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vRolActualizacion";
                                    param.Value = objTrabajador.vRolCreacion;
                                    cmd.Parameters.Add(param);


                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.Int;
                                    param.ParameterName = "@iCodPerfil";
                                    param.Value = objTrabajador.iCodPerfil;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vPerfil";
                                    param.Value = objTrabajador.vCodPerfil;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.SqlDbType = SqlDbType.VarChar;
                                    param.ParameterName = "@vWebUsr";
                                    param.Value = objTrabajador.WEBUSR;
                                    cmd.Parameters.Add(param);

                                    param = new SqlParameter();
                                    param.Direction = ParameterDirection.Output;
                                    param.SqlDbType = SqlDbType.Int;
                                    param.ParameterName = "@onFlagOK";
                                    cmd.Parameters.Add(param);

                                    cmd.ExecuteNonQuery();

                                    onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                                    if (onRpta >= 0)
                                    {

                                        tx.Commit();
                                    }
                                    else { tx.Rollback(); rollback = true; }

                                }
                                catch (Exception ex)
                                {
                                    nRpta = -1;
                                    tx.Rollback();
                                    rollback = true;
                                    conn.Close();
                                    conn.Dispose();
                                    cmd.Dispose();
                                    //Generando Archivo Log
                                    new LogWriter(ex.Message);
                                    throw new Exception("Error al Registrar");
                                }
                                finally
                                {
                                    conn.Close();
                                    conn.Dispose();
                                    cmd.Dispose();
                                }
                            }
                        }
                        return nRpta;
                    }
                }

            }
            return nRpta;
        }

        public int EliminarTrabajador(TrabajadorDTO objTrabajador)
        {
            int nRpta = 0;
            bool rollback = false;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    using (SqlTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            cmd.Transaction = tx;
                            int onRpta = -1;

                            cmd.CommandText = "[dbo].[UP_MAE_EliminarTrabajador]";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();


                            SqlParameter param = new SqlParameter("@iCodTrabajador", SqlDbType.Int);
                            param.Value = objTrabajador.iCodTrabajador;
                            cmd.Parameters.Add(param);


                            param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar);
                            param.Value = objTrabajador.vUsuActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar);
                            param.Value = objTrabajador.vUsuActualizacion;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter();
                            param.Direction = ParameterDirection.Output;
                            param.SqlDbType = SqlDbType.Int;
                            param.ParameterName = "@onFlagOK";
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                            if (onRpta >= 0)
                            {

                                tx.Commit();
                            }
                            else { tx.Rollback(); rollback = true; }

                        }
                        catch (Exception ex)
                        {
                            nRpta = -1;
                            tx.Rollback();
                            rollback = true;
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al eliminar");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
                return nRpta;
            }
        }

        public List<TrabajadorDTO> ListarTrabajador()
        {
            throw new NotImplementedException();
        }


        public List<TrabajadorDTO> ListarTipDocIdentidad()
        {
            TrabajadorDTO objEntidad = null;
            List<TrabajadorDTO> lstTipDocuIdentidad = new List<TrabajadorDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ListarTipDocIdentitdad]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new TrabajadorDTO();
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.iCodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.vDescripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.siEstadoDoc = short.Parse(reader["siEstado"].ToString());
                                lstTipDocuIdentidad.Add(objEntidad);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener el Listado de Áreas");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstTipDocuIdentidad;
        }


        /// <summary>
        /// Método Paa Obtener Si es duplicado de jefe por area
        /// </summary>
        /// <returns>Retorna Numero Entero</returns>  
        public int GetJefePorArea(TrabajadorDTO objTrabajador)
        {
            int cant = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_Mae_ObtenerJefePorArea]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodArea";
                    param.Value = objTrabajador.iCodArea;
                    cmd.Parameters.Add(param);
                

                     param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iOperador";
                    param.Value = objTrabajador.iOperador;
                    cmd.Parameters.Add(param);
                

                     param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodTrabajador";
                    param.Value = objTrabajador.iCodTrabajador;
                    cmd.Parameters.Add(param);


                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iEstado";
                    param.Value = objTrabajador.siEstado;
                    cmd.Parameters.Add(param);
                     

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                if (!Convert.IsDBNull(reader["Cantidad"])) cant = Convert.ToInt32(reader["Cantidad"]);
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Consultar");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return cant;
        }


        public int GetWuserDuplicados(TrabajadorDTO objTrabajador)
        {
            int cant = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_Mae_ObtenerWebUserDuplicado]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@vWebUsr";
                    param.Value = objTrabajador.WEBUSR;
                    cmd.Parameters.Add(param);
                    

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iOperador";
                    param.Value = objTrabajador.iOperador;
                    cmd.Parameters.Add(param);
                                       
                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodTrabajador";
                    param.Value = objTrabajador.iCodTrabajador;
                    cmd.Parameters.Add(param);
                                   
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                if (!Convert.IsDBNull(reader["Cantidad"])) cant = Convert.ToInt32(reader["Cantidad"]);
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Consultar");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return cant;
        }

        #endregion
    }
}


