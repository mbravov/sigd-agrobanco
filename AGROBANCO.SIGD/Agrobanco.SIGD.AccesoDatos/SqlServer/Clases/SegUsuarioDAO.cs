﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using System.Data.SqlClient;
using System.Data;
using Agrobanco.SIGD.Comun;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class SegUsuarioDAO : ISegUsuarioDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public SegUsuarioDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }
        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Método Público para el Listado de Áreas
        /// </summary>
        /// <returns>Retorna Lista genérica de Áreas</returns>  
        public ESegUsuario GetUsuario(ESegUsuario EObj)
        {
            ESegUsuario objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[up_Obtener_Usuario]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.VarChar;
                    param.ParameterName = "@pWebusr";
                    param.Value = EObj.Webusr;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new ESegUsuario();
                                if (!Convert.IsDBNull(reader["iCodTrabajador"])) objEntidad.iCodTrabajador = Convert.ToInt32(reader["iCodTrabajador"]);
                                if (!Convert.IsDBNull(reader["WEBUSR"])) objEntidad.Webusr = Convert.ToString(reader["Webusr"]);
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener el usuario");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }
        #endregion
    }
}
