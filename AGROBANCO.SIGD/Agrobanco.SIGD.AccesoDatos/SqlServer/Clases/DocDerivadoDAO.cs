﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Agrobanco.SIGD.Comun;
using System.IO;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.AccesoDatos.ImplementationsDb2;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class DocDerivadoDAO : IDocDerivaDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public DocDerivadoDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Método Público para el Obtener Datos de un Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public List<DocDerivacion> GetDocumentoDerivado(DocDerivacion EObj)
        {
            List<DocDerivacion> lstDocDerivacion = new List<DocDerivacion>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentosDerivados]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = EObj.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                DocDerivacion objEntidad = new DocDerivacion();
                                if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocumentoDerivacion = Convert.ToInt32(reader["iCodDocumentoDerivacion"]);
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["iCodResponsable"])) objEntidad.CodResponsable = Convert.ToInt32(reader["iCodResponsable"]);
                                if (!Convert.IsDBNull(reader["siEstadoDerivacion"])) objEntidad.EstadoDerivacion = Convert.ToInt16(reader["siEstadoDerivacion"]);
                                if (!Convert.IsDBNull(reader["siTipoAcceso"])) objEntidad.TipoAcceso = Convert.ToInt16(reader["siTipoAcceso"]);
                                if (!Convert.IsDBNull(reader["dFecDerivacion"])) objEntidad.FecDerivacion = DateTime.Parse(reader["dFecDerivacion"].ToString());

                                if (!Convert.IsDBNull(reader["NombreArea"])) objEntidad.NombreArea = Convert.ToString(reader["NombreArea"]);
                                if (!Convert.IsDBNull(reader["NombreResponsable"])) objEntidad.NombreResponsable = Convert.ToString(reader["NombreResponsable"]);
                                if (!Convert.IsDBNull(reader["WEBUSRRESPONSABLE"])) objEntidad.WEBUsrResponsable = Convert.ToString(reader["WEBUSRRESPONSABLE"]);
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                if (!Convert.IsDBNull(reader["vComentario"])) objEntidad.Comentarios = Convert.ToString(reader["vComentario"]);

                                lstDocDerivacion.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocDerivacion;
        }
        
        /// <summary>
        /// Método Público para el Obtener Datos de un Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public List<DocDerivacion> GetDocumentoDerivadoEmpresasSIED(DocDerivacion EObj)
        {
            List<DocDerivacion> lstDocDerivacion = new List<DocDerivacion>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentosDerivadosEmpresasSIED]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumento";
                    param.Value = EObj.CodDocumento;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                DocDerivacion objEntidad = new DocDerivacion();
                                if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocumentoDerivacion = Convert.ToInt32(reader["iCodDocumentoDerivacion"]);
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["dFecDerivacion"])) objEntidad.FecDerivacion = DateTime.Parse(reader["dFecDerivacion"].ToString());

                                if (!Convert.IsDBNull(reader["vNombreEntidadSIED"])) objEntidad.NombreEntidadSIED = Convert.ToString(reader["vNombreEntidadSIED"]);
                                if (!Convert.IsDBNull(reader["vRucSIED"])) objEntidad.rucSied = Convert.ToString(reader["vRucSIED"]);
                                if (!Convert.IsDBNull(reader["iFormatoDocumentoSIED"])) objEntidad.FormatoDocSied = Convert.ToString(reader["iFormatoDocumentoSIED"]);
                                if (!Convert.IsDBNull(reader["iCodEntidadSIED"])) objEntidad.CodEntidadSied = Convert.ToInt32(reader["iCodEntidadSIED"]);
                                if (!Convert.IsDBNull(reader["siTipoAcceso"])) objEntidad.TipoAcceso = Convert.ToInt16(reader["siTipoAcceso"]);

                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);

                                lstDocDerivacion.Add(objEntidad);
                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return lstDocDerivacion;
        }
        /// <summary>
        /// Método Público para el Obtener Datos de un Documento Derivado
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public DocDerivacion GetDocumentoDerivadoPorId(int CodDocumentoDerivacion)
        {
            DocDerivacion objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorId]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();

                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodDocumentoDerivacion";
                    param.Value = CodDocumentoDerivacion;
                    cmd.Parameters.Add(param);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                objEntidad = new DocDerivacion();
                                if (!Convert.IsDBNull(reader["iCodDocumentoDerivacion"])) objEntidad.CodDocumentoDerivacion = Convert.ToInt32(reader["iCodDocumentoDerivacion"]);
                                if (!Convert.IsDBNull(reader["iCodDocumento"])) objEntidad.CodDocumento = Convert.ToInt32(reader["iCodDocumento"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["iCodArea"])) objEntidad.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                if (!Convert.IsDBNull(reader["iCodResponsable"])) objEntidad.CodResponsable = Convert.ToInt32(reader["iCodResponsable"]);
                                if (!Convert.IsDBNull(reader["siEstadoDerivacion"])) objEntidad.EstadoDerivacion = Convert.ToInt16(reader["siEstadoDerivacion"]);
                                if (!Convert.IsDBNull(reader["siTipoAcceso"])) objEntidad.TipoAcceso = Convert.ToInt16(reader["siTipoAcceso"]);
                                if (!Convert.IsDBNull(reader["dFecDerivacion"])) objEntidad.FecDerivacion = DateTime.Parse(reader["dFecDerivacion"].ToString());

                                if (!Convert.IsDBNull(reader["NombreArea"])) objEntidad.NombreArea = Convert.ToString(reader["NombreArea"]);
                                if (!Convert.IsDBNull(reader["NombreResponsable"])) objEntidad.NombreResponsable = Convert.ToString(reader["NombreResponsable"]);
                                //Auditoria
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = DateTime.Parse(reader["dFecCreacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = DateTime.Parse(reader["dFecActualizacion"].ToString());
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);

                            }

                        }
                        catch (Exception ex)
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                            //Generando Archivo Log
                            new LogWriter(ex.Message);
                            throw new Exception("Error al Obtener datos del trabajador");
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                            cmd.Dispose();
                        }
                    }
                }
            }
            return objEntidad;
        }
        /// <summary>
        /// Método Público para el Registro de un Documento Derivación
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento Derivación</returns>  
        public int RegistrarDocumentoDerivacion(DocDerivacion objDocDerivacion)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
               
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocumentosDerivados]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                        param.Value = objDocDerivacion.CodDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@iCodArea", SqlDbType.Int);
                        param.Value = objDocDerivacion.CodArea;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@iCodResponsable", SqlDbType.Int);
                        param.Value = objDocDerivacion.CodResponsable;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@siTipoAcceso", SqlDbType.SmallInt);
                        param.Value = objDocDerivacion.TipoAcceso;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
                        param.Value = objDocDerivacion.FecDerivacion;
                        cmd.Parameters.Add(param);
                        //auditoria
                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vUsuCreacion";
                        param.Value = objDocDerivacion.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vHstCreacion";
                        param.Value = objDocDerivacion.HostCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vInsCreacion";
                        param.Value = objDocDerivacion.InstanciaCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vLgnCreacion";
                        param.Value = objDocDerivacion.LoginCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vRolCreacion";
                        param.Value = objDocDerivacion.RolCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);

                        throw new Exception("Error al Registrar Doc Derivacion");
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
                
            }

            return nRpta;
        }



        public int RegistrarDocumentoDerivacionSIED(DocDerivacion objDocDerivacion)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocumentosDerivadosSIED]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                        param.Value = objDocDerivacion.CodDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@iCodArea", SqlDbType.Int);
                        param.Value = null;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@iCodResponsable", SqlDbType.Int);
                        param.Value = null;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@siTipoAcceso", SqlDbType.SmallInt);
                        param.Value = objDocDerivacion.TipoAcceso;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@dFecDerivacion", SqlDbType.DateTime);
                        param.Value = objDocDerivacion.FecDerivacion;
                        cmd.Parameters.Add(param);



                        param = new SqlParameter("@iCodEntidadSIED", SqlDbType.Int);
                        param.Value = objDocDerivacion.CodEntidadSied;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vNombreEntidadSIED", SqlDbType.VarChar);
                        param.Value = objDocDerivacion.NombreEntidadSIED;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vRucSIED", SqlDbType.VarChar);
                        param.Value = objDocDerivacion.rucSied;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@iFormatoDocumentoSIED", SqlDbType.VarChar);
                        param.Value = objDocDerivacion.FormatoDocSied;
                        cmd.Parameters.Add(param);


                        //auditoria
                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vUsuCreacion";
                        param.Value = objDocDerivacion.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vHstCreacion";
                        param.Value = objDocDerivacion.HostCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vInsCreacion";
                        param.Value = objDocDerivacion.InstanciaCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vLgnCreacion";
                        param.Value = objDocDerivacion.LoginCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vRolCreacion";
                        param.Value = objDocDerivacion.RolCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);

                        throw new Exception("Error al Registrar Doc Derivacion");
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }

            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para el Inactivar de un Documento Derivación
        /// </summary>
        /// <returns>Retorna el Indicador de Inactivar del Documento Derivación</returns>  
        public int InactivarDocumentoDerivacion(DocDerivacion objDocDerivacion, out string outCorrelativoDoc)
        {
            int nRpta = 0;
            outCorrelativoDoc = "";
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MOV_InactivarDocumentosDerivados]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumentoDerivacion", SqlDbType.Int);
                        param.Value = objDocDerivacion.CodDocumentoDerivacion;
                        cmd.Parameters.Add(param);
                        //auditoria
                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vUsuActualizacion";
                        param.Value = objDocDerivacion.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vHstActualizacion";
                        param.Value = objDocDerivacion.HostCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vInsActualizacion";
                        param.Value = objDocDerivacion.InstanciaCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vLgnActualizacion";
                        param.Value = objDocDerivacion.LoginCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vRolActualizacion";
                        param.Value = objDocDerivacion.RolCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);

                        throw new Exception("Error al Inactivar Doc Derivacion");
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }

                }
            }

            return nRpta;
        }
        /// <summary>
        /// Método Público para eliminar de un Documento Derivación
        /// </summary>
        /// <returns>Retorna el Indicador de eliminacion del Documento Derivación</returns>   
        public int ElimarDocumentoDerivacionCC(DocDerivacion objDocDerivacion)
        {
            int onRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        onRpta = -1;

                        cmd.CommandText = "[dbo].[UP_MOV_EliminarDerivacionCC]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                        param.Value = objDocDerivacion.CodDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        onRpta = -1;
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);

                        throw new Exception("Error al Eliminar Doc Derivacion");
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }

            }

            return onRpta;
        }

        public int AnularDocumentoDerivacion(DocDerivacion docDerivacion, string comentario)
        {
            int oRpta = 0;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        oRpta = -1;

                        cmd.CommandText = "[dbo].[UP_MOV_AnularDerivacion]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                        param.Value = docDerivacion.CodDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@iCodDocumentoDerivacion", SqlDbType.Int);
                        param.Value = docDerivacion.CodDocumentoDerivacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vUsuCreacion";
                        param.Value = docDerivacion.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vHstCreacion";
                        param.Value = docDerivacion.HostCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vInsCreacion";
                        param.Value = docDerivacion.InstanciaCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vLgnCreacion";
                        param.Value = docDerivacion.LoginCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vRolCreacion";
                        param.Value = docDerivacion.RolCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        oRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                        DocComentario objComentario = new DocComentario();
                        objComentario.CodDocumento = docDerivacion.CodDocumento;
                        objComentario.CodDocDerivacion = docDerivacion.CodDocumentoDerivacion;
                        objComentario.CodDocMovimiento = oRpta;
                        objComentario.Comentario = comentario;
                        objComentario.UsuarioCreacion = docDerivacion.UsuarioCreacion;
                        objComentario.RolCreacion = docDerivacion.RolCreacion;
                        objComentario.LoginCreacion = docDerivacion.LoginCreacion;

                        oRpta = InsertarMovComentario(objComentario, cmd);


                    }
                    catch (Exception ex)
                    {
                        oRpta = -1;
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);

                        throw new Exception("Error al Registrar Doc Derivacion");
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }

            return oRpta;            
        }


        public int AnularDocumentoDerivacionSinComentario(DocDerivacion docDerivacion, string comentario)
        {
            int oRpta = 0;

            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        oRpta = -1;

                        cmd.CommandText = "[dbo].[UP_MOV_AnularDerivacion]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
                        param.Value = docDerivacion.CodDocumento;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@iCodDocumentoDerivacion", SqlDbType.Int);
                        param.Value = docDerivacion.CodDocumentoDerivacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vUsuCreacion";
                        param.Value = docDerivacion.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vHstCreacion";
                        param.Value = docDerivacion.HostCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vInsCreacion";
                        param.Value = docDerivacion.InstanciaCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vLgnCreacion";
                        param.Value = docDerivacion.LoginCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vRolCreacion";
                        param.Value = docDerivacion.RolCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        oRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        oRpta = -1;
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);

                        throw new Exception("Error al Registrar Doc Derivacion");
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }

            return oRpta;
        }

        private int InsertarMovComentario(DocComentario objDocComentario, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocComentario]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@vComentario", SqlDbType.VarChar, 1000);
            param.Value = objDocComentario.Comentario;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocMovimiento", SqlDbType.Int);
            param.Value = objDocComentario.CodDocMovimiento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocumento", SqlDbType.Int);
            param.Value = objDocComentario.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocDerivacion", SqlDbType.Int);
            param.Value = objDocComentario.CodDocDerivacion;
            cmd.Parameters.Add(param);


            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocComentario.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocComentario.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocComentario.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocComentario.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocComentario.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        #endregion
    }
}
