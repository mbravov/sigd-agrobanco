﻿using Agrobanco.SIGD.AccesoDatos.DataDb2;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.SIGD.AccesoDatos.ImplementationsDb2
{
    public class DSegSistema
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public DSegSistema()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }

        public ESegSistema ObtenerSegSistema(ESegSistema EObj)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("P_VCODSISTEMA", 20, EObj.vCodSistema);
            _db2DataContext.RunStoredProcedure(library + ".PA_SEGSISTEMA_QRY01", out IDataReader reader, lDataParam);
            ESegSistema _EObj = new ESegSistema();
            while (reader.Read())
            {
                _EObj.vCodSistema = reader.GetString("VCODSISTEMA");
                _EObj.vNombre = reader.GetString("VNOMBRE");
                _EObj.vDescripcion = reader.GetString("VDESCRIPCION");
            }

            return _EObj;
        }

    }
}
