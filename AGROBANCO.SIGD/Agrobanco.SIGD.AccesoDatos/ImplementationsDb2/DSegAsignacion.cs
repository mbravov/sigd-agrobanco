﻿using Agrobanco.SIGD.AccesoDatos.DataDb2;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.SIGD.AccesoDatos.ImplementationsDb2
{
    public class DSegAsignacion
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public DSegAsignacion()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }

        public List<ESegAsignacion> ListaSegAsignacion(ESegAsignacion strWebusr)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("P_WEBUSR", 20, strWebusr.webusr);
            _db2DataContext.RunStoredProcedure(library + ".PA_SEGASIGNACION_QRY01", out IDataReader reader, lDataParam);
            ESegAsignacion ObjE = new ESegAsignacion();
            List<ESegAsignacion> ListObjE = new List<ESegAsignacion>();
            while (reader.Read())
            {
                ObjE.vCodAsignacion = reader.GetString("VCODASIGNACION");
                ObjE.vCodPerfil = reader.GetString("VCODPERFIL");
                ObjE.vCodSistema = reader.GetString("VCODSISTEMA");
                ObjE.webusr = reader.GetString("WEBUSR");
                ListObjE.Add(ObjE);
                break;
            }

            return ListObjE;
        }

    }
}
