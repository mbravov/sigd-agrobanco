﻿using Agrobanco.SIGD.Entidades.WebService;
using System.Web.Mvc;
using ItemAnexo = AGROBANCO.TEST.Presentacion.WSDocumentoSIGD.ItemAnexo;

namespace AGROBANCO.TEST.Presentacion.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.----test";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public ActionResult registrarDocumentoSIED(TramiteDocumentario tramiteDocumentario)
        {
            WSDocumentoSIGD.WSDocumentoSIGD objService = new WSDocumentoSIGD.WSDocumentoSIGD();
            ItemAnexo[] Anexos = new ItemAnexo[tramiteDocumentario.anexos.Count];
            for (var x =0;x< tramiteDocumentario.anexos.Count;x++)
            {
                Anexos[x] = new ItemAnexo
                {
                    AlmacenArchivo = tramiteDocumentario.anexos[x].almacenArchivo,
                    Descripcion = tramiteDocumentario.anexos[x].descripcion,
                    NombreFIsico = tramiteDocumentario.anexos[x].nombreFisico
                };
                
            }
            
            return Json(objService.WM_RegistrarTransaccion(tramiteDocumentario.tipoEntidad, tramiteDocumentario.entidad, tramiteDocumentario.numeroDocumento,
                                                            tramiteDocumentario.fechaDocumento, tramiteDocumentario.dirigidoA, tramiteDocumentario.referencia, tramiteDocumentario.asunto,
                                                            tramiteDocumentario.prioridad, tramiteDocumentario.plazo, tramiteDocumentario.indicacion, tramiteDocumentario.otraIndicacion,
                                                            Anexos));
        }
    }
}