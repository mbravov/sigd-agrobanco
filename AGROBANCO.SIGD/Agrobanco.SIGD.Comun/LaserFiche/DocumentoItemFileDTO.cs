﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public class DocumentoItemFileDTO
    {
        public int Id { get; set; }
        public int TipoArchivo { get; set; }
    }
}
