﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public class RegistraDocumentoDTO
    {
        /// <summary>
        /// Ejemplo:\{NumeroDocumento}\{NumeroSolicitud}
        /// </summary>
        public string SubRuta { get; set; }
        public string ArchivoBase64 { get; set; }
        public string NombreArchivo { get; set; }
        public string Extension { get; set; }

    }
}
