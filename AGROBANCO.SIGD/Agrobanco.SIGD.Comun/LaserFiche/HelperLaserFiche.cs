﻿//using Laserfiche.RepositoryAccess;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Agrobanco.SIGD.Comun.LaserFiche
//{
//    public class HelperLaserFiche : IDisposable
//    {
//        private readonly string _repositoryName;
//        private readonly string _userName;
//        private readonly string _password;
//        private readonly string _hostName;

//        public HelperLaserFiche(string repositoryName)
//        {

//            //Compose();
//            _repositoryName = repositoryName;
//            _userName = ApplicationKeys.LaserFicheUser;// _decriptService.ReadValue("Usuario");
//            _password = ApplicationKeys.LaserFichePassword; //_decriptService.ReadValue("Clave");
//            _hostName = ApplicationKeys.LaserFicheServer;//_decriptService.ReadValue("Server"); // "41.50.13.193";
//        }
//        private Session GetSession()
//        {
//            var repositoryReg = new RepositoryRegistration { ServerName = _hostName, Name = _repositoryName };
//            var _sessionLf = new Session();
//            _sessionLf.LogIn(_userName, _password, repositoryReg);
//            return _sessionLf;
//        }
//        private bool isValidPath(string path, out Exception ex)
//        {
//            bool validoPath = false;
//            ex = null;
//            switch (path)
//            {
//                case null:
//                    ex = new ArgumentNullException(nameof(path));
//                    break;
//                case "":
//                    ex = new ArgumentException(nameof(path));
//                    break;
//            }
//            return validoPath;
//        }
//        public FolderInfo GetOrCreateFolderInfo(string path)
//        {

//            var repositoryReg = new RepositoryRegistration { ServerName = _hostName, Name = _repositoryName };
//            Exception ex = null;
//            FolderInfo folderInfo = null;
//            if (!isValidPath(path, out ex))
//            {
//                throw ex;
//            }
//            using (var _sessionLf = GetSession())
//            {
//                var entry = Entry.TryGetEntryInfo(path, _sessionLf);
//                switch (entry)
//                {
//                    case null:
//                        folderInfo = CreateHelper(path, _sessionLf);
//                        break;
//                    case FolderInfo folder:
//                        folder.Unlock();
//                        folderInfo = folder;
//                        break;
//                    default:
//                        throw new DuplicateObjectException("Object already exists");
//                        break;
//                }
//            }
//            return folderInfo;
//        }

//        private FolderInfo CreateHelper(string path, Session session)
//        {
//            EntryInfo entry = null;
//            var toCreate = new Stack<string>();
//            if (!path.StartsWith("\\"))
//                path = "\\" + path;

//            // This is equivalent to the recursion in the other
//            // solution, but with an explicit stack instead of relying
//            // on the call stack for state.
//            while (entry == null)
//            {
//                toCreate.Push(path.Split('\\').Last());
//                path = path.Substring(0, path.LastIndexOf('\\'));
//                if (path == "")
//                    path = "\\";
//                entry = Entry.TryGetEntryInfo(path, session);
//            }
//            if (!(entry is FolderInfo folder))
//                throw new DuplicateObjectException("La raiz de la ruta no es una carpeta.");

//            // Walk back up the stack as if returning from recursive
//            // calls.
//            FolderInfo parent = null;
//            try
//            {
//                while (toCreate.Count > 0)
//                {
//                    parent?.Dispose();
//                    parent = folder;
//                    folder = new FolderInfo(session);
//                    path = toCreate.Pop();
//                    folder.Create(parent, path, EntryNameOption.None);
//                }
//            }
//            catch (Exception)
//            {
//                // Guarantee that folder does not leak if an Exception
//                // is thrown.
//                folder.Dispose();
//                throw;
//            }
//            finally
//            {
//                // Guarantee that parent does not leak.
//                parent?.Dispose();
//            }
//            folder.Unlock();
//            return folder;
//        }


//    }
//}
