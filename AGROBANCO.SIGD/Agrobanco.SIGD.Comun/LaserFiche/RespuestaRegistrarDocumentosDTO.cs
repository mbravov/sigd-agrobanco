﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public class RespuestaRegistrarDocumentosDTO : RespuestaBaseDTO
    {
        public List<ArchivoRegistradoDTO> ListaDocumentos { get; set; }
    }
    public class ArchivoRegistradoDTO
    {
        public string NombreArchivo { get; set; }
        public int? IdArchivo { get; set; }

    }
}
