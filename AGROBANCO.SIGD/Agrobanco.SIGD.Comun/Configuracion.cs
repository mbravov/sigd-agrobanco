﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace Agrobanco.SIGD.Comun
{
   public static class Configuracion
    {
        /// <summary>
        /// Nombre del Key que contiene la cadena de conexión a la Base de datos DBAgrSIGD
        /// </summary>
        //public static string ClaveConnectionStringSQL
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["ConnectionString"].ToString();
        //    }
        //}
        /// <summary>
        /// Ignorar Conexión con DB2AS400
        /// </summary>
        public static bool IgnorarLoginDB2AS400
        {
            get
            {
                return ConfigurationManager.AppSettings["IgnorarLoginDB2AS400"] ==null ? false: Convert.ToBoolean(ConfigurationManager.AppSettings["IgnorarLoginDB2AS400"].ToString());
            }
        }
        
        /// <summary>
        /// Nombre del Key que contiene la cadena de conexión a la Base de datos DBAgrSIGD
        /// </summary>
        public static string RutaLogError
        {
            get
            {
                return ConfigurationManager.AppSettings["RutaLogError"].ToString();
            }
        }
        /// <summary>
        /// Nombre del Key que contiene la cadena de conexión a la Base de datos DBAgrSIGD
        /// </summary>
        public static string MaxLengthArchivo
        {
            get
            {
                return ConfigurationManager.AppSettings["TamañoMáximoArchivo"].ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string SeguridadUrlAutenticate
        {
            get
            {
                return ConfigurationManager.AppSettings["SeguridadUrlAutenticate"].ToString();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string SeguridadUrlGetUsuario
        {
            get
            {
                return ConfigurationManager.AppSettings["SeguridadUrlGetUsuario"].ToString();
            }
        }
        public static string SeguridadUrlAutenticateDummy
        {
            get
            {
                return ConfigurationManager.AppSettings["SeguridadUrlAutenticateDummy"].ToString();
            }
        }

        public static string rutaLogsError
        {
            get
            {
                return ConfigurationManager.AppSettings["rutaLogsError"].ToString();
            }
        }
        public static string rutaLogsErrorApi
        {
            get
            {
                return ConfigurationManager.AppSettings["rutaLogsErrorApi"].ToString();
            }
        }
    }
}
