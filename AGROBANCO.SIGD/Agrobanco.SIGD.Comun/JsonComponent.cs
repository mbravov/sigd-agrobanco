﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace Agrobanco.SIGD.Comun
{
    public static class JsonComponent
    {
        public static string Serialize<T>(T obj)
        {

            if (obj == null)
                return "";

            var jserialiser = new JavaScriptSerializer();
            var json = jserialiser.Serialize(obj);

            return json.ToString();

        }

        public static T Deserialize<T>(string json)
        {

            T _result = default(T);

            if (string.IsNullOrEmpty(json) || string.IsNullOrWhiteSpace(json))
                return _result;

            JavaScriptSerializer js2 = new JavaScriptSerializer();
            js2.MaxJsonLength = Int32.MaxValue;
            _result = js2.Deserialize<T>(json);

            return _result;
        }

        public static object DeserializeObject(string json)
        {
            if (string.IsNullOrEmpty(json) || string.IsNullOrWhiteSpace(json))
                return null;

            JavaScriptSerializer js2 = new JavaScriptSerializer();
            return js2.DeserializeObject(json);

        }

        public static string SerializeXmlNode(XmlDocument doc)
        {
            try
            {
                return JsonConvert.SerializeXmlNode(doc);
            }
            catch (Exception)
            {

                return "";
            }

        }
        public static XmlDocument DeserializeXmlNode(string json)
        {
            try
            {
                return JsonConvert.DeserializeXmlNode(json);
            }
            catch (Exception)
            {

                return null;
            }

        }
    }
}
