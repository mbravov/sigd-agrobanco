﻿using Agrobanco.SIGD.AccesoDatos.ImplementationsDb2;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using System.Collections.Generic;

namespace Agrobanco.SIGD.LogicaNegocio.ImplementationsDb2
{
    public class NCntrlweb
    {

        public ECntrlweb GetPerfilUsuarioDummy(ELoginRequest login)
        {
            ECntrlweb info = new ECntrlweb();
            info.webusr = login.Username.ToUpper();
            info.webacp = "";
            return info;
        }
        public ECntrlweb LoginUsuario(ELoginRequest login)
        {
            CntrlwebData ObjDCnt = new CntrlwebData();
            return ObjDCnt.LoginUsuario(login);
        }
        public ECntrlweb ConsultarUsuario(ELoginRequest login)
        {
            CntrlwebData ObjDCnt = new CntrlwebData();
            return ObjDCnt.ConsultarUsuario(login);
        }
    }
}
