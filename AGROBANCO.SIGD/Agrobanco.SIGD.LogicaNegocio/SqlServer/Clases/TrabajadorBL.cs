﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class TrabajadorBL : ITrabajador
    {
        private IMaeTrabajadorDAO TrabajadorBD;
        private string strCadenaConexion;

        public TrabajadorBL()
        {
            SqlServerAccess sqlAccess = new SqlServerAccess();
            
            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.TrabajadorBD = new MaeTrabajadorDAO(strCadenaConexion);
        }

        public List<TrabajadorDTO> ListarTrabajadorPorArea(EMaeTrabajador objTrabajador)
        {
            return this.TrabajadorBD.GetTrabajadorPorArea(objTrabajador);
        }
        
         public List<TrabajadorDTO> ListarTrabajadorPorAreaSIED()
        {
            return this.TrabajadorBD.GetTrabajadorPorAreaSIED();
        }

        public List<TrabajadorDTO> GetTrabajadores()
        {
            return this.TrabajadorBD.GetTrabajadores();
        }



        #region Métodos Públicos
        /// <summary>
        /// Método Público para el Listado de Trabajadores
        /// </summary>
        /// <returns>Retorna Lista genérica de Trabajadores</returns>  
        public List<TrabajadorDTO> ListarTrabajador()
        {
            return this.TrabajadorBD.ListarTrabajador();
        }

        public List<TrabajadorDTO> ListarTrabajadorPaginado(TrabajadorDTO objParams, out int totalRegistros)
        {
            return this.TrabajadorBD.ListarTrabajadorPaginado(objParams, out totalRegistros);
        }

        public TrabajadorDTO ObtenerTabajadorPorId(TrabajadorDTO objParams)
        {
            return this.TrabajadorBD.ObtenerTabajadorPorId(objParams);
        }

        public TrabajadorDTO ObtenerTabajadorPorWEBUSR(TrabajadorDTO objParams)
        {
            return this.TrabajadorBD.ObtenerTabajadorPorWEBUSR(objParams);
        }

        public int RegistrarTrabajador(TrabajadorDTO objArea)
        {
            return this.TrabajadorBD.RegistrarTrabajador(objArea);

        }

        public int ActualizarTrabajador(TrabajadorDTO objArea)
        {
            return this.TrabajadorBD.ActualizarTrabajador(objArea);
        }

        public int EliminarTrabajador(TrabajadorDTO objArea)
        {
            return this.TrabajadorBD.EliminarTrabajador(objArea);
        }

        public List<TrabajadorDTO> ListarTipDocIdentidad()
        {
            return this.TrabajadorBD.ListarTipDocIdentidad();
        }



        #endregion
    }
}
