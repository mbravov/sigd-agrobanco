﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class RepresentanteBL:IRepresentanteBL
    {
        #region Constructor

        private IRepresentanteDAO RepresentanteBD;
        private string strCadenaConexion;

        public RepresentanteBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.RepresentanteBD = new RepresentanteDAO(strCadenaConexion);
        }


        #endregion


        #region Métodos Públicos
        /// <summary>
        /// Método Público para el buscar Representante
        /// </summary>
        /// <returns>Retorna Lista genérica de Representante</returns>  
        public List<Representante> BuscarRepresentante(FiltroMantenimientoBaseDTO filtro, out int totalRegistros)
        {
            return this.RepresentanteBD.BuscarRepresentante(filtro, out totalRegistros);
        }

        /// <summary>
        /// Método Público para el obtener un(a) Representante
        /// </summary>
        /// <returns>Retorna Lista genérica de Representante</returns>  
        public Representante ObtenerRepresentante(Representante objRepresentante)
        {
            return this.RepresentanteBD.ObtenerRepresentante(objRepresentante);
        }

        /// <summary>
        /// Método Público para Registrar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Registrar(Representante objRepresentante)
        {
            return this.RepresentanteBD.Registrar(objRepresentante);
        }
        /// <summary>
        /// Método Público para Actualizar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Actualizar(Representante objRepresentante)
        {
            return this.RepresentanteBD.Actualizar(objRepresentante);
        }


        /// <summary>
        /// Método Público para Eliminar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Eliminar(Representante objRepresentante)
        {
            return this.RepresentanteBD.Eliminar(objRepresentante);
        }

        public Representante ValidarRepresentantePorNumDocumento(Representante objRepresentante)
        {
            return this.RepresentanteBD.ValidarRepresentantePorNumDocumento(objRepresentante);
        }

        #endregion
    }
}
