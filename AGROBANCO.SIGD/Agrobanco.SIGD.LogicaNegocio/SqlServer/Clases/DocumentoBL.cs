﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using Agrobanco.SIGD.Presentacion.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Agrobanco.SIGD.Comun.Enumerados;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class DocumentoBL : IDocumentoBL
    {
        #region Constructor

        private IDocumentoDAO DocumentoBD;
        private IAreaDAO AreaBD;
        private IDocDerivaDAO DocumentoDeriva;
        private string strCadenaConexion;
        private string rutaProdHabilitado = ConfigurationManager.AppSettings["rutaProdHabilitado"];

        public DocumentoBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.DocumentoBD = new DocumentoDAO(strCadenaConexion);
            this.DocumentoDeriva = new DocDerivadoDAO(strCadenaConexion);
            this.AreaBD = new AreaDAO(strCadenaConexion);
        }


        #endregion

        #region Métodos Públicos


        /// <summary>
        /// Método Público para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public Documento ObtenerDocumento(Documento objParams, out string informacion)
        {
            informacion = "";
            Documento DocEObj = this.DocumentoBD.GetDocumento(objParams);
            if (DocEObj != null)
            {
                DocAdjunto DocAdjEObj = new DocAdjunto();
                DocAdjEObj.CodDocumento = objParams.CodDocumento;

                DocAnexo DocAnexoEObj = new DocAnexo();
                DocAnexoEObj.CodDocumento = objParams.CodDocumento;

                DocDerivacion DocDerivacionObj = new DocDerivacion();
                DocDerivacionObj.CodDocumento = objParams.CodDocumento;

                DocEObj.LstDocAdjunto = this.DocumentoBD.GetDocumentosAdjuntos(DocAdjEObj);
                DocEObj.LstDocAnexo = this.DocumentoBD.GetDocumentoAnexos(DocAnexoEObj);
                DocEObj.LstDocDerivacion = this.DocumentoDeriva.GetDocumentoDerivado(DocDerivacionObj);
                //DocEObj.LstDocAnexo = new DocDerivacion().this.DocumentoBD.GetDocumentoAnexos(DocAnexoEObj);
                if (DocEObj.LstDocAdjunto.Count > 0 || DocEObj.LstDocAnexo.Count > 0)
                {
                    try
                    {
                        List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                        foreach (var item in DocEObj.LstDocAdjunto)
                        {
                            ListaDocLaser.Add(new DocumentoItemFileDTO()
                            {
                                Id = item.CodLaserfiche,
                                TipoArchivo = Convert.ToInt32(TipoArchivos.Adjuntos)
                            });
                        }

                        foreach (var item in DocEObj.LstDocAnexo)
                            ListaDocLaser.Add(new DocumentoItemFileDTO()
                            {
                                Id = item.CodLaserfiche,
                                TipoArchivo = item.TipoArchivo
                            });


                        EnvioConsultaArchivosDTO laserEObj = new EnvioConsultaArchivosDTO();
                        laserEObj.ListaDocumentos = ListaDocLaser;

                        LaserFiche laserObj = new LaserFiche();
                        DocEObj.ArchivosLaserFiche = laserObj.ConsultaDefinicionArchivos(laserEObj);
                    }
                    catch (Exception ex)
                    {
                        new LogWriter(ex.Message);
                        informacion = "Neg: Error al Obtener el Datos del Documento:";
                        //                        throw new Exception("Neg: Error al Obtener el Datos del Documento:");
                    }
                }
            }

            return DocEObj;

        }

        public Documento ObtenerAdjuntos(Documento objParams, out string informacion)
        {
            informacion = "";
            Documento DocEObj = this.DocumentoBD.GetDocumentoRecepcionSIED(objParams);
            if (DocEObj != null)
            {
                DocAdjunto DocAdjEObj = new DocAdjunto();
                DocAdjEObj.CodDocumento = objParams.CodDocumento;


                DocEObj.LstDocAdjunto = this.DocumentoBD.GetDocumentosAdjuntos(DocAdjEObj);

            }

            return DocEObj;

        }
        /// <summary>
        /// Método Público para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public Documento ObtenerDocumentoRecepcionSIED(Documento objParams, out string informacion)
        {
            informacion = "";
            Documento DocEObj = this.DocumentoBD.GetDocumentoRecepcionSIED(objParams);
            if (DocEObj != null)
            {
                DocAdjunto DocAdjEObj = new DocAdjunto();
                DocAdjEObj.CodDocumento = objParams.CodDocumento;

                DocAnexo DocAnexoEObj = new DocAnexo();
                DocAnexoEObj.CodDocumento = objParams.CodDocumento;

                DocDerivacion DocDerivacionObj = new DocDerivacion();
                DocDerivacionObj.CodDocumento = objParams.CodDocumento;

                DocEObj.LstDocAdjunto = this.DocumentoBD.GetDocumentosAdjuntos(DocAdjEObj);
                DocEObj.LstDocAnexo = this.DocumentoBD.GetDocumentoAnexos(DocAnexoEObj);
                DocEObj.LstDocDerivacion = this.DocumentoDeriva.GetDocumentoDerivado(DocDerivacionObj);
                //DocEObj.LstDocAnexo = new DocDerivacion().this.DocumentoBD.GetDocumentoAnexos(DocAnexoEObj);
                if (DocEObj.LstDocAdjunto.Count > 0 || DocEObj.LstDocAnexo.Count > 0)
                {
                    try
                    {
                        List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                        foreach (var item in DocEObj.LstDocAdjunto)
                        {
                            ListaDocLaser.Add(new DocumentoItemFileDTO()
                            {
                                Id = item.CodLaserfiche,
                                TipoArchivo = Convert.ToInt32(TipoArchivos.Adjuntos)
                            });
                        }

                        foreach (var item in DocEObj.LstDocAnexo)
                            ListaDocLaser.Add(new DocumentoItemFileDTO()
                            {
                                Id = item.CodLaserfiche,
                                TipoArchivo = item.TipoArchivo
                            });


                        EnvioConsultaArchivosDTO laserEObj = new EnvioConsultaArchivosDTO();
                        laserEObj.ListaDocumentos = ListaDocLaser;

                        LaserFiche laserObj = new LaserFiche();
                        DocEObj.ArchivosLaserFiche = laserObj.ConsultaDefinicionArchivos(laserEObj);

                        DocEObj.ArchivosLaserFiche.ListaDocumentos?.ForEach(laser => {
                            DocEObj.LstDocAnexo?.ForEach(anexo => {
                                if (laser.IdDocumento == anexo.CodLaserfiche)
                                {
                                    laser.AlmacenArchivoSIED = anexo.AlmacenArchivoSIED;
                                    laser.DescripcionSIED = anexo.DescripcionSIED;
                                    laser.NombreFisicoSIED = anexo.NombreFisicoSIED;
                                }
                            });
                            DocEObj.LstDocAdjunto?.ForEach(adjunto => {
                                if (laser.IdDocumento == adjunto.CodLaserfiche)
                                {
                                    laser.AlmacenArchivoSIED = adjunto.AlmacenArchivoSIED;
                                    laser.DescripcionSIED = adjunto.DescripcionSIED;
                                    laser.NombreFisicoSIED = adjunto.NombreFisicoSIED;
                                }
                            });
                        });
                    }
                    catch (Exception ex)
                    {
                        new LogWriter(ex.Message);
                        informacion = "Neg: Error al Obtener el Datos del Documento:";
                        //                        throw new Exception("Neg: Error al Obtener el Datos del Documento:");
                    }
                }
            }

            return DocEObj;

        }

        /// <summary>
        /// Método Público para establecer el documento principal
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public int EstablecerDocumentoPrincipalSIED(int CodDocumento)
        {
            int result = 0;
            try
            {
                result = this.DocumentoBD.EstablecerDocumentoPrincipalSIED(CodDocumento);
            }
            catch (Exception ex)
            {
                new LogWriter(ex.Message);
            }

            return result;

        }

        /// <summary>
        /// Método Público para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public Documento ObtenerDocumentoInterno(Documento objParams, out string informacion)
        {
            informacion = "";
            Documento DocEObj = this.DocumentoBD.GetDocumento(objParams);
            if (DocEObj != null)
            {
                DocAdjunto DocAdjEObj = new DocAdjunto();
                DocAdjEObj.CodDocumento = objParams.CodDocumento;

                DocAnexo DocAnexoEObj = new DocAnexo();
                DocAnexoEObj.CodDocumento = objParams.CodDocumento;

                DocDerivacion DocDerivacionObj = new DocDerivacion();
                DocDerivacionObj.CodDocumento = objParams.CodDocumento;

                DocEObj.LstDocAdjunto = this.DocumentoBD.GetDocumentosAdjuntos(DocAdjEObj);
                DocEObj.LstDocAnexo = this.DocumentoBD.GetDocumentoAnexos(DocAnexoEObj);
                DocEObj.LstDocDerivacion = this.DocumentoDeriva.GetDocumentoDerivado(DocDerivacionObj);
                var LstDocCC = DocEObj.LstDocDerivacion.FindAll(x => x.TipoAcceso == Convert.ToInt16(EnumTipoAcceso.ConCopia));
                var LstDocDerivados = DocEObj.LstDocDerivacion.FindAll(x => x.TipoAcceso == Convert.ToInt16(EnumTipoAcceso.Derivado));
                DocEObj.LstDocCC = LstDocCC;
                DocEObj.LstDocDerivados = LstDocDerivados;
                //DocEObj.LstDocAnexo = new DocDerivacion().this.DocumentoBD.GetDocumentoAnexos(DocAnexoEObj);
                if (DocEObj.LstDocAdjunto.Count > 0 || DocEObj.LstDocAnexo.Count > 0)
                {
                    try
                    {
                        List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                        foreach (var item in DocEObj.LstDocAdjunto)
                        {
                            ListaDocLaser.Add(new DocumentoItemFileDTO()
                            {
                                Id = item.CodLaserfiche,
                                TipoArchivo = Convert.ToInt32(TipoArchivos.Adjuntos)
                            });
                        }

                        foreach (var item in DocEObj.LstDocAnexo)
                            ListaDocLaser.Add(new DocumentoItemFileDTO()
                            {
                                Id = item.CodLaserfiche,
                                TipoArchivo = item.TipoArchivo
                            });


                        EnvioConsultaArchivosDTO laserEObj = new EnvioConsultaArchivosDTO();
                        laserEObj.ListaDocumentos = ListaDocLaser;

                        LaserFiche laserObj = new LaserFiche();
                        DocEObj.ArchivosLaserFiche = laserObj.ConsultaDefinicionArchivos(laserEObj);
                    }
                    catch (Exception ex)
                    {
                        new LogWriter(ex.Message);
                        informacion = "Neg: Error al Obtener el Datos del Documento:";
                        //                        throw new Exception("Neg: Error al Obtener el Datos del Documento:");
                    }
                }
            }

            return DocEObj;

        }
      
        /// <summary>
        /// Método Público para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public Documento ObtenerDocumentoSIED(Documento objParams, out string informacion)
        {
            informacion = "";
            Documento DocEObj = this.DocumentoBD.GetDocumentoEnvioSIED(objParams);
            if (DocEObj != null)
            {
                DocAdjunto DocAdjEObj = new DocAdjunto();
                DocAdjEObj.CodDocumento = objParams.CodDocumento;

                DocAnexo DocAnexoEObj = new DocAnexo();
                DocAnexoEObj.CodDocumento = objParams.CodDocumento;

                DocDerivacion DocDerivacionObj = new DocDerivacion();
                DocDerivacionObj.CodDocumento = objParams.CodDocumento;

                DocEObj.LstDocAdjunto = this.DocumentoBD.GetDocumentosAdjuntos(DocAdjEObj);
                DocEObj.LstDocAnexo = this.DocumentoBD.GetDocumentoAnexos(DocAnexoEObj);

                DocEObj.LstDocDerivados = this.DocumentoDeriva.GetDocumentoDerivadoEmpresasSIED(DocDerivacionObj);
                DocEObj.LstDocDerivacion = this.DocumentoDeriva.GetDocumentoDerivado(DocDerivacionObj);

                if (DocEObj.LstDocAdjunto.Count > 0 || DocEObj.LstDocAnexo.Count > 0)
                {
                    try
                    {
                        List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                        foreach (var item in DocEObj.LstDocAdjunto)
                        {
                            ListaDocLaser.Add(new DocumentoItemFileDTO()
                            {
                                Id = item.CodLaserfiche,
                                TipoArchivo = Convert.ToInt32(TipoArchivos.Adjuntos)
                            });
                        }

                        foreach (var item in DocEObj.LstDocAnexo)
                            ListaDocLaser.Add(new DocumentoItemFileDTO()
                            {
                                Id = item.CodLaserfiche,
                                TipoArchivo = item.TipoArchivo
                            });


                        EnvioConsultaArchivosDTO laserEObj = new EnvioConsultaArchivosDTO();
                        laserEObj.ListaDocumentos = ListaDocLaser;

                        LaserFiche laserObj = new LaserFiche();
                        DocEObj.ArchivosLaserFiche = laserObj.ConsultaDefinicionArchivos(laserEObj);
                    }
                    catch (Exception ex)
                    {
                        new LogWriter(ex.Message);
                        informacion = "Neg: Error al Obtener el Datos del Documento:";
                        //                        throw new Exception("Neg: Error al Obtener el Datos del Documento:");
                    }
                }
            }

            return DocEObj;

        }

        /// <summary>
        /// Método Público para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public EnvioSIED ObtenerDocumentoSIED_SeviceEnvio(Documento objParams)
        {
            var result = this.DocumentoBD.ObtenerDocumentoSIED_SeviceEnvio(objParams);
            return result;
        }
        /// <summary>
        /// Método Público para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        public RespuestaConsultaArchivosDTO DescargarArchivoLaserfiche(int codLaserFiche)
        {
            LaserFiche laserObj = new LaserFiche();
            RespuestaConsultaArchivosDTO archivosLaserFiche = laserObj.ConsultaArchivosBytes(new EnvioConsultaArchivosDTO()
            {
                ListaDocumentos = new List<DocumentoItemFileDTO>() { new DocumentoItemFileDTO()
                {
                    Id = codLaserFiche,
                }
                }
            });

            return archivosLaserFiche;

        }

        /// <summary>
        /// Método Público para el Listado de Bandeja Mesa de Partes
        /// </summary>
        /// <returns>Retorna lista generica de la Bandeja Mesa de Partes</returns>  
        public List<DocumentoBandejaDTO> ListarBandejaMesaPartes(DocumentoBandejaParamsDTO objParams)
        {
            //var usuarioRestriccion = ConfigurationManager.AppSettings["WEBUSER_SIED"];
            var lista = this.DocumentoBD.ListarBandejaMesaPartes(objParams);

            return lista;
        }
        public List<TipoDocumentoDTO> ListarTiposDocumento(TipoDocumentoDTO objParams, out int totalRegistros)
        {
            return this.DocumentoBD.ListarTiposDocumentos(objParams, out totalRegistros);
        }

        public List<DocumentoBandejaDTO> ListarBandejaEntrada(DocumentoBandejaEntradaDTO objParams, out int totalRegistros)
        {
            return this.DocumentoBD.ListarBandejaEntrada(objParams, out totalRegistros);
        }

        public List<DocumentoBandejaDTO> ListarBandejaRecepcionSIEDFONAFE(DocumentoBandejaEntradaDTO objParams, out int totalRegistros)
        {
            return this.DocumentoBD.ListarBandejaRecepcionSIEDFONAFE(objParams, out totalRegistros);
        }

        public List<DocumentoBandejaDTO> ListarBandejaEnvioSied(DocumentoBandejaEntradaDTO objParams, out int totalRegistros)
        {
            return this.DocumentoBD.ListarBandejaEnvioSied(objParams, out totalRegistros);
        }

        public List<DocumentoSeguimientoDTO> GetSeguimientoDocumento(int iCodDocumento)
        {
            return this.DocumentoBD.GetSeguimientoDocumento(iCodDocumento);
        }

        public List<DocumentoSeguimientoDTO> GetCabeceraSeguimiento(int iCodDocumento)
        {

            return this.DocumentoBD.GetCabeceraSeguimiento(iCodDocumento);
        }

        public List<DocumentoSeguimientoDTO> GetCabeceraSeguimientoInterno(int iCodDocumento)
        {
            var lista = this.DocumentoBD.GetCabeceraSeguimientoInterno(iCodDocumento);
            AreaDTO objAreaDto = new AreaDTO();
            foreach (var x in lista)
            {
                var descripcionArea = "";
                string[] arrCodigosInternos = x.AreaPara.Split('|');
                for (var y = 0; y < arrCodigosInternos.Length; y++)
                {
                    objAreaDto = new AreaDTO();
                    objAreaDto.CodArea = Convert.ToInt32(arrCodigosInternos[y]);
                    var objArea = this.AreaBD.ObtenerAreasPorId(objAreaDto);
                    descripcionArea = ((descripcionArea.Length==0)?"":(descripcionArea +" ,")) + objArea.Descripcion;
                }
                x.AreaPara = descripcionArea;

             }

            foreach(var orden  in lista)
            {
                if(orden.EstadoDocumento == 5)
                {
                    var item = orden;
                    lista.Remove(item); //Removes the specific item
                    lista.Add(item);
                    break;
                }
            }


            return lista;
        }

        public List<DocumentoSeguimientoDTO> GetDetalleSeguimiento(int iCodDocumento)
        {
            return this.DocumentoBD.GetDetalleSeguimiento(iCodDocumento);
        }

        public List<ComentarioDocumentoDTO> GetComentariosDocumento(int iCodDocumento)
        {
            return this.DocumentoBD.GetComentariosDocumento(iCodDocumento);
        }
        /// <summary>
        /// Método Público para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        public int RegistrarDocumento(Documento objMovDocumento, out string outCorrelativoDoc,out int RespuestaCorreo)
        {
            RespuestaCorreo = Constantes.RESPUESTA_ERROR;
            int rpta = this.DocumentoBD.RegistrarDocumento(objMovDocumento, out outCorrelativoDoc);
            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (rpta > 0)   
            {
                if (objMovDocumento.EstadoDocumento == Convert.ToInt16(EstadoDocumento.PENDIENTE))
                {
                    var lstConCopia = new List<String>();
                    var lstPara = this.DocumentoBD.ListarEmailPorAreaJefe(objMovDocumento.CodArea.ToString());
                    if (objMovDocumento.CodAreaCC != null)
                    {
                        lstConCopia = this.DocumentoBD.ListarEmailPorAreaJefe(objMovDocumento.CodAreaCC);
                    }

                    string cuerpocorreo = CuerpoCorreoRegistrarDocumento(objMovDocumento, outCorrelativoDoc);

                    var correo = objEnvioCorreo.EnviarCorreo(lstPara, lstConCopia, cuerpocorreo);
                    RespuestaCorreo = correo;
                }
            }

            return rpta;

        }

        /// <summary>
        /// Método Público para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        public int RegistrarDocumentoRecepcionSIED(Documento objMovDocumento, out string outCorrelativoDoc, out int RespuestaCorreo)
        {
            RespuestaCorreo = Constantes.RESPUESTA_ERROR;
            objMovDocumento.siFlagSIED = Convert.ToInt16(DestinoSied.RECEPCION);
            int rpta = this.DocumentoBD.RegistrarDocumentoRecepcionSIED(objMovDocumento, out outCorrelativoDoc);
            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (rpta > 0)
            {
                if (objMovDocumento.EstadoDocumento == Convert.ToInt16(EstadoDocumento.PENDIENTE))
                {
                    var lstConCopia = new List<string>();
                    var lstPara = this.DocumentoBD.ListarEmailPorAreaJefe(objMovDocumento.CodArea.ToString());
                    if (objMovDocumento.CodAreaCC != null)
                    {
                        lstConCopia = this.DocumentoBD.ListarEmailPorAreaJefe(objMovDocumento.CodAreaCC);
                    }

                    string cuerpocorreo = CuerpoCorreoRegistrarDocumento(objMovDocumento, outCorrelativoDoc);
                    try
                    {
                        var correo = objEnvioCorreo.EnviarCorreo(lstPara, lstConCopia, cuerpocorreo);
                        RespuestaCorreo = correo;
                    }
                    catch (Exception e)
                    {
                        RespuestaCorreo = Constantes.RESPUESTA_ERROR;
                    }
                    
                }
            }

            return rpta;

        }
        /// <summary>
        /// Método Público para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        public int RegistrarDocumentoBandejaEntrada(Documento objMovDocumento, out string outCorrelativoDoc)
        {
            int rpta = this.DocumentoBD.RegistrarDocumentoBandejaEntrada(objMovDocumento, out outCorrelativoDoc);

            return rpta;
        }
        
        /// <summary>
        /// Método Público para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>  
        public int RegistrarDocumentoBandejaEnvioSIED(Documento objMovDocumento, out string outCorrelativoDoc)
        {
            int rpta = this.DocumentoBD.RegistrarDocumentoBandejaEnvioSIED(objMovDocumento, out outCorrelativoDoc);

            return rpta;
        }
        /// <summary>
        /// Método Público para Finalizar Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Finalizar Documento</returns> 
        public int FinalizarDocumento(ComentarioDocumentoDTO objDocumento, out string mensaje)
        {
            return this.DocumentoBD.FinalizarDocumento(objDocumento, out mensaje);
        }

        public int FinalizarDocumentoBandejaEntrada(ComentarioDocumentoDTO objDocumento, out string mensaje,out int RespuestaCorreo)
        {
            int rpta = this.DocumentoBD.FinalizarDocumentoBandejaEntrada(objDocumento, out mensaje);
            RespuestaCorreo = Constantes.RESPUESTA_ERROR;
            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (rpta > 0)
            {
                string cuerpocorreo;
               var lstPara = this.DocumentoBD.ListarEmailPorTrabajador(objDocumento.CodResponsable.ToString());
               var lstConCopia = new List<String>();
                if(objDocumento.OrigenFinalizar == Convert.ToInt16(TipoOrigen.EXTERNO))
                {
                    cuerpocorreo = CuerpoCorreoFinalizarDocumentoExterno(objDocumento);
                }
                else
                {
                    cuerpocorreo = CuerpoCorreoFinalizarDocumentoInterno(objDocumento);
                }

               var correo = objEnvioCorreo.EnviarCorreo(lstPara, lstConCopia, cuerpocorreo);
                RespuestaCorreo = correo;
            }
            return rpta;
        }
        /// <summary>
        /// Método Público para validar antes Finalizar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Finalizar Documento</returns> 
        public int ValidarFinalizarDocumento(ComentarioDocumentoDTO objDocumento, out string mensaje, out string codDerivados)
        {
            return this.DocumentoBD.ValidarFinalizarDocumento(objDocumento, out mensaje, out codDerivados);
        }
        /// <summary>
        /// Método Público para validar antes Finalizar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Finalizar Documento</returns> 
        public int ValidarDevolverDocumento(ComentarioDocumentoDTO objDocumento, out string mensaje, out string codDerivados)
        {
            return this.DocumentoBD.ValidarDevolverDocumento(objDocumento, out mensaje, out codDerivados);
        }
        /// <summary>
        /// Método Público para Subir Documento digital
        /// </summary>
        /// <returns>Retorna el Indicador</returns> 
        public int SubirDocumentoLaserFicher(Documento objDocumento)
        {
            var rpta = this.DocumentoBD.SubirDocumentoLaserFicher(objDocumento,out Documento objDocCorreo);
            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (rpta > 0)
            {
                if ((objDocCorreo.Origen != Convert.ToInt16(TipoOrigen.INTERNOSIED)) || (objDocCorreo.Origen != Convert.ToInt16(TipoOrigen.EXTERNOSIED)))
                {
                    if ((objDocCorreo.EstadoDocumento == Convert.ToInt16(EstadoDocumento.PENDIENTE))
                    && (String.IsNullOrEmpty(objDocumento.CodDocDerivacion.ToString()) == true)
                    && (objDocCorreo.Origen == Convert.ToInt16(TipoOrigen.INTERNO)))
                    {
                        var CodAreaBELst = this.DocumentoBD.GetDocumentosDerivadosPorCodDocumento(objDocumento.CodDocumento, Convert.ToInt16(EnumTipoAcceso.Derivado));
                        var CodAreaCCLst = this.DocumentoBD.GetDocumentosDerivadosPorCodDocumento(objDocumento.CodDocumento, Convert.ToInt16(EnumTipoAcceso.ConCopia));
                        var CodAreaBE = ""; var CodAreaCC = "";
                        foreach (var i in CodAreaBELst)
                        {
                            CodAreaBE = CodAreaBE + i.CodArea + "|";
                        }
                        foreach (var i in CodAreaCCLst)
                        {
                            CodAreaCC = CodAreaCC + i.CodArea + "|";
                        }

                        var lstConCopia = new List<String>();
                        var lstPara = this.DocumentoBD.ListarEmailPorAreaJefe(CodAreaBE);
                        if (CodAreaCC != null)
                        {
                            lstConCopia = this.DocumentoBD.ListarEmailPorAreaJefe(CodAreaCC);
                        }
                        string cuerpocorreo = CuerpoCorreoRegistrarDocumentoInterno(objDocCorreo, objDocCorreo.Correlativo);

                        var correo = objEnvioCorreo.EnviarCorreo(lstPara, lstConCopia, cuerpocorreo);
                    }
                }
            }
            return rpta;
        }

        /// <summary>
        /// Método Público para Anular Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Anular Documento</returns> 
        public int AnularDocumento(ComentarioDocumentoDTO objDocumento)
        {
            return this.DocumentoBD.AnularDocumento(objDocumento);
        }

        public int AnularDocumentoYDerivados(ComentarioDocumentoDTO objDocumento)
        {
            int RespuestaCorreo = Constantes.RESPUESTA_ERROR;
            List<DocDerivacion> lstDocDerivacion = new List<DocDerivacion>();
            List<string> lstPara = new List<string>();

            lstDocDerivacion = this.DocumentoBD.GetDocumentosDerivadosPorCodDocumento(objDocumento.CodDocumento, Convert.ToInt32(EnumTipoAcceso.Derivado)).FindAll(x => x.EstadoDerivacion != Convert.ToInt16(EstadoDocumento.ANULADO));
            lstPara.Add(this.DocumentoBD.ListarEmailResponsablePorCodigoDocumento(objDocumento.CodDocumento.ToString()).FirstOrDefault());

            foreach (var i in lstDocDerivacion)
            {
                var anularDerivacion = this.DocumentoDeriva.AnularDocumentoDerivacionSinComentario(i, Constantes.MENSAJE_COMENTARIO_DERIVADO_ANULADO);
                lstPara.Add(this.DocumentoBD.ListarEmailPorTrabajador(i.CodResponsable.ToString()).FirstOrDefault());
            }
            int anularDocumento = this.DocumentoBD.AnularDocumentoEnvioSIED(objDocumento);

            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (anularDocumento > 0)
            {
                var lstConCopia = new List<String>();
                string cuerpocorreo = CuerpoCorreoAnularDocumentoEnvioSIED(objDocumento);

                var correo = objEnvioCorreo.EnviarCorreo(lstPara, lstConCopia, cuerpocorreo);
                RespuestaCorreo = correo;
            }
            return anularDocumento;
        }

        public int AnularDocumentoEnvioSIED(ComentarioDocumentoDTO objDocumento)
        {
            int RespuestaCorreo = Constantes.RESPUESTA_ERROR;
            List<DocDerivacion> lstDocDerivacion = new List<DocDerivacion>();
            List<string> lstPara = new List<string>();

            lstDocDerivacion = this.DocumentoBD.GetDocumentosDerivadosPorCodDocumento(objDocumento.CodDocumento, Convert.ToInt32(EnumTipoAcceso.Derivado)).FindAll( x => x.EstadoDerivacion != Convert.ToInt16(EstadoDocumento.ANULADO));
            lstPara.Add(this.DocumentoBD.ListarEmailResponsablePorCodigoDocumento(objDocumento.CodDocumento.ToString()).FirstOrDefault());

            foreach (var i in lstDocDerivacion)
            {
                var anularDerivacion = this.DocumentoDeriva.AnularDocumentoDerivacion(i,Constantes.MENSAJE_COMENTARIO_DERIVADO_ANULADO);
                lstPara.Add(this.DocumentoBD.ListarEmailPorTrabajador(i.CodResponsable.ToString()).FirstOrDefault());
            }
            int anularDocumento = this.DocumentoBD.AnularDocumentoEnvioSIED(objDocumento);

            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (anularDocumento > 0)
            {
                    var lstConCopia = new List<String>();                  
                    string cuerpocorreo = CuerpoCorreoAnularDocumentoEnvioSIED(objDocumento);

                    var correo = objEnvioCorreo.EnviarCorreo(lstPara, lstConCopia, cuerpocorreo);
                    RespuestaCorreo = correo;
            }
            return anularDocumento;
        }
        public int AnularDocumentoRecepcionSIED(ComentarioDocumentoDTO objDocumento)
        {
            int RespuestaCorreo = Constantes.RESPUESTA_ERROR;
            List<DocDerivacion> lstDocDerivacion = new List<DocDerivacion>();
            List<string> lstPara = new List<string>();

            lstDocDerivacion = this.DocumentoBD.GetDocumentosDerivadosPorCodDocumento(objDocumento.CodDocumento, Convert.ToInt32(EnumTipoAcceso.Derivado));
            lstPara.Add(this.DocumentoBD.ListarEmailResponsablePorCodigoDocumento(objDocumento.CodDocumento.ToString()).FirstOrDefault());

            foreach (var i in lstDocDerivacion)
            {
                var anularDerivacion = this.DocumentoDeriva.AnularDocumentoDerivacion(i, Constantes.MENSAJE_COMENTARIO_DERIVADO_ANULADO);
                lstPara.Add(this.DocumentoBD.ListarEmailPorTrabajador(i.CodResponsable.ToString()).FirstOrDefault());
            }
            int anularDocumento = this.DocumentoBD.AnularDocumentoRecepcionSIED(objDocumento);

            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (anularDocumento > 0)
            {
                var lstConCopia = new List<String>();
                string cuerpocorreo = CuerpoCorreoAnularDocumentoRecepcionSIED(objDocumento);

                var correo = objEnvioCorreo.EnviarCorreo(lstPara, lstConCopia, cuerpocorreo);
                RespuestaCorreo = correo;
            }
            return anularDocumento;
        }
        
        /// <summary>
        /// Método Público para La actualizacion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Actualizacion del Documento</returns>  
        public int ActualizarDocumento(Documento objMovDocumento)
        {

            int rpta = this.DocumentoBD.ActualizarDocumento(objMovDocumento);

            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (rpta > 0)
            {
                if (objMovDocumento.EstadoDocumento == Convert.ToInt16(EstadoDocumento.PENDIENTE))
                {
                    var lstConCopia = new List<String>();
                    var lstPara = this.DocumentoBD.ListarEmailPorAreaJefe(objMovDocumento.CodArea.ToString());
                    if (objMovDocumento.CodAreaCC != null)
                    {
                        lstConCopia = this.DocumentoBD.ListarEmailPorAreaJefe(objMovDocumento.CodAreaCC);
                    }

                    string cuerpocorreo = CuerpoCorreoRegistrarDocumento(objMovDocumento, objMovDocumento.Correlativo);

                    var correo = objEnvioCorreo.EnviarCorreo(lstPara, lstConCopia, cuerpocorreo);
                }
            }

            return rpta;

        }
        public int ActualizarDocumentoInterno(Documento objMovDocumento)
        {
            int rpta = this.DocumentoBD.ActualizarDocumentoInterno(objMovDocumento);
          /* EL PROCESO DE ENVIO DE CORREO SE HACE AL FIRMAR EL DOCUMENTO
           * EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (rpta > 0)
            {
                if (objMovDocumento.EstadoDocumento == Convert.ToInt16(EstadoDocumento.PENDIENTE))
                {
                    var lstConCopia = new List<String>();
                    var lstPara = this.DocumentoBD.ListarEmailPorAreaJefe(objMovDocumento.CodAreaBE.ToString());
                    if (objMovDocumento.CodAreaCC != null)
                    {
                        lstConCopia = this.DocumentoBD.ListarEmailPorAreaJefe(objMovDocumento.CodAreaCC);
                    }

                    string cuerpocorreo = CuerpoCorreoRegistrarDocumentoInterno(objMovDocumento, objMovDocumento.Correlativo);

                    var correo = objEnvioCorreo.EnviarCorreo(lstPara, lstConCopia, cuerpocorreo);
                }
            }*/

            return rpta;
        }

        
        public int ActualizarDocumentoInternoEnvioSIED(Documento objMovDocumento)
        {
            int rpta = this.DocumentoBD.ActualizarDocumentoInternoEnvioSIED(objMovDocumento);
            return rpta;
        }
        /// <summary>
        /// Interface para Calcular la fecha de plazo del documento
        /// </summary>
        /// <returns>Retorna el Indicador de Fecha de Plazo del Documento</returns>  
        public DateTime CalcularFechaPlazoDocumento(int cantidadPlazo, DateTime fechaInicio)
        {
            return this.DocumentoBD.CalcularFechaPlazoDocumento(cantidadPlazo, fechaInicio);
        }
        public int DevolverDocumentoInterno(ComentarioDocumentoDTO objDocumento)
        {
            return this.DocumentoBD.DevolverDocumentoInterno(objDocumento);
        }

        public int DevolverDocumento(ComentarioDocumentoDTO objDocumento)
        {
            return this.DocumentoBD.DevolverDocumento(objDocumento);
        }
        /// <summary>
        /// Interface para Derivar Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Derivacion Documento</returns> 
        public int DerivarDocumento(DerivarDocumentoDTO objDocumento, out int resultadoCorreo)
        {
            resultadoCorreo = Constantes.RESPUESTA_ERROR;
            DerivarDocumentoDTO objAntes = new DerivarDocumentoDTO();
            DerivarDocumentoDTO objDespues = new DerivarDocumentoDTO();    
                
            objAntes = this.DocumentoBD.GetEstadoDocumentoMovimiento(Convert.ToInt32(objDocumento.CodDocumento), 1);
                        
            int rpta = this.DocumentoBD.RegistrarDerivarDocumento(objDocumento);
            EnvioCorreo objEnvioCorreo = new EnvioCorreo();
            if (rpta > 0)
            {
                objDespues = this.DocumentoBD.GetEstadoDocumentoMovimiento(objDocumento.CodDocumento, 1);
                for (int x = 0; x < objDocumento.DerivarDocumento.Count; x++)
                {
                    string codTrabajador = objDocumento.DerivarDocumento[x].CodTrabajadores[0].ToString();
                    var lstPara = this.DocumentoBD.ListarEmailPorTrabajador(codTrabajador);
                    var cuerpocorreo = CuerpoCorreoDerivarDocumento(objDocumento);
                    var correo = objEnvioCorreo.EnviarCorreo(lstPara, null, cuerpocorreo);
                    resultadoCorreo = correo;
                }

                if (objAntes.iEstadoDocumento == (int)Enumerados.EstadoDocumento.PENDIENTE && objDespues.iEstadoDocumento == (int)Enumerados.EstadoDocumento.EN_PROCESO)
                {
                    if(objAntes.siOrigen == Convert.ToInt16(TipoOrigen.EXTERNO))
                    {
                        string codTrabajador1 = "";
                        string codTrabajador2 = "";
                        codTrabajador1 = Convert.ToString(objAntes.CodTrabajadores);
                        codTrabajador2 = Convert.ToString(objAntes.iCodTrabajadorInter);
                        
                       
                            var lstParaRes1 = this.DocumentoBD.ListarEmailPorTrabajador(codTrabajador1);
                            var lstParaRes2 = this.DocumentoBD.ListarEmailPorTrabajador(codTrabajador2);
                            var cuerpocorreoRes = CuerpoCorreoEnProcesoDocumento(objDocumento);
                            var correoRes1 = objEnvioCorreo.EnviarCorreo(lstParaRes1, null, cuerpocorreoRes);
                            var correoRes2 = objEnvioCorreo.EnviarCorreo(lstParaRes2, null, cuerpocorreoRes);

                    }
                    else if(objAntes.siOrigen == Convert.ToInt16(TipoOrigen.INTERNO))
                    {
                        var lstParaRes = this.DocumentoBD.ListarEmailPorTrabajador(objAntes.siOrigen == Convert.ToInt16(TipoOrigen.EXTERNO) ? Convert.ToString(objAntes.CodTrabajadores) : Convert.ToString(objAntes.iCodTrabajadorInter));
                        var cuerpocorreoRes = CuerpoCorreoEnProcesoDocumento(objDocumento);
                        var correoRes = objEnvioCorreo.EnviarCorreo(lstParaRes, null, cuerpocorreoRes);
                    }
                  
                }
            }
            return rpta;

        }

        #endregion



        /// <summary>
        /// Interface para Registrar Avance
        /// </summary>
        /// <returns>Retorna el Indicador de Derivacion Documento</returns> 
        public int RegistrarAvance(RegistrarAvanceDTO objDocumento, out int resultadoCorreo)
        { int valor = 0;
            DerivarDocumentoDTO objListaIni = new DerivarDocumentoDTO();
            resultadoCorreo = Constantes.RESPUESTA_ERROR;
            objListaIni = this.DocumentoBD.GetEstadoDocumentoMovimiento(Convert.ToInt32(objDocumento.CodDocumento), 1);         
            EnvioCorreo objEnvioCorreo = new EnvioCorreo();           

            DerivarDocumentoDTO objDoc = new DerivarDocumentoDTO();
            objDoc.CodDocumento = objDocumento.CodDocumento;
            objDoc.Comentario = objDocumento.Comentario;            
            objDoc.Correlativo = objListaIni.Correlativo;

            valor = this.DocumentoBD.RegistrarAvance(objDocumento);
            if (valor > 0)
            {
                DerivarDocumentoDTO objListaFin = new DerivarDocumentoDTO();
                objListaFin = this.DocumentoBD.GetEstadoDocumentoMovimiento(Convert.ToInt32(objDocumento.CodDocumento), 1);
                if (objListaIni.siOrigen == Convert.ToInt16(TipoOrigen.EXTERNO) || objListaIni.siOrigen == Convert.ToInt16(TipoOrigen.INTERNO))
                {                    
                    if (objListaIni.iEstadoDocumento == (int)Enumerados.EstadoDocumento.PENDIENTE && objListaFin.iEstadoDocumento == (int)Enumerados.EstadoDocumento.EN_PROCESO)
                    {
                        var lstParaRes = this.DocumentoBD.ListarEmailPorTrabajador(objListaIni.siOrigen == Convert.ToInt16(TipoOrigen.EXTERNO) ? Convert.ToString(objListaIni.CodTrabajadores) : Convert.ToString(objListaIni.iCodTrabajadorInter));
                        var cuerpocorreoRes = CuerpoCorreoEnProcesoDocumento(objDoc);
                        var correoRes = objEnvioCorreo.EnviarCorreo(lstParaRes, null, cuerpocorreoRes);
                        resultadoCorreo = correoRes;
                    }
                    else
                    {
                        resultadoCorreo = 1;
                    }
                }
            }           
           
            return valor;


        }

        public List<DocDerivacion> ObtenerDocumentoDerivado(DocDerivacion objParams)
        {
            return this.DocumentoDeriva.GetDocumentoDerivado(objParams);
        }

        public DocDerivacion ObtenerDocumentoDerivadoxTrabajador(DocDerivacion objParams)
        {
            return this.DocumentoBD.GetDocumentoDerivadoxTrabajador(objParams);
        }
        
        public List<DocDerivacion> GetDocumentosDerivadosPorCodDocumento(int codigoDocumento, int tipoAcceso)
        {
            return this.DocumentoBD.GetDocumentosDerivadosPorCodDocumento( codigoDocumento,  tipoAcceso);
        }

        public int FinalizarDocumentoEnvioSIED(Documento objDocumento)
        {
            return this.DocumentoBD.FinalizarDocumentoEnvioSIED(objDocumento);
        }

        public int RegistrarTipoDocumento(TipoDocumentoDTO objDocumento)
        {
            return this.DocumentoBD.RegistrarTipoDocumento(objDocumento);
        }
        public DocDerivacion GetDocumentoDerivadoPorId(int codigo)
        {
            return this.DocumentoBD.GetDocumentoDerivadoPorId(codigo);
        }
        public int ActualizarTipoDocumento(TipoDocumentoDTO objDocumento)
        {
            return this.DocumentoBD.ActualizarTipoDocumento(objDocumento);
        }

        public TipoDocumentoDTO ObtenerTipoDocumento(TipoDocumentoDTO objParams)
        {
            return this.DocumentoBD.ObtenerTipoDocumento(objParams);
        }
        
        public TipoDocumentoDTO ObtenerTipoDocumentoPorDescripcion(TipoDocumentoDTO objParams)
        {
            return this.DocumentoBD.ObtenerTipoDocumentoPorDescripcion(objParams);
        }
        public int EliminarTipoDocumento(TipoDocumentoDTO objParams)
        {
            return this.DocumentoBD.EliminarTipoDocumento(objParams);
        }

        public string CuerpoCorreoRegistrarDocumento(Documento objMovDocumento, string outCorrelativoDoc)
        {
            string body = string.Empty;
            var ruta = "";

            if (rutaProdHabilitado!="1")
            {
                string root = System.Web.Hosting.HostingEnvironment.MapPath("~");
                string parent = Path.GetDirectoryName(root);
                string grandParent = Path.GetDirectoryName(parent);
                ruta = grandParent + "\\Agrobanco.SIGD.Presentacion\\Plantillas\\DocumentoExterno.html";
            }
            else
            {
                ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + "DocumentoExterno.html";
            }
            var _stPrioridad = "";

            if (objMovDocumento.siFlagSIED == 2)
            {
                var _stPri_Media = "<label style='color:#41af4f'>Normal</label>";
                var _stPri_Alta = "<label style='color:#f5a623'>Urgente</label>";
                var _stPri_Urgente = "<label style='color:#ff005a'>Muy Urgente</label>";

                switch (objMovDocumento.Prioridad)
                {
                    case 1: //Normal
                        _stPrioridad = _stPri_Media;
                        break;
                    case 2://Urgente
                        _stPrioridad = _stPri_Alta;
                        break;
                    case 3://Muy Urgente
                        _stPrioridad = _stPri_Urgente;
                        break;
                }
            }
            else
            {
                var _stPri_Baja = "<label style='color:#248ef3'>Baja</label>";
                var _stPri_Media = "<label style='color:#41af4f'>Media</label>";
                var _stPri_Alta = "<label style='color:#f5a623'>Alta</label>";
                var _stPri_Urgente = "<label style='color:#ff005a'>Urgente</label>";


                switch (objMovDocumento.Prioridad)
                {
                    case 1: //baja
                        _stPrioridad = _stPri_Baja;
                        break;
                    case 2://media
                        _stPrioridad = _stPri_Media;
                        break;
                    case 3://alta
                        _stPrioridad = _stPri_Alta;
                        break;
                    case 4://urgente
                        _stPrioridad = _stPri_Urgente;
                        break;
                }
            }
          

            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }

                IParametroBL objServicioParam = new ParametroBL();

                var tipoEntidad = objServicioParam.ListarParametrosPorGrupo(Convert.ToInt32(GrupoParametros.TIPOENTIDADSIED));
                var tipoEntidadDes = "";
                //es recepcion SIED
                if (objMovDocumento.siFlagSIED == 2)
                {
                    tipoEntidadDes = tipoEntidad.Where(x => x.Valor == objMovDocumento.TipoEntidadSIED).ToList().FirstOrDefault().Campo;
                }

                body = body.Replace("{FechaPlazo}", objMovDocumento.FechaPlazo.ToShortDateString());
                body = body.Replace("{NumDocumento}", objMovDocumento.NumDocumento);
                body = body.Replace("{Origen}", "Externo");
                body = body.Replace("{Prioridad}", _stPrioridad);
                body = body.Replace("{Plazo}", objMovDocumento.Plazo.ToString());
                body = body.Replace("{outCorrelativoDoc}", outCorrelativoDoc);
                body = body.Replace("{RemitidoDescripcionPor}", (objMovDocumento.siFlagSIED == 2)? tipoEntidadDes + " - "  + objMovDocumento.EntidadSIED : objMovDocumento.RemitidoDescripcionPor + " - " +objMovDocumento.DescripcionRemitente);
            }
            catch (Exception e)
            {
                throw e;
            }
            return body;
        }

        public string CuerpoCorreoRegistrarDocumentoInterno(Documento objMovDocumento, string outCorrelativoDoc)
        {
            string body = string.Empty;
            var ruta = "";

            if (rutaProdHabilitado!="1")
            {
                string root = System.Web.Hosting.HostingEnvironment.MapPath("~");
                string parent = Path.GetDirectoryName(root);
                string grandParent = Path.GetDirectoryName(parent);
                ruta = grandParent + "\\Agrobanco.SIGD.Presentacion\\Plantillas\\DocumentoInterno.html";
            }
            else
            {
                ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + "DocumentoInterno.html";
            }

            var _stPri_Baja = "<label style='color:#248ef3'>Baja</label>";
            var _stPri_Media = "<label style='color:#41af4f'>Media</label>";
            var _stPri_Alta = "<label style='color:#f5a623'>Alta</label>";
            var _stPri_Urgente = "<label style='color:#ff005a'>Urgente</label>";
            var _stPrioridad = _stPri_Baja;

            switch (objMovDocumento.Prioridad)
            {
                case 1: //baja
                    _stPrioridad = _stPri_Baja;
                    break;
                case 2://media
                    _stPrioridad = _stPri_Media;
                    break;
                case 3://alta
                    _stPrioridad = _stPri_Alta;
                    break;
                case 4://urgente
                    _stPrioridad = _stPri_Urgente;
                    break;
            }

            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{FechaPlazo}", objMovDocumento.FechaPlazo.ToShortDateString());
                body = body.Replace("{Plazo}", objMovDocumento.Plazo.ToString());
                body = body.Replace("{Origen}", "Interno");
                body = body.Replace("{Prioridad}", _stPrioridad);
                body = body.Replace("{outCorrelativoDoc}", outCorrelativoDoc);
                body = body.Replace("{RemitidoDescripcionPor}", objMovDocumento.Area);
            }
            catch (Exception e)
            {

            }
            return body;
        }

        public string CuerpoCorreoDerivarDocumento(DerivarDocumentoDTO objMovDocumento)
        {
            string body = string.Empty;
            var ruta = "";
            var documentoRecepcionSIED = new Documento();
            var esRecepcionSIED = this.DocumentoBD.GetFlagSIED(objMovDocumento.CodDocumento);

            if (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION))
            {
                Documento doc = new Documento();
                doc.CodDocumento = objMovDocumento.CodDocumento;
                documentoRecepcionSIED = this.DocumentoBD.GetDocumentoRecepcionSIED(doc);
            }



            if (rutaProdHabilitado!="1")
            {
                string root = System.Web.Hosting.HostingEnvironment.MapPath("~");
                string parent = Path.GetDirectoryName(root);
                string grandParent = Path.GetDirectoryName(parent);
                if (objMovDocumento.flgSIED)
                {
                    ruta = grandParent + "\\Agrobanco.SIGD.Presentacion\\Plantillas\\DerivacionDocumentoSIED.html";
                }
                else
                {
                    ruta = grandParent + "\\Agrobanco.SIGD.Presentacion\\Plantillas\\DerivacionDocumento.html";
                }
                
            }
            else
            {                
                //ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + "DerivacionDocumento.html";
                if (objMovDocumento.flgSIED)
                {
                    ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + ConfigurationManager.AppSettings["nombrePlantillaDerivacionSIED"];
                }
                else
                {
                    ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + ConfigurationManager.AppSettings["nombrePlantillaDerivacion"];
                }
            }

            Documento obj = new Documento();
            obj = this.DocumentoBD.GetDetalleCorreoDerivacion(objMovDocumento.CodDocumento);

            var _stPri_Baja = "<label style='color:#248ef3'>Baja</label>";
            var _stPri_Media = "<label style='color:#41af4f'>Media</label>";
            var _stPri_Alta = "<label style='color:#f5a623'>Alta</label>";
            var _stPri_Urgente = "<label style='color:#ff005a'>Urgente</label>";
            var _stPrioridad = _stPri_Baja;

            //recepción documentos SIED
            var _stPri_NormalSIED = "<label style='color:#41af4f'>Normal</label>";
            var _stPri_UrgenteSIED = "<label style='color:#f5a623'>Urgente</label>";
            var _stPri_MuyUrgenteSIED = "<label style='color:#ff005a'>Muy Urgente</label>";
            var origen = "";
            switch (obj.Prioridad)
            {
                case 1: //baja
                    _stPrioridad = (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION)) ? _stPri_NormalSIED : _stPri_Baja;
                    break;
                case 2://media
                    _stPrioridad = (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION)) ? _stPri_UrgenteSIED : _stPri_Media;
                    break;
                case 3://alta
                    _stPrioridad = (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION)) ? _stPri_MuyUrgenteSIED : _stPri_Alta;
                    break;
                case 4://urgente
                    _stPrioridad = _stPri_Urgente;
                    break;
            }
            var areaEmpresa = "";
            if (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION))
            {
                areaEmpresa = "Remitente : " + documentoRecepcionSIED.TipoEntidadSIED + " - " + documentoRecepcionSIED.EntidadSIED;
            }
            else
            {
                if (obj.Origen == Convert.ToInt16(TipoOrigen.EXTERNO))
                {
                    areaEmpresa = "Empresa : " + obj.DescripcionEmpresa;
                }
                else
                {
                    areaEmpresa = "Área : " + obj.DescripcionEmpresa;
                }
            }
          

            if (obj.Origen == Convert.ToInt16(TipoOrigen.EXTERNO))
            {
                origen = "Externo";
            }else if (obj.Origen == Convert.ToInt16(TipoOrigen.INTERNO))
            {
                origen = "Interno";
            }
            else if (obj.Origen == Convert.ToInt16(TipoOrigen.EXTERNOSIED))
            {
                origen = "Externo SIED";
            }
            else if (obj.Origen == Convert.ToInt16(TipoOrigen.INTERNOSIED))
            {
                origen = "Interno SIED";
            }

            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Correlativo}", objMovDocumento.Correlativo);
                body = body.Replace("{Origen}", origen);
                body = body.Replace("{Comentario}", objMovDocumento.Comentario);
                body = body.Replace("{Prioridad}", _stPrioridad);
                body = body.Replace("{Plazo}", obj.Plazo.ToString());
                body = body.Replace("{FechaPlazo}", obj.FechaPlazo.ToShortDateString());
                body = body.Replace("{Representante}", (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION)) ? " - " : obj.Representante);
                body = body.Replace("{AREAEMPRESA}", areaEmpresa);
                body = body.Replace("{DERIVADOPOR}", obj.DerivadoPor);
                body = body.Replace("{Accion}", obj.TipoAccion);
            }
            catch (Exception e)
            {

            }
            return body;
        }

        public List<DocAnexo> GetDocumentoAnexos(DocAnexo EObj)
        {
            var lstanexo= this.DocumentoBD.GetDocumentoAnexos(EObj);

            if (lstanexo.Count > 0)
            {
                try
                {
                    List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                    foreach (var item in lstanexo)
                    {
                        ListaDocLaser.Add(new DocumentoItemFileDTO()
                        {
                            Id = item.CodLaserfiche,
                            TipoArchivo = Convert.ToInt32(TipoArchivos.Adjuntos)
                        });
                    }

                    EnvioConsultaArchivosDTO laserEObj = new EnvioConsultaArchivosDTO();

                    laserEObj.ListaDocumentos = ListaDocLaser;

                    LaserFiche laserObj = new LaserFiche();
                    var lstlaserobj = laserObj.ConsultaDefinicionArchivos(laserEObj); 
                    foreach(var x in lstlaserobj.ListaDocumentos)
                    {
                        lstanexo.Where(c => c.CodLaserfiche == x.IdDocumento)
                            .Select(c => { c.NombreDocumento = x.NombreArchivo; return c; }).ToList();
                    }

                }
                catch (Exception ex)
                {
                    new LogWriter(ex.Message);
                }
            }

            return lstanexo;

        }

        
        public List<DocAnexo> GetDocumentoAnexosNoLF(DocAnexo EObj)
        {
            var lstanexo = this.DocumentoBD.GetDocumentoAnexos(EObj);

            return lstanexo;

        }

        public string CuerpoCorreoFinalizarDocumentoExterno(ComentarioDocumentoDTO objMovDocumento)
        {
            string body = string.Empty;
            var ruta = "";
            var esRecepcionSIED = this.DocumentoBD.GetFlagSIED(objMovDocumento.CodDocumento);
            var documentoRecepcionSIED = new Documento();
            if (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION))
            {
                Documento doc = new Documento();
                doc.CodDocumento = objMovDocumento.CodDocumento;
                documentoRecepcionSIED = this.DocumentoBD.GetDocumentoRecepcionSIED(doc);
            }

            if (rutaProdHabilitado!="1")
            {
                string root = System.Web.Hosting.HostingEnvironment.MapPath("~");
                string parent = Path.GetDirectoryName(root);
                string grandParent = Path.GetDirectoryName(parent);
                ruta = grandParent + "\\Agrobanco.SIGD.Presentacion\\Plantillas\\FinalizarDocumentoExterno.html";
            }
            else
            {
                ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + "FinalizarDocumentoExterno.html";
            }
            Documento obj = new Documento();
            obj = this.DocumentoBD.GetDetalleCorreoFinalizar(objMovDocumento.CodDocumento);
            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Origen}", "Externo");
                body = body.Replace("{Correlativo}", obj.Correlativo);
                body = body.Replace("{NroDocumento}", obj.NumDocumento);
                body = body.Replace("{FechaPlazo}", obj.FechaPlazo.ToShortDateString());
                body = body.Replace("{Remitente}", (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION))? documentoRecepcionSIED.TipoEntidadSIED + " - " + documentoRecepcionSIED.EntidadSIED:(obj.Representante + " - " + obj.DescripcionEmpresa));
                body = body.Replace("{Comentario}", objMovDocumento.Comentario);
            }catch(Exception e)
            {

            }
            return body;
        }

        public string CuerpoCorreoFinalizarDocumentoInterno(ComentarioDocumentoDTO objMovDocumento)
        {  
            string body = string.Empty;
            var ruta = "";

            if (rutaProdHabilitado!="1")
            {
                string root = System.Web.Hosting.HostingEnvironment.MapPath("~");
                string parent = Path.GetDirectoryName(root);
                string grandParent = Path.GetDirectoryName(parent);
                ruta = grandParent + "\\Agrobanco.SIGD.Presentacion\\Plantillas\\FinalizarDocumentoInterno.html";
            }
            else
            {
                ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + "FinalizarDocumentoInterno.html";
            }

            Documento obj = new Documento();
            obj = this.DocumentoBD.GetDetalleCorreoFinalizar(objMovDocumento.CodDocumento);
            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Origen}", "Interno");
                body = body.Replace("{Correlativo}", obj.Correlativo);
                body = body.Replace("{FechaPlazo}", obj.FechaPlazo.ToShortDateString());
                body = body.Replace("{Remitente}", obj.Area);
                body = body.Replace("{Comentario}", objMovDocumento.Comentario);
            }
            catch (Exception e)
            {

            }
            return body;
        }

        public string CuerpoCorreoAnularDocumentoEnvioSIED(ComentarioDocumentoDTO objMovDocumento)
        {
            string body = string.Empty;
            var ruta = "";

            if (rutaProdHabilitado != "1")
            {
                string root = System.Web.Hosting.HostingEnvironment.MapPath("~");
                string parent = Path.GetDirectoryName(root);
                string grandParent = Path.GetDirectoryName(parent);
                ruta = grandParent + "\\Agrobanco.SIGD.Presentacion\\Plantillas\\AnularDocumentoEnvioSIED.html";
            }
            else
            {
                ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + "AnularDocumentoEnvioSIED.html";
            }

            Documento obj = new Documento();
            obj = this.DocumentoBD.GetDetalleCorreoAnularDocumentoEnvioSIED(objMovDocumento.CodDocumento);
            try
            {

                var _stPri_Baja = "<label style='color:#248ef3'>Baja</label>";
                var _stPri_Media = "<label style='color:#41af4f'>Media</label>";
                var _stPri_Alta = "<label style='color:#f5a623'>Alta</label>";
                var _stPri_Urgente = "<label style='color:#ff005a'>Urgente</label>";
                var _stPrioridad = _stPri_Baja;

                switch (obj.Prioridad)
                {
                    case 1: //baja
                        _stPrioridad = _stPri_Baja;
                        break;
                    case 2://media
                        _stPrioridad = _stPri_Media;
                        break;
                    case 3://alta
                        _stPrioridad = _stPri_Alta;
                        break;
                    case 4://urgente
                        _stPrioridad = _stPri_Urgente;
                        break;
                }

                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{tipoRegistro}", obj.Origen == Convert.ToInt16(TipoOrigen.INTERNO) ? "Interno" : "Externo");
                body = body.Replace("{nroTramite}", obj.Correlativo);
                body = body.Replace("{anuladopor}", obj.NombreTrab + ' ' + obj.ApePaternoTrab + ' ' + obj.ApeMaternoTrab );
                body = body.Replace("{prioridad}", _stPrioridad);
                body = body.Replace("{Comentario}", objMovDocumento.Comentario);
            }
            catch (Exception e)
            {

            }
            return body;
        }

        public string CuerpoCorreoAnularDocumentoRecepcionSIED(ComentarioDocumentoDTO objMovDocumento)
        {
            string body = string.Empty;
            var ruta = "";

            if (rutaProdHabilitado != "1")
            {
                string root = System.Web.Hosting.HostingEnvironment.MapPath("~");
                string parent = Path.GetDirectoryName(root);
                string grandParent = Path.GetDirectoryName(parent);
                ruta = grandParent + "\\Agrobanco.SIGD.Presentacion\\Plantillas\\AnularDocumentoRecepcionSIED.html";
            }
            else
            {
                ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + "AnularDocumentoRecepcionSIED.html";
            }

            Documento obj = new Documento();
            obj = this.DocumentoBD.GetDetalleCorreoAnularDocumentoEnvioSIED(objMovDocumento.CodDocumento);
            try
            {

                var _stPri_Baja = "<label style='color:#248ef3'>Baja</label>";
                var _stPri_Media = "<label style='color:#41af4f'>Normal</label>";
                var _stPri_Alta = "<label style='color:#f5a623'>Urgente</label>";
                var _stPri_Urgente = "<label style='color:#ff005a'>Muy Urgente</label>";
                var _stPrioridad = _stPri_Baja;

                switch (obj.Prioridad)
                {
                    case 1: //baja
                        _stPrioridad = _stPri_Media;
                        break;
                    case 2://media
                        _stPrioridad = _stPri_Alta;
                        break;
                    case 3://alta
                        _stPrioridad = _stPri_Urgente;
                        break;

                }

                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{tipoRegistro}", obj.Origen == Convert.ToInt16(TipoOrigen.EXTERNO) ? "Externo" : "Interno");
                body = body.Replace("{nroTramite}", obj.Correlativo);
                body = body.Replace("{anuladopor}", obj.NombreTrab + ' ' + obj.ApePaternoTrab + ' ' + obj.ApeMaternoTrab);
                body = body.Replace("{prioridad}", _stPrioridad);
                body = body.Replace("{Comentario}", objMovDocumento.Comentario);
            }
            catch (Exception e)
            {

            }
            return body;
        }

        public string CuerpoCorreoEnProcesoDocumento(DerivarDocumentoDTO objMovDocumento)
        {
            string body = string.Empty;
            var ruta = "";
            var documentoRecepcionSIED = new Documento();
            var esRecepcionSIED = this.DocumentoBD.GetFlagSIED(objMovDocumento.CodDocumento);
            var plantilla = "EnProcesoDocumento.html";

            if(esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION))
            {
                Documento doc = new Documento();
                doc.CodDocumento = objMovDocumento.CodDocumento;
                documentoRecepcionSIED = this.DocumentoBD.GetDocumentoRecepcionSIED(doc);
            }


            if (rutaProdHabilitado != "1")
            {
                string root = System.Web.Hosting.HostingEnvironment.MapPath("~");
                string parent = Path.GetDirectoryName(root);
                string grandParent = Path.GetDirectoryName(parent);
                ruta = grandParent + "\\Agrobanco.SIGD.Presentacion\\Plantillas\\"+ plantilla;
            }
            else
            {
                ruta = ConfigurationManager.AppSettings["rutaPlantillaCorreosProd"] + plantilla;
            }

            Documento obj = new Documento();
            obj = this.DocumentoBD.GetDetalleCorreoAvance(objMovDocumento.CodDocumento);
                       


            var _stPri_Baja = "<label style='color:#248ef3'>Baja</label>";
            var _stPri_Media = "<label style='color:#41af4f'>Media</label>";
            var _stPri_Alta = "<label style='color:#f5a623'>Alta</label>";
            var _stPri_Urgente = "<label style='color:#ff005a'>Urgente</label>";

            //recepción documentos SIED
            var _stPri_NormalSIED = "<label style='color:#41af4f'>Normal</label>";
            var _stPri_UrgenteSIED = "<label style='color:#f5a623'>Urgente</label>";
            var _stPri_MuyUrgenteSIED = "<label style='color:#ff005a'>Muy Urgente</label>";
            var _stPrioridad = _stPri_Baja;

            switch (obj.Prioridad)
            {
                case 1: //baja
                    _stPrioridad = (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION))? _stPri_NormalSIED : _stPri_Baja;
                    break;
                case 2://media
                    _stPrioridad = (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION)) ? _stPri_UrgenteSIED : _stPri_Media;
                    break;
                case 3://alta
                    _stPrioridad = (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION)) ? _stPri_MuyUrgenteSIED : _stPri_Alta;
                    break;
                case 4://urgente
                    _stPrioridad = _stPri_Urgente;
                    break;
            }
            var areaEmpresa = "";

            if(esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION))
            {
                areaEmpresa = "Remitente : " + documentoRecepcionSIED.TipoEntidadSIED + " - " + documentoRecepcionSIED.EntidadSIED;
            }
            else
            {
                if (obj.Origen == Convert.ToInt16(TipoOrigen.EXTERNO))
                {
                    areaEmpresa = "Empresa : " + obj.DescripcionEmpresa;
                }
                else
                {
                    areaEmpresa = "Área : " + obj.DescripcionEmpresa;
                }
            }

            


            try
            {
                using (StreamReader reader = new StreamReader(ruta))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Correlativo}", objMovDocumento.Correlativo);
                body = body.Replace("{Origen}", obj.Origen == Convert.ToInt16(TipoOrigen.EXTERNO) ? "Externo" : "Interno");
                body = body.Replace("{Comentario}", objMovDocumento.Comentario);
                body = body.Replace("{Prioridad}", _stPrioridad);
                body = body.Replace("{Plazo}", obj.Plazo.ToString());
                body = body.Replace("{FechaPlazo}", obj.FechaPlazo.ToShortDateString());
                body = body.Replace("{Representante}", (esRecepcionSIED == Convert.ToInt16(FlagSIED.RECEPCION))?" - ": obj.Representante);
                body = body.Replace("{AREAEMPRESA}", areaEmpresa);
                body = body.Replace("{DERIVADOPOR}", obj.DerivadoPor);
            }
            catch (Exception e)
            {

            }
            return body;
        }


        public int ValidarFirmante(int codDocumento, int codTrabajador)
        {
            return this.DocumentoBD.ValidarFirmante(codDocumento, codTrabajador);
        }


        public int DocumentoAtencionFirma(int CodDocumento, int Atencion)
        {
            return this.DocumentoBD.DocumentoAtencionFirma(CodDocumento, Atencion);
        }

        public int ObtenerEstadoFirmaDocumento(int CodDocumento)
        {
            return this.DocumentoBD.ObtenerEstadoFirmaDocumento(CodDocumento);
        }

        public int ObtenerCantidadFirmas(int CodDocumento, out int posx, out int posy)
        {
            return this.DocumentoBD.ObtenerCantidadFirmas(CodDocumento,out  posx, out posy);
        }

        public int ObtenerCodLaserFIchePorCodDocumento(int id)
        {
            return this.DocumentoBD.ObtenerCodLaserFIchePorCodDocumento(id);
        }


        public List<int> ObtenerCodLaserFIcheAnexosPorCodDocumento(int id)
        {
            return this.DocumentoBD.ObtenerCodLaserFIcheAnexosPorCodDocumento(id);
        }

        public int RegistrarCargo(RegistroCargoDTO registroCargo)
        {
            int valor = DocumentoBD.RegistrarCargo(registroCargo);

            return valor;
        }

        public int RegistrarCargoSIED(RegistroCargoDTO registroCargo)
        {
            int valor = DocumentoBD.RegistrarCargoSIED(registroCargo);

            return valor;
        }

        public int registrarcargoRecepcionSIED(RegistroCargoDTO registroCargo)
        {
            int valor = DocumentoBD.registrarcargoRecepcionSIED(registroCargo);

            return valor;
        }

        

        public int ObtenerTotalDerivaciones(int codDocumento, int tipoAcceso)
        {
            int valor = DocumentoBD.ObtenerTotalDerivaciones(codDocumento, tipoAcceso);

            return valor;
        }

        public int AnularDerivacion(DocDerivacion docDerivacion, string comentario)
        {
            int rpta = DocumentoDeriva.AnularDocumentoDerivacion(docDerivacion, comentario);

            return rpta;
        }

        //public int AnularDerivacion(DocDerivacion docDerivacion, string comentario)
        //{
        //    int codMovimiento = DocumentoDeriva.AnularDocumentoDerivacion(docDerivacion);

        //    if (codMovimiento > 0)
        //    {
        //        DocumentoBD.
        //    }

        //}
        public int ObtenerEstado(int codDocumento)
        {
            return this.DocumentoBD.GetEstadoDocumentoMovimiento(Convert.ToInt32(codDocumento), 1).iEstadoDocumento;
        }
    }
}
