﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
   public class AreaBL :IAreaBL
    {
        #region Constructor

        private IAreaDAO AreaBD;
        private string strCadenaConexion;

        public AreaBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.AreaBD = new AreaDAO(strCadenaConexion);
        }


        #endregion


        #region Métodos Públicos
        /// <summary>
        /// Método Público para el Listado de Áreas
        /// </summary>
        /// <returns>Retorna Lista genérica de Áreas</returns>  
        public List<AreaDTO> ListarArea()
        {
            return this.AreaBD.ListarArea();
        }
        
        public List<AreaDTO> ListarAreaSIED()
        {
            return this.AreaBD.ListarAreaSIED();
        }

        public List<AreaDTO> ListarAreasBandeja(AreaDTO objParams, out int totalRegistros)
        {
            return this.AreaBD.ListarAreasPaginado(objParams, out totalRegistros);
        }

        public AreaDTO ObtenerAreasPorId(AreaDTO objParams)
        {
            return this.AreaBD.ObtenerAreasPorId(objParams);
        }

        public int RegistrarArea(AreaDTO objArea)
        {
            return this.AreaBD.RegistrarArea(objArea);

        }

        public int ActualizarArea(AreaDTO objArea)
        {
            return this.AreaBD.ActualizarArea(objArea);
        }

        public int EliminarArea(AreaDTO objArea)
        {
            return this.AreaBD.EliminarArea(objArea);
        }

        #endregion
    }
}
