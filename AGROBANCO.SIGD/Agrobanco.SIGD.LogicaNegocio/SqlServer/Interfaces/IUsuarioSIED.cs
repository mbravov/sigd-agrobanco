﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
    public interface IUsuarioSIED
    {
        /// <summary>
        /// Interface para el Listado de UsuarioSIED
        /// </summary>
        /// <returns>Retorna Lista genérica de UsuarioSIED</returns>     
        UsuarioSIEDDTO ObtenerUsuarioSIED();
        /// <summary>
        /// Interface para grabar/actualizar el UsuarioSIED
        /// </summary>
        /// <returns>Retorna un obejeto UsuarioSIED</returns>     
        int GrabarUsuarioSIED(UsuarioSIEDDTO obj);
        /// <summary>
        /// Interface para grabar/actualizar el UsuarioSIED
        /// </summary>
        /// <returns>Retorna un obejeto UsuarioSIED</returns>     
        int EditarUsuarioSIED(UsuarioSIEDDTO obj);

    }
}
