﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
    public interface IFeriadoBL
    {
        /// <summary>
        /// Método Público para el buscar Feriado
        /// </summary>
        /// <returns>Retorna Lista genérica de Feriado</returns>  
        List<Feriado> BuscarFeriado(FiltroMantenimientoBaseDTO texto, out int totalRegistros);
        /// <summary>
        /// Método Público para el obtener un(a) Feriado
        /// </summary>
        /// <returns>Retorna Lista genérica de Feriado</returns>  
        Feriado ObtenerFeriado(Feriado objFeriado);
        /// <summary>
        /// Método Público para Registrar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Registrar(Feriado objFeriado);
        /// <summary>
        /// Método Público para Actualizar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Actualizar(Feriado objFeriado);
        /// <summary>
        /// Método Público para Eliminar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Eliminar(Feriado objFeriado);
    }
}
