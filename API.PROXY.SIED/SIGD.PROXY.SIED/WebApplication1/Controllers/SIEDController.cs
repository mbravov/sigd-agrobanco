﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SIGD.PROXY.SIED.Model;
using SIGD.PROXY.SIED.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIGD.PROXY.SIED.Controllers
{

    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api")]
    [ApiController]
    public class SIEDController : Controller
    {

        private readonly ILogger<SIEDController> _logger;

        public SIEDController(ILogger<SIEDController> logger)
        {
            _logger = logger;
        }

        [HttpGet("ListarFirmantes")]
        [ProducesResponseType(200, Type = typeof(RespuestaOperacionServicio))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object ListarFirmantes()
        {
            try
            {
                _logger.LogInformation("Endpoint {endpoint} {verb}", "/api/SIED/ListarFirmantes", "GET");

                ProxySIED proxi = new ProxySIED();
                var response = proxi.ListarFirmantes();

                _logger.LogInformation("Response Endpoint {endpoint} {verb} {response}", "/api/SIED/ListarFirmantes", "GET", JsonConvert.SerializeObject(response).ToString());
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception {endpoint} {verb} {msg}", "/api/SIED/ListarFirmantes", "GET", ex.Message);
                throw ex;
            }
        }


        [HttpGet("ListarEmpresas")]
        [ProducesResponseType(200, Type = typeof(RespuestaOperacionServicio))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object ListarEmpresas()
        {
            try
            {
                _logger.LogInformation("Endpoint {endpoint} {verb}", "/api/SIED/ListarEmpresas", "GET");

                ProxySIED proxi = new ProxySIED();
                var response = proxi.ListarEmpresas();

                _logger.LogInformation("Response Endpoint {endpoint} {verb} {response}", "/api/SIED/ListarEmpresas", "GET", JsonConvert.SerializeObject(response).ToString());
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception {endpoint} {verb} {msg}", "/api/SIED/ListarEmpresas", "GET", ex.Message);
                throw ex;
            }
        }


        [HttpPost("RegistrarDocumento")]
        [ProducesResponseType(200, Type = typeof(RespuestaOperacionServicio))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object RegistrarDocumento([Required][FromHeader(Name = "Token")] string token, EnvioSIED objEnvioSIED)
        {
            try
            {
                _logger.LogInformation("Endpoint {endpoint} {verb}", "/api/SIED/RegistrarDocumento", "POST");

                ProxySIED proxi = new ProxySIED();
                var response = proxi.RegistrarDocumento(token, objEnvioSIED.codDocumento);

                _logger.LogInformation("Response Endpoint {endpoint} {verb} {response}", "/api/SIED/RegistrarDocumento", "POST", JsonConvert.SerializeObject(response).ToString());
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception {endpoint} {verb} {msg}", "/api/SIED/RegistrarDocumento", "POST", ex.Message);
                throw ex;
            }
        }
    }
}
