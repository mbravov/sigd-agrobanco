﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIGD.PROXY.SIED.Model
{
    public class RespuestaOperacionServicio
    {
        public int Resultado { get; set; }

        public string correlativo { get; set; }

        public string Error { get; set; }
        public string Informacion { get; set; }
        public Object data { set; get; }
        public int ResultadoCorreo { get; set; }
        public int posx { get; set; }
        public int posy { get; set; }

        public string codigo { get; set; }
        public string mensaje { get; set; }
        public int codigoDocumentoSIED { get; set; }
        public string codigoCUO { get; set; }
        public List<int> listEnteros { get; set; }
        public List<EmpresasSIED> listaEmpresa { get; set; }
        public List<FirmantesSIED> listaFirmante { get; set; }
        public List<DocDerivacion> lstDocDerivacion { get; set; }
    }

    public class EmpresasSIED
    {

        public int codEmpresa { get; set; }
        public string nomEmpresaCorto { get; set; }
        public string nombreEmpresa { get; set; }
        public string rucEmpresa { get; set; }
        public string tipoEmpresa { get; set; }
        public string CodFormatoDocumentoEmpresas { get; set; }
    }

    public class FirmantesSIED
    {

        public int codEmpresa { get; set; }
        public string nomEmpresa { get; set; }
        public string cargoPersona { get; set; }
        public int codCargoPersona { get; set; }
        public string dniPersona { get; set; }
        public string nomPersona { get; set; }
        public string tipoFirma { get; set; }
        public string unidadOrganica { get; set; }
    }
}

