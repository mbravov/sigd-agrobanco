﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIGD.PROXY.SIED.Model
{
    public class RespuestaConsultaArchivosDTO
    {
        public List<ConsultaArchivoDTO> ListaDocumentos { get; set; }
    }

    public class ConsultaArchivoDTO 
    {
        public int IdDocumento { get; set; }
        public string ArchivoBase64 { get; set; }
        public byte[] ArchivoBytes { get; set; }
        public string NombreArchivo { get; set; }
        public string Extension { get; set; }
        public decimal Peso { get; set; }
        public int TipoArchivo { get; set; }
    }
}
