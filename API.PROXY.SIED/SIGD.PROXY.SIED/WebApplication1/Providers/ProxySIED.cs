﻿using Newtonsoft.Json;
using SIGD.PROXY.SIED.Comun;
using SIGD.PROXY.SIED.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SIGD.PROXY.SIED.Providers
{
    public class ProxySIED
    {
        string userSIED = "";
        string claveSIED = "";
       public ProxySIED()
        {
            Services.AccesoSIED(out userSIED, out claveSIED);
        }
        
        public RespuestaOperacionServicio ListarFirmantes()
        {
            try
            {
                var respuesta = Services.Get<RespuestaOperacionServicio>(Util.ObtenerValueFromKey("SIEDListarFirmantes"), userSIED, claveSIED);
                return respuesta;
            }
            catch(Exception e)
            {
                throw e;
            }
            
        }

        public RespuestaOperacionServicio ListarEmpresas()
        {
            try
            {
                var respuesta = Services.Get<RespuestaOperacionServicio>(Util.ObtenerValueFromKey("SIEDListarEmpresas"), userSIED, claveSIED);
                return respuesta;
            }
            catch(Exception e)
            {
                throw e;
            }
            
        }

        public RespuestaOperacionServicio RegistrarDocumento(string token,int codDocumento)
        {
            try
            {
            RespuestaOperacionServicio rptServ;
            var doc = ObtenerDocumentoSIED(token, codDocumento);
            var listaDocumentosAnexos = SubirArchivosLaserFicheSFTP(token, codDocumento, doc.numeroDocumento);

            var repLegal = ObtenerRepresentanteLegal(token);

            if (repLegal.Estado == Convert.ToInt16(Enumerados.Estado.Inactivo).ToString())
            {
                rptServ = new RespuestaOperacionServicio();
                rptServ.codigo = "-1";
                rptServ.mensaje = "No puede enviar este documento porque no tiene habilitado un representante legal, por favor regularizar en el maestro de Representate Legal";
                return rptServ;
            }

            var dataListaFirmantes = ListarFirmantes();
            var Remitente = dataListaFirmantes.listaFirmante.Find(x => x.dniPersona == repLegal.NroDoc);
            remitente rem = new remitente();

            if (Remitente == null)
            {
                rptServ = new RespuestaOperacionServicio();
                rptServ.codigo = "-1";
                rptServ.mensaje = "El DNI configurado en el maestro de Representante Legal, no esta autorizado en el servicio ListarFirmando de FONAFE.";
                return rptServ;
            }
            else
            {
                rem.codCargo = Remitente.codCargoPersona;
                rem.dni = Remitente.dniPersona;
            }

            var EmpresasSIED = GetDocumentosDerivadosPorCodDocumento(token, codDocumento, Convert.ToInt16(Enumerados.EnumTipoAcceso.EmpresaSIED));

            List<EmpresaDestino> listaEmpresaDestino = new List<EmpresaDestino>();
            EmpresaDestino empDestino;
            foreach (var emp in EmpresasSIED)
            {
                empDestino = new EmpresaDestino();
                empDestino.nombreDestinatario = Remitente.nomPersona;
                empDestino.nombreCargo = Remitente.cargoPersona;
                empDestino.unidadOrganica = doc.origen == Convert.ToInt16(Enumerados.TipoOrigen.EXTERNOSIED) ? Remitente.unidadOrganica : "";
                empDestino.nombreEntidad = emp.NombreEntidadSIED;
                empDestino.ruc = emp.rucSied;
                empDestino.original = Convert.ToInt32(emp.FormatoDocSied);
                listaEmpresaDestino.Add(empDestino);
            }

            EnvioSIED objEnvioSIED = new EnvioSIED();
            objEnvioSIED.dniFirmante = repLegal.NroDoc;
            objEnvioSIED.numeroDocumento = doc.numeroDocumento;
            objEnvioSIED.numSecuencial = codDocumento;
            objEnvioSIED.asunto = doc.asunto;
            objEnvioSIED.categoriaAsunto = doc.categoriaAsunto;
            objEnvioSIED.codSTD = codDocumento;
            objEnvioSIED.tipoDocumento = doc.tipoDocumento;
            objEnvioSIED.esExterno = doc.origen == Convert.ToInt16(Enumerados.TipoOrigen.EXTERNOSIED) ? 1 : 0;
            objEnvioSIED.nroFolio = doc.origen == Convert.ToInt16(Enumerados.TipoOrigen.EXTERNOSIED) ? codDocumento : 0; ;
            objEnvioSIED.codigoDocumentoSIEDRef = codDocumento;

            objEnvioSIED.remitente = rem;
            objEnvioSIED.listaEmpresaDestino = listaEmpresaDestino;
            objEnvioSIED.urlDocumento = Util.ObtenerValueFromKey("rutaSFTPSubidaHome") + Util.ObtenerValueFromKey("rutaSFTPSubidaEmpresas") + "/" + Util.ObtenerValueFromKey("RUCAGROBANCO") + "/" + DateTime.Now.Year.ToString() + Util.ObtenerValueFromKey("rutaSFTPCodSTD");
            objEnvioSIED.nomDocPrincipal = doc.numeroDocumento + ".pdf";
            objEnvioSIED.listaDocumentosAnexos = listaDocumentosAnexos;

                var jsonData = JsonConvert.SerializeObject(objEnvioSIED);

                var respuesta = Services.Post<RespuestaOperacionServicio>(Util.ObtenerValueFromKey("SIEDRegistrarDocumento"), objEnvioSIED, userSIED, claveSIED);
            return respuesta;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public List<documentoSeg> SubirArchivosLaserFicheSFTP(string token,int codDocumento, string correlativo)
        {
            try
            {
                List<documentoSeg> listaDocumentosAnexos = new List<documentoSeg>();
                documentoSeg objDocSeg;

                var codDocPrincipal = GetCodLaserficheDocumento(token, codDocumento);
                var ListCodLF = GetCodLaserficheAnexosDocumento(token, codDocumento);

                var stream = GetDocumentoLaserFiche(token, codDocPrincipal);
                var rutaEnvioSiedLogica = Util.ObtenerValueFromKey("rutaSFTPSubidaHome") + Util.ObtenerValueFromKey("rutaSFTPSubidaEmpresas") + "/" + Util.ObtenerValueFromKey("RUCAGROBANCO") + "/" + DateTime.Now.Year.ToString() + Util.ObtenerValueFromKey("rutaSFTPCodSTD");
                var rutaEnvioSiedFisica = Util.ObtenerValueFromKey("rutaSFTPSubidaHome") + "/" + DateTime.Now.Year.ToString() + Util.ObtenerValueFromKey("rutaSFTPCodSTD");

                SFTP.UploadSFTPFileFromStream(correlativo + ".pdf", rutaEnvioSiedFisica, stream);

                var cont = 0;
                foreach (var anx in ListCodLF)
                {
                    cont++;
                    var streamAnx = GetDocumentoLaserFiche(token, anx);
                    var nombreAnx = correlativo + "_ANX" + cont + ".pdf";
                    SFTP.UploadSFTPFileFromStream(nombreAnx, rutaEnvioSiedFisica, streamAnx);
                    objDocSeg = new documentoSeg();

                    objDocSeg.nomDocumento = nombreAnx;
                    objDocSeg.desDocumento = "";
                    listaDocumentosAnexos.Add(objDocSeg);

                }
                return listaDocumentosAnexos;
            }
            catch(Exception e)
            {
                throw e;
            }
        
        }


        public int GetCodLaserficheDocumento(string token,int codDocumento)
        {
            try
            {
                var respuesta = Services.GetWithToken<RespuestaOperacionServicio>(Util.ObtenerValueFromKey("URLAPI_SIGD") + "/documento/GetCodLaserFIchePorCodDocumento?CodDocumento=" + codDocumento, token);
                var item = Convert.ToInt32(respuesta.data);

                return item;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<int> GetCodLaserficheAnexosDocumento(string token, int codDocumento)
        {

            try
            {
                var respuesta = Services.GetWithToken<RespuestaOperacionServicio>(Util.ObtenerValueFromKey("URLAPI_SIGD") + "/documento/GetCodLaserFIcheAnexosPorCodDocumento?CodDocumento=" + codDocumento, token);
                return respuesta.listEnteros; ;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private EnvioSIED ObtenerDocumentoSIED(string token,int codDocumento)
        {
            try
            {
                var respuesta = Services.GetWithToken<RespuestaOperacionServicio>(Util.ObtenerValueFromKey("URLAPI_SIGD") + "/documento/ObtenerDocumentoSIED_SeviceEnvio?CodDocumento=" + codDocumento, token);

                var item = Util.ConvertToObject<EnvioSIED>(respuesta.data);
                if (item == null)
                {
                    item = new EnvioSIED();
                }
                return item;
            }
            catch(Exception e)
            {
                throw e;
            } 
        }

        private RepresentanteLegal ObtenerRepresentanteLegal(string token)
        {
            try
            {
                var respuesta = Services.GetWithToken<RespuestaOperacionServicio>(Util.ObtenerValueFromKey("URLAPI_SIGD") + "/RepresentanteLegal/ObtenerRepresentanteLegal", token);

                var item = Util.ConvertToObject<RepresentanteLegal>(respuesta.data);
                if (item == null)
                {
                    item = new RepresentanteLegal();
                }
                return item;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public Stream GetDocumentoLaserFiche(string token, int codLaserfiche)
        {
            try
            {
                var respuesta = Services.GetWithToken<RespuestaOperacionServicio>(Util.ObtenerValueFromKey("URLAPI_SIGD") + "/documento/ObtenerDocumentoLaserfiche/" + codLaserfiche, token);
                RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

                if (respuesta.Resultado == 1)
                {
                    rptArchivos = Util.ConvertToObject<RespuestaConsultaArchivosDTO>(respuesta.data);
                }

                respuesta.data = rptArchivos;

                byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
                MemoryStream ms2 = new MemoryStream(imageBytes, 0,
                  imageBytes.Length, true, true);


                return ms2;
            }
            catch(Exception e)
            {
                throw e;
            }
           
        }

        public List<DocDerivacion> GetDocumentosDerivadosPorCodDocumento(string token, int CodDocumento, int TipoAcceso)
        {
            RespuestaOperacionServicio respuesta = Services.GetWithToken<RespuestaOperacionServicio>(Util.ObtenerValueFromKey("URLAPI_SIGD") + "/documento/GetDocumentosDerivadosPorCodDocumento?CodDocumento=" + CodDocumento + "&TipoAcceso=" + TipoAcceso, token);

            return respuesta.lstDocDerivacion;
        }
    }
}
