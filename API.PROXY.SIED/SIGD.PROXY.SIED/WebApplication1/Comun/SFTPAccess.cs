﻿using Agrobanco.Security.Criptography;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Threading.Tasks;

namespace SIGD.PROXY.SIED.Comun
{
    public class SFTPAccess
    {
        [Import(typeof(IValueReader), AllowRecomposition = true)]
        private IValueReader _decriptService;
        private int SFTPFicheEnableEncrypt = Convert.ToInt16(Util.ObtenerValueFromKey("SFTPApplicationNameEnableEncrypt"));
        private int SIEDEnableEncrypt = Convert.ToInt16(Util.ObtenerValueFromKey("SIEDApplicationNameEnableEncrypt"));
        private int EnableEncriptHab = 1;
        private int EnableEncriptDes = 0;


        public void SFTPAccessConnection(out string _userName, out string _password, out string _hostName, out int _port)
        {
            _userName = "";
            _password = "";
            _hostName = "";
            _port = 0;
            try
            {
                Compose();
                _decriptService.Application = Util.ObtenerValueFromKey("SFTPApplicationName");
                _decriptService.ClaveEncriptado = Util.ObtenerValueFromKey("RegeditPass");
                if (SFTPFicheEnableEncrypt == EnableEncriptHab)
                {
                    _userName = _decriptService.ReadValue("Usuario");// _decriptService.ReadValue("Usuario");
                    _password = _decriptService.ReadValue("Clave"); //_decriptService.ReadValue("Clave");
                    _hostName = _decriptService.ReadValue("Host");//_decriptService.ReadValue("Server"); // "41.50.13.193";
                    _port = Convert.ToInt32(_decriptService.ReadValue("Puerto"));

                }
                else if (SIEDEnableEncrypt == EnableEncriptDes)
                {
                    _userName = ReadKey("Usuario", Util.ObtenerValueFromKey("SFTPApplicationName"));// _decriptService.ReadValue("Usuario");
                    _password = ReadKey("Clave", Util.ObtenerValueFromKey("SFTPApplicationName")); //_decriptService.ReadValue("Clave");
                    _hostName = ReadKey("Host", Util.ObtenerValueFromKey("SFTPApplicationName"));//_decriptService.ReadValue("Server"); // "41.50.13.193";
                    _port = Convert.ToInt32(ReadKey("Puerto", Util.ObtenerValueFromKey("SFTPApplicationName")));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public void ServicesSiedAccessConnection(out string _userName, out string _password)
        {
            _userName = "";
            _password = "";
            try
            {
                Compose();
                _decriptService.Application = Util.ObtenerValueFromKey("SIEDApplicationName");
                _decriptService.ClaveEncriptado = Util.ObtenerValueFromKey("RegeditPass");
                if (SIEDEnableEncrypt == EnableEncriptHab)
                {
                    _userName = _decriptService.ReadValue("Usuario");
                    _password = _decriptService.ReadValue("Clave");

                }
                else if (SIEDEnableEncrypt == EnableEncriptDes)
                {
                    _userName = ReadKey("Usuario", Util.ObtenerValueFromKey("SIEDApplicationName"));
                    _password = ReadKey("Clave", Util.ObtenerValueFromKey("SIEDApplicationName"));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        private void Compose()
        {
            var encryptCatalog = new AssemblyCatalog(System.Reflection.Assembly.GetAssembly(typeof(IValueReader)));
            var catalog = new AggregateCatalog(encryptCatalog);
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }


        private string ReadKey(string value, string folder)
        {
            Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
            RegistryKey masterKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\AGROBANCO\\" + folder);
            string valueret = "";
            if (masterKey != null)
            {
                valueret = masterKey.GetValue(value).ToString();
            }
            masterKey.Close();
            return valueret;
        }
    }
}