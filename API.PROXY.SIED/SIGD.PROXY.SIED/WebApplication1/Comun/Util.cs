﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SIGD.PROXY.SIED.Comun
{
    public static class Util
    {
        public static string ObtenerValueFromKey(string keys)
        {
            var configuration = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json", false)
                  .Build();

            return configuration[keys];
        }

        public static T ConvertToObject<T>(object jsonObject)
        {
            string str = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObject);
            T result = default(T);

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);

            return result;
        }
    }
}
