﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SIGD.PROXY.SIED.Comun
{
    public static class Services
    {

        public static T Get<T>(string url, string usuario, string clave)
        {
            var _response = string.Empty;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(usuario + ":" + clave));
                req.Headers.Add("Authorization", "Basic " + credentials);
                req.Method = "GET";
                req.ContentType = "application/json";

                req.Timeout = Timeout.Infinite; ;

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string _json = reader.ReadToEnd();
                _response = _json;

                T _lista = JsonConvert.DeserializeObject<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static T Post<T>(string url, Object data, string usuario, string clave)
        {
           var _response = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );
                var jsonData = JsonConvert.SerializeObject(data);

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(usuario + ":" + clave));
                req.Headers.Add("Authorization", "Basic " + credentials);
                req.Method = "post";
                req.ContentType = "application/json";
                req.Timeout = Timeout.Infinite; 

                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    string json = jsonData;

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)req.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    _response = streamReader.ReadToEnd();
                }

                T _lista = JsonConvert.DeserializeObject<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static T GetWithToken<T>(string url, string autorization)
        {
            var _response = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

                // Prueba de obtención de alumno vía HTTP GET
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "GET";
                req.ContentType = "application/json";
                string token = autorization;
                req.Headers.Add("Authorization", token);

                req.Timeout = Timeout.Infinite; ;
                //timeout

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string _json = reader.ReadToEnd();
                _response = _json;

                T _lista = JsonConvert.DeserializeObject<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void AccesoSIED(out string user, out string clave)
        {
            SFTPAccess con = new SFTPAccess();
            try
            {
                con.ServicesSiedAccessConnection(out user, out clave);
            }
            catch (Exception e)
            {
                user = "";
                clave = "";
            }
        }
    }
}
