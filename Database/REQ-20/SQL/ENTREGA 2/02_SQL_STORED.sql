USE [DBAgrSIGD]
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ListarTrabajadores]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ListarTrabajadores] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_ListarTrabajadores]
AS
BEGIN
	SELECT 
		T.[iCodTrabajador], [vNumDocumento], [vNombres], [vCargo], [vUbicacion], [iAnexo], [vEmail],
		[vApePaterno], [vApeMaterno], [siEsJefe], [iCodArea], [iCodTipDocIdentidad], [vCelular],
		U.WEBUSR
	FROM [dbo].[MaeTrabajador] T
		INNER JOIN DBO.SegUsuario U
		ON U.iCodTrabajador = T.iCodTrabajador
	WHERE 
	 U.siEstado= 1
	AND T.siEstado = 1
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ListarUsuarioSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ListarUsuarioSIED] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UP_MAE_ListarUsuarioSIED]
AS
BEGIN
	SELECT [iCodUsuarioSIED]
      ,[WEBUSR]
	  ,iCodTrabajador
      ,[vNombre]
      ,[vApePaterno]
      ,[vApeMaterno]
      ,[vEmail]
      ,[vNroDoc]
      ,[vUsuCreacion]
      ,[dFecCreacion]
      ,[vHstCreacion]
      ,[vInsCreacion]
      ,[vLgnCreacion]
      ,[vRolCreacion]
      ,[vUsuActualizacion]
      ,[dFecActualizacion]
      ,[vHstActualizacion]
      ,[vInsActualizacion]
      ,[vLgnActualizacion]
      ,[vRolActualizacion]
  FROM [dbo].[MaeUsuarioSIED]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_GrabarUsuarioSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_GrabarUsuarioSIED] AS' 
END
GO
ALTER   procedure [dbo].[UP_MAE_GrabarUsuarioSIED]
@WEBUSR varchar(20),
@iCodTrabajador bigint,
@vNombre varchar(20),
@vApePaterno varchar(20),
@vApeMaterno varchar(20),
@vEmail varchar(50),
@vNroDoc varchar(20),
@vUsuCreacion varchar(100) = null,
@vHstCreacion varchar(20) = null,
@vInsCreacion varchar(50) = null,
@vLgnCreacion varchar(20) = null,
@vRolCreacion varchar(20) = null,
@onFlagOK INT OUTPUT
AS
BEGIN

	INSERT INTO [dbo].[MaeUsuarioSIED]
           ([WEBUSR]
		   ,[iCodTrabajador]
           ,[vNombre]
           ,[vApePaterno]
           ,[vApeMaterno]
           ,[vEmail]
           ,[vNroDoc]
           ,[vUsuCreacion]
           ,[dFecCreacion]
           ,[vHstCreacion]
           ,[vInsCreacion]
           ,[vLgnCreacion]
           ,[vRolCreacion])
     VALUES
          (@WEBUSR, 
		   @iCodTrabajador,
           @vNombre, 
           @vApePaterno,
           @vApeMaterno, 
           @vEmail, 
           @vNroDoc,
           @vUsuCreacion, 
           GETDATE(),
           @@SERVERNAME, 
           @@SERVICENAME, 
           @vLgnCreacion,
           @vRolCreacion)

IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodUsuarioSIED) FROM [dbo].[MaeUsuarioSIED]

END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ActualizarUsuarioSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ActualizarUsuarioSIED] AS' 
END
GO
alter procedure [dbo].[UP_MAE_ActualizarUsuarioSIED]
@WEBUSR varchar(20),
@iCodTrabajador bigint,
@vNombre varchar(20),
@vApePaterno varchar(20),
@vApeMaterno varchar(20),
@vEmail varchar(50),
@vNroDoc varchar(20),
@vUsuActualizacion varchar(50)=null ,
@dFecActualizacion datetime=null ,
@vLgnActualizacion varchar(50)=null ,
@vRolActualizacion varchar(50)=null,
@onFlagOK int output
as
begin

  update [dbo].[MaeUsuarioSIED]
   set [WEBUSR] = @WEBUSR
   	  ,iCodTrabajador = @iCodTrabajador
      ,[vNombre] = @vNombre
      ,[vApePaterno] = @vApePaterno
      ,[vApeMaterno] = @vApeMaterno
      ,[vEmail] = @vEmail
      ,[vNroDoc] = @vNroDoc
      ,[vUsuActualizacion] = @vUsuActualizacion
      ,[dFecActualizacion] = getdate()
      ,[vHstActualizacion] = @@SERVERNAME
      ,[vInsActualizacion] = @@SERVICENAME
      ,[vLgnActualizacion] = @vLgnActualizacion
      ,[vRolActualizacion] = @vRolActualizacion
	  where icodusuarioSIED = (select top 1 icodusuarioSIED from [MaeUsuarioSIED] )

set @onFlagOK=(select top 1 icodusuarioSIED from [MaeUsuarioSIED] )

end
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Log_UsuarioSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_Log_UsuarioSIED] AS' 
END
GO
alter  PROCEDURE [dbo].[UP_Log_UsuarioSIED]
@WEBUSR varchar(20),
@vNombre varchar(20),
@vApePaterno varchar(20),
@vApeMaterno varchar(20),
@vEmail varchar(50),
@vNroDoc varchar(20),
@vUsuCreacion varchar(100) = null,
@vHstCreacion varchar(20) = null,
@vInsCreacion varchar(50) = null,
@vLgnCreacion varchar(20) = null,
@vRolCreacion varchar(20) = null,
@onFlagOK INT OUTPUT
AS
BEGIN

	INSERT INTO [dbo].[LogUsuarioSIED]
           ([iCodUsuarioSIED]
		   ,[WEBUSR]
           ,[vNombre]
           ,[vApePaterno]
           ,[vApeMaterno]
           ,[vEmail]
           ,[vNroDoc]
           ,[vUsuCreacion]
           ,[dFecCreacion]
           ,[vHstCreacion]
           ,[vInsCreacion]
           ,[vLgnCreacion]
           ,[vRolCreacion])
     VALUES
          (
		   (select iCodUsuarioSIED from MAEUsuarioSied where WEBUSR = @WEBUSR),
		   @WEBUSR, 
           @vNombre, 
           @vApePaterno,
           @vApeMaterno, 
           @vEmail, 
           @vNroDoc,
           @vUsuCreacion, 
           GETDATE(),
           @@SERVERNAME, 
           @@SERVICENAME, 
           @vLgnCreacion,
           @vRolCreacion)

IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodLogUsuarioSIED) FROM [dbo].[LogUsuarioSIED]

END
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ListarRepresentanteLegal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ListarRepresentanteLegal] AS' 
END
GO
alter  PROCEDURE [dbo].[UP_MAE_ListarRepresentanteLegal]
AS
BEGIN
	SELECT iCodRepresentanteLegal
      ,[vNombre]
      ,[vApePaterno]
      ,[vApeMaterno]
      ,[vEmail]
      ,[vNroDoc]
	  ,siEstado
      ,[vUsuCreacion]
      ,[dFecCreacion]
      ,[vHstCreacion]
      ,[vInsCreacion]
      ,[vLgnCreacion]
      ,[vRolCreacion]
      ,[vUsuActualizacion]
      ,[dFecActualizacion]
      ,[vHstActualizacion]
      ,[vInsActualizacion]
      ,[vLgnActualizacion]
      ,[vRolActualizacion]
  FROM [dbo].[MaeRepresentanteLegal]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Log_UsuarioRepresentanteLegal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_Log_UsuarioRepresentanteLegal] AS' 
END
GO
alter  PROCEDURE [dbo].[UP_Log_UsuarioRepresentanteLegal]

@vNombre varchar(20),
@vApePaterno varchar(20),
@vApeMaterno varchar(20),
@vEmail varchar(50) = null,
@vNroDoc varchar(20),
@siEstado smallint,
@vUsuCreacion varchar(100) = null,
@vHstCreacion varchar(20) = null,
@vInsCreacion varchar(50) = null,
@vLgnCreacion varchar(20) = null,
@vRolCreacion varchar(20) = null,
@onFlagOK INT OUTPUT
AS
BEGIN

	INSERT INTO [dbo].[LogRepresentanteLegal]
           ([iCodRepresentanteLegal]
           ,[vNombre]
           ,[vApePaterno]
           ,[vApeMaterno]
           ,[vEmail]
           ,[vNroDoc]
		   ,[siEstado]
           ,[vUsuCreacion]
           ,[dFecCreacion]
           ,[vHstCreacion]
           ,[vInsCreacion]
           ,[vLgnCreacion]
           ,[vRolCreacion])
     VALUES
          (
		   (select top 1 [iCodRepresentanteLegal] from MAERepresentanteLegal), 
           @vNombre, 
           @vApePaterno,
           @vApeMaterno, 
           @vEmail, 
           @vNroDoc,
		   @siEstado,
           @vUsuCreacion, 
           GETDATE(),
           @@SERVERNAME, 
           @@SERVICENAME, 
           @vLgnCreacion,
           @vRolCreacion)

IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX([iCodRepresentanteLegal]) FROM [dbo].[LogRepresentanteLegal]

END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ActualizarRepresentanteLegal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ActualizarRepresentanteLegal] AS' 
END
GO
alter procedure [dbo].[UP_MAE_ActualizarRepresentanteLegal]
@vNombre varchar(50),
@vApePaterno varchar(20),
@vApeMaterno varchar(20),
@vEmail varchar(50) = null,
@vNroDoc varchar(20),
@siEstado smallint,
@vUsuActualizacion varchar(50)=null ,
@dFecActualizacion datetime=null ,
@vLgnActualizacion varchar(50)=null ,
@vRolActualizacion varchar(50)=null,
@onFlagOK int output
as
begin

  update [dbo].[MaeRepresentanteLegal]
   set [vNombre] = @vNombre
      ,[vApePaterno] = @vApePaterno
      ,[vApeMaterno] = @vApeMaterno
      ,[vEmail] = @vEmail
      ,[vNroDoc] = @vNroDoc
	  ,[siEstado] = @siEstado
      ,[vUsuActualizacion] = @vUsuActualizacion
      ,[dFecActualizacion] = getdate()
      ,[vHstActualizacion] = @@SERVERNAME
      ,[vInsActualizacion] = @@SERVICENAME
      ,[vLgnActualizacion] = @vLgnActualizacion
      ,[vRolActualizacion] = @vRolActualizacion
	  where icodRepresentanteLegal = (select top 1 icodRepresentanteLegal from [MaeRepresentanteLegal] )

set @onFlagOK=(select top 1 icodRepresentanteLegal from [MaeRepresentanteLegal] )

end

GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_GrabarRepresentanteLegal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_GrabarRepresentanteLegal] AS' 
END
GO
alter  procedure dbo.UP_MAE_GrabarRepresentanteLegal
@vNombre varchar(20),
@vApePaterno varchar(20),
@vApeMaterno varchar(20),
@vEmail varchar(50),
@vNroDoc varchar(20),
@siEstado smallint,
@vUsuCreacion varchar(100) = null,
@vHstCreacion varchar(20) = null,
@vInsCreacion varchar(50) = null,
@vLgnCreacion varchar(20) = null,
@vRolCreacion varchar(20) = null,
@onFlagOK INT OUTPUT
AS
BEGIN

	INSERT INTO [dbo].[MaeRepresentanteLegal]
           ([vNombre]
           ,[vApePaterno]
           ,[vApeMaterno]
           ,[vEmail]
           ,[vNroDoc]
		   ,siEstado
           ,[vUsuCreacion]
           ,[dFecCreacion]
           ,[vHstCreacion]
           ,[vInsCreacion]
           ,[vLgnCreacion]
           ,[vRolCreacion])
     VALUES
          (
           @vNombre, 
           @vApePaterno,
           @vApeMaterno, 
           @vEmail, 
           @vNroDoc,
		   @siEstado,
           @vUsuCreacion, 
           GETDATE(),
           @@SERVERNAME, 
           @@SERVICENAME, 
           @vLgnCreacion,
           @vRolCreacion)

IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodRepresentanteLegal) FROM [dbo].[MaeRepresentanteLegal]

END

GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumentoSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocumentoSIED] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocumentoSIED]
  @iCodSecuencial INT = NULL--2
 ,@vCorrelativo VARCHAR(100)= NULL--3
 ,@iAnio INT = NULL--4
 ,@siEstado SMALLINT --5
 ,@siOrigen SMALLINT = NULL --6
 ,@vNumDocumento VARCHAR(100) = NULL--7
 ,@siEstDocumento SMALLINT = NULL--8
 ,@dFecDocumento DATETIME = NULL--9
 ,@dFecRegistro DATETIME = NULL--10
 ,@dFecDerivacion DATETIME=NULL
 ,@vAsunto VARCHAR(1000) = NULL--11
 ,@siPrioridad SMALLINT = NULL--12
 ,@vReferencia VARCHAR(1000) = NULL--13
 ,@dFecRecepcion DATETIME = NULL--14
 ,@siPlazo SMALLINT = NULL--15
 ,@dFecPlazo DATETIME = NULL--16
 ,@vObservaciones VARCHAR(1000) = NULL--17
 ,@iCodTipDocumento INT = NULL --18
 ,@iCodRepresentante INT--19
 ,@iCodArea INT = NULL --20
 ,@iCodTrabajador INT= NULL--21
 ,@iCodTipDocumentoSIED VARCHAR(5)= NULL
 ,@iCodTipAsuntoSIED VARCHAR(5)= NULL
 ,@siFlagSIED INT = NULL 
 ,@vUsuCreacion VARCHAR(100) = NULL--22
 ,@vHstCreacion VARCHAR(20) = NULL--23
 ,@vInsCreacion VARCHAR(50) = NULL--24
 ,@vLgnCreacion VARCHAR(20) = NULL--25
 ,@vRolCreacion VARCHAR(20) = NULL--26
 ,@onFlagOK INT OUTPUT--27
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocumento]
-- Objetivo:	Registrar un nuevo Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
--				@iCodSecuencial -  Número de Secuencia
--				@iAnio		  - Anio Correlativo
--				@siOrigen	  - Estado del Registro: 1 - Activo; 0 - Inactivo
--				@vNumDocumento	  - Número de Documento
--				@siEstDocumento	  - Estado del Documento
--				@dFecDocumento	  - Fecha del Documento
--				@dFecRegistro	  - Fecha de Registro
--				@vAsunto			- Asunto
--				@siPrioridad		- Indicador de Prioridad
--				@vReferencia		- Referencia
--				@dFecRecepcion	  - Fecha de Recepción
--				@siPlazo			- Indicador de Plazo
--				@dFecPlazo			- Fecha de Plazo
--				@vObservaciones		- Observaciones
--				@iCodTipDocumento	- Codigo Tipo Documento
--				@iCodRepresentante  - Código de Representante
--				@iCodArea			- Código de Área
--				@iCodTrabajador	    - Codigo de Trabajador
--				@iCodTipDocumentoSIED - Codigo del tipo documento SIED
--				@iCodTipAsuntoSIED - Codigo del tipo asunto SIED
--				@siFlagSIED - Identificador si el documento es con destino SIED 1= ENVIO SIED 2= RECEPCIÓN SIED
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

	

		INSERT INTO dbo.MovDocumento(iCodSecuencial,iAnio,siEstado,
									siOrigen,vNumDocumento,siEstDocumento,
									dFecDocumento,dFecRegistro,vAsunto,
									siPrioridad,vReferencia,dFecRecepcion,
									siPlazo,dFecPlazo,vObservaciones,
									iCodTipDocumento,iCodRepresentante,iCodArea,
									iCodTrabajador,vUsuCreacion,dFecCreacion,
									vHstCreacion,vInsCreacion,vLgnCreacion,
									vRolCreacion,vCorrelativo,dFecDerivacion,siFlagFirmando,
									iCodTipDocumentoSIED,iCodTipAsuntoSIED,siFlagSIED
									)
									VALUES(									
									@iCodSecuencial,@iAnio,1,
									@siOrigen,@vNumDocumento,@siEstDocumento,
									@dFecDocumento,getdate(),@vAsunto,
									@siPrioridad,@vReferencia,@dFecRecepcion,
									@siPlazo,@dFecPlazo,@vObservaciones,
									@iCodTipDocumento,@iCodRepresentante,@iCodArea,
									@iCodTrabajador,@vUsuCreacion,GETDATE(),
									@@SERVERNAME,@@servicename,@vLgnCreacion,
									@vRolCreacion,@vCorrelativo,@dFecDerivacion,0,
									@iCodTipDocumentoSIED,@iCodTipAsuntoSIED,@siFlagSIED
									)




		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocumento) FROM dbo.MovDocumento 
			ELSE
				SET @onFlagOK=0


 END


GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumentosDerivadosSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocumentosDerivadosSIED] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UP_MOV_RegistrarDocumentosDerivadosSIED]
(
  @iCodDocumento INT
 ,@iCodArea INT = NULL
 ,@iCodResponsable INT = NULL
 ,@siTipoAcceso SMALLINT
 ,@dFecDerivacion datetime
 ,@iCodEntidadSIED INT = NULL
 ,@vNombreEntidadSIED VARCHAR(200) = NULL
 ,@vRucSIED VARCHAR(20) = NULL
 ,@iFormatoDocumentoSIED VARCHAR(2) = NULL
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocumentosDerivadosSIED]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN
  DECLARE @estadoDerivacion int = NULL
  IF(@siTipoAcceso =  1)
  BEGIN
  SET @estadoDerivacion = 2
  END
  

INSERT INTO MovDocumentoDerivacion
(
 iCodDocumento, 
siEstado, iCodArea,
iCodResponsable, siEstadoDerivacion, 
siTipoAcceso, dFecDerivacion, 
vNombreEntidadSIED,vRucSIED,
iFormatoDocumentoSIED,iCodEntidadSIED,
vUsuCreacion, dFecCreacion, 
vHstCreacion, vInsCreacion, 
vLgnCreacion, vRolCreacion
)
VALUES        
(
 @iCodDocumento, 
1, @iCodArea, 
@iCodResponsable, @estadoDerivacion, 
@siTipoAcceso, @dFecDerivacion,
@vNombreEntidadSIED,@vRucSIED,
@iFormatoDocumentoSIED,@iCodEntidadSIED,
@vUsuCreacion,GETDATE(),
@@SERVERNAME,@@SERVICENAME,
@vLgnCreacion,@vRolCreacion
)

IF @@ROWCOUNT  > 0 
	SELECT @onFlagOK=MAX(iCodDocumentoDerivacion) FROM dbo.MovDocumentoDerivacion 
ELSE
	SET @onFlagOK=0

END


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocMovimiento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimiento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimiento]
  @iCodDocumento INT
 ,@iCodArea INT = NULL
 ,@iCodTrabajador INT = NULL
 ,@dFecRecepcion DATETIME = NULL
 ,@dFecLectura DATETIME = NULL
 ,@siPlazo SMALLINT
 ,@dFecPlazo DATETIME = NULL
 ,@siNivel SMALLINT
 ,@siEstFirma INT
 ,@dFecDerivacion DATETIME=NULL
 ,@dFecVisado DATETIME = NULL
 ,@siEstVisado SMALLINT
 ,@dFecFirma DATETIME = NULL
 ,@iCodDocDerivacion int =NULL
 ,@iCodAreaDerivacion int =NULL
 ,@iCodResponsableDerivacion int= NULL
 ,@siEstadoDerivacion int =NULL
 ,@iTipoMovimiento int =NULL
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocMovimiento]
-- Objetivo:	Registrar Movimiento de los Documentos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código del Documento
--				@iCodArea -  Codigo de Área
--				@iCodTrabajador - Código de Trabajador
--				@dFecRecepcion - Fecha de Recepción
--				@dFecLectura - Fecha de Lectura
--				@siPlazo : Indicador de Plazo
--				@dFecPlazo : Fecha de Plazo
--				@siNivel : Indicador de Nivel
--				@siEstFirma: Indicador de Firma
--				@dFecVisado : Fecha de Visado
--				@siEstVisado : Indicador de Estado Visado
--				@dFecFirma : Fecha de Firma
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 07/02/20
-- =============================================
DECLARE @iCodMovAnterior INT;
BEGIN

	SET @iCodMovAnterior = (SELECT MAX(iCodDocMovimiento) FROM dbo.MovDocMovimiento WHERE iCodDocumento=@iCodDocumento AND siEstado=1)
	
	declare @estadoDoc int ;
	select @estadoDoc = siEstDocumento
	from MovDocumento
	where iCodDocumento=@iCodDocumento

	INSERT INTO dbo.MovDocMovimiento(siEstado,
									dFecRecepcion,
									dFecLectura,
									iCodMovAnterior,
									siPlazo,
									dFecPlazo,
									siNivel,
									siEstFirma,
									dFecVisado,
									siEstVisado,
									dFecFirma,
									iCodDocumento,
									iCodArea,
									iCodTrabajador,
									iCodDocDerivacion,
									iCodAreaDerivacion,
									iCodResponsableDerivacion,
									siEstadoDerivacion,
									iTipoMovimiento,
									vUsuCreacion,
									dFecCreacion,
									vHstCreacion,
									vInsCreacion,
									vLgnCreacion,
									vRolCreacion,
									dFecDerivacion,
									iEstadoDocumento)
									VALUES(									
									1,
									@dFecRecepcion,
									@dFecLectura,
									@iCodMovAnterior,
									@siPlazo,
									@dFecPlazo,
									@siNivel,
									@siEstFirma,
									@dFecVisado,
									@siEstVisado,
									@dFecFirma,
									@iCodDocumento,
									@iCodArea,
									@iCodTrabajador,
   								    @iCodDocDerivacion,
									@iCodAreaDerivacion,
									@iCodResponsableDerivacion,
									@siEstadoDerivacion,
									@iTipoMovimiento,
									@vUsuCreacion,
									GETDATE(),
									@@SERVERNAME,
									@@SERVICENAME,
									@vLgnCreacion,
									@vRolCreacion,
									@dFecDerivacion,
									@estadoDoc
									)



		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocMovimiento) FROM dbo.MovDocMovimiento 
			ELSE
				SET @onFlagOK=0

 END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UF_MOV_OBTENERCORRELATIVOSIED]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
DROP FUNCTION [dbo].[UF_MOV_OBTENERCORRELATIVOSIED]
END
GO
 CREATE FUNCTION [dbo].[UF_MOV_OBTENERCORRELATIVOSIED]
(
	@origen int,
	@iCodArea int,
	@iCodTipDocumento varchar(5),
	@Anio int,
	@Tipo int
)
RETURNS varchar(100)
AS
BEGIN
	declare @salida varchar(100)='';
	
	declare @CORRELATIVO varchar(50), 
			@TIPODOC VARCHAR(5), 
			@AREA VARCHAR(10),
			@NRO_TIPODOC_AREA_ANIO_CORR VARCHAR(100),
			@CORRELATIVOFINAL varchar(50);
	--select @TIPODOC=vAbreviatura from MaeTipDocumento where iCodTipDocumento=@iCodTipDocumento;
	select @TIPODOC=vValor2 from ParParametro where vValor=@iCodTipDocumento and iCodGrupo in (14,15);
	select @AREA=vAbreviatura from MaeArea where iCodArea=@iCodArea;
	select @CORRELATIVO=max(iCorrelativo)
	from MaeSecuencialsied a
	where a.iAnio=@Anio and a.siOrigen=@origen and siestado=1;

	set @CORRELATIVO=isnull(cast(@CORRELATIVO as int),0)+1;
	
	select @NRO_TIPODOC_AREA_ANIO_CORR=max(iCorrelativo) from MaeSecuencialsied   a
	where a.iAnio=@Anio and a.siOrigen=@origen
	AND a.iCodTipDocumento=@iCodTipDocumento
	AND a.iCodArea=@iCodArea 
	and siestado=1

	set @NRO_TIPODOC_AREA_ANIO_CORR=isnull(cast(@NRO_TIPODOC_AREA_ANIO_CORR as int),0)+1;
	
	if(@origen=3)--interno SIED--
	begin
			select @salida= vValor from ParParametro where iCodParametro=3 and icodgrupo=4 and siestado=1
			set @CORRELATIVOFINAL=@NRO_TIPODOC_AREA_ANIO_CORR
	end
	else 
	begin
		if(@origen=4)--externo SIED
		begin
			select @salida= vValor from ParParametro where iCodParametro=3 and icodgrupo=4 and siestado=1
			set @CORRELATIVOFINAL=@CORRELATIVO
		end
	end
	if(@Tipo=1)
	begin
		return @CORRELATIVOFINAL;
	end
	
	set @salida= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@salida,'[CORRELATIVO]',right('000000000'+@CORRELATIVO,4))
										,'[ANIO]',cast(@Anio as varchar(5)))
										,'[AREA]',isnull(@AREA,''))
										,'[NRO_TIPODOC_AREA_ANIO_CORRELATIVO]',right('000000000'+isnull(@NRO_TIPODOC_AREA_ANIO_CORR,''),4))
										,'[TIPODOC]',isnull(@TIPODOC,''))+'-SIED'

	RETURN @salida;
END
GO
 
 IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Generar_SecuencialSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_Generar_SecuencialSIED] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_Generar_SecuencialSIED]
  @iOrigen INT
 ,@iCodTipoDocumento VARCHAR(5)
 ,@iCodArea INT
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onSecuencial INT OUTPUT
 ,@onCorrelativo VARCHAR(100) OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Generar_Secuencial]
-- Objetivo:	Generar Secuencial de Documentos
-- Módulo:		Maestros
-- Input:		@iCodTipoDocumento : Código Tipo Documento
--				@iCodArea	: Código de Área
-- Output:
--				@onCorrelativo - Correlativo Generado
-- Create date: 27/01/20
-- =============================================
DECLARE @AnioActual AS INT
DECLARE @IdSecuencialMax AS INT
DECLARE @CorrelativoExist AS VARCHAR(100)
DECLARE @SecuencialExist AS INT
declare @onCorrelativoNumero AS int;
BEGIN
  
  SET @AnioActual=YEAR(GETDATE()) 
	--VALIDAMOS SI HAY SECUENCIAS USADAS
	IF(@iOrigen = (SELECT iCodParametro FROM ParParametro  WHERE vCampo='EXTERNO SIED'))
	BEGIN
		 SELECT @CorrelativoExist= vCorrelativo , @SecuencialExist=iCodSecuencial FROM  MaeSecuencialSIED WHERE siOrigen = @iOrigen AND iAnio  = @AnioActual AND iCodArea=@iCodArea  AND iCodTipDocumento=@iCodTipoDocumento AND fusado = 0;
	END
	ELSE IF(@iOrigen = (SELECT iCodParametro FROM ParParametro  WHERE vCampo='INTERNO SIED'))
	BEGIN
		 SELECT @CorrelativoExist= vCorrelativo , @SecuencialExist=iCodSecuencial FROM  MaeSecuencialSIED WHERE siOrigen = @iOrigen AND iAnio  = @AnioActual AND iCodArea=@iCodArea  AND iCodTipDocumento=@iCodTipoDocumento AND fusado = 0;
	END

  --Máximo Identificador
  SELECT @IdSecuencialMax=MAX(iCodSecuencial) FROM dbo.MaeSecuencialSIED

	IF @IdSecuencialMax IS NULL 
	    SET @IdSecuencialMax=1;
	ELSE
		SET @IdSecuencialMax=@IdSecuencialMax+1;


   

   if(@CorrelativoExist is not null)
   begin
   SET @onSecuencial  =  @SecuencialExist
   SET @onCorrelativo  =  @CorrelativoExist
    UPDATE MaeSecuencialSIED set fUsado = 1,
							iCodArea = @iCodArea,
							iCodTipDocumento = @iCodTipoDocumento
							where vCorrelativo = @CorrelativoExist;
   end
   else
   begin

   declare @Anio int =YEAR(GETDATE())
   SET @onSecuencial=dbo.UF_MOV_OBTENERCORRELATIVOSIED(@iOrigen,@iCodArea,@iCodTipoDocumento,@Anio, 1)
   SET @onCorrelativo=dbo.UF_MOV_OBTENERCORRELATIVOSIED(@iOrigen,@iCodArea,@iCodTipoDocumento,@Anio, 2)
    --INSERTANDO NUEVO SECUENCIAL
	INSERT INTO dbo.MaeSecuencialSIED(
	iAnio,
	siEstado,
	iCodSecuencial,
	iCorrelativo,
	vCorrelativo,
	iCodArea,
	iCodTipDocumento,
	vUsuCreacion,
	dFecCreacion,
	vHstCreacion,
	vInsCreacion,
	vLgnCreacion,
	vRolCreacion,
	siOrigen,
	fUsado
	)
	VALUES(
	@AnioActual,
	1,
	@IdSecuencialMax,
	@onSecuencial,
	@onCorrelativo,
	@iCodArea,
	@iCodTipoDocumento,
	@vUsuCreacion,
	GETDATE(),
	@@SERVERNAME,
	@@servicename,
	@vLgnCreacion,
	@vRolCreacion,
	@iOrigen,
	1
	)
end

  IF @@ROWCOUNT  > 0 
     SELECT @onCorrelativo
  ELSE
	 SELECT 0

 END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_BandejaEnvioSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_Listar_BandejaEnvioSIED] AS' 
END
GO

ALTER  PROCEDURE [dbo].[UP_MOV_Listar_BandejaEnvioSIED] (
@pDescripcion VARCHAR(100),
@icodTrabajador int,
@FechaInicio DATETIME ,
@FechaFin DATETIME,
@iPagina int,
@iTamPagina int
)
AS

BEGIN
	
	declare  @tmpEmpresasDestino table(siEstado int , icodDocumento int,vNombreEntidadSied varchar(500))
	insert into @tmpEmpresasDestino select DISTINCT siEstado, iCodDocumento, vNombreEntidadSIED = STUFF((SELECT ', ' + vNombreEntidadSIED
    FROM [MovDocumentoDerivacion] WHERE iCodDocumento = t.iCodDocumento and siEstado = 1
    FOR XML PATH('')), 1, 1, '')
	FROM [MovDocumentoDerivacion] AS t
	where vNombreEntidadSIED is not null and siEstado = 1;

	with datatodo as (
	select 
	m.iCodDocumento,
	m.siPrioridad,
	m.siEstDocumento,
	CONVERT(VARCHAR(10), m.dFecRecepcion, 103) AS Recepcion,
	m.vCorrelativo,
	m.vAsunto,
	c.vDescripcion AS Remitente,
	der.vNombreEntidadSIED DrigidoA,
	CAST(m.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), m.dFecPlazo, 103) AS Plazo,
	tipdoc.vCampo TipoDocumento,
	asun.vCampo Asunto,
	m.dFecRegistro,
	estDoc.iOrden OrdenEstado,
	priori.iOrden OrdenPrioridad,
	isnull(estDoc.vCampo,'') vEstadoDescripcion
	,isnull(priori.vCampo,'') vEstadoPrioridad
	,m.siOrigen AS Origen
	,(CASE WHEN M.siEstDocumento IN (2, 6) THEN 1 ELSE 0 END) AS HabilitarDerivar 
	,m.iCodDocumentoRespuestaSIED
	,m.iCodRespuestaSIED
	FROM MovDocumento m
	INNER JOIN [dbo].[MaeArea] c ON m.iCodArea = c.iCodArea
	INNER JOIN  @tmpEmpresasDestino der on m.iCodDocumento= der.iCodDocumento and der.siEstado = 1
	INNER JOIN (SELECT iCodParametro,vValor,vValor2,vCampo from ParParametro where iCodGrupo in(14,15)) tipdoc on m.iCodTipDocumentoSIED = tipdoc.vValor
	INNER JOIN (SELECT iCodParametro,vValor,vValor2,vCampo from ParParametro where iCodGrupo in(16)) asun on m.iCodTipAsuntoSIED = asun.vValor
	left join ParParametro estDoc on estDoc.iCodGrupo=1 and estDoc.iCodParametro=m.siEstDocumento and estDoc.iCodParametro<>0
	left join ParParametro priori on priori.iCodGrupo=3 and priori.iCodParametro=m.siPrioridad and priori.iCodParametro<>0
	where m.siFlagSIED = 1 and m.iCodTrabajador = @icodTrabajador and
	CONCAT(
					m.vCorrelativo, ' ', der.vNombreEntidadSIED , ' ',m.iCodDocumentoRespuestaSIED,' ',iCodRespuestaSIED,
					tipdoc.vCampo, '',tipdoc.vValor2, '', 
					ISNULL(tipdoc.vValor2, ''), ' ',  ISNULL(asun.vCampo, ''), ' ', 
					isnull(priori.vCampo,''),' ',isnull(estDoc.vCampo,'')
			)	
			LIKE '%' + @pDescripcion+ '%' 
    AND 
	m.dFecRegistro between cast(@FechaInicio as datetime)
	and dateadd(ms, -3, (dateadd(day, +1, convert(varchar, cast(@FechaFin as datetime), 101))))
	--ORDER BY estDoc.iOrden asc,priori.iOrden ASC,dFecRegistro desc
	)
	SELECT *,(select count(1) from dataTodo)TotalRegistros  FROM  dataTodo
	order by dFecRegistro desc,OrdenEstado ASC, OrdenPrioridad ASC
	OFFSET (@iPagina-1)*@iTamPagina ROWS
	FETCH NEXT @iTamPagina ROWS ONLY;
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentEnvioSIED]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentEnvioSIED]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentEnvioSIED]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumentEnvioSIED]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
SELECT 
			a.[iCodDocumento],
			a.[iCodSecuencial],
			a.[vCorrelativo],
			a.[iAnio],
			a.[siEstado],
			a.[siOrigen],
			a.[siEstDocumento],
			a.[dFecDocumento],
			a.[dFecRegistro],
			a.[vAsunto],
			a.[siPrioridad],
			a.[vReferencia],
			a.[dFecRecepcion],
			a.[siPlazo],
			a.[dFecPlazo],
			a.[vObservaciones],
			a.[iCodTipDocumentoSIED], 
			a.[iCodTipAsuntoSIED],
			(select  vCampo from parparametro where vValor = a.iCodTipDocumentoSIED and iCodGrupo in (14,15)) vTipoDocumento,
			a.iCodTipDocumentoSIED as iCodTipDocumentoSIED,
			a.[iCodArea],
			d.vDescripcion as vArea,
			a.[iCodTrabajador],
			e.vNombres as vNombreTrab,
			e.vApePaterno as vApePaternoTrab,
			e.vApeMaterno as vApeMaternoTrab,
			e.siFirma,
			ADJ.iCodDocAdjunto,
			a.iCodRespuestaSIED,
			a.iCodDocumentoRespuestaSIED,
			a.iCodCuoRespuestaSIED,
			a.vMensajeSIED,
			a.[vUsuCreacion],
			a.[dFecCreacion],
			a.[vHstCreacion],
			a.[vInsCreacion],
			a.[vLgnCreacion],
			a.[vRolCreacion],
			a.[vUsuActualizacion],
			a.[dFecActualizacion],
			a.[vHstActualizacion],
			a.[vInsActualizacion],
			a.[vLgnActualizacion],
			a.[vRolActualizacion],
			(left(convert(varchar, a.[dFecDocumento], 106),6)+'. '+cast(year(a.[dFecDocumento]) as varchar)+' '+right(convert(varchar, a.[dFecDocumento], 100),7))vFecDocumento,
			tre.vNombres+' '+tre.vApePaterno+' '+tre.vApeMaterno vUsuarioRegistro,
			tre.iCodTrabajador AS CodResponsable,
			CASE WHEN a.siEstDocumento = 3
			THEN 
			(select TOP 1 vComentario from MovDocComentario WHERE iCodDocumento = @iCodDocumento ORDER BY iCodDocComentario DESC)
			ELSE
			''
			END as comentarioAnulacion

		FROM [dbo].[MovDocumento] a
		INNER JOIN [dbo].[MaeArea] d
		ON a.iCodArea=d.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] e
		ON a.iCodTrabajador=e.iCodTrabajador
		LEFT JOIN [SegUsuario] ure on a.vUsuCreacion=ure.WEBUSR and ure.siEstado=1
		inner join [MaeTrabajador] tre on ure.iCodTrabajador=tre.iCodTrabajador
		LEFT JOIN MovDocAdjunto ADJ ON ADJ.iCodDocumento=A.iCodDocumento
		WHERE a.iCodDocumento = @iCodDocumento
END

GO



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivadosEmpresasSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosEmpresasSIED] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosEmpresasSIED]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].ma.[UP_MOV_ObtenerDocumentosAnexos]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN
	SELECT        
		dd.iCodDocumentoDerivacion, 
		dd.iCodDocumento, 
		dd.siEstado, 
		dd.dFecDerivacion, 
		dd.vNombreEntidadSIED,
		dd.vRucSIED,
		dd.iFormatoDocumentoSIED,
		dd.iCodEntidadSIED,		
		dd.vUsuCreacion, 
		dd.dFecCreacion, 
		dd.vHstCreacion, 
		dd.vInsCreacion, 
		dd.vLgnCreacion, 
		dd.vRolCreacion, 
		dd.vUsuActualizacion, 
		dd.dFecActualizacion, 
		dd.vHstActualizacion, 
		dd.vInsActualizacion, 
		dd.vLgnActualizacion, 
		dd.vRolActualizacion,
		dd.siTipoAcceso 
	FROM            
		MovDocumentoDerivacion dd
		left join MaeArea ma on dd.iCodArea=ma.iCodArea
		left join MaeTrabajador mt on dd.iCodResponsable=mt.iCodTrabajador
		LEFT JOIN SegUsuario SEG ON MT.iCodTrabajador=SEG.iCodTrabajador
	WHERE dd.iCodDocumento = @iCodDocumento
		AND  dd.siEstado = 1 and dd.siTipoAcceso = 3;

END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ActualizarDocumentoEnvioSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ActualizarDocumentoEnvioSIED] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_ActualizarDocumentoEnvioSIED]
 @iCodDocumento int
 ,@siOrigen SMALLINT = NULL 
 ,@siEstDocumento SMALLINT = NULL
 ,@dFecDocumento DATETIME = NULL
 ,@dFecRegistro DATETIME = NULL
 ,@dFecDerivacion DATETIME = NULL
 ,@vAsunto VARCHAR(1000) = NULL
 ,@siPrioridad SMALLINT = NULL
 ,@vReferencia VARCHAR(1000) = NULL
 ,@dFecRecepcion DATETIME = NULL
 ,@siPlazo SMALLINT = NULL
 ,@dFecPlazo DATETIME = NULL
 ,@vObservaciones VARCHAR(1000) = NULL
 ,@iCodTipAsuntoSIED VARCHAR(5)= NULL
 ,@iCodArea INT
 ,@iCodTrabajador INT
 ,@vUsuActualizacion VARCHAR(100) = NULL
 ,@vHstActualizacion VARCHAR(20) = NULL
 ,@vInsActualizacion VARCHAR(50) = NULL
 ,@vLgnActualizacion VARCHAR(20) = NULL
 ,@vRolActualizacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocumento]
-- Objetivo:	Registrar un nuevo Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
--				@iCodSecuencial -  Número de Secuencial
--				@iAnio		  - Anio Correlativo
--				@siOrigen	  - Estado del Registro: 1 - Activo; 0 - Inactivo
--				@vNumDocumento	  - Número de Documento
--				@siEstDocumento	  - Estado del Documento
--				@dFecDocumento	  - Fecha del Documento
--				@dFecRegistro	  - Fecha de Registro
--				@vAsunto			- Asunto
--				@siPrioridad		- Indicador de Prioridad
--				@vReferencia		- Referencia
--				@dFecRecepcion	  - Fecha de Recepción
--				@siPlazo			- Indicador de Plazo
--				@dFecPlazo			- Fecha de Plazo
--				@vObservaciones		- Observaciones
--				@iCodTipDocumento	- Codigo Tipo Documento
--				@iCodRepresentante  - Código de Representante
--				@iCodArea			- Código de Área
--				@iCodTrabajador	    - Codigo de Trabajador
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN
	
	--SELECT * 
	--FROM MaeTrabajador @iCodArea


		SET @onFlagOK = 0;

		UPDATE dbo.MovDocumento
		SET siEstDocumento = @siEstDocumento,
			dFecDocumento = @dFecDocumento,
			--dFecRegistro = @dFecRegistro,
			vAsunto = @vAsunto,
			siPrioridad = @siPrioridad,
			vReferencia = @vReferencia,
		    dFecRecepcion = @dFecRecepcion,
			siPlazo = @siPlazo,
			dFecPlazo = @dFecPlazo,
			vObservaciones = @vObservaciones,
			iCodArea = @iCodArea,
			iCodTrabajador = @iCodTrabajador,
			iCodTipAsuntoSIED = @iCodTipAsuntoSIED,
			vUsuActualizacion = @vUsuActualizacion,
			dFecActualizacion = GETDATE(),
			vHstActualizacion = @@SERVERNAME,
			vInsCreacion = @@SERVICENAME,
			vLgnActualizacion = @vUsuActualizacion,
			vRolActualizacion = @vRolActualizacion,
			dFecDerivacion=@dFecDerivacion
		WHERE iCodDocumento = @iCodDocumento

		SET @onFlagOK = 1
		


 END

GO



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_EliminarDocumentoDerivacionEnvioSIED]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_EliminarDocumentoDerivacionEnvioSIED] AS' 
END
GO

alter PROCEDURE [dbo].[UP_MOV_EliminarDocumentoDerivacionEnvioSIED]
  @iCodDocumentoDerivacion INT,
  @vUsuario varchar(50)
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_CambiarEstadoDocumentoDerivacion]
-- Objetivo:	Cambio Estado Documento Derivación
-- Módulo:		Movimientos
-- Input:		@iCodDocumentoDerivacion - Código de Documento derivación
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN

		UPDATE MovDocumentoDerivacion
		set 		
		siEstado=0,
		dFecActualizacion=getdate(),
		vUsuActualizacion=@vUsuario
		WHERE iCodDocumentoDerivacion = @iCodDocumentoDerivacion AND  siTipoAcceso = 3;


		
		SELECT @onFlagOK=@iCodDocumentoDerivacion
		

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerTotalDerivaciones]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerTotalDerivaciones]
GO

CREATE PROCEDURE [dbo].[UP_MOV_ObtenerTotalDerivaciones]
(
	@iCodDocumento INT,
	@siTipoAcceso INT
)
AS
BEGIN

	SELECT COUNT(*) AS TotalDerivaciones
	FROM [dbo].[MovDocumentoDerivacion] D		
	WHERE D.siEstado = 1
		AND D.siTipoAcceso = @siTipoAcceso
		AND D.iCodDocumento = @iCodDocumento
		AND D.vRucSIED IS NULL
		
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_BandejaEntrada]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[UP_MOV_Listar_BandejaEntrada]
GO

CREATE  PROCEDURE [dbo].[UP_MOV_Listar_BandejaEntrada] (
@vTextoBusqueda VARCHAR(100),
@dFechaDesde datetime,
@dFechaHasta datetime,
@iTipoBandeja int,
@iCodTrabajador int,
@vCodPerfil varchar(50),
@iPagina int,
@iTamPagina int
)
AS
/*
Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3

Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4
*/
BEGIN

	DECLARE @EstadoIngresado INT

	SELECT 
		@EstadoIngresado = icodParametro 
	FROM ParParametro WHERE iCodGrupo=1 AND vValor IN ('ING');

	with areasTrab as
	(
		select iCodArea from [dbo].[MaeTrabajador] where  iCodTrabajador= @iCodTrabajador
	),
	dataTodo as
	(
		SELECT 
			 a.iCodDocumento,a.siEstado AS Estado
			,a.siEstDocumento as EstadoDocumento
			,a.siPrioridad AS Prioridad
			,CASE WHEN siOrigen=1	THEN a.dFecRegistro
									ELSE a.dFecDerivacion END	AS RecibidoDate
			,CASE WHEN siOrigen=1	THEN CONVERT(VARCHAR(10), a.dFecRegistro, 103) 
									ELSE CONVERT(VARCHAR(10), a.dFecDerivacion, 103)  END	AS Recibido
			,CONVERT(VARCHAR(10), a.dFecRecepcion, 103) AS Recepcion
			,a.iCodSecuencial AS Reg
			,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
			,ISNULL(a.vAsunto, '') AS DocumentoAsunto
			,MR.iCodRepresentante AS CodRepresentante
			,ISNULL(mr.vNombres,c.vDescripcion) AS Remitente
			,c.vAbreviatura AS Derivado
			,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
			,a.siPlazo
			,ma.iCodDocumento as ExisteAdjunto
			,a.vCorrelativo as Correlativo
			,E.iCodEmpresa AS CodEmpresa
			,E.vDescripcion AS NombreEmpresa
			,a.dFecRegistro				
			,c.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,isnull(priori.vCampo,'') vEstadoPrioridad
			,estDoc.iOrden OrdenEstado
			,priori.iOrden OrdenPrioridad
			,a.iCodTrabajador
			,d.vNombres+' '+d.vApePaterno+' '+d.vApeMaterno NombreTrabajador			
			,a.dFecPlazo
			,a.siOrigen
			,concat(a.vCorrelativo, ' ', E.vDescripcion , ' ',
					b.vAbreviatura, '',b.vDescripcion, '', ISNULL(a.vNumDocumento, ''), ' ',
					ISNULL(b.vDescripcion, ''), ' ',  ISNULL(a.vAsunto, ''), ' ', 
					isnull(priori.vCampo,''),' ',mr.vNombres) AS FILTRO ,
					f.iCodDocumentoFirma,
					a.siFlagSIED
		FROM [dbo].[MovDocumento] a WITH(NOLOCK)
		--INNER JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		INNER JOIN [dbo].[MaeArea] c WITH(NOLOCK) ON a.iCodArea = c.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] d WITH(NOLOCK) ON a.iCodTrabajador = d.iCodTrabajador
		--LEFT JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON ((a.iCodTipDocumento IS NOT NULL AND a.iCodTipDocumento = b.iCodTipDocumento) OR (a.iCodTipDocumento IS NULL))
		LEFT JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		LEFT join [dbo].[MaeRepresentante] mr WITH(NOLOCK) ON a.iCodRepresentante = mr.iCodRepresentante
		LEFT JOIN MaeEmpresa E WITH(NOLOCK) ON E.siEstado = 1 AND E.iCodEmpresa = mr.iCodEmpresa 
		left join  (SELECT distinct iCodDocumento fROM MovDocAdjunto WITH(NOLOCK)) ma on a.iCodDocumento = ma.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.siEstDocumento and estDoc.iCodParametro<>0
		left join ParParametro priori WITH(NOLOCK) on priori.iCodGrupo=3 and priori.iCodParametro=a.siPrioridad and priori.iCodParametro<>0
	    LEFT JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma  WITH(NOLOCK)GROUP BY iCodDocumento) f on a.iCodDocumento = f.iCodDocumento
		WHERE 
		a.siEstado=1
		and dFecRegistro >= cast(@dFechaDesde as date)
		and dFecRegistro < cast(DATEADD(DAY, 1, @dFechaHasta) as date)		
	),
	dataResponsablesDoc as
	(
		select 	
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DA' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
		from dataTodo a
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.EstadoDocumento and estDoc.iCodParametro<>0
		where
		a.iCodArea in (select iCodArea from areasTrab)
		and
		(
			isnull(a.iCodTrabajador,-1) = @iCodTrabajador
		)		and isnull(siFlagSIED,'') <> 2
	),
	dataResponsablesDerivacion as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DD' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and (a.siFlagSIED <> 1 or a.siFlagSIED is null)
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
		union all
				select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DD' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		INNER JOIN MaeUsuarioSIED SIED ON dd.iCodResponsable = SIED.iCodTrabajador
		--INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and a.siFlagSIED IS NULL
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)


	),
	dataResponsablesDerivacionSIED as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,tipdoc.vCampo AS Documento--a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DDSIED' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		INNER JOIN MovDocumento DOC ON a.iCodDocumento = doc.iCodDocumento
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		LEFT JOIN ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		LEFT JOIN (SELECT iCodParametro,vValor,vValor2,vCampo from ParParametro where iCodGrupo in(14,15)) tipdoc on DOC.iCodTipDocumentoSIED = tipdoc.vValor
		--INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResponsablesCC as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DC' as TipoDocIng
			/*,a.iCodDocumentoFirma*/
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		where
			dd.siEstado=1
			and dd.siTipoAcceso=2
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResultado as
	(
		select *,0 iCodDocumentoDerivacion 
		from dataResponsablesDoc
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		select * 
		from dataResponsablesDerivacion
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDoc
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		SELECT *
		FROM dataResponsablesDerivacionSIED
		WHERE icodDocumento NOT IN(
					SELECT iCodDocumento FROM dataResponsablesDoc
					UNION
					SELECT iCodDocumento FROM dataResponsablesDerivacion
					UNION 
					SELECT iCodDocumento FROM dataResponsablesCC					
				)
		UNION
		select *,0 iCodDocumentoFirma, NULL siFlagSIED, 0 iCodDocumentoDerivacion
		from dataResponsablesCC
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union
					select iCodDocumento from dataResponsablesDerivacionSIED
					union 
					select iCodDocumento from dataResponsablesDoc
				)
	), dataresultadofin as (
	select 
	a.*, 
	dbo.UF_MOV_OBTENERNOMJEFE(iCodArea) Jefe,  
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento=2 THEN 1 ELSE 0 END HabilitarSeguimiento,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarDerivar,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN'))
	THEN 1 ELSE 0 END HabilitarDevolver,
	case when TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarFinalizar,
	case when 
		(TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO')))
		OR
		(TipoDocIng = 'DD' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN')))
	THEN 1 ELSE 0 END HabilitarRegistrarAvance
	--(select count(1) from dataResultado)TotalRegistros 
	from dataResultado A
	where
	upper(A.FILTRO)
				LIKE '%' + upper(@vTextoBusqueda) + '%' 
	AND
	(
			(		--RECIBIDOS
				@iTipoBandeja=1 AND 
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						AND siOrigen=2 AND  (a.siFlagSIED <> 1 or a.siFlagSIED IS NULL)
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen=1 AND a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen IN (3, 4) AND a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
						AND TipoDocIng = 'DDSIED'
					)
					
				)
			)		--CON PLAZO
			OR
			(
				@iTipoBandeja=2 AND 			
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						AND siOrigen=2 and  (a.siFlagSIED <> 1 or a.siFlagSIED IS NULL)
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen=1 and a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen IN (3, 4) AND a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
						AND TipoDocIng = 'DDSIED'
					)
				)
				/*AND
				a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in ('PEN','DER'))
				and datediff(day,getdate(), A.dFecPlazo)>=1 */
				AND A.siPlazo > 0
			)
		)
		)SELECT *,(select count(1) from dataresultadofin)TotalRegistros  FROM  dataresultadofin
	order by dFecRegistro desc
	OFFSET (@iPagina-1)*@iTamPagina ROWS
	FETCH NEXT @iTamPagina ROWS ONLY;
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_AnularDerivacion]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[UP_MOV_AnularDerivacion]
GO

CREATE PROCEDURE [dbo].[UP_MOV_AnularDerivacion]
(
	@iCodDocumento INT,
	@iCodDocumentoDerivacion INT,
	@vUsuCreacion VARCHAR(100) = NULL,
	@vHstCreacion VARCHAR(20) = NULL,
	@vInsCreacion VARCHAR(50) = NULL,
	@vLgnCreacion VARCHAR(20) = NULL,
	@vRolCreacion VARCHAR(20) = NULL,
	@onFlagOK INT OUTPUT
)
AS
BEGIN
	
	--DECLARE @onFlagOK INT
	DECLARE @siEstadoAntiguo INT
	DECLARE @newEstadoDocumentoDerivacion INT = 3
	DECLARE @iCodArea INT
	DECLARE @iCodTrabajador INT

	SELECT 
		@siEstadoAntiguo = siEstadoDerivacion,
		@iCodArea = iCodArea,
		@iCodTrabajador = iCodResponsable
	FROM MovDocumentoDerivacion
	

	UPDATE MovDocumentoDerivacion
	SET siEstadoDerivacion = 3,--ANULADO,
		vUsuActualizacion = @vUsuCreacion,
		dFecActualizacion = GETDATE(),
		vHstActualizacion = @@SERVERNAME,
		vInsActualizacion = @@SERVICENAME,
		vLgnCreacion = vLgnCreacion,
		vRolCreacion = vRolCreacion
	WHERE iCodDocumentoDerivacion = @iCodDocumentoDerivacion
		AND iCodDocumento = @iCodDocumento

	 EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
	 @iCodDocumento, @iCodDocumentoDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
	 @siEstadoAntiguo/*@siEstadoDerivacionPendiente*/,2/*@tipoMovimiento*/,@newEstadoDocumentoDerivacion /*@iEstadoDocumento*/,
	 @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
	 @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT
	 
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumentoRecepcionSIED]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocumentoRecepcionSIED]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocumentoRecepcionSIED]
  @iCodSecuencial INT = NULL--2
 ,@vCorrelativo VARCHAR(100)= NULL--3
 ,@iAnio INT = NULL--4
 ,@siEstado SMALLINT --5
 ,@siOrigen SMALLINT = NULL --6
 ,@vNumDocumento VARCHAR(100) = NULL--7
 ,@siEstDocumento SMALLINT = NULL--8
 ,@dFecDocumento DATETIME = NULL--9
 ,@dFecRegistro DATETIME = NULL--10
 ,@dFecDerivacion DATETIME=NULL
 ,@vAsunto VARCHAR(1000) = NULL--11
 ,@siPrioridad SMALLINT = NULL--12
 ,@vReferencia VARCHAR(1000) = NULL--13
 ,@dFecRecepcion DATETIME = NULL--14
 ,@siPlazo SMALLINT = NULL--15
 ,@dFecPlazo DATETIME = NULL--16
 ,@vObservaciones VARCHAR(1000) = NULL--17
 ,@iCodTipDocumento INT --18
 ,@iCodRepresentante INT--19
 ,@iCodArea INT--20
 ,@iCodTrabajador INT--21
 ,@siFlagSied INT
 ,@vUsuCreacion VARCHAR(100) = NULL--22
 ,@vHstCreacion VARCHAR(20) = NULL--23
 ,@vInsCreacion VARCHAR(50) = NULL--24
 ,@vLgnCreacion VARCHAR(20) = NULL--25
 ,@vRolCreacion VARCHAR(20) = NULL--26
 ,@onFlagOK INT OUTPUT--27
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocumento]
-- Objetivo:	Registrar un nuevo Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
--				@iCodSecuencial -  Número de Secuencia
--				@iAnio		  - Anio Correlativo
--				@siOrigen	  - Estado del Registro: 1 - Activo; 0 - Inactivo
--				@vNumDocumento	  - Número de Documento
--				@siEstDocumento	  - Estado del Documento
--				@dFecDocumento	  - Fecha del Documento
--				@dFecRegistro	  - Fecha de Registro
--				@vAsunto			- Asunto
--				@siPrioridad		- Indicador de Prioridad
--				@vReferencia		- Referencia
--				@dFecRecepcion	  - Fecha de Recepción
--				@siPlazo			- Indicador de Plazo
--				@dFecPlazo			- Fecha de Plazo
--				@vObservaciones		- Observaciones
--				@iCodTipDocumento	- Codigo Tipo Documento
--				@iCodRepresentante  - Código de Representante
--				@iCodArea			- Código de Área
--				@iCodTrabajador	    - Codigo de Trabajador
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

	

		INSERT INTO dbo.MovDocumento(iCodSecuencial,iAnio,siEstado,
									siOrigen,vNumDocumento,siEstDocumento,
									dFecDocumento,dFecRegistro,vAsunto,
									siPrioridad,vReferencia,dFecRecepcion,
									siPlazo,dFecPlazo,vObservaciones,
									iCodTipDocumento,iCodRepresentante,iCodArea,
									iCodTrabajador,siFlagSIED, vUsuCreacion,dFecCreacion,
									vHstCreacion,vInsCreacion,vLgnCreacion,
									vRolCreacion,vCorrelativo,dFecDerivacion,siFlagFirmando)
									VALUES(									
									@iCodSecuencial,@iAnio,1,
									@siOrigen,@vNumDocumento,@siEstDocumento,
									@dFecDocumento,getdate(),@vAsunto,
									@siPrioridad,@vReferencia,@dFecRecepcion,
									@siPlazo,@dFecPlazo,@vObservaciones,
									@iCodTipDocumento,@iCodRepresentante,@iCodArea,
									@iCodTrabajador,@siFlagSIED,@vUsuCreacion,GETDATE(),
									@@SERVERNAME,@@servicename,@vLgnCreacion,
									@vRolCreacion,@vCorrelativo,@dFecDerivacion,0
									)




		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocumento) FROM dbo.MovDocumento 
			ELSE
				SET @onFlagOK=0


 END

GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_BandejaRecepcionSIED]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_BandejaRecepcionSIED]
END
GO

 CREATE PROCEDURE [dbo].[UP_MOV_Listar_BandejaRecepcionSIED] (
@vTextoBusqueda VARCHAR(100),
@dFechaDesde datetime,
@dFechaHasta datetime,
@iTipoBandeja int,
@iCodTrabajador int,
@vCodPerfil varchar(50),
@iPagina int,
@iTamPagina int
)
AS
/*
Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3

Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4
*/
BEGIN

	DECLARE @EstadoIngresado INT

	SELECT 
		@EstadoIngresado = icodParametro 
	FROM ParParametro WHERE iCodGrupo=1 AND vValor IN ('ING');

	with areasTrab as
	(
		select iCodArea from [dbo].[MaeTrabajador] where  iCodTrabajador= @iCodTrabajador
	),
	dataTodo as
	(
		SELECT 
			 a.iCodDocumento,a.siEstado AS Estado
			,a.siEstDocumento as EstadoDocumento
			,a.siPrioridad AS Prioridad
			,CASE WHEN siOrigen=1	THEN a.dFecRegistro
									ELSE a.dFecDerivacion END	AS RecibidoDate
			,CASE WHEN siOrigen=1	THEN CONVERT(VARCHAR(10), a.dFecRegistro, 103) 
									ELSE CONVERT(VARCHAR(10), a.dFecDerivacion, 103)  END	AS Recibido
			,CONVERT(VARCHAR(10), a.dFecRecepcion, 103) AS Recepcion
			,a.iCodSecuencial AS Reg
			,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
			,ISNULL(a.vAsunto, '') AS DocumentoAsunto
			,MR.iCodRepresentante AS CodRepresentante
			,ISNULL(mr.vNombres,c.vDescripcion) AS Remitente
			,c.vAbreviatura AS Derivado
			,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
			,a.siPlazo
			,ma.iCodDocumento as ExisteAdjunto
			,a.vCorrelativo as Correlativo
			,E.iCodEmpresa AS CodEmpresa
			,E.vDescripcion AS NombreEmpresa
			,a.dFecRegistro				
			,c.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,isnull(priori.vCampo,'') vEstadoPrioridad
			,estDoc.iOrden OrdenEstado
			,priori.iOrden OrdenPrioridad
			,a.iCodTrabajador
			,d.vNombres+' '+d.vApePaterno+' '+d.vApeMaterno NombreTrabajador			
			,a.dFecPlazo
			,a.siOrigen
			,concat(a.vCorrelativo, ' ', E.vDescripcion , ' ',
					b.vAbreviatura, '',b.vDescripcion, '', ISNULL(a.vNumDocumento, ''), ' ',
					ISNULL(b.vDescripcion, ''), ' ',  ISNULL(a.vAsunto, ''), ' ', 
					isnull(priori.vCampo,''),' ',mr.vNombres) AS FILTRO ,
					f.iCodDocumentoFirma,
					a.siFlagSIED
		FROM [dbo].[MovDocumento] a WITH(NOLOCK)
		--INNER JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		INNER JOIN [dbo].[MaeArea] c WITH(NOLOCK) ON a.iCodArea = c.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] d WITH(NOLOCK) ON a.iCodTrabajador = d.iCodTrabajador
		--LEFT JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON ((a.iCodTipDocumento IS NOT NULL AND a.iCodTipDocumento = b.iCodTipDocumento) OR (a.iCodTipDocumento IS NULL))
		LEFT JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		LEFT join [dbo].[MaeRepresentante] mr WITH(NOLOCK) ON a.iCodRepresentante = mr.iCodRepresentante
		LEFT JOIN MaeEmpresa E WITH(NOLOCK) ON E.siEstado = 1 AND E.iCodEmpresa = mr.iCodEmpresa 
		left join  (SELECT distinct iCodDocumento fROM MovDocAdjunto WITH(NOLOCK)) ma on a.iCodDocumento = ma.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.siEstDocumento and estDoc.iCodParametro<>0
		left join ParParametro priori WITH(NOLOCK) on priori.iCodGrupo=3 and priori.iCodParametro=a.siPrioridad and priori.iCodParametro<>0
	    LEFT JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma  WITH(NOLOCK)GROUP BY iCodDocumento) f on a.iCodDocumento = f.iCodDocumento
		WHERE 
		a.siEstado=1
		and dFecRegistro >= cast(@dFechaDesde as date)
		and dFecRegistro < cast(DATEADD(DAY, 1, @dFechaHasta) as date) and a.siFlagSIED = 2		 
	),
	dataResponsablesDoc as
	(
		select 	
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DA' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
		from dataTodo a
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.EstadoDocumento and estDoc.iCodParametro<>0
		where
		a.iCodArea in (select iCodArea from areasTrab)
		and
		(
			isnull(a.iCodTrabajador,-1) = @iCodTrabajador
		)
	),
	dataResponsablesDerivacion as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DD' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResponsablesDerivacionSIED as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DDSIED' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		--INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResponsablesCC as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DC' as TipoDocIng
			/*,a.iCodDocumentoFirma*/
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		where
			dd.siEstado=1
			and dd.siTipoAcceso=2
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResultado as
	(
		select *,0 iCodDocumentoDerivacion 
		from dataResponsablesDoc
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		select * 
		from dataResponsablesDerivacion
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDoc
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		SELECT *
		FROM dataResponsablesDerivacionSIED
		WHERE icodDocumento NOT IN(
					SELECT iCodDocumento FROM dataResponsablesDoc
					UNION
					SELECT iCodDocumento FROM dataResponsablesDerivacion
					UNION 
					SELECT iCodDocumento FROM dataResponsablesCC					
				)
		UNION
		select *,0 iCodDocumentoFirma, NULL siFlagSIED, 0 iCodDocumentoDerivacion
		from dataResponsablesCC
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union
					select iCodDocumento from dataResponsablesDerivacionSIED
					union 
					select iCodDocumento from dataResponsablesDoc
				)
	), dataresultadofin as (
	select 
	a.*, 
	dbo.UF_MOV_OBTENERNOMJEFE(iCodArea) Jefe,  
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento=2 THEN 1 ELSE 0 END HabilitarSeguimiento,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarDerivar,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN'))
	THEN 1 ELSE 0 END HabilitarDevolver,
	case when TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarFinalizar,
	case when 
		(TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO')))
		OR
		(TipoDocIng = 'DD' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN')))
	THEN 1 ELSE 0 END HabilitarRegistrarAvance
	--(select count(1) from dataResultado)TotalRegistros 
	from dataResultado A
	where
	upper(A.FILTRO)
				LIKE '%' + upper(@vTextoBusqueda) + '%' 
	AND
	(
			(		--RECIBIDOS
				@iTipoBandeja=1 AND 
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						AND siOrigen=2 AND a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen=1 AND a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('PEN','DEV','PRO','ATE','DER','ANU'))
					)
					
				)
			)		--CON PLAZO
			OR
			(
				@iTipoBandeja=2 AND 			
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						AND siOrigen=2 and a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen=1 and a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen IN (3, 4) AND a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
						AND TipoDocIng = 'DDSIED'
					)
				)
				/*AND
				a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in ('PEN','DER'))
				and datediff(day,getdate(), A.dFecPlazo)>=1 */
				AND A.siPlazo > 0
			)
		)
		)SELECT *,(select count(1) from dataresultadofin)TotalRegistros  FROM  dataresultadofin
	order by dFecRegistro desc
	OFFSET (@iPagina-1)*@iTamPagina ROWS
	FETCH NEXT @iTamPagina ROWS ONLY;
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentoRecepcionSIED]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentoRecepcionSIED]
END
GO
CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentoRecepcionSIED]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumento]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
		SELECT 
			a.[iCodDocumento],
			a.[iCodSecuencial],
			a.[vCorrelativo],
			a.[iAnio],
			a.[siEstado],
			a.[siOrigen],
			a.[vNumDocumento],
			a.[siEstDocumento],
			a.[dFecDocumento],
			a.[dFecRegistro],
			a.[vAsunto],
			a.[siPrioridad],
			a.[vReferencia],
			a.[dFecRecepcion],
			a.[siPlazo],
			a.[dFecPlazo],
			a.[vObservaciones],
			a.[iCodTipDocumento],
			b.vDescripcion as vTipoDocumento,
			a.[iCodRepresentante],
			c.vNombres as vRepresentante,
			a.[iCodArea],
			d.vDescripcion as vArea,
			a.[iCodTrabajador],
			e.vNombres as vNombreTrab,
			e.vApePaterno as vApePaternoTrab,
			e.vApeMaterno as vApeMaternoTrab,
			e.siFirma,
			em.iCodEmpresa,
			em.vDescripcion as vDescripcionEmpresa,
			ADJ.iCodDocAdjunto,
			a.[vUsuCreacion],
			a.[dFecCreacion],
			a.[vHstCreacion],
			a.[vInsCreacion],
			a.[vLgnCreacion],
			a.[vRolCreacion],
			a.[vUsuActualizacion],
			a.[dFecActualizacion],
			a.[vHstActualizacion],
			a.[vInsActualizacion],
			a.[vLgnActualizacion],
			a.[vRolActualizacion],
			(left(convert(varchar, a.[dFecDocumento], 106),6)+'. '+cast(year(a.[dFecDocumento]) as varchar)+' '+right(convert(varchar, a.[dFecDocumento], 100),7))vFecDocumento,
			tre.vNombres+' '+tre.vApePaterno+' '+tre.vApeMaterno vUsuarioRegistro,
			tre.iCodTrabajador AS CodResponsable,
			iCodUltimoMovimiento = (select max(iCodDocMovimiento) from MovDocMovimiento
									where iCodDocumento=@iCodDocumento
									and siEstado=1
									),
			(select isnull(max(icoddocumento),0) from MovDocumentoFirma where iCodDocumento = @iCodDocumento) vDocumentoFirmado,
			(select isnull(max(icoddocumento),0) from MovDocumentoFirma where iCodDocumento = @iCodDocumento) vDocumentoFirmado,
			CASE WHEN a.siEstDocumento = 3
			THEN 
			(select TOP 1 vComentario from MovDocComentario WHERE iCodDocumento = @iCodDocumento ORDER BY iCodDocComentario DESC)
			ELSE
			''
			END as comentarioAnulacion
		FROM [dbo].[MovDocumento] a
		INNER JOIN [dbo].[MaeTipDocumento] b
		ON a.iCodTipDocumento=b.iCodTipDocumento
		LEFT JOIN [dbo].[MaeRepresentante] c
		ON a.iCodRepresentante=c.iCodRepresentante
		INNER JOIN [dbo].[MaeArea] d
		ON a.iCodArea=d.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] e
		ON a.iCodTrabajador=e.iCodTrabajador
		LEFT JOIN [dbo].[MaeEmpresa] em
		on c.iCodEmpresa = em.iCodEmpresa
		LEFT JOIN [SegUsuario] ure on a.vUsuCreacion=ure.WEBUSR and ure.siEstado=1
		inner join [MaeTrabajador] tre on ure.iCodTrabajador=tre.iCodTrabajador
		LEFT JOIN MovDocAdjunto ADJ ON ADJ.iCodDocumento=A.iCodDocumento
		WHERE a.iCodDocumento=@iCodDocumento AND a.siFlagSIED = 2;
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivados]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivados]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivados]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].ma.[UP_MOV_ObtenerDocumentosAnexos]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN

	SELECT DISTINCT     
	dd.iCodDocumentoDerivacion, 
	dd.iCodDocumento, 
	dd.siEstado, 
	dd.iCodArea, 
	dd.iCodResponsable, 
	dd.siEstadoDerivacion, 
	dd.siTipoAcceso, 
	dd.dFecDerivacion, 
	dd.vUsuCreacion, 
	dd.dFecCreacion, 
	dd.vHstCreacion, 
	dd.vInsCreacion, 
	dd.vLgnCreacion, 
	dd.vRolCreacion, 
	dd.vUsuActualizacion, 
	dd.dFecActualizacion, 
	dd.vHstActualizacion, 
	dd.vInsActualizacion, 
	dd.vLgnActualizacion, 
	dd.vRolActualizacion,
	ma.vDescripcion as NombreArea,
	mt.vNombres+' '+mt.vApePaterno+' '+mt.vApeMaterno as NombreResponsable,
	SEG.WEBUSR WEBUSRRESPONSABLE,
	DC.vComentario
	FROM            
	MovDocumentoDerivacion dd
	left join MaeArea ma on dd.iCodArea=ma.iCodArea
	left join MaeTrabajador mt on dd.iCodResponsable=mt.iCodTrabajador
	LEFT JOIN SegUsuario SEG ON MT.iCodTrabajador=SEG.iCodTrabajador
	LEFT JOIN MovDocComentario DC ON DD.iCodDocumento = DC.iCodDocumento AND DD.iCodDocumentoDerivacion = DC.iCodDocumentoDerivacion AND DC.siEstado = 1
	AND DC.vComentario NOT IN ( SELECT 'MOTIVO FIRMA : ' + vCampo from ParParametro where iCodGrupo = 12)
	WHERE dd.iCodDocumento = @iCodDocumento 
	and  dd.siEstado = 1 and dd.siTipoAcceso <>3;

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDetalleCorreoAnularEnvioSIED]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoAnularEnvioSIED]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoAnularEnvioSIED]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumento]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
			SELECT	  a.iCodDocumento,
			a.vCorrelativo,
			a.siPrioridad,
			a.siOrigen,
			T.vNombres,
			T.vApePaterno,
			T.vApeMaterno
		FROM [dbo].[MovDocumento] a
		INNER JOIN SegUsuario S ON S.WEBUSR = a.vUsuCreacion
		INNER JOIN MaeTrabajador T ON T.iCodTrabajador = S.iCodTrabajador
		--INNER JOIN (SELECT TOP 1 iCodDocumento,vComentario FROM MovDocComentario WHERE iCodDocumentoDerivacion IS NULL AND iCodDocumento = @iCodDocumento ORDER BY iCodDocComentario DESC) co on a.iCodDocumento = co.iCodDocumento
		where a.iCodDocumento=@iCodDocumento
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_EmailResponsable_PorCodDocumento]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_Listar_EmailResponsable_PorCodDocumento]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_Listar_EmailResponsable_PorCodDocumento]
@CodDocumento AS INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Empresa]
-- Objetivo:	Listar Empresas
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de Empresas	
-- Create date: 04/02/20
-- =============================================
BEGIN
	
	SELECT T.vEmail FROM MovDocumento M
	INNER JOIN MaeTrabajador T ON  M.iCodTrabajador =  T.iCodTrabajador AND T.siEstado = 1
	WHERE M.iCodDocumento = @CodDocumento

 END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerOrigen]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerOrigen]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_ObtenerOrigen]
  @iCodDocumento INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerOrigen]]
-- Objetivo:	Obtener el origen del documento
-- Módulo:		documento
-- Input:		@iCodDocumento - Código de Documento
-- Create date: 20/01/21
-- =============================================
BEGIN
	SELECT siOrigen FROM MovDocumento WHERE iCodDocumento = @iCodDocumento;
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentEnvioSIED_ServiceSIED]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentEnvioSIED_ServiceSIED]
END
GO
CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentEnvioSIED_ServiceSIED]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumentEnvioSIED]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
		SELECT 
		m.vCorrelativo,
		m.iCodTipDocumentoSIED,
		m.vAsunto,
		m.iCodTipAsuntoSIED,
		m.siOrigen
		FROM MovDocumento m
		WHERE m.iCodDocumento = @iCodDocumento
END

GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ActualizarDocumentoEnvioSIEDService]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ActualizarDocumentoEnvioSIEDService]
END
GO
CREATE PROCEDURE [dbo].[UP_MOV_ActualizarDocumentoEnvioSIEDService]
 @iCodDocumento int,
 @iCodRespuestaSIED varchar(20),
 @iCodDocumentoRespuestaSIED int,
 @vCodCUORespuestaSIED varchar(100)  = NULL,
 @vMensajeSIED varchar(250),
 @vUsuActualizacion VARCHAR(100) = NULL,
 @vRolActualizacion VARCHAR(20) = NULL,
 @onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ActualizarDocumentoEnvioSIEDService]
-- Objetivo:	Actualizar los campos respuesta del servicio SIED
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
--				@iCodRespuestaSIED -  Código respuesta SIED
--				@iCodDocumentoRespuestaSIED		  - código del documento respuesta SIED
--				@vCodCUORespuestaSIED	  - Código CUO Respuesta SIED
--				@vMensajeSIED	  - Mensaje Respuesta SIED
--				@vUsuActualizacion	  - Usuario Actualización
--				@vRolActualizacion	  - Rol Actualización
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN
	
	--SELECT * 
	--FROM MaeTrabajador @iCodArea


		SET @onFlagOK = 0;

		UPDATE dbo.MovDocumento
		SET 
			iCodRespuestaSIED = @iCodRespuestaSIED,
			iCodDocumentoRespuestaSIED = @iCodDocumentoRespuestaSIED,
			iCodCuoRespuestaSIED = @vCodCUORespuestaSIED,
			vMensajeSIED = @vMensajeSIED,
			vUsuActualizacion = @vUsuActualizacion,
			dFecActualizacion = GETDATE(),
			vHstActualizacion = @@SERVERNAME,
			vInsCreacion = @@SERVICENAME,
			vLgnActualizacion = @vUsuActualizacion,
			vRolActualizacion = @vRolActualizacion

		WHERE iCodDocumento = @iCodDocumento

		SET @onFlagOK = 1
		
 END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerCodLaserFIcheAnexosPorCodDocumento]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerCodLaserFIcheAnexosPorCodDocumento]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_ObtenerCodLaserFIcheAnexosPorCodDocumento]
  @CodDocumento INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerCodDocumentoPorCodLaserFIche]
-- Objetivo:	Obtener el Id de documento por codigo Laserfiche
-- Módulo:		Movimientos
-- Input:		@@iCodDocumento - Código de Documento 
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 19/02/2020
-- =============================================
BEGIN
	
	SELECT 
	iCodLaserfiche
	FROM MovDocAnexo
	WHERE iCodDocumento = @CodDocumento


 END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_AreaSIED]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_Listar_AreaSIED]
END
GO


CREATE PROCEDURE [dbo].[UP_MAE_Listar_AreaSIED]
AS
-- =============================================
-- Nombre:	[dbo].[[UP_MAE_Listar_AreaSIED]]
-- Objetivo:	Listar Áreas
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de Áreas	
-- Create date: 30/01/20
-- =============================================
BEGIN
	
	SELECT
		a.iCodArea,
		a.vDescripcion,
		a.siEstado
	FROM MaeUsuarioSIED s
	INNER JOIN SegUsuario SE ON S.WEBUSR = SE.WEBUSR
	INNER JOIN MaeTrabajador tr on tr.iCodTrabajador = se.iCodTrabajador
	INNER JOIN MaeArea a ON tr.iCodArea = a.iCodArea
	WHERE a.siEstado = 1;
	
 
 END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ListarTrabajadorPorAreaSIED]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_ListarTrabajadorPorAreaSIED]
END
GO

CREATE PROCEDURE [dbo].[UP_MAE_ListarTrabajadorPorAreaSIED]
AS
BEGIN
	SELECT 
		T.[iCodTrabajador], T.[vNumDocumento], T.[vNombres], T.[vCargo], T.[vUbicacion], T.[iAnexo], T.[vEmail],
		T.[vApePaterno], T.[vApeMaterno], T.[siEsJefe], T.[iCodArea], T.[iCodTipDocIdentidad], T.[vCelular],
		U.WEBUSR
	FROM [dbo].[MaeTrabajador] T
		INNER JOIN DBO.SegUsuario U
		ON U.iCodTrabajador = T.iCodTrabajador
		INNER JOIN MaeUsuarioSIED s on t.iCodTrabajador = s.iCodTrabajador
	WHERE 
	 U.siEstado= 1
	AND T.siEstado = 1

END

GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_Documento]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_Documento]
END
GO
CREATE PROCEDURE [dbo].[UP_MOV_Listar_Documento] (
@pDescripcion VARCHAR(100),
@FechaInicio DATETIME ,
@FechaFin DATETIME 
)
AS
/*

Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3


Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4

*/
BEGIN
	SELECT a.iCodDocumento,a.siEstado AS Estado
	    ,a.siEstDocumento as EstadoDocumento
		,a.siPrioridad AS Prioridad
		,CONVERT(VARCHAR(10), a.dFecRegistro, 103) AS Recibido
		,CONVERT(VARCHAR(10), a.dFecRecepcion, 103) AS Recepcion
		,a.iCodSecuencial AS Reg
		,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
		,ISNULL(a.vAsunto, '') AS DocumentoAsunto
		,MR.iCodRepresentante AS CodRepresentante
		,mr.vNombres AS Remitente
		,c.vAbreviatura AS Derivado
		,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
		,ma.iCodDocumento as ExisteAdjunto
		,a.vCorrelativo as Correlativo
		,E.iCodEmpresa AS CodEmpresa
		,E.vDescripcion AS NombreEmpresa
		, d.vNombres+' '+d.vApePaterno+' '+d.vApeMaterno NombreTrabajador
		,dbo.UF_MOV_OBTENERNOMJEFE(a.iCodArea) Jefe
		,isnull(estDoc.vCampo,'') vEstadoDescripcion
		,isnull(priori.vCampo,'') vEstadoPrioridad
		,A.vUsuCreacion
	FROM [dbo].[MovDocumento] a
	INNER JOIN [dbo].[MaeTipDocumento] b ON a.iCodTipDocumento = b.iCodTipDocumento
	INNER JOIN [dbo].[MaeArea] c ON a.iCodArea = c.iCodArea
	INNER JOIN [dbo].[MaeTrabajador] d ON a.iCodTrabajador = d.iCodTrabajador
	inner join [dbo].[MaeRepresentante] mr ON a.iCodRepresentante = mr.iCodRepresentante
	INNER JOIN MaeEmpresa E ON E.siEstado = 1 AND E.iCodEmpresa = mr.iCodEmpresa 
	left join  (SELECT distinct iCodDocumento fROM MovDocAdjunto) ma on a.iCodDocumento = ma.iCodDocumento
	left join ParParametro estDoc on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.siEstDocumento and estDoc.iCodParametro<>0
	left join ParParametro priori on priori.iCodGrupo=3 and priori.iCodParametro=a.siPrioridad and priori.iCodParametro<>0
	WHERE
		CONCAT(
					a.vCorrelativo, ' ', E.vDescripcion , ' ',
					b.vAbreviatura, '',b.vDescripcion, '', ISNULL(a.vNumDocumento, ''), ' ',
					ISNULL(b.vDescripcion, ''), ' ',  ISNULL(a.vAsunto, ''), ' ', 
					isnull(priori.vCampo,''),' ',isnull(estDoc.vCampo,''),' ',mr.vNombres
			)	
			LIKE '%' + @pDescripcion+ '%' 
    AND 
	a.dFecRegistro between cast(@FechaInicio as datetime)
	and dateadd(ms, -3, (dateadd(day, +1, convert(varchar, cast(@FechaFin as datetime), 101))))
	and a.siFlagSIED IS NULL
	ORDER BY dFecRegistro desc

END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDetalleCorreoDerivacion]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoDerivacion]
END
GO

 CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoDerivacion]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumento]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
		SELECT	  a.iCodDocumento,
			a.siPlazo,
			a.dFecPlazo,
			a.siPrioridad,
			a.siOrigen,
			CASE WHEN A.siOrigen = 2  THEN R.vNombres
				 WHEN A.siOrigen in (1,3,4) THEN (SELECT  TJEFE.vApePaterno + ' '+ TJEFE.vApeMaterno + ' '  + TJEFE.vNombres
											FROM MaeTrabajador TJEFE WHERE iCodArea = ARE.iCodArea AND siEsJefe = 1)   END AS REPRESENTANTE,
			CASE WHEN A.siOrigen = 2  THEN  E.vDescripcion
				 WHEN A.siOrigen in (1,3,4) THEN ARE.vDescripcion END AS EMPRESA,
			T.vApePaterno + ' '+ T.vApeMaterno + ' '  + t.vNombres AS DERIVADOPOR,
			par.vCampo TipoAccion
		FROM [dbo].[MovDocumento] a
		INNER JOIN MovDocumentoDerivacion DER ON A.iCodDocumento =  DER.iCodDocumento AND DER.siTipoAcceso = 1 --AND ISNULL(A.vUsuActualizacion,A.vUsuCreacion) = ISNULL(DER.vUsuActualizacion,DER.vUsuCreacion)
		LEFT JOIN MaeRepresentante r on a.iCodRepresentante = r.iCodRepresentante
		INNER JOIN SegUsuario S ON S.WEBUSR = DER.vUsuCreacion
		INNER JOIN MaeTrabajador T ON T.iCodTrabajador = S.iCodTrabajador
		LEFT JOIN MaeEmpresa E ON R.iCodEmpresa = E.iCodEmpresa
		LEFT JOIN MaeArea ARE ON T.iCodArea = ARE.iCodArea
		LEFT JOIN (SELECT * FROM ParParametro where iCodGrupo=8) PAR ON PAR.iCodParametro = der.iTipoAccion
		where a.iCodDocumento=@iCodDocumento
		ORDER BY  DER.dFecCreacion DESC
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_AnularDerivacion]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_AnularDerivacion]
END
GO
CREATE PROCEDURE [dbo].[UP_MOV_AnularDerivacion]
(
	@iCodDocumento INT,
	@iCodDocumentoDerivacion INT,
	@vUsuCreacion VARCHAR(100) = NULL,
	@vHstCreacion VARCHAR(20) = NULL,
	@vInsCreacion VARCHAR(50) = NULL,
	@vLgnCreacion VARCHAR(20) = NULL,
	@vRolCreacion VARCHAR(20) = NULL,
	@onFlagOK INT OUTPUT
)
AS
BEGIN
	
	--DECLARE @onFlagOK INT
	DECLARE @siEstadoAntiguo INT
	DECLARE @newEstadoDocumentoDerivacion INT = 3
	DECLARE @iCodArea INT
	DECLARE @iCodTrabajador INT

	SELECT 
		@siEstadoAntiguo = siEstadoDerivacion,
		@iCodArea = iCodArea,
		@iCodTrabajador = iCodResponsable
	FROM MovDocumentoDerivacion
	

	UPDATE MovDocumentoDerivacion
	SET siEstadoDerivacion = 3,--ANULADO,
		vUsuActualizacion = @vUsuCreacion,
		dFecActualizacion = GETDATE(),
		vHstActualizacion = @@SERVERNAME,
		vInsActualizacion = @@SERVICENAME,
		vLgnCreacion = vLgnCreacion,
		vRolCreacion = vRolCreacion
	WHERE iCodDocumentoDerivacion = @iCodDocumentoDerivacion
		AND iCodDocumento = @iCodDocumento

	 EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
	 @iCodDocumento, @iCodDocumentoDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
	 @newEstadoDocumentoDerivacion/*@siEstadoDerivacionPendiente*/,2/*@tipoMovimiento*/,@newEstadoDocumentoDerivacion /*@iEstadoDocumento*/,
	 @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
	 @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT
	 
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_CabeceraSeguimiento]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimiento]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimiento]
  @iCodDocumento INT 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_Listar_CabeceraSeguimiento]
-- Objetivo:	Lista la cabecera de los seguimientos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Create date: 27/02/20
-- =============================================
BEGIN

WITH SEGUIMIENTO AS (
	
	SELECT
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	ARE.vDescripcion AS AREADESDE,
	TRAPARA.vApePaterno + ' ' + TRAPARA.vApeMaterno +  ' ' + TRAPARA.vNombres AS NOMBREPARA,
	AREPARA.vDescripcion AS AREAPARA,
	mov.dFecCreacion AS FECHA,
	MOV.iEstadoDocumento,
	MOV.iCodDocMovimiento,
	CASE WHEN MOV.iEstadoDocumento  IN (5,7) THEN doc.iCodDocumento ELSE 0  END iCodDocAuxiliar
	FROM MovDocMovimiento MOV
	INNER JOIN MovDocumento DOC ON MOV.iCodDocumento = DOC.iCodDocumento
	INNER JOIN SegUsuario USU on DOC.vUsuCreacion=USU.WEBUSR and USU.siEstado=1
	INNER JOIN MaeTrabajador TRA ON USU.iCodTrabajador = TRA.iCodTrabajador 
	INNER JOIN MaeArea ARE ON TRA.iCodArea = ARE.iCodArea
	INNER JOIN MaeArea AREPARA ON MOV.iCodArea = AREPARA.iCodArea 
	INNER JOIN MaeTrabajador TRAPARA ON  MOV.iCodTrabajador = TRAPARA.iCodTrabajador 
	WHERE MOV.iCodDocumento = @iCodDocumento AND iEstadoDocumento <> 6 AND Mov.iCodDocDerivacion is null 

	UNION ALL

	SELECT DISTINCT
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	ARE.vDescripcion AS AREADESDE,
	TRAPARA.vApePaterno + ' ' + TRAPARA.vApeMaterno +  ' ' + TRAPARA.vNombres AS NOMBREPARA,
	AREPARA.vDescripcion AS AREAPARA,
	/*ISNULL(DOC.dFecActualizacion,DOC.dFecCreacion) AS FECHA,*/
	(SELECT TOP 1 r.dFecCreacion FROM MovDocMovimiento r WHERE r.iCodDocumento = @iCodDocumento and iEstadoDocumento = 6 order by iCodDocMovimiento desc) as FECHA,
	MOV.iEstadoDocumento,
	0 iCodDocMovimiento,
	MOV.iCodDocumento iCodDocAuxiliar
	FROM MovDocumento DOC 
	INNER JOIN MovDocMovimiento MOV ON MOV.iCodDocumento = DOC.iCodDocumento AND MOV.iEstadoDocumento = 6
	INNER JOIN SegUsuario USU on DOC.vUsuCreacion=USU.WEBUSR and USU.siEstado=1
	INNER JOIN MaeTrabajador TRA ON USU.iCodTrabajador = TRA.iCodTrabajador 
	INNER JOIN MaeArea ARE ON TRA.iCodArea = ARE.iCodArea
	INNER JOIN MaeArea AREPARA ON DOC.iCodArea = AREPARA.iCodArea 
	INNER JOIN MaeTrabajador TRAPARA ON  DOC.iCodTrabajador = TRAPARA.iCodTrabajador 
	WHERE MOV.iCodDocumento = @iCodDocumento AND MOV.iEstadoDocumento = 6 AND Mov.iTipoMovimiento IN (0,2,1,3))
	SELECT * FROM SEGUIMIENTO ORDER BY FECHA ASC
 END
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]
(
	 @iCodDocumento INT ,
	 @siTipoAcceso INT
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]
-- Objetivo:	Obtener Documentos DocumentosDerivadosPorCodDocumento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Input:		@siTipoAcceso - Tipo Acceso
-- Output		Entidad Documentos Derivados
-- Create date: 19/02/2020
-- =============================================
BEGIN
SELECT        
dd.iCodDocumentoDerivacion, 
dd.iCodDocumento, 
dd.siEstado, 
dd.iCodArea, 
dd.iCodResponsable, 
dd.siEstadoDerivacion, 
dd.siTipoAcceso, 
dd.dFecDerivacion, 
dd.vUsuCreacion, 
dd.dFecCreacion, 
dd.vHstCreacion, 
dd.vInsCreacion, 
dd.vLgnCreacion, 
dd.vRolCreacion, 
dd.vUsuActualizacion, 
dd.dFecActualizacion, 
dd.vHstActualizacion, 
dd.vInsActualizacion, 
dd.vLgnActualizacion, 
dd.vRolActualizacion,
DD.iTipoAccion,
dd.vNombreEntidadSIED,
dd.vRucSIED,
dd.iFormatoDocumentoSIED,
dd.iCodEntidadSIED
FROM            
MovDocumentoDerivacion dd
WHERE dd.iCodDocumento = @iCodDocumento 
AND DD.siTipoAcceso = @siTipoAcceso
END

GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]
END
GO


CREATE PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]
  @iCodDocumento INT 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_Listar_CabeceraSeguimiento]
-- Objetivo:	Lista la cabecera de los seguimientos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Create date: 27/02/20
-- =============================================
BEGIN

WITH CABECERA AS (
	select 
		TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
		MAEA.vDescripcion AS AREADESDE,
		MOV.vCodigosInternos AS AREAPARA,
		MOV.iEstadoDocumento,
		(select max(dfecCreacion) from  MovDocMovimiento where  iCodDocumento= @iCodDocumento  and iEstadoDocumento =  Mov.iEstadoDocumento )  AS FECHA,
		cast(MOV.iCodDocDerivacion as varchar)iCodDocDerivacion
		FROM MovDocMovimiento MOV 
		INNER JOIN MovDocumento DOC ON MOV.iCodDocumento = DOC.iCodDocumento
		INNER JOIN SegUsuario seg ON DOC.vUsuCreacion = SEG.WEBUSR 
		INNER JOIN MaeTrabajador TRA ON SEG.iCodTrabajador = TRA.iCodTrabajador
		INNER JOIN MaeArea MAEA ON DOC.iCodArea = MAEA.iCodArea
		INNER JOIN MaeTrabajador TRAPARA ON MOV.iCodTrabajador = TRAPARA.iCodTrabajador
		WHERE MOV.iCodDocumento = @iCodDocumento AND MOV.vCodigosInternos  IS NOT NULL
	--	ORDER BY MOV.iCodDocDerivacion DESC OFFSET 0 ROWS
), 
DERIVACIONCODIGOS AS (
	SELECT AREAPARA,iEstadoDocumento, icodDocderivacion = STUFF((
		SELECT N' |' + icodDocderivacion FROM CABECERA
		WHERE AREAPARA = x.AREAPARA AND
			   iEstadoDocumento = x.iEstadoDocumento-- AND iEstadoDocumento <>1
		FOR XML PATH(''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 2, N'')
	FROM CABECERA AS x
	GROUP BY AREAPARA,iEstadoDocumento
	--ORDER BY icodDocderivacion DESC OFFSET 0 ROWS
),  
CABFIN AS  (
SELECT  DISTINCT
CAB.NOMBREDESDE,
CAB.AREADESDE,
CAB.AREAPARA,
CAB.iEstadoDocumento,
MOV.dFecCreacion FECHA,
CAB.icodDocderivacion AS iCodDerivacionesInternas,
0 as iCodDocumento
FROM  CABECERA CAB 
INNER JOIN MovDocMovimiento MOV ON CAB.iCodDocDerivacion = MOV.iCodDocDerivacion
WHERE  cab.iEstadoDocumento = CASE WHEN  CHARINDEX('|',cab.AREAPARA) > 0 THEN  '' ELSE 1 END 

UNION  ALL

SELECT  DISTINCT
CAB.NOMBREDESDE,
CAB.AREADESDE,
CAB.AREAPARA,
CAB.iEstadoDocumento,
CAB.FECHA,
DER.icodDocderivacion AS iCodDerivacionesInternas,
0 as iCodDocumento
FROM  CABECERA CAB 
INNER  JOIN DERIVACIONCODIGOS DER
ON CAB.AREAPARA = DER.AREAPARA AND CAB.iEstadoDocumento =  DER.iEstadoDocumento
WHERE  cab.iEstadoDocumento <> CASE WHEN  CHARINDEX('|',cab.AREAPARA) > 0 THEN  '' ELSE 1 END 
AND CAB.iEstadoDocumento = DER.iEstadoDocumento
--ORDER BY DER.icodDocderivacion DESC

UNION ALL

SELECT DISTINCT
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	MAEA.vDescripcion AS AREADESDE,
	(select mm.vCodigosInternos from MovDocMovimiento mm where iCodDocumento =  @iCodDocumento 
	and  iCodDocMovimiento = (select max(iCodDocMovimiento) from MovDocMovimiento where iCodDocumento =  @iCodDocumento
	and vCodigosInternos is not null))  AS AREAPARA,
	MOV.iEstadoDocumento,			
	(select max(dfecCreacion) from  MovDocMovimiento where  iCodDocumento= @iCodDocumento  and iEstadoDocumento =  Mov.iEstadoDocumento )  AS FECHA,
	null iCodDerivacionesInternas,
	doc.iCodDocumento
	FROM MovDocMovimiento MOV 
	INNER JOIN MovDocumento DOC ON MOV.iCodDocumento = DOC.iCodDocumento
	INNER JOIN SegUsuario seg ON DOC.vUsuCreacion = SEG.WEBUSR 
	INNER JOIN MaeTrabajador TRA ON SEG.iCodTrabajador = TRA.iCodTrabajador
	INNER JOIN MaeArea MAEA ON DOC.iCodArea = MAEA.iCodArea
	INNER JOIN MaeTrabajador TRAPARA ON MOV.iCodTrabajador = TRAPARA.iCodTrabajador
	WHERE MOV.iCodDocumento = @iCodDocumento and mov.iEstadoDocumento IN(6,5,3,7) AND Mov.iTipoMovimiento IN (0,2,1,4,3,6,5,8)
	)
	
SELECT * FROM CABFIN  ORDER  BY Fecha
-- case when iCodDerivacionesInternas is null then 'zzzzzz' else iCodDerivacionesInternas end
END

GO