TRUNCATE TABLE [dbo].[ParParametro];

INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vValor2],[iOrden], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (1, 0, '', NULL, NULL, 'ESTADOS DEL DOCUMENTO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 1, 'ING', NULL, 0, 'INGRESADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 2, 'PEN', NULL, 1, 'PENDIENTE',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 3, 'ANU', NULL, 6, 'ANULADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 4, 'DER', NULL, 5, 'DERIVADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 5, 'ATE', NULL, 4, 'ATENDIDO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 6, 'PRO', NULL, 3, 'EN PROCESO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 7, 'DEV', NULL, 2, 'DEVUELTO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
		  
GO
/*INSERCIÓN DE DATOS - TIPO ORIGEN*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (2, 0, '', 'ORIGEN',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (2, 1, 'IN', 'INTERNO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (2, 2, 'EXT', 'EXTERNO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
		   
		  
GO
/*INSERCIÓN DE DATOS - PRIORIDAD*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vValor2],[iOrden], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (3, 0, '', NULL,NULL,'PRIORIDAD',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (3, 1, 'B', NULL,4, 'BAJA',0,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (3, 2, 'M', NULL,3, 'NORMAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (3, 3, 'A', NULL,2, 'URGENTE',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (3, 4, 'U', NULL,1, 'MUY URGENTE',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
	
/*INSERCIÓN DE DATOS - CORRELATIVOS*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (4, 0, '', 'CORRELATIVOS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (4, 1, '[CORRELATIVO]-[ANIO]', 'MESA DE PARTES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (4, 2, '[TIPODOC]-[NRO_TIPODOC_AREA_ANIO_CORRELATIVO]-[ANIO]-AGROBANCO-[AREA]', 'BANDEJA DE ENTRADA',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (4, 3, '[TIPODOC]-[NRO_TIPODOC_AREA_ANIO_CORRELATIVO]-[ANIO]-AGROBANCO-[AREA]', 'BANDEJA DE SALIDA',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)

/*INSERCIÓN DE DATOS - ESTADO*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (5, 0, '', 'ESTADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (5, 1, '1', 'ACTIVO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (5, 2, '0', 'INACTIVO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
		   
/*INSERCIÓN DE DATOS - TIPO REPRESENTANTE*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (6, 0, '', 'TIPO REPRESENTANTE',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (6, 1, '1', 'NATURAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (6, 2, '2', 'JURÍDICO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
/*INSERCIÓN DE DATOS - TIPO FERIADO*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (7, 0, '', 'TIPO FERIADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (7, 1, '1', 'FIN DE SEMANA',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (7, 2, '2', 'FERIADO GENERAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (7, 3, '3', 'FERIADO ESTATAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
	
INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(8,0,'','TIPO ACCIONES',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(8,1,1,'NECESARIA',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8, 2, 2, 'REVISAR/INFORMAR', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8,3,3,'CONOCIMIENTOS Y FINES',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8,4,4,'PREPARAR RESPUESTA',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8,5,5,'PARA DIRECTORIO',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8,6,6,'ARCHIVAR',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
( 8,7,7,'OTROS',1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER)

/*INSERCIÓN DE DATOS - IDENTIFICADOR JEFE*/
INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(9,0,'','ESTADO JEFE',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(9,1,1,'SI',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(9,2,2, 'NO', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER)

INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(10,0,'','INDICACIONES',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(10,1,1,'COORDINAR',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10, 2, 2, 'TOMAR ACCIÓN', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,3,3,'EMITIR RESPUESTA PERSONAL',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,4,4,'EMITIR PROYECTO DE RESPUESTA',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,5,5,'PARA CONOCIMIENTO',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,6,6,'PARA SEGUIMIENTO',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,7,7,'OTROS',1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER)

INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(11,0,'','AFIRMACION',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(11,1,1,'SI',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(11,2,0, 'NO', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER)



INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(12,0,'','MOTIVOS FIRMA',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(12,1,1,'SOY EL AUTOR DEL DOCUMENTO',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(12,2,2, 'EN SEÑAL DE CONFORMIDAD', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(12,3,3, 'DOY V°B°', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER)