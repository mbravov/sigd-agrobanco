USE [DBAgrSIGD]
GO

/* Agregando columnas a la entidad dbo.MovDocumento */

ALTER TABLE dbo.MovDocumento ADD vTipoEntidadSIED VARCHAR(10);
ALTER TABLE dbo.MovDocumento ADD vEntidadSIED VARCHAR(11);
ALTER TABLE dbo.MovDocumento ADD vDirigidoASIED VARCHAR(10);
ALTER TABLE dbo.MovDocumento ADD siPrioridadSIED VARCHAR(2);
ALTER TABLE dbo.MovDocumento ADD vIndicacionSIED VARCHAR(2);

/* Agregando columnas a la entidad dbo.MovDocAnexo */

ALTER TABLE dbo.MovDocAnexo ADD vAlmacenArchivoSIED  VARCHAR(5);
ALTER TABLE dbo.MovDocAnexo ADD vDescripcionSIED VARCHAR(200);
ALTER TABLE dbo.MovDocAnexo ADD vNombreFisicoSIED VARCHAR(200);

/* Agregando columnas a la entidad dbo.MovDocAdjunto */

ALTER TABLE dbo.MovDocAdjunto ADD vAlmacenArchivoSIED  VARCHAR(5);
ALTER TABLE dbo.MovDocAdjunto ADD vDescripcionSIED VARCHAR(200);
ALTER TABLE dbo.MovDocAdjunto ADD vNombreFisicoSIED VARCHAR(200);