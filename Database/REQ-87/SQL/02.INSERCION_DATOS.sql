USE [DBAgrSIGD]
GO

INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vValor2],[iOrden], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
	 /* Tipo Entidades */
	 	   (17, 0,'', NULL, NULL, 'TIPO DE ENTIDADES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
           (17, 1, 1, NULL, NULL, 'EMPRESAS OPERATIVAS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (17, 2, 2, NULL, NULL, 'INSTITUCIONES ESTATALES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (17, 3, 3, NULL, NULL, 'PERSONAS NATURALES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (17, 4, 4, NULL, NULL, 'PERSONAS JURIDICAS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
	/* Prioridades */
		   (18, 0,'', NULL, NULL, 'PRIORIDADES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (18, 1, 1, NULL, NULL, 'Normal',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (18, 2, 2, NULL, NULL, 'Urgente',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (18, 3, 3, NULL, NULL, 'Muy Urgente',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
	 /* Indicaciones */
	 	   (19, 0,'', NULL, NULL, 'INDICACIONES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
           (19, 1, 1, NULL, NULL, 'Coordinar',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (19, 2, 2, NULL, NULL, 'Tomar acción',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (19, 3, 3, NULL, NULL, 'Emitir respuesta personal',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (19, 4, 4, NULL, NULL, 'Emitir proyecto de respuesta',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),	
		   (19, 5, 5, NULL, NULL, 'Para conocimiento',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (19, 6, 6, NULL, NULL, 'Para seguimiento',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (19, 7, 7, NULL, NULL, 'Otros',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
	 /* MOTIVO FIRMA*/	   
		   (12, 4,4, NULL, NULL, 'MESA DE PARTES - OFICINA PRINCIPAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
           (12, 5, 5, NULL, NULL, 'MESA DE PARTES - GERENCIA GENERAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)

GO