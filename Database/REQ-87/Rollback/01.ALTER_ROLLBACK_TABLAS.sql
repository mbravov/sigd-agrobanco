USE [DBAgrSIGD]
GO

/* Rollback */

/* Eliminando columnas a la entidad dbo.MovDocumento */

ALTER TABLE dbo.MovDocumento DROP COLUMN vTipoEntidadSIED;
ALTER TABLE dbo.MovDocumento DROP COLUMN vEntidadSIED;
ALTER TABLE dbo.MovDocumento DROP COLUMN vDirigidoASIED;
ALTER TABLE dbo.MovDocumento DROP COLUMN siPrioridadSIED;
ALTER TABLE dbo.MovDocumento DROP COLUMN vIndicacionSIED;

/* Eliminando columnas a la entidad dbo.MovDocAnexo */

ALTER TABLE dbo.MovDocAnexo DROP COLUMN vAlmacenArchivoSIED;
ALTER TABLE dbo.MovDocAnexo DROP COLUMN vDescripcionSIED;
ALTER TABLE dbo.MovDocAnexo DROP COLUMN vNombreFisicoSIED;

/* Eliminando columnas a la entidad dbo.MovDocAdjunto */

ALTER TABLE dbo.MovDocAdjunto DROP COLUMN vAlmacenArchivoSIED;
ALTER TABLE dbo.MovDocAdjunto DROP COLUMN vDescripcionSIED;
ALTER TABLE dbo.MovDocAdjunto DROP COLUMN vNombreFisicoSIED;