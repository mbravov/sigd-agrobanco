USE [DBAgrSIGD]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UF_MOV_OBTENERCORRELATIVO]') AND type in (N'FN'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UF_MOV_OBTENERCORRELATIVO] AS' 
END
GO

ALTER FUNCTION [dbo].[UF_MOV_OBTENERCORRELATIVO]  
(  
 @origen int,  
 @iCodArea int,  
 @iCodTipDocumento int,  
 @Anio int,  
 @Tipo int  
)  
RETURNS varchar(100)  
AS  
BEGIN  
 declare @salida varchar(100)='';  
   
 declare @CORRELATIVO varchar(50),   
   @TIPODOC VARCHAR(5),   
   @AREA VARCHAR(10),  
   @NRO_TIPODOC_AREA_ANIO_CORR VARCHAR(100),  
   @CORRELATIVOFINAL varchar(50);  
  
 select @TIPODOC=vAbreviatura from MaeTipDocumento where iCodTipDocumento=@iCodTipDocumento;  
 select @AREA=vAbreviatura from MaeArea where iCodArea=@iCodArea;  
 select @CORRELATIVO=max(iCorrelativo)  
 from MaeSecuencial a  
 where a.iAnio=@Anio and a.siOrigen=@origen and siestado=1  
  
 set @CORRELATIVO=isnull(cast(@CORRELATIVO as int),0)+1;  
   
 select @NRO_TIPODOC_AREA_ANIO_CORR=max(iCorrelativo) from MaeSecuencial  a  
 where a.iAnio=@Anio and a.siOrigen=@origen  
 AND a.iCodTipDocumento=@iCodTipDocumento  
 AND a.iCodArea=@iCodArea   
 and siestado=1  
  
 set @NRO_TIPODOC_AREA_ANIO_CORR=isnull(cast(@NRO_TIPODOC_AREA_ANIO_CORR as int),0)+1;  
   
 if(@origen=1)--interno--  
 begin  
   select @salida= vValor from ParParametro where iCodParametro=2 and icodgrupo=4 and siestado=1  
   set @CORRELATIVOFINAL=@NRO_TIPODOC_AREA_ANIO_CORR  
 end  
 else   
 begin  
  if(@origen=2)--externo--mesa de partes  
  begin  
   select @salida= vValor from ParParametro where iCodParametro=1 and icodgrupo=4 and siestado=1  
   set @CORRELATIVOFINAL=@CORRELATIVO  
  end  
 end  
 if(@Tipo=1)  
 begin  
  return @CORRELATIVOFINAL;  
 end  
   
 set @salida= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@salida,'[CORRELATIVO]',right('000000000'+@CORRELATIVO,4))  
          ,'[ANIO]',cast(@Anio as varchar(5)))  
          ,'[AREA]',isnull(@AREA,''))  
          ,'[NRO_TIPODOC_AREA_ANIO_CORRELATIVO]',right('000000000'+isnull(@NRO_TIPODOC_AREA_ANIO_CORR,''),4))  
          ,'[TIPODOC]',isnull(@TIPODOC,''))  
  
 RETURN @salida;  
END  
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Generar_Secuencial]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_Generar_Secuencial] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_Generar_Secuencial]  
  @iOrigen INT  
 ,@iCodTipoDocumento INT
 ,@iCodArea INT  
 ,@vUsuCreacion VARCHAR(100) = NULL  
 ,@vHstCreacion VARCHAR(20) = NULL  
 ,@vInsCreacion VARCHAR(50) = NULL  
 ,@vLgnCreacion VARCHAR(20) = NULL  
 ,@vRolCreacion VARCHAR(20) = NULL  
 ,@onSecuencial INT OUTPUT  
 ,@onCorrelativo VARCHAR(100) OUTPUT  
AS  
-- =============================================  
-- Nombre: [dbo].[UP_MAE_Generar_Secuencial]  
-- Objetivo: Generar Secuencial de Documentos  
-- Módulo:  Maestros  
-- Input:  @iCodTipoDocumento : Código Tipo Documento  
--    @iCodArea : Código de Área  
-- Output:  
--    @onCorrelativo - Correlativo Generado  
-- Create date: 27/01/20  
-- =============================================  
DECLARE @AnioActual AS INT  
DECLARE @IdSecuencialMax AS INT  
DECLARE @CorrelativoExist AS VARCHAR(100)  
DECLARE @SecuencialExist AS INT  
declare @onCorrelativoNumero AS int;  
BEGIN  
    
  SET @AnioActual=YEAR(GETDATE())   
 --VALIDAMOS SI HAY SECUENCIAS USADAS  
 IF(@iOrigen = (SELECT iCodParametro FROM ParParametro  WHERE vCampo='EXTERNO'))  
	 BEGIN  
	   SELECT @CorrelativoExist= vCorrelativo , @SecuencialExist=iCodSecuencial 
	   FROM  MaeSecuencial 
	   WHERE siOrigen = @iOrigen AND iAnio  = @AnioActual AND fusado = 0;  
	 END  
 ELSE IF(@iOrigen = (SELECT iCodParametro FROM ParParametro  WHERE vCampo='INTERNO'))  
	 BEGIN  
	   SELECT @CorrelativoExist= vCorrelativo , @SecuencialExist=iCodSecuencial 
	   FROM  MaeSecuencial 
	   WHERE siOrigen = @iOrigen AND iAnio  = @AnioActual AND iCodArea=@iCodArea  AND iCodTipDocumento=@iCodTipoDocumento AND fusado = 0;  
	 END  
  
  --Máximo Identificador  
  SELECT @IdSecuencialMax=MAX(iCodSecuencial) FROM dbo.MaeSecuencial  
  
 IF @IdSecuencialMax IS NULL   
     SET @IdSecuencialMax=1;  
 ELSE  
  SET @IdSecuencialMax=@IdSecuencialMax+1;  
  
  
     
  
   if(@CorrelativoExist is not null)  
   begin  
   SET @onSecuencial  =  @SecuencialExist  
   SET @onCorrelativo  =  @CorrelativoExist  
    UPDATE MaeSecuencial set fUsado = 1,  
       iCodArea = @iCodArea,  
       iCodTipDocumento = @iCodTipoDocumento  
       where vCorrelativo = @CorrelativoExist;  
   end  
   else  
   begin  
  
   declare @Anio int =YEAR(GETDATE())  
   SET @onSecuencial=dbo.UF_MOV_OBTENERCORRELATIVO(@iOrigen,@iCodArea,@iCodTipoDocumento,@Anio, 1)  
   SET @onCorrelativo=dbo.UF_MOV_OBTENERCORRELATIVO(@iOrigen,@iCodArea,@iCodTipoDocumento,@Anio, 2)  
    --INSERTANDO NUEVO SECUENCIAL  
 INSERT INTO dbo.MaeSecuencial(  
					 iAnio,  
					 siEstado,  
					 iCodSecuencial,  
					 iCorrelativo,  
					 vCorrelativo,  
					 iCodArea,  
					 iCodTipDocumento,  
					 vUsuCreacion,  
					 dFecCreacion,  
					 vHstCreacion,  
					 vInsCreacion,  
					 vLgnCreacion,  
					 vRolCreacion,  
					 siOrigen,  
					 fUsado  
					 )  
 VALUES(  
		 @AnioActual,  
		 1,  
		 @IdSecuencialMax,  
		 @onSecuencial,  
		 @onCorrelativo,  
		 @iCodArea,  
		 @iCodTipoDocumento,  
		 @vUsuCreacion,  
		 GETDATE(),  
		 @@SERVERNAME,  
		 @@servicename,  
		 @vLgnCreacion,  
		 @vRolCreacion,  
		 @iOrigen,  
		 1  
		 )  
end  
  
  IF @@ROWCOUNT  > 0   
     SELECT @onCorrelativo  
  ELSE  
  SELECT 0  
  
 END  
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumentoRecepcionSIED]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocumentoRecepcionSIED] AS' 
END
GO
  
      
ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocumentoRecepcionSIED]    
  @iCodSecuencial INT = NULL--2    
 ,@vCorrelativo VARCHAR(100)= NULL--3    
 ,@iAnio INT = NULL--4    
 ,@siEstado SMALLINT --5    
 ,@siOrigen SMALLINT = NULL --6    
 ,@vNumDocumento VARCHAR(100) = NULL--7    
 ,@siEstDocumento SMALLINT = NULL--8    
 ,@dFecDocumento DATETIME = NULL--9    
 ,@dFecRegistro DATETIME = NULL--10    
 ,@dFecDerivacion DATETIME=NULL    
 ,@vAsunto VARCHAR(1000) = NULL--11    
 ,@siPrioridad SMALLINT = NULL--12    
 ,@vReferencia VARCHAR(1000) = NULL--13    
 ,@dFecRecepcion DATETIME = NULL--14    
 ,@siPlazo SMALLINT = NULL--15    
 ,@dFecPlazo DATETIME = NULL--16    
 ,@vObservaciones VARCHAR(1000) = NULL--17    
 ,@iCodTipDocumento INT = NULL--18    
 ,@iCodRepresentante INT--19    
 ,@iCodArea INT--20    
 ,@iCodTrabajador INT--21    
 ,@siFlagSied INT    
 ,@vUsuCreacion VARCHAR(100) = NULL--22    
 ,@vHstCreacion VARCHAR(20) = NULL--23    
 ,@vInsCreacion VARCHAR(50) = NULL--24    
 ,@vLgnCreacion VARCHAR(20) = NULL--25    
 ,@vRolCreacion VARCHAR(20) = NULL--26    
 ,@onFlagOK INT OUTPUT--27    
AS    
-- =============================================    
-- Nombre: [dbo].[UP_MOV_RegistrarDocumento]    
-- Objetivo: Registrar un nuevo Documento    
-- Módulo:  Movimientos    
-- Input:  @iCodDocumento - Código de Documento    
--    @iCodSecuencial -  Número de Secuencia    
--    @iAnio    - Anio Correlativo    
--    @siOrigen   - Estado del Registro: 1 - Activo; 0 - Inactivo    
--    @vNumDocumento   - Número de Documento    
--    @siEstDocumento   - Estado del Documento    
--    @dFecDocumento   - Fecha del Documento    
--    @dFecRegistro   - Fecha de Registro    
--    @vAsunto   - Asunto    
--    @siPrioridad  - Indicador de Prioridad    
--    @vReferencia  - Referencia    
--    @dFecRecepcion   - Fecha de Recepción    
--    @siPlazo   - Indicador de Plazo    
--    @dFecPlazo   - Fecha de Plazo    
--    @vObservaciones  - Observaciones    
--    @iCodTipDocumento - Codigo Tipo Documento    
--    @iCodRepresentante  - Código de Representante    
--    @iCodArea   - Código de Área    
--    @iCodTrabajador     - Codigo de Trabajador    
--    @vUsuCreacion - Campo Auditoria Usuario Creación    
--    @vHstCreacion - Campo Auditoría Host    
--    @vInsCreacion - Campo Auditoría Instancia    
--    @vLgnCreacion - Campo Auditoría Login    
--    @vRolCreacion - Campo Auditoría Rol    
-- Output  @onFlagOK - Indicador de Salida    
-- Create date: 27/01/20    
-- =============================================    
BEGIN    
    
     
    
  INSERT INTO dbo.MovDocumento(iCodSecuencial,iAnio,siEstado,    
         siOrigen,vNumDocumento,siEstDocumento,    
         dFecDocumento,dFecRegistro,vAsunto,    
         siPrioridad,vReferencia,dFecRecepcion,    
         siPlazo,dFecPlazo,vObservaciones,    
         iCodTipDocumento,iCodRepresentante,iCodArea,    
         iCodTrabajador,siFlagSIED, vUsuCreacion,dFecCreacion,    
         vHstCreacion,vInsCreacion,vLgnCreacion,    
         vRolCreacion,vCorrelativo,dFecDerivacion,siFlagFirmando)    
         VALUES(             
         @iCodSecuencial,@iAnio,1,    
         @siOrigen,@vNumDocumento,@siEstDocumento,    
         @dFecDocumento,getdate(),@vAsunto,    
         @siPrioridad,@vReferencia,@dFecRecepcion,    
         @siPlazo,@dFecPlazo,@vObservaciones,    
         @iCodTipDocumento,@iCodRepresentante,@iCodArea,    
         @iCodTrabajador,@siFlagSIED,@vUsuCreacion,GETDATE(),    
         @@SERVERNAME,@@servicename,@vLgnCreacion,    
         @vRolCreacion,@vCorrelativo,@dFecDerivacion,0    
         )    
    
    
    
    
    IF @@ROWCOUNT  > 0     
    SELECT @onFlagOK=MAX(iCodDocumento) FROM dbo.MovDocumento     
   ELSE    
    SET @onFlagOK=0    
    
    
 END    
 GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocAnexo]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocAnexo] AS' 
END
GO

   
ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocAnexo]  
  @iCodLaserfiche  INT = NULL  
 ,@iCodDocMovimiento INT  
 ,@iCodDocumento INT   
 ,@iSecuencia INT   
 ,@iTipoArchivo int=1  
 ,@vNombre VARCHAR(50) = NULL  
 ,@vUsuCreacion VARCHAR(100) = NULL  
 ,@vHstCreacion VARCHAR(20) = NULL  
 ,@vInsCreacion VARCHAR(50) = NULL  
 ,@vLgnCreacion VARCHAR(20) = NULL  
 ,@vRolCreacion VARCHAR(20) = NULL  
 ,@onFlagOK INT OUTPUT  
AS  
-- =============================================  
-- Nombre: [dbo].[UP_MOV_RegistrarDocAnexo]  
-- Objetivo: Registrar Anexos a un Documento  
-- Módulo:  Movimientos  
-- Input:  @iCodLaserfiche - Código de Documento en LaserFiche  
--    @iCodDocMovimiento -  Codigo de Documento Movimiento  
--    @iCodDocumento -  Codigo de Documento  
--    @iSecuencia - Secuencial de anexo  
--    @vNombre - Nombre Documento  
--    @vUsuCreacion - Campo Auditoria Usuario Creación  
--    @vHstCreacion - Campo Auditoría Host  
--    @vInsCreacion - Campo Auditoría Instancia  
--    @vLgnCreacion - Campo Auditoría Login  
--    @vRolCreacion - Campo Auditoría Rol  
-- Output  @onFlagOK - Indicador de Salida  
-- Create date: 19/02/2020  
-- =============================================  
BEGIN  
  
 INSERT INTO dbo.MovDocAnexo(iCodLaserfiche,      
         siEstado,    
         iCodDocMovimiento,  
         iCodDocumento,  
         vNombre,  
         iSecuencia,  
         vUsuCreacion,  
         dFecCreacion,  
         vHstCreacion,  
         vInsCreacion,  
         vLgnCreacion,  
         vRolCreacion,  
         iTipoArchivo)  
         VALUES(           
         @iCodLaserfiche,  
         1,   
         @iCodDocMovimiento,  
         @iCodDocumento,  
         @vNombre,  
         @iSecuencia,  
         @vUsuCreacion,  
         GETDATE(),  
         @@SERVERNAME,  
         @@SERVICENAME,  
         @vLgnCreacion,  
         @vRolCreacion,  
         @iTipoArchivo   
         )  
  
  
  
    IF @@ROWCOUNT  > 0   
    SELECT @onFlagOK=MAX(iCodDocAnexo) FROM dbo.MovDocAnexo   
   ELSE  
    SET @onFlagOK=0  
  
 END  
 GO
 
 
 IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_BandejaRecepcionSIED]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_Listar_BandejaRecepcionSIED] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_Listar_BandejaRecepcionSIED] (
@vTextoBusqueda VARCHAR(100),
@dFechaDesde datetime,
@dFechaHasta datetime,
@iTipoBandeja int,
@iCodTrabajador int,
@vCodPerfil varchar(50),
@iPagina int,
@iTamPagina int
)
AS
/*
Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3

Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4
*/
BEGIN

	DECLARE @EstadoIngresado INT

	SELECT 
		@EstadoIngresado = icodParametro 
	FROM ParParametro WHERE iCodGrupo=1 AND vValor IN ('ING');

	with areasTrab as
	(
		select iCodArea from [dbo].[MaeTrabajador] where  iCodTrabajador= @iCodTrabajador
	),
	dataTodo as
	(
		SELECT 
			 a.iCodDocumento,a.siEstado AS Estado
			,a.siEstDocumento as EstadoDocumento
			,a.siPrioridad AS Prioridad
			,CASE WHEN siOrigen=1	THEN a.dFecRegistro
									ELSE a.dFecDerivacion END	AS RecibidoDate
			,CASE WHEN siOrigen=1	THEN CONVERT(VARCHAR(10), a.dFecRegistro, 103) 
									ELSE CONVERT(VARCHAR(10), a.dFecDerivacion, 103)  END	AS Recibido
			,CONVERT(VARCHAR(10), a.dFecRecepcion, 103) AS Recepcion
			,a.iCodSecuencial AS Reg
			,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
			,ISNULL(a.vAsunto, '') AS DocumentoAsunto
			,MR.iCodRepresentante AS CodRepresentante
			,ISNULL(mr.vNombres,c.vDescripcion) AS Remitente
			,c.vAbreviatura AS Derivado
			,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
			,a.siPlazo
			,ma.iCodDocumento as ExisteAdjunto
			,a.vCorrelativo as Correlativo
			,E.iCodEmpresa AS CodEmpresa
			,E.vDescripcion AS NombreEmpresa
			,a.dFecRegistro				
			,c.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,isnull(priori.vCampo,'') vEstadoPrioridad
			,estDoc.iOrden OrdenEstado
			,priori.iOrden OrdenPrioridad
			,a.iCodTrabajador
			,d.vNombres+' '+d.vApePaterno+' '+d.vApeMaterno NombreTrabajador			
			,a.dFecPlazo
			,a.siOrigen
			,concat(a.vCorrelativo, ' ', E.vDescripcion , ' ',
					b.vAbreviatura, '',b.vDescripcion, '', ISNULL(a.vNumDocumento, ''), ' ',
					ISNULL(b.vDescripcion, ''), ' ',  ISNULL(a.vAsunto, ''), ' ', 
					isnull(priori.vCampo,''),' ',mr.vNombres) AS FILTRO ,
					f.iCodDocumentoFirma,
					a.siFlagSIED
		FROM [dbo].[MovDocumento] a WITH(NOLOCK)
		--INNER JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		INNER JOIN [dbo].[MaeArea] c WITH(NOLOCK) ON a.iCodArea = c.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] d WITH(NOLOCK) ON a.iCodTrabajador = d.iCodTrabajador
		--LEFT JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON ((a.iCodTipDocumento IS NOT NULL AND a.iCodTipDocumento = b.iCodTipDocumento) OR (a.iCodTipDocumento IS NULL))
		LEFT JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		LEFT join [dbo].[MaeRepresentante] mr WITH(NOLOCK) ON a.iCodRepresentante = mr.iCodRepresentante
		LEFT JOIN MaeEmpresa E WITH(NOLOCK) ON E.siEstado = 1 AND E.iCodEmpresa = mr.iCodEmpresa 
		left join  (SELECT distinct iCodDocumento fROM MovDocAdjunto WITH(NOLOCK)) ma on a.iCodDocumento = ma.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.siEstDocumento and estDoc.iCodParametro<>0
		left join ParParametro priori WITH(NOLOCK) on priori.iCodGrupo=3 and priori.iCodParametro=a.siPrioridad and priori.iCodParametro<>0
	    LEFT JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma  WITH(NOLOCK)GROUP BY iCodDocumento) f on a.iCodDocumento = f.iCodDocumento
		WHERE 
		a.siEstado=1
		and dFecRegistro >= cast(@dFechaDesde as date)
		and dFecRegistro < cast(DATEADD(DAY, 1, @dFechaHasta) as date) and a.siFlagSIED = 2		 
	),
	dataResponsablesDoc as
	(
		select 	
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DA' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
		from dataTodo a
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.EstadoDocumento and estDoc.iCodParametro<>0
		where
		a.iCodArea in (select iCodArea from areasTrab)
		and
		(
			isnull(a.iCodTrabajador,-1) = @iCodTrabajador
		)
	),
	dataResponsablesDerivacion as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DD' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResponsablesDerivacionSIED as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DDSIED' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		--INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResponsablesCC as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DC' as TipoDocIng
			/*,a.iCodDocumentoFirma*/
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		where
			dd.siEstado=1
			and dd.siTipoAcceso=2
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResultado as
	(
		select *,0 iCodDocumentoDerivacion 
		from dataResponsablesDoc
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		select * 
		from dataResponsablesDerivacion
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDoc
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		SELECT *
		FROM dataResponsablesDerivacionSIED
		WHERE icodDocumento NOT IN(
					SELECT iCodDocumento FROM dataResponsablesDoc
					UNION
					SELECT iCodDocumento FROM dataResponsablesDerivacion
					UNION 
					SELECT iCodDocumento FROM dataResponsablesCC					
				)
		UNION
		select *,0 iCodDocumentoFirma, NULL siFlagSIED, 0 iCodDocumentoDerivacion
		from dataResponsablesCC
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union
					select iCodDocumento from dataResponsablesDerivacionSIED
					union 
					select iCodDocumento from dataResponsablesDoc
				)
	), dataresultadofin as (
	select 
	a.*, 
	dbo.UF_MOV_OBTENERNOMJEFE(iCodArea) Jefe,  
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento=2 THEN 1 ELSE 0 END HabilitarSeguimiento,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarDerivar,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN'))
	THEN 1 ELSE 0 END HabilitarDevolver,
	case when TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarFinalizar,
	case when 
		(TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO')))
		OR
		(TipoDocIng = 'DD' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN')))
	THEN 1 ELSE 0 END HabilitarRegistrarAvance
	--(select count(1) from dataResultado)TotalRegistros 
	from dataResultado A
	where
	upper(A.FILTRO)
				LIKE '%' + upper(@vTextoBusqueda) + '%' 
	AND
	(
			(		--RECIBIDOS
				@iTipoBandeja=1 AND 
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						AND siOrigen=2 AND a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen=1 AND a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('PEN','DEV','PRO','ATE','DER','ANU'))
					)
					
				)
			)		--CON PLAZO
			OR
			(
				@iTipoBandeja=2 AND 			
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						AND siOrigen=2 and a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen=1 and a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen IN (3, 4) AND a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
						AND TipoDocIng = 'DDSIED'
					)
				)
				/*AND
				a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in ('PEN','DER'))
				and datediff(day,getdate(), A.dFecPlazo)>=1 */
				AND A.siPlazo > 0
			)
		)
		)SELECT *,(select count(1) from dataresultadofin)TotalRegistros  FROM  dataresultadofin
	order by dFecRegistro desc
	OFFSET (@iPagina-1)*@iTamPagina ROWS
	FETCH NEXT @iTamPagina ROWS ONLY;
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentoRecepcionSIED]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentoRecepcionSIED] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentoRecepcionSIED]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumento]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
		SELECT 
			a.[iCodDocumento],
			a.[iCodSecuencial],
			a.[vCorrelativo],
			a.[iAnio],
			a.[siEstado],
			a.[siOrigen],
			a.[vNumDocumento],
			a.[siEstDocumento],
			a.[dFecDocumento],
			a.[dFecRegistro],
			a.[vAsunto],
			a.[siPrioridad],
			a.[vReferencia],
			a.[dFecRecepcion],
			a.[siPlazo],
			a.[dFecPlazo],
			a.[vObservaciones],
			a.[iCodTipDocumento],
			b.vDescripcion as vTipoDocumento,
			a.[iCodRepresentante],
			c.vNombres as vRepresentante,
			a.[iCodArea],
			d.vDescripcion as vArea,
			a.[iCodTrabajador],
			e.vNombres as vNombreTrab,
			e.vApePaterno as vApePaternoTrab,
			e.vApeMaterno as vApeMaternoTrab,
			e.siFirma,
			em.iCodEmpresa,
			em.vDescripcion as vDescripcionEmpresa,
			ADJ.iCodDocAdjunto,
			a.[vUsuCreacion],
			a.[dFecCreacion],
			a.[vHstCreacion],
			a.[vInsCreacion],
			a.[vLgnCreacion],
			a.[vRolCreacion],
			a.[vUsuActualizacion],
			a.[dFecActualizacion],
			a.[vHstActualizacion],
			a.[vInsActualizacion],
			a.[vLgnActualizacion],
			a.[vRolActualizacion],
			(left(convert(varchar, a.[dFecDocumento], 106),6)+'. '+cast(year(a.[dFecDocumento]) as varchar)+' '+right(convert(varchar, a.[dFecDocumento], 100),7))vFecDocumento,
			tre.vNombres+' '+tre.vApePaterno+' '+tre.vApeMaterno vUsuarioRegistro,
			tre.iCodTrabajador AS CodResponsable,
			iCodUltimoMovimiento = (select max(iCodDocMovimiento) from MovDocMovimiento
									where iCodDocumento=@iCodDocumento
									and siEstado=1
									),
			(select isnull(max(icoddocumento),0) from MovDocumentoFirma where iCodDocumento = @iCodDocumento) vDocumentoFirmado,
			(select isnull(max(icoddocumento),0) from MovDocumentoFirma where iCodDocumento = @iCodDocumento) vDocumentoFirmado,
			CASE WHEN a.siEstDocumento = 3
			THEN 
			(select TOP 1 vComentario from MovDocComentario WHERE iCodDocumento = @iCodDocumento ORDER BY iCodDocComentario DESC)
			ELSE
			''
			END as comentarioAnulacion
		FROM [dbo].[MovDocumento] a
		INNER JOIN [dbo].[MaeTipDocumento] b
		ON a.iCodTipDocumento=b.iCodTipDocumento
		LEFT JOIN [dbo].[MaeRepresentante] c
		ON a.iCodRepresentante=c.iCodRepresentante
		INNER JOIN [dbo].[MaeArea] d
		ON a.iCodArea=d.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] e
		ON a.iCodTrabajador=e.iCodTrabajador
		LEFT JOIN [dbo].[MaeEmpresa] em
		on c.iCodEmpresa = em.iCodEmpresa
		LEFT JOIN [SegUsuario] ure on a.vUsuCreacion=ure.WEBUSR and ure.siEstado=1
		inner join [MaeTrabajador] tre on ure.iCodTrabajador=tre.iCodTrabajador
		LEFT JOIN MovDocAdjunto ADJ ON ADJ.iCodDocumento=A.iCodDocumento
		WHERE a.iCodDocumento=@iCodDocumento AND a.siFlagSIED = 2;
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosAnexos]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAnexos] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAnexos]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].ma.[UP_MOV_ObtenerDocumentosAnexos]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN
SELECT ma.[iCodDocAnexo]
      ,ma.[iCodLaserfiche]
      ,ma.[siEstado]
      ,ma.[iCodDocMovimiento]
	  ,ma.[vNombre]
	  ,ma.[iSecuencia]
      ,ma.[vUsuCreacion]
      ,ma.[dFecCreacion]
      ,ma.[vHstCreacion]
      ,ma.[vInsCreacion]
      ,ma.[vLgnCreacion]
      ,ma.[vRolCreacion]
      ,ma.[vUsuActualizacion]
      ,ma.[dFecActualizacion]
      ,ma.[vHstActualizacion]
      ,ma.[vInsActualizacion]
      ,ma.[vLgnActualizacion]
      ,ma.[vRolActualizacion]
	  ,ma.[iTipoArchivo]
  FROM [dbo].[MovDocAnexo] ma
  WHERE ma.iCodDocumento = @iCodDocumento 
  and siEstado=1
END

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_EstablecerDocumentoPrincipalSIED]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_EstablecerDocumentoPrincipalSIED]
GO






IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_BandejaEntrada]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_Listar_BandejaEntrada] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UP_MOV_Listar_BandejaEntrada] (
@vTextoBusqueda VARCHAR(100),
@dFechaDesde datetime,
@dFechaHasta datetime,
@iTipoBandeja int,
@iCodTrabajador int,
@vCodPerfil varchar(50),
@iPagina int,
@iTamPagina int
)
AS
/*
Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3

Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4
*/
BEGIN

	DECLARE @EstadoIngresado INT

	SELECT 
		@EstadoIngresado = icodParametro 
	FROM ParParametro WHERE iCodGrupo=1 AND vValor IN ('ING');

	with areasTrab as
	(
		select iCodArea from [dbo].[MaeTrabajador] where  iCodTrabajador= @iCodTrabajador
	),
	dataTodo as
	(
		SELECT 
			 a.iCodDocumento,a.siEstado AS Estado
			,a.siEstDocumento as EstadoDocumento
			,a.siPrioridad AS Prioridad
			,CASE WHEN siOrigen=1	THEN a.dFecRegistro
									ELSE a.dFecDerivacion END	AS RecibidoDate
			,CASE WHEN siOrigen=1	THEN CONVERT(VARCHAR(10), a.dFecRegistro, 103) 
									ELSE CONVERT(VARCHAR(10), a.dFecDerivacion, 103)  END	AS Recibido
			,CONVERT(VARCHAR(10), a.dFecRecepcion, 103) AS Recepcion
			,a.iCodSecuencial AS Reg
			,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
			,ISNULL(a.vAsunto, '') AS DocumentoAsunto
			,MR.iCodRepresentante AS CodRepresentante
			,ISNULL(mr.vNombres,c.vDescripcion) AS Remitente
			,c.vAbreviatura AS Derivado
			,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
			,a.siPlazo
			,ma.iCodDocumento as ExisteAdjunto
			,a.vCorrelativo as Correlativo
			,E.iCodEmpresa AS CodEmpresa
			,E.vDescripcion AS NombreEmpresa
			,a.dFecRegistro				
			,c.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,isnull(priori.vCampo,'') vEstadoPrioridad
			,estDoc.iOrden OrdenEstado
			,priori.iOrden OrdenPrioridad
			,a.iCodTrabajador
			,d.vNombres+' '+d.vApePaterno+' '+d.vApeMaterno NombreTrabajador			
			,a.dFecPlazo
			,a.siOrigen
			,concat(a.vCorrelativo, ' ', E.vDescripcion , ' ',
					b.vAbreviatura, '',b.vDescripcion, '', ISNULL(a.vNumDocumento, ''), ' ',
					ISNULL(b.vDescripcion, ''), ' ',  ISNULL(a.vAsunto, ''), ' ', 
					isnull(priori.vCampo,''),' ',mr.vNombres) AS FILTRO ,
					f.iCodDocumentoFirma,
					a.siFlagSIED
		FROM [dbo].[MovDocumento] a WITH(NOLOCK)
		--INNER JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		INNER JOIN [dbo].[MaeArea] c WITH(NOLOCK) ON a.iCodArea = c.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] d WITH(NOLOCK) ON a.iCodTrabajador = d.iCodTrabajador
		--LEFT JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON ((a.iCodTipDocumento IS NOT NULL AND a.iCodTipDocumento = b.iCodTipDocumento) OR (a.iCodTipDocumento IS NULL))
		LEFT JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		LEFT join [dbo].[MaeRepresentante] mr WITH(NOLOCK) ON a.iCodRepresentante = mr.iCodRepresentante
		LEFT JOIN MaeEmpresa E WITH(NOLOCK) ON E.siEstado = 1 AND E.iCodEmpresa = mr.iCodEmpresa 
		left join  (SELECT distinct iCodDocumento fROM MovDocAdjunto WITH(NOLOCK)) ma on a.iCodDocumento = ma.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.siEstDocumento and estDoc.iCodParametro<>0
		left join ParParametro priori WITH(NOLOCK) on priori.iCodGrupo=3 and priori.iCodParametro=a.siPrioridad and priori.iCodParametro<>0
	    LEFT JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma  WITH(NOLOCK)GROUP BY iCodDocumento) f on a.iCodDocumento = f.iCodDocumento
		WHERE 
		a.siEstado=1
		and dFecRegistro >= cast(@dFechaDesde as date)
		and dFecRegistro < cast(DATEADD(DAY, 1, @dFechaHasta) as date)		
	),
	dataResponsablesDoc as
	(
		select 	
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DA' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
		from dataTodo a
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.EstadoDocumento and estDoc.iCodParametro<>0
		where
		a.iCodArea in (select iCodArea from areasTrab)
		and
		(
			isnull(a.iCodTrabajador,-1) = @iCodTrabajador
		)		and isnull(siFlagSIED,'') <> 2
	),
	dataResponsablesDerivacion as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DD' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and (a.siFlagSIED <> 1 or a.siFlagSIED is null)
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
		union all
				select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DD' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		INNER JOIN MaeUsuarioSIED SIED ON dd.iCodResponsable = SIED.iCodTrabajador
		--INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and a.siFlagSIED IS NULL
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)


	),
	dataResponsablesDerivacionSIED as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,tipdoc.vCampo AS Documento--a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DDSIED' as TipoDocIng
			,a.iCodDocumentoFirma
			,a.siFlagSIED
			,DD.iCodDocumentoDerivacion 			
		from dataTodo a
		INNER JOIN MovDocumento DOC ON a.iCodDocumento = doc.iCodDocumento
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		LEFT JOIN ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		LEFT JOIN (SELECT iCodParametro,vValor,vValor2,vCampo from ParParametro where iCodGrupo in(14,15)) tipdoc on DOC.iCodTipDocumentoSIED = tipdoc.vValor
		--INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResponsablesCC as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DC' as TipoDocIng
			/*,a.iCodDocumentoFirma*/
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		where
			dd.siEstado=1
			and dd.siTipoAcceso=2
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResultado as
	(
		select *,0 iCodDocumentoDerivacion 
		from dataResponsablesDoc
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		select * 
		from dataResponsablesDerivacion
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDoc
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		SELECT *
		FROM dataResponsablesDerivacionSIED
		WHERE icodDocumento NOT IN(
					SELECT iCodDocumento FROM dataResponsablesDoc
					UNION
					SELECT iCodDocumento FROM dataResponsablesDerivacion
					UNION 
					SELECT iCodDocumento FROM dataResponsablesCC					
				)
		UNION
		select *,0 iCodDocumentoFirma, NULL siFlagSIED, 0 iCodDocumentoDerivacion
		from dataResponsablesCC
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union
					select iCodDocumento from dataResponsablesDerivacionSIED
					union 
					select iCodDocumento from dataResponsablesDoc
				)
	), dataresultadofin as (
	select 
	a.*, 
	dbo.UF_MOV_OBTENERNOMJEFE(iCodArea) Jefe,  
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento=2 THEN 1 ELSE 0 END HabilitarSeguimiento,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarDerivar,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN'))
	THEN 1 ELSE 0 END HabilitarDevolver,
	case when TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarFinalizar,
	case when 
		(TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO')))
		OR
		(TipoDocIng = 'DD' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN')))
	THEN 1 ELSE 0 END HabilitarRegistrarAvance
	--(select count(1) from dataResultado)TotalRegistros 
	from dataResultado A
	where
	upper(A.FILTRO)
				LIKE '%' + upper(@vTextoBusqueda) + '%' 
	AND
	(
			(		--RECIBIDOS
				@iTipoBandeja=1 AND 
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						AND siOrigen=2 AND  (a.siFlagSIED <> 1 or a.siFlagSIED IS NULL)
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen=1 AND a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen IN (3, 4) AND a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
						AND TipoDocIng = 'DDSIED'
					)
					
				)
			)		--CON PLAZO
			OR
			(
				@iTipoBandeja=2 AND 			
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						AND siOrigen=2 and  (a.siFlagSIED <> 1 or a.siFlagSIED IS NULL)
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen=1 and a.siFlagSIED IS NULL
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('PEN','DEV','PRO','ATE','DER','ANU'))
						AND siOrigen IN (3, 4) AND a.siFlagSIED IS NOT NULL AND a.siFlagSIED = 1
						AND TipoDocIng = 'DDSIED'
					)
				)
				/*AND
				a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in ('PEN','DER'))
				and datediff(day,getdate(), A.dFecPlazo)>=1 */
				AND A.siPlazo > 0
			)
		)
		)SELECT *,(select count(1) from dataresultadofin)TotalRegistros  FROM  dataresultadofin
	order by dFecRegistro desc
	OFFSET (@iPagina-1)*@iTamPagina ROWS
	FETCH NEXT @iTamPagina ROWS ONLY;
END



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosAdjuntos]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAdjuntos] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAdjuntos]  
(  
 @iCodDocumento INT   
)  
AS  
-- =============================================  
-- Nombre: [dbo].[UP_MOV_ObtenerDocumentosAdjuntos]  
-- Objetivo: Obtener Documentos Adjuntos  
-- Módulo:  Movimientos  
-- Input:  @iCodDocumento - Código de Documento    
-- Output  Entidad Documentos Adjuntos  
-- Create date: 07/02/2020  
-- =============================================  
BEGIN  
SELECT [iCodDocAdjunto]  
      ,[iCodLaserfiche]  
      ,[siEstado]  
      ,[iCodDocumento]  
      ,[vUsuCreacion]  
      ,[dFecCreacion]  
      ,[vHstCreacion]  
      ,[vInsCreacion]  
      ,[vLgnCreacion]  
      ,[vRolCreacion]  
      ,[vUsuActualizacion]  
      ,[dFecActualizacion]  
      ,[vHstActualizacion]  
      ,[vInsActualizacion]  
      ,[vLgnActualizacion]  
      ,[vRolActualizacion]  
  FROM [dbo].[MovDocAdjunto]  
  WHERE iCodDocumento=@iCodDocumento and siEstado = 1  
END  

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerFlagSIED]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'DROP PROCEDURE [dbo].[UP_MOV_ObtenerFlagSIED] AS' 
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Reporte_AtencionDocumentos]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_Reporte_AtencionDocumentos] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_Reporte_AtencionDocumentos] 
@vNroTramite  VARCHAR(MAX),
@fFechaInicio datetime,
@fFechaFin datetime,
@vCodAreaResponsable  VARCHAR(MAX),
@vCodAreaDerivada  VARCHAR(MAX),
@vCodTipoDocumento  VARCHAR(MAX),
@vEstadoDocumento VARCHAR(MAX),
@siFirmado VARCHAR(MAX),
@NumeroPagina INT,
@tamaniopagina INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_Reporte_AtencionDocumentos] 
-- Objetivo:    Reporte de Atencion de Documentos
-- Módulo:		Reportes
-- Input:		@vCodArea varchar,  @vIdTipoDocumento varchar , @fFechaInicio datetime, @fFechaFin datetime
-- Output:  
--  Reporte de Atencion de Documentos
-- Create date: 15/05/20
-- =============================================
BEGIN

WITH REPORTE AS (

--DOCUMENTO
SELECT 
	DOC.vCorrelativo,
	CASE WHEN REP.iCodRepresentante IS NULL THEN AREA.vDescripcion + ' - ' + TRA.vApePaterno + ' ' + TRA.vApeMaterno + ' ' + TRA.vNombres ELSE
		REP.vNombres + ' - ' + EMP.vDescripcion END Remitente,
	CASE WHEN DOC.siOrigen  = 1 THEN ''ELSE AREA.vDescripcion + ' - ' + TRA.vApePaterno + ' ' + TRA.vApeMaterno + ' ' + TRA.vNombres END Destinatario,
	DOC.vAsunto,
	FIRM.vComentario,
	FIRM.iCodDocumentoFirma,
	FIRM.dFechaFirma,
	DOC.siEstDocumento,
	par.vCampo DescripcionEstado
FROM MovDocumento DOC 
INNER JOIN MaeArea AREA ON DOC.iCodArea = AREA.iCodArea
LEFT JOIN MaeRepresentante REP ON DOC.iCodRepresentante = REP.iCodRepresentante
LEFT JOIN MaeEmpresa EMP ON REP.iCodEmpresa=EMP.iCodEmpresa
INNER JOIN MaeTrabajador TRA ON DOC.iCodTrabajador = TRA.iCodTrabajador
LEFT JOIN MovDocumentoFirma FIRM ON DOC.iCodDocumento = FIRM.iCodDocumento AND FIRM.iCodDerivacion IS NULL
INNER JOIN  [dbo].[ParParametro] Par on DOC.siEstDocumento = Par.iCodParametro AND Par.iCodGrupo= 1
WHERE ( DOC.vCorrelativo like '%'+  @vNroTramite  + '%'    )
AND ( DOC.iCodArea IN (SELECT * FROM dbo.Split(@vCodAreaResponsable, ',')) OR 'TODOS' = @vCodAreaResponsable)
AND ( DOC.iCodTipDocumento IN (SELECT * FROM dbo.Split(@vCodTipoDocumento, ',')) OR 'TODOS' = @vCodTipoDocumento)
AND ( DOC.siEstDocumento IN (SELECT * FROM dbo.Split(@vEstadoDocumento, ',')) OR 'TODOS' = @vEstadoDocumento)
AND ((CASE WHEN FIRM.iCodDocumentoFirma IS NULL  THEN '0' 
		 WHEN FIRM.iCodDocumentoFirma IS NOT NULL THEN '1' 
		END ) = @siFirmado OR  'TODOS' = @siFirmado)
AND dFecDocumento >= cast(@fFechaInicio AS DATE)
AND dFecDocumento < cast(DATEADD(DAY, 1, @fFechaFin) AS DATE)
UNION ALL
--DERIVACIONES
SELECT 
	DOC.vCorrelativo,
	AREA.vDescripcion + ' - ' + TRADOC.vApePaterno + ' ' + TRADOC.vApeMaterno + ' ' + TRADOC.vNombres  Remitente,
	TRADER.vDescripcion +' - '+TRA.vApePaterno + ' ' + TRA.vApeMaterno + ' ' + TRA.vNombres Destinatario,
	DOC.vAsunto,
	FIRM.vComentario,
	FIRM.iCodDocumentoFirma,
	FIRM.dFechaFirma,
	DER.siEstadoDerivacion,
	par.vCampo DescripcionEstado
FROM MovDocumentoDerivacion DER
INNER JOIN MovDocumento DOC ON DER.iCodDocumento = DOC.iCodDocumento
INNER JOIN MaeArea AREA ON DOC.iCodArea = AREA.iCodArea
INNER JOIN MaeTrabajador TRA ON DER.iCodResponsable = TRA.iCodTrabajador
LEFT JOIN MovDocumentoFirma FIRM ON DER.iCodDocumentoDerivacion = FIRM.iCodDerivacion 
INNER JOIN  [dbo].[ParParametro] Par on DER.siEstadoDerivacion = Par.iCodParametro AND Par.iCodGrupo= 1
INNER JOIN MaeTrabajador TRADOC ON DOC.iCodTrabajador = TRADOC.iCodTrabajador
INNER JOIN MAEAREA TRADER ON TRA.iCodArea = TRADER.iCodArea
WHERE ( DOC.vCorrelativo like '%'+  @vNroTramite  + '%'    )
AND ( DOC.iCodArea IN (SELECT * FROM dbo.Split(@vCodAreaResponsable, ',')) OR 'TODOS' = @vCodAreaResponsable)
AND ( DER.iCodArea IN (SELECT * FROM dbo.Split(@vCodAreaDerivada, ',')) OR 'TODOS' = @vCodAreaDerivada)
AND ( DOC.iCodTipDocumento IN (SELECT * FROM dbo.Split(@vCodTipoDocumento, ',')) OR 'TODOS' = @vCodTipoDocumento)
AND ( DER.siEstadoDerivacion IN (SELECT * FROM dbo.Split(@vEstadoDocumento, ',')) OR 'TODOS' = @vEstadoDocumento)
AND dFecDocumento >= cast(@fFechaInicio AS DATE)
AND dFecDocumento < cast(DATEADD(DAY, 1, @fFechaFin) AS DATE)
AND ((CASE WHEN FIRM.iCodDocumentoFirma IS NULL  THEN '0'
		 WHEN FIRM.iCodDocumentoFirma IS NOT NULL THEN '1' END) = @siFirmado OR 'TODOS' = @siFirmado)
)

SELECT 
      *,(SELECT COUNT(*) FROM REPORTE) TotalRegistros FROM REPORTE
	  ORDER BY 1,2,3
	  offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only; 
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Reporte_SeguimientoDocumentos]') AND type in  (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_Reporte_SeguimientoDocumentos] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_Reporte_SeguimientoDocumentos] 
@vNroTramite VARCHAR(100),
@vAño CHAR(4),
@vCodArea  VARCHAR(MAX),
@vIdTipoDocumento   VARCHAR(MAX),
@NumeroPagina INT,
@tamaniopagina INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_Reporte_SeguimientoDocumentos] 
-- Objetivo:    Reporte de documento de entrada para seguimiento 
-- Módulo:		Reportes
-- Input:		@vNroTramite varchar,@vAño CHAR, @vCodArea VARCHAR,   @vIdTipoDocumento varchar , @NumeroPagina INT , @tamaniopagina INT
-- Output:   
-- Reporte Mesa de Partes.
-- Create date: 19/05/20
-- =============================================
BEGIN

WITH REPORTE AS (

SELECT   
--MovDoc.iCodDocumento ,
--MovDoc.siOrigen ,
MovDoc.vCorrelativo  Nro_Tramite,
FORMAT(MovDoc.dFecDocumento , 'dd-MM-yyyy HH:mm:ss') FechaDocumento,
(CASE MovDoc.siOrigen WHEN 1 THEN 
(SELECT ISNULL(UPPER(vDescripcion),'')  FROM [dbo].[MaeArea]
WHERE iCodArea = MovDoc.iCodArea )
WHEN 2 THEN 
(SELECT ISNULL( ME.vDescripcion,'') + ' - ' + ISNULL( UPPER(MR.vNombres) ,'') 
FROM [dbo].[MaeRepresentante] MR  
INNER JOIN  MaeEmpresa ME ON MR.iCodEmpresa = ME.iCodEmpresa 
WHERE MR.iCodRepresentante = MovDoc.iCodRepresentante ) 
ELSE '' END) Remitente,


(CASE MovDoc.siOrigen WHEN 1 THEN   
''
 
WHEN 2 THEN 
(SELECT ISNULL(UPPER(vDescripcion),'')  FROM [dbo].[MaeArea]
WHERE iCodArea = MovDoc.iCodArea ) + ' - ' + 
(SELECT UPPER( MTR.vNombres) + ' ' + UPPER(MTR.vApePaterno) + ' ' + UPPER(MTR.vApeMaterno)
FROM [dbo].[MaeTrabajador] MTR  WHERE MTR.iCodTrabajador = MovDoc.iCodTrabajador)

ELSE '' END) Destinatario,

MovDoc.[vAsunto] Asunto,
MovDoc.vObservaciones Observacion,

--- EXTERNO : 2 , INTERNO : 1
 Par.vCampo EstadoDoc,
 MovDoc.siEstDocumento  IdEstadoDoc
FROM [dbo].[MovDocumento] MovDoc 
INNER JOIN  DBO.MaeTrabajador MaeTrab  ON MovDoc.iCodTrabajador = MaeTrab.iCodTrabajador
INNER JOIN  [dbo].MaeArea MaArea on  MovDoc.iCodArea = MaArea.iCodArea 
INNER JOIN  [dbo].[ParParametro] Par on MovDoc.siEstDocumento = Par.iCodParametro AND Par.iCodGrupo= 1
WHERE MovDoc.siEstado = 1 
AND ( MovDoc.iCodArea in (SELECT * FROM dbo.Split(@vCodArea, ','))    OR @vCodArea= '0') 
AND ( MovDoc.vCorrelativo like '%'+  @vNroTramite  + '%'    )
AND ( MovDoc.iCodTipDocumento in (SELECT * FROM  dbo.Split(@vIdTipoDocumento, ','))  OR @vIdTipoDocumento= '0' ) 
AND ( DATEPART(YEAR,MovDoc.dFecDocumento) = @vAño OR @vAño= '0' ) 


UNION ALL

SELECT   
--MovDoc.iCodDocumento ,
--MovDoc.siOrigen ,
MovDoc.vCorrelativo  Nro_Tramite,
FORMAT(MovDoc.dFecDocumento , 'dd-MM-yyyy HH:mm:ss') FechaDocumento,
(CASE MovDoc.siOrigen WHEN 1 THEN 
(SELECT ISNULL(UPPER(vDescripcion),'')  FROM [dbo].[MaeArea]
WHERE iCodArea = MovDoc.iCodArea )
WHEN 2 THEN 

(SELECT  UPPER(MAE.vDescripcion) +' - '+ UPPER( MTR.vNombres) + ' ' + UPPER(MTR.vApePaterno) + ' ' + UPPER(MTR.vApeMaterno) 
 FROM  
MaeArea MAE 
INNER JOIN [dbo].[MaeTrabajador] MTR ON MTR.iCodTrabajador  = MovDer.iCodResponsable 
WHERE  MAE.iCodArea = MovDer.iCodArea  )

ELSE '' END) Remitente,

(CASE MovDoc.siOrigen WHEN 1 THEN   

(SELECT  UPPER(MAE.vDescripcion) +' - '+ UPPER( MTR.vNombres) + ' ' + UPPER(MTR.vApePaterno) + ' ' + UPPER(MTR.vApeMaterno) 
 FROM  
MaeArea MAE 
INNER JOIN [dbo].[MaeTrabajador] MTR ON MTR.iCodTrabajador  = MovDer.iCodResponsable 
WHERE  MAE.iCodArea = MovDer.iCodArea  )

WHEN 2 THEN 

(SELECT ISNULL(UPPER(vDescripcion),'')  FROM [dbo].[MaeArea]
WHERE iCodArea = MovDoc.iCodArea ) + ' - ' + 
(SELECT UPPER( MTR.vNombres) + ' ' + UPPER(MTR.vApePaterno) + ' ' + UPPER(MTR.vApeMaterno)
FROM [dbo].[MaeTrabajador] MTR  WHERE MTR.iCodTrabajador = MovDoc.iCodTrabajador)
ELSE '' END) Destinatario,

MovDoc.[vAsunto] Asunto,
MovDoc.vObservaciones Observacion,

--- EXTERNO : 2 , INTERNO : 1
 Par.vCampo EstadoDoc,
 MovDer.siEstadoDerivacion   IdEstadoDoc
FROM [dbo].[MovDocumento] MovDoc 
INNER JOIN  [dbo].MovDocumentoDerivacion MovDer ON MovDoc.iCodDocumento = MovDer.iCodDocumento AND MovDoc.dFecDerivacion IS NOT NULL
AND MovDer.siTipoAcceso= 1 /*AND MovDer.iTipoAccion IS NOT NULL*/
INNER JOIN  DBO.MaeTrabajador MaeTrab  ON MovDoc.iCodTrabajador = MaeTrab.iCodTrabajador
INNER JOIN  [dbo].MaeArea MaArea on  MovDoc.iCodArea = MaArea.iCodArea 
INNER JOIN  [dbo].[ParParametro] Par on MovDer.siEstadoDerivacion = Par.iCodParametro AND Par.iCodGrupo= 1
WHERE MovDoc.siEstado = 1 AND MovDer.siEstado= 1
AND ( MovDoc.iCodArea in (SELECT * FROM dbo.Split(@vCodArea, ','))    OR @vCodArea= '0') 
AND ( MovDoc.vCorrelativo like '%'+  @vNroTramite  + '%'    )
AND ( MovDoc.iCodTipDocumento in (SELECT * FROM  dbo.Split(@vIdTipoDocumento, ','))  OR @vIdTipoDocumento= '0' ) 
AND ( DATEPART(YEAR,MovDoc.dFecDocumento) = @vAño OR @vAño= '0' ) 
)

SELECT 
     --iCodDocumento
     --,siOrigen
	 Nro_Tramite 
	,FechaDocumento     
	,Remitente
	,Destinatario
	,Asunto
	,Observacion
	,EstadoDoc	
	,IdEstadoDoc  
	  ,(SELECT COUNT(*) FROM REPORTE) TotalRegistros
	  FROM REPORTE
	   ORDER BY  Nro_Tramite,Remitente
	  offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only;;
	   

END
GO
