/*
Consideraciones:

1. Si quiere registrar un usuario, debe validar previamente la existencia de ese usuario en la tabla AGRCYFILES.CNTRLWEB:
	- Si existe puede proceder con su registro
	- Si no existe, debe gestionar su creación en esa tabla
1. Validar previamente si ya se encuentra registrado el usuario y si el registro se encuentra activo
	- si existe y está activo no puede registrarlo
	- si no existe, puede registrarlo
2. Debe obtener el máximo valor del campo VCODASI en la tabla  AGRCYFILES.SDSASIM01 y sumarle 1 para usarlo en el registro.
3. Luego de registrar un usuario en DB2, se tiene que registrar el usuario en la aplicación SIGD a través de Gestión de Trabajadores.
*/

/*
-- TABLA: AGRCYFILES.SDSASIM01
-- CAMPOS:
-- VCODASI = CODIGO IDENTIFICADOR DE REGISTRO, 
-- VCODPRF = PERFIL,
-- VCODSIS = CODIGO SISTEMA,
-- WEBUSR = USUARIO A REGISTRAR(CNTRLWEB.WEBUSR)
-- VESTMOD = ESTADO DEL REGISTROS,
-- VUCRMOD = USUARIO DE CREACIÓN, 
-- VDCRMOD = FECHA DE CREACIÓN,
-- VHCRMOD = HOST DE CREACIÓN
*/

/*
SCRIPTS DE EJEMPLO PARA LA GESTIÓN
*/
-- VALIDACIÓN DE USUARIO EN EL SISTEMA
SELECT *
FROM AGRCYFILES.SDSASIM01
WHERE WEBUSR = 'AGROVG';


-- OBTENER EL MÁXIMO VALOR DE CODIGO EN TABLA AGRCYFILES.SDSASIM01
SELECT MAX(VCODASI)
FROM AGRCYFILES.SDSASIM01;

-- INSERCIÓN DE NUEVO USUARIO AL SISTEMA
-- EN ESTE EJEMPLO, SE ESTÁ REGISTRANDO AL USUARIO AGROVG CON PERFIL ADMIN

INSERT INTO AGRCYFILES.SDSASIM01(VCODASI, VCODPRF, VCODSIS, WEBUSR, VESTMOD, VUCRMOD, VDCRMOD, VHCRMOD)
VALUES
('1', 'ADMIN', '01', 'AGROVG', 1, 'ADMIN', CURRENT DATE, 'localhost');

-- ACTUALIZACIÓN DE PERFIL DE USUARIO
-- EN ESTE EJEMPLO, SE ESTÁ ACTUALIZANDO AL USUARIO AGROVG CON PERFIL ADMIN
UPDATE AGRCYFILES.SDSASIM01
SET VCODPRF = 'ADMIN'
WHERE WEBUSR = 'AGROVG';

-- ACTIVAR/DESACTIVAR USUARIO
-- VESTMOD 
-- 0 : DESACTIVAR
-- 1 : ACTIVAR

UPDATE AGRCYFILES.SDSASIM01
SET VESTMOD = 0
WHERE WEBUSR = 'AGROVG';
