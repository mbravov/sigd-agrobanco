DELETE AGRCYFILES.SDSPRMM01;
DELETE AGRCYFILES.SDSASIM01;

DELETE AGRCYFILES.SDSPRFM01;
DELETE AGRCYFILES.SDSOPCM01;
DELETE AGRCYFILES.SDSMODM01;
DELETE AGRCYFILES.SDSSISM01;

DELETE FROM AGRCYFILES.CNTRLWEB WHERE WEBUSR LIKE '%AGRSIGD%';
INSERT INTO AGRCYFILES.CNTRLWEB 
(WEBUSR, WEBACP, WEBSTS, WEBAGE, WEBTIP, WEBFNC, WEBLSU, WEBLSM, WEBLSD, WEBLSY, WEBLST, WEBOPM, WEBOPD, WEBOPY, WEBLPM, WEBLPD, WEBLPY, 
WEBRJC, WEBNOM, WEBEMA, WEBCEA, WEBOPE, WEBUSRW, WEBFECEST, WEBTIMEST, WEBFECACT, WEBTIMACT, WEBFECACC, WEBTIMACC, PERFIL)
VALUES
('AGRSIGD', 'ea2d401c67b882a5ad9d43f81c12c14a', '1', 1, 3, '999', 'AGRWLH', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
6, 'CAROLINA YATACO', 'cyataco_@agrobanco.com.pe', 0, 0, 'cyataco_', CURRENT_DATE,  TIMESTAMP ( CURRENT_DATE , CURRENT_TIME ), CURRENT_DATE,   
TIMESTAMP ( CURRENT_DATE , CURRENT_TIME ), CURRENT_DATE,  TIMESTAMP ( CURRENT_DATE , CURRENT_TIME ), 9001);

INSERT INTO AGRCYFILES.SDSSISM01 (VCODSIS, VNOMSIS, VDESSIS, VESTSIS, VRUTSIS, VUCRSIS, VDCRSIS, VHCRSIS)
VALUES ('01', 'SIGD', 'SIGD', 1, NULL, 'ADMIN', CURRENT DATE, '');

INSERT INTO AGRCYFILES.SDSMODM01(VCODMOD, VCODSIS, VESTMOD, VNOMMOD, VDESMOD, VCPAMOD, VURLMOD, VUCRMOD, VDCRMOD, VHCRMOD, VICOMOD)
VALUES 
('MDP', '01', 1, 'MESA DE PARTES', '', NULL, 'MesaPartes', 'ADMIN', CURRENT DATE, 'localhost','insert_drive_file'),
('BDE', '01', 1, 'BANDEJA DE ENTRADA', '', NULL, NULL,  'ADMIN', CURRENT DATE, 'localhost','input'),
--('BDS', '01', 1, 'BANDEJA DE SALIDA', '', NULL, NULL, 'ADMIN', CURRENT DATE, 'localhost','send'),
('MNT', '01', 1, 'MANTENIMIENTO', '', NULL, NULL, 'ADMIN', CURRENT DATE, 'localhost','developer_board'),
('REP', '01', 1, 'REPORTES', '', NULL, NULL, 'ADMIN', CURRENT DATE, 'localhost','timeline');

INSERT INTO AGRCYFILES.SDSOPCM01 (VCODOPC, VCODMOD, VCODSIS, VESTOPC, VUCROPC, VDCROPC, VHCROPC, VNOMOPC, VDESOPC, VCPAOPC, VURLOPC)
VALUES 
('MDP', 'MDP', '01', 0, 'ADMIN', CURRENT DATE, 'localhost', 'MESA DE PARTES', 'MESA DE PARTES', NULL, 'MesaPartes'),
('REC', 'BDE', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'RECIBIDOS', 'RECIBIDOS', NULL, 'BandejaEntrada/Recibidos'),
('PLZ', 'BDE', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'CON PLAZO', 'CON PLAZO', NULL, 'BandejaEntrada/ConPlazo'),
-- ('PND', 'BDS', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'PENDIENTES', 'PENDIENTES', NULL, 'BandejaEntrada/Pendientes'),
-- ('VIS', 'BDS', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'VISADOS', 'VISADOS', NULL, 'BandejaEntrada/Visados'),
-- ('FIR', 'BDS', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'FIRMADOS', 'FIRMADOS', NULL, 'BandejaEntrada/Firmados'),
('TDC', 'MNT', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'TIPOS DE DOCUMENTO', 'TIPOS DE DOCUMENTO', NULL, 'Mantenimiento/TiposDeDocumento'),
('AES', 'MNT', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'ASUNTOS ESTÁNDAR', 'ASUNTOS ESTÁNDAR', NULL, 'Mantenimiento/AsuntosEstandar'),
('EMP', 'MNT', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'EMPRESAS', 'EMPRESAS', NULL, 'Mantenimiento/Empresas'),
('FER', 'MNT', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'FERIADOS', 'FERIADOS', NULL, 'Mantenimiento/Feriados'),
('GRP', 'MNT', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'ÁREAS', 'ÁREAS', NULL, 'Mantenimiento/Areas'),
('JDE', 'MNT', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'TRABAJADORES', 'TRABAJADORES', NULL, 'Mantenimiento/Trabajadores'),
('SEC', 'MNT', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'SECUENCIALES', 'SECUENCIALES', NULL, 'Mantenimiento/Secuenciales'),
('RMP', 'REP', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'REG. MESA PARTES', 'REG. MESA PARTES', NULL, 'Reportes/RegMesaPartes'),
('ATE', 'REP', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'ATENCIÓN DOCUMENTOS', 'ATENCIÓN DOCUMENTOS', NULL, 'Reportes/AtencionDocumentos'),
('AFE', 'REP', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'ÁREA, FECHA Y ESTADO', 'ÁREA, FECHA Y ESTADO', NULL, 'Reportes/AreaFechaEstado'),
('SDC', 'REP', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'SEGUIMIENTO DCMNTOS', 'SEGUIMIENTO DCMNTOS', NULL, 'Reportes/SeguimientoDeDocumentos');


INSERT INTO AGRCYFILES.SDSPRFM01 (VCODPRF, VCODSIS, VESTMOD, VUCRMOD, VDCRMOD, VHCRMOD, VNOMPRF, VDESPRF)
VALUES 
('USER', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'USUARIO', 'USUARIO'),
('ADMIN', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'ADMINISTRADOR', 'ADMINISTRADOR'),
('SECRE', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'SECRETARIA', 'SECRETARIA'),
('JEFE', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'JEFE DE ÁREA', 'JEFE DE ÁREA'),
('MESA', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'MESA DE PARTES', 'MESA DE PARTES'),
('GERENTE', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'GERENTE', 'GERENTE'),
('MAESTROS', '01', 1, 'ADMIN', CURRENT DATE, 'localhost', 'MAESTROS', 'MAESTROS');

INSERT INTO LIBDST.SDSPRMM01
(VCODPRM, VCODOPC, VCODMOD, VCODSIS, VCODACC, VCODPRF , VESTACC, VUCRMOD, VDCRMOD, VHCRMOD)
VALUES

('1', 'MDP', 'MDP', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('2', 'REC', 'BDE', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('3', 'PLZ', 'BDE', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
--('4', 'PND', 'BDS', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
--('5', 'VIS', 'BDS', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
--('6', 'FIR', 'BDS', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('4', 'TDC', 'MNT', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('5', 'AES', 'MNT', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('6', 'EMP', 'MNT', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('7', 'FER', 'MNT', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('8', 'GRP', 'MNT', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('9', 'JDE', 'MNT', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('10', 'SEC', 'MNT', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('11', 'RMP', 'REP', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('12', 'ATE', 'REP', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('13', 'AFE', 'REP', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('14', 'SDC', 'REP', '01', '', 'ADMIN', 1, 'ADMIN', CURRENT DATE, 'localhost'),

('15', 'REC', 'BDE', '01', '', 'SECRE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('16', 'PLZ', 'BDE', '01', '', 'SECRE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
--('20', 'PND', 'BDS', '01', '', 'SECRE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('17', 'ATE', 'REP', '01', '', 'SECRE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('18', 'AFE', 'REP', '01', '', 'SECRE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('19', 'SDC', 'REP', '01', '', 'SECRE', 1, 'ADMIN', CURRENT DATE, 'localhost'),

('20', 'MDP', 'MDP', '01', '', 'MESA', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('21', 'REC', 'BDE', '01', '', 'MESA', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('22', 'PLZ', 'BDE', '01', '', 'MESA', 1, 'ADMIN', CURRENT DATE, 'localhost'),
--('27', 'PND', 'BDS', '01', '', 'MESA', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('23', 'ATE', 'REP', '01', '', 'MESA', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('24', 'AFE', 'REP', '01', '', 'MESA', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('25', 'SDC', 'REP', '01', '', 'MESA', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('26', 'RMP', 'REP', '01', '', 'MESA', 1, 'ADMIN', CURRENT DATE, 'localhost'),

--('32', 'VIS', 'BDS', '01', '', 'JEFE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
--('33', 'PND', 'BDS', '01', '', 'JEFE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('27', 'REC', 'BDE', '01', '', 'JEFE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('28', 'PLZ', 'BDE', '01', '', 'JEFE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('29', 'ATE', 'REP', '01', '', 'JEFE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('30', 'AFE', 'REP', '01', '', 'JEFE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('31', 'SDC', 'REP', '01', '', 'JEFE', 1, 'ADMIN', CURRENT DATE, 'localhost'),

--('39', 'VIS', 'BDS', '01', '', 'GERENTE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
--('40', 'PND', 'BDS', '01', '', 'GERENTE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
--('41', 'FIR', 'BDS', '01', '', 'GERENTE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('32', 'REC', 'BDE', '01', '', 'GERENTE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('33', 'PLZ', 'BDE', '01', '', 'GERENTE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('34', 'ATE', 'REP', '01', '', 'GERENTE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('35', 'AFE', 'REP', '01', '', 'GERENTE', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('36', 'SDC', 'REP', '01', '', 'GERENTE', 1, 'ADMIN', CURRENT DATE, 'localhost'),

('37', 'TDC', 'MNT', '01', '', 'MAESTROS', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('38', 'AES', 'MNT', '01', '', 'MAESTROS', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('39', 'EMP', 'MNT', '01', '', 'MAESTROS', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('40', 'FER', 'MNT', '01', '', 'MAESTROS', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('41', 'GRP', 'MNT', '01', '', 'MAESTROS', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('42', 'JDE', 'MNT', '01', '', 'MAESTROS', 1, 'ADMIN', CURRENT DATE, 'localhost'),
('43', 'SEC', 'MNT', '01', '', 'MAESTROS', 1, 'ADMIN', CURRENT DATE, 'localhost');

INSERT INTO LIBDST.SDSASIM01(VCODASI, VCODPRF, VCODSIS, WEBUSR, VESTMOD, VUCRMOD, VDCRMOD, VHCRMOD)
VALUES
('1', 'MAESTROS', '01', 'AGRSIGD', 1, 'ADMIN', CURRENT DATE, 'localhost');