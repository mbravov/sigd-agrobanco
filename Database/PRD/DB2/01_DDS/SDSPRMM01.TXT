      *--------------------------------------------------------------*
      *  Autor   : Canvia.                                           *
      *--------------------------------------------------------------*
      *  Descripci�n: Guarda las opciones permitidas por perfil.     *
      *--------------------------------------------------------------*
      * NOMBRE DE ARCHIVO NATIVO :  SDSPRMM01                        *
      * ALIAS  DE ARCHIVO        :  SEGPERMISO                       *
      *--------------------------------------------------------------*
     A                                      UNIQUE
     A          R RPRMM01
      *
     A            VCODPRM       10A         COLHDG('C�DIGO DE PERMISO')
     A                                      TEXT('VCODPERMISO')
     A            VCODOPC       10A         COLHDG('C�DIGO DE OPCION')
     A                                      TEXT('VCODOPCION')
     A            VCODMOD       10A         COLHDG('C�DIGO DE MODULO')
     A                                      TEXT('VCODMODULO')
     A            VCODSIS       10A         COLHDG('C�DIGO DE SISTEMA')
     A                                      TEXT('VCODSISTEMA')
     A            VCODPRF       10A         COLHDG('C�DIGO DE PERFIL')
     A                                      TEXT('VCODPERFIL')
     A            VCODACC       10A         COLHDG('C�DIGO DE ACCION')
     A                                      TEXT('VCODACCION')
     A                                      ALWNULL
     A            VESTACC        1S 0       COLHDG('ESTADO')
     A                                      TEXT('SIESTADO')
      *                                     1=ACTIVO
      *                                     0=INACTIVO
      * DATOS DE CREACION
     A            VUCRMOD       50A         COLHDG('USUARIO' 'CREACION')
     A                                      TEXT('VUSUCREACION')
     A            VDCRMOD         Z         COLHDG('FECHA  ' 'CREACION')
     A                                      TEXT('DFECCREACION')
     A            VHCRMOD       50A         COLHDG('HOST   ' 'CREACION')
     A                                      TEXT('VHSTCREACION')
      * DATOS DE MODIFICACION
     A            VUMOMOD       50A         COLHDG('USUARIO' 'MODIFICA')
     A                                      TEXT('VUSUACTUALIZACION')
     A                                      ALWNULL
     A            VDMOMOD         Z         COLHDG('FECHA  ' 'MODIFICA')
     A                                      TEXT('DFECACTUALIZACION')
     A                                      ALWNULL
     A            VHMOMOD       50A         COLHDG('HOST   ' 'MODIFICA')
     A                                      TEXT('VHSTACTUALIZACION')
     A                                      ALWNULL
      *
     A          K VCODPRM
     A          K VCODOPC
     A          K VCODMOD
     A          K VCODSIS
     A          K VCODPRF
     A          K VCODACC
